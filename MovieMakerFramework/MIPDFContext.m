//  MIPDFContext.m
//  MovingImages
//
//  Created by Kevin Meaney on 09/06/2014.
//  Copyright (c) 2015 Zukini Ltd.

// #import "DDLog.h"

#import "MIPDFContext.h"
#import "MICGContext.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#import "MIBaseObject_Protected.h"

@import QuartzCore;

@interface MIPDFContext ()

@property (strong) MICGContext *_miCGContext;
@property (strong) dispatch_queue_t _drawingQueue;

/// Debug name of the element in which drawing failed.
@property (readonly, strong) NSString *_elementDebugName;

/// Return the width of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _width;

/// Return the height of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _height;

/// This is the path to where the pdf file will be saved when asked.
@property (readonly, strong) NSString *_pathToPDFFile;

/// Is done, has _finalize been called already.
@property (atomic, assign) BOOL _done;

/// Return an array of string objects. Each string is a different blend mode.
+(NSArray *)_blendModes;

@end

#pragma mark -
#pragma mark CLASS MIPDFContext implementation.

@implementation MIPDFContext

#pragma mark Public methods

-(instancetype)initWithPath:(NSString *)pdfFilePath
                       size:(CGSize)size
                  cgContext:(CGContextRef)cgContext
                       name:(NSString *)theName
                  inContext:(MIContext *)miContext
{
    self = [super initWithBaseObjectType:MICGPDFContextKey
                                    name:theName
                               inContext:miContext];

    if (self)
    {
        dispatch_queue_t defaultPriorityQueue;
        defaultPriorityQueue = dispatch_get_global_queue(
                                         DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_queue_t myQueue;
        NSString *queueName = [NSString stringWithFormat:
                               @"com.zukini.mipdfcontext.%p", self];
        myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(myQueue, defaultPriorityQueue);
        self->__width = size.width;
        self->__height = size.height;
        CGPDFContextBeginPage(cgContext, nil);
        self._miCGContext = [[MICGContext alloc]
                             initWithCGContext:cgContext
                                     miContext:self.miContext
                                         owner:self];
        self._drawingQueue = myQueue;
        self->__pathToPDFFile = pdfFilePath;
    }
    
    return self;
}

-(void)finalize
{
    dispatch_sync(self._drawingQueue, ^
    {
        if (!self._done)
        {
            CGPDFContextEndPage(self._miCGContext.context);
            CGPDFContextClose(self._miCGContext.context);
            self._done = YES;
        }
    });
    [super finalize];
}

-(void)close
{
    [self finalize];
    [super baseObjectClose];
}

#pragma mark Public Class Methods

+(NSArray *)_blendModes
{
    return [MICGContext blendModes];
}

#pragma mark Public Class MIBaseObjectSemiPublicInterface methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    context = context ? context : [MIContext defaultContext];

    if (propertyKey == NULL)
    {
        MILog(@"Missing property key", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property key",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyBlendModes])
    {
        NSArray *blendModes = [MIPDFContext _blendModes];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                    blendModes);
        replyDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MICGPDFContextKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property requested.", commandDict);
        replyDict = MIMakeReplyDictionary(
                                @"Error: Unknown property",
                                MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    context = context ? context : [MIContext defaultContext];
    NSString *pathToPDFFile = MIUtilityGetFilePathFromCommandAndVariables(
                                                commandDict, context.variables);

    NSDictionary *size = commandDict[MIJSONKeySize];
    NSNumber *height;
    NSNumber *width;
    if (size)
    {
        height = size[MIJSONKeyHeight];
        width = size[MIJSONKeyWidth];
    }
    else
    {
        height = commandDict[MIJSONKeyHeight];
        width = commandDict[MIJSONKeyWidth];
    }

    if (pathToPDFFile && height && width)
    {
        MIPDFContext *object;
        CGSize theSize;
        theSize.height = [height doubleValue];
        theSize.width = [width doubleValue];
        NSString *objectName = commandDict[MIJSONKeyObjectName];

        CGContextRef cgContext;
        
        CGRect pdfBox;
        pdfBox.origin = CGPointMake(0.0, 0.0);
        pdfBox.size = theSize;
        
        NSURL *pdfURL = [NSURL fileURLWithPath:pathToPDFFile];
        cgContext = CGPDFContextCreateWithURL((__bridge CFURLRef)pdfURL,
                                              &pdfBox, nil);
        if (cgContext)
        {
            object = [[MIPDFContext alloc] initWithPath:pathToPDFFile
                                                   size:theSize
                                              cgContext:cgContext
                                                   name:objectName
                                              inContext:context];
            CGContextRelease(cgContext);
        }

        if (object)
        {
            MIBaseReference objectReference = [object reference];
            NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                   (long)objectReference];
            replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                              MIReplyErrorNoError,
                                                              @(objectReference));
        }
        else
        {
            MILog(@"Failed to create pdf context.", commandDict);
            replyDict = MIMakeReplyDictionary(
                                    @"Error: Failed to create pdf context",
                                    MIReplyErrorOptionValueInvalid);
        }
    }
    else
    {
        MILog(@"Missing option creating context.", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing Option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MICGPDFContextKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

+(NSDictionary *)handleCalculateGraphicSizeOfTextCommand:(NSDictionary *)dict
                                               inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    return MIUtilityCalculateGraphicSizeOfTest(dict, context.variables);
}

#pragma mark Manual implementation of MICGBitmapContext properties.

-(NSString *)elementDebugName
{
    return self._miCGContext.elementErrorDebugName;
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface instance methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict = NULL;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    NSString *resultString;
    NSNumber *resultNum;
    NSDictionary *resultDict;

    size_t resultVal;
    if ([propertyType isEqualToString:MIJSONKeyWidth])
    {
        resultVal = self._width;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeyHeight])
    {
        resultVal = self._height;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeySize])
    {
        resultDict = @{ MIJSONKeyWidth : @(self._width),
                        MIJSONKeyHeight : @(self._height) };
    }
    else if ([propertyType isEqualToString:MIJSONPropertyFile])
    {
        resultString = self._pathToPDFFile;
    }
    else
    {
        replyDict = [super handleCommonGetPropertyCommand:commandDict];
        if (!replyDict)
        {
            MILog(@"Requested unknown property", commandDict);
            replyDict = MIMakeReplyDictionary(
                                        @"Error: Requested unknown property",
                                        MIReplyErrorUnknownProperty);
        }
    }

    if (!replyDict && (resultString || resultDict))
    {
        if (resultDict)
        {
            replyDict = MIMakeReplyDictionaryWithDictionaryValue(resultDict, NO);
        }
        else
        {
            if (resultNum)
            {
                replyDict = MIMakeReplyDictionaryWithNumericValue(
                                                          resultString,
                                                          MIReplyErrorNoError,
                                                          resultNum);
            }
            else
            {
                replyDict = MIMakeReplyDictionary(resultString,
                                                  MIReplyErrorNoError);
            }
        }
    }
    return replyDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(@"Invalid destination for properties.", commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }

    NSMutableDictionary *propertyDict;
    propertyDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    propertyDict[MIJSONKeyObjectType] = self.baseObjectType;
    propertyDict[MIJSONKeyObjectName] = self.name;
    propertyDict[MIJSONKeyObjectReference] = @(self.reference);
    propertyDict[MIJSONKeyWidth] = @(self._width);
    propertyDict[MIJSONKeyHeight] = @(self._height);
    propertyDict[MIJSONPropertyFile] = self._pathToPDFFile;

    replyDict = MIUtilitySaveDictionaryToDestination(commandDict,
                                                       propertyDict,
                                                       YES);
    return replyDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleDrawElementCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict = NULL;
    NSDictionary *drawDictionary = commandDict[MIJSONPropertyDrawInstructions];
    if (drawDictionary)
    {
        // OK we might have a valid property dict.
        __block BOOL success = NO;
        dispatch_sync(self._drawingQueue, ^
        {
            // Reset the element error debug name before calling
            // handleDictionaryDrawElementCommand as if it is already set then
            // handleDictionaryDrawElementCommand wont set it again as
            // handleDictionaryDrawElementCommand can be called recursively and it
            // doesn't want to reset
            self._miCGContext.elementErrorDebugName = nil;
            success = [self._miCGContext
                      handleDictionaryDrawElementCommand:drawDictionary];
        });
        
        if (success)
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        else
        {
            NSString *message;
            NSString *elementDebugName = self.elementDebugName;
            if (elementDebugName)
            {
                message = [[NSString alloc] initWithFormat:@"%@%@",
                           @"Error: Badly formed element with name: ",
                           elementDebugName];
            }
            else
                message = @"Error: Problem interpreting draw dictionary";
            
            MILog(message, commandDict);
            replyDict = MIMakeReplyDictionary(message,
                                              MIReplyErrorOperationFailed);
        }
    }
    else
    {
        MILog(@"Missing drawing information dict.", commandDict);
        replyDict = MIMakeReplyDictionary(
                            @"Error: Missing drawing information dictionary.",
                            MIReplyErrorMissingOption);
    }
    return replyDict;
}

-(NSDictionary *)handleFinalizePageAndStartNew:(NSDictionary *)commandDict
{
    dispatch_sync(self._drawingQueue, ^
    {
        CGPDFContextEndPage(self._miCGContext.context);
        CGPDFContextBeginPage(self._miCGContext.context, nil);
    });
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

@end
