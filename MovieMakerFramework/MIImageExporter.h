//  MIImageExporter.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"

// Protocol header files.
#import "MIBaseObjectPublicInterface.h"
#import "MIBaseObjectSemiPublicInterface.h"

/**
 The `MIImageExporter` class is an Objective-C thin wrapper for a
 CGImageDestinationRef object. This is one of the MovingImages Framework base
 classes. For documentation on CGImageDestinationRef please refer to:
 The Image I/O Programming Guide. The CGImageDestinationRef object is created
 at the point when the export is requested.
 
 For an object of the MIImageExporter class only the export action has an
 option to perform the operation asynchronously because this is the only
 operation where the overhead of dealing with an asynchronous operation
 is less than the benefits gained of being able to continue execution elsewhere.
 Access to other methods belonging to this object need to be blocked until 
 the export has finished.
*/
@interface MIImageExporter : MIBaseObject <MIBaseObjectSemiPublicInterface>

/// Property dictionary associated with the image file. Not to a specific image.
@property (strong) NSMutableDictionary *propertyDictionary;

/// Init the image exporter with a url and an image type (public.jpeg etc.).
-(instancetype)initWithURL:(NSURL *)exportURL
                   utiType:(NSString *)utiType
                      name:(NSString *)theName
                 inContext:(MIContext *)context;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end
