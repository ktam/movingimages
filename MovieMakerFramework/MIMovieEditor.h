//  MIMovieEditor.h
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"

// MIMovieEditorKey

@interface MIMovieEditor : MIBaseObject <MIBaseObjectSemiPublicInterface>

/// Initialize the Movie Editor object with a name and the context it belongs to.
-(instancetype)initWithName:(NSString *)objectName inContext:(MIContext *)context;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end
