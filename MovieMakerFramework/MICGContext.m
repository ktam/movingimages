//  MICGContext.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd. All rights reserved.

@import Foundation;
@import CoreGraphics;

#include "CGPathFromSVGPath.h"
#import "NSString+DDMathParsing.h"
#import "MICGContext.h"
#import "MICGImage.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIUtilities.h"

#pragma mark CLASS MCGContext private interface.

@interface MICGContext ()

@property (weak, readonly) id _owner;
@property (weak, readonly) MIContext *_miContext;

-(BOOL)_drawAttributedString:(NSAttributedString *)attrStr
                      inPath:(CGPathRef)thePath
                   orAtPoint:(CGPoint)thePoint;


-(NSNumber *)_moveTo:(NSDictionary *)pointDict inPath:(CGMutablePathRef)thePath;
-(NSNumber *)_addLine:(NSDictionary *)lineDict toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_addBezierCurve:(NSDictionary *)bezierCurveDict
                      toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_addQuadraticCurve:(NSDictionary *)quadraticCurveDict
                         toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_addArc:(NSDictionary *)arcDict toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_addArcToPoint:(NSDictionary *)arcPointDict
                     toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_assignRectangle:(NSDictionary *)rectangleDict
                       toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_assignRoundedRectangle:(NSDictionary *)roundedRectangleDict
                              toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_assignOval:(NSDictionary *)ovalDict
                  toPath:(CGMutablePathRef)thePath;
-(NSNumber *)_close:(NSDictionary *)closeDict subPath:(CGMutablePathRef)thePath;

-(NSNumber *)_handleDrawStrokeRectangleElement:(NSDictionary *)rectangleDict;

-(CGPathRef)_createPathUsingPathElementsInArray:(NSArray *)elements
                                     startPoint:(CGPoint)startPoint
                                                            CF_RETURNS_RETAINED;
-(CGPathRef)_createPath:(NSDictionary *)pathDict CF_RETURNS_RETAINED;
-(CGPathRef)_createPathFromSVGPath:(NSString *)svgPath CF_RETURNS_RETAINED;

-(NSNumber *)_handleDrawStrokeRoundedRectangle:(NSDictionary *)rectangleDict;
-(NSNumber *)_handleDrawFillRectangleElement:(NSDictionary *)rectangleDict;
-(NSNumber *)_handleDrawFillRoundedRectangle:(NSDictionary *)rectangleDict;
-(NSNumber *)_handleDrawStrokeOvalElement:(NSDictionary *)rectangleDict;
-(NSNumber *)_handleDrawFillOvalElement:(NSDictionary *)rectangleDict;
-(NSNumber *)_handleDrawLine:(NSDictionary *)lineDict;
-(NSNumber *)_handleDrawStrokePathElement:(NSDictionary *)pathDict;
-(NSNumber *)_handleDrawFillPathElement:(NSDictionary *)pathDict;
-(NSNumber *)_handleDrawFillStrokePathElement:(NSDictionary *)pathDict;
-(NSNumber *)_handleDrawArrayOfElements:(NSDictionary *)elements;
-(NSNumber *)_handleDrawLinesFromPoints:(NSArray *)points;
-(NSNumber *)_handleDrawLines:(NSDictionary *)linesDict;
-(NSNumber *)_handleDrawBasicString:(NSDictionary *)stringDict;
-(NSNumber *)_handleDrawLinearGradientFill:(NSDictionary *)gradientFillDict;
-(NSNumber *)_handleDrawRadialGradientFill:(NSDictionary *)gradientFillDict;
-(NSNumber *)_handleDrawImage:(NSDictionary *)drawImageDict;
-(NSNumber *)_handleDrawFillPathAddInnerShadow:(NSDictionary *)innerShadowPath;

-(BOOL)_handleDictionaryDrawElementCommand:(NSDictionary *)command;

-(BOOL)_applyTransformations:(NSArray *)transformations;

// I am treating applying the apply transform as mutually exclusive as applying
// the array of transformations. If both are defined then the affine transform
// is ignored.
-(BOOL)_applyAffineTranform:(NSDictionary *)affineTransform;
-(BOOL)_applyShadow:(NSDictionary *)shadowDict;
-(BOOL)_applyClipping:(NSDictionary *)clippingDict;
-(BOOL)_applyImageAsMask:(NSDictionary *)maskDict;
-(void)_assignLineDashSegments:(NSArray *)dashSegments phase:(NSNumber *)dashPhase;

/// Returns true if the graphic state was pushed.
-(BOOL)_setOptionalContextValuesAndPushState:(NSDictionary *)elementDict;

@end

#pragma mark - Static (local) utility functions.

static NSDictionary *GetPathCreationSelectorDictionary()
{
    return
    @{
      MIJSONValuePathMoveTo               : @"_moveTo:inPath:",
      MIJSONValuePathLine                 : @"_addLine:toPath:",
      MIJSONValuePathBezierCurve          : @"_addBezierCurve:toPath:",
      MIJSONValuePathQuadraticCurve       : @"_addQuadraticCurve:toPath:",
      MIJSONValuePathRectangle            : @"_assignRectangle:toPath:",
      MIJSONValuePathRoundedRectangle     : @"_assignRoundedRectangle:toPath:",
      MIJSONValuePathOval                 : @"_assignOval:toPath:",
      MIJSONValuePathArc                  : @"_addArc:toPath:",
      MIJSONValuePathAddArcToPoint        : @"_addArcToPoint:toPath:",
      MIJSONValueCloseSubPath             : @"_close:subPath:"
    };
}

static SEL GetPathCreationSelectorFromElementType(NSString *elementType)
{
    static NSDictionary *selectorDict = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        selectorDict = GetPathCreationSelectorDictionary();
    });
    
    NSString *selectorString = selectorDict[elementType];
    if (!selectorString)
        return (SEL)0;
    
    return NSSelectorFromString(selectorString);
}

static NSDictionary *GetSelectorDictionary()
{
    return
    @{
        MIJSONValueRectangleStrokeElement :
                                        @"_handleDrawStrokeRectangleElement:",
        MIJSONValueRectangleFillElement : @"_handleDrawFillRectangleElement:",
        MIJSONValueRoundedRectangleStrokeElement :
                                        @"_handleDrawStrokeRoundedRectangle:",
        MIJSONValueRoundedRectangleFillElement :
                                           @"_handleDrawFillRoundedRectangle:",
        MIJSONValueOvalFillElement       : @"_handleDrawFillOvalElement:",
        MIJSONValueOvalStrokeElement     : @"_handleDrawStrokeOvalElement:",
        MIJSONValueLineElement           : @"_handleDrawLine:",
        MIJSONValueLineElements          : @"_handleDrawLines:",
        MIJSONValueBasicStringElement    : @"_handleDrawBasicString:",
        MIJSONValueArrayOfElements       : @"_handleDrawArrayOfElements:",
        MIJSONValuePathStrokeElement     : @"_handleDrawStrokePathElement:",
        MIJSONValuePathFillElement       : @"_handleDrawFillPathElement:",
		MIJSONValuePathFillAndStrokeElement :@"_handleDrawFillStrokePathElement:",
        MIJSONValueLinearGradientFill    : @"_handleDrawLinearGradientFill:",
        MIJSONValueRadialGradientFill    : @"_handleDrawRadialGradientFill:",
        MIJSONValueDrawImage             : @"_handleDrawImage:",
        MIJSONValuePathFillInnerShadowElement :
                                        @"_handleDrawFillPathAddInnerShadow:"
    };
}

static SEL GetSelectorFromElementType(NSString *elementType)
{
    static NSDictionary *selectorDict = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        selectorDict = GetSelectorDictionary();
    });
    
    NSString *selectorString = selectorDict[elementType];
    if (!selectorString)
        return (SEL)0;
    
    return NSSelectorFromString(selectorString);
}

/**
 Set the blend mode:
 For information about trying to understand about blending modes check out.
 http://ericasadun.com/2013/03/20/blending-mode-sample-images/
 In the Quartz 2D Programming Guide: Bitmap Images and Image masks has a section
 on Using Blend Modes with Images which I should look at.
*/
static NSDictionary *GetBlendModeDictionary()
{
    return
    @{
      MIJSONValueBlendModeNormal: @(kCGBlendModeNormal),
      MIJSONValueBlendModeMultiply : @(kCGBlendModeMultiply),
      MIJSONValueBlendModeScreen : @(kCGBlendModeScreen),
      MIJSONValueBlendModeOverlay : @(kCGBlendModeOverlay),
      MIJSONValueBlendModeDarken : @(kCGBlendModeDarken),
      MIJSONValueBlendModeLighten : @(kCGBlendModeLighten),
      MIJSONValueBlendModeColorDodge : @(kCGBlendModeColorDodge),
      MIJSONValueBlendModeColorBurn : @(kCGBlendModeColorBurn),
      MIJSONValueBlendModeSoftLight : @(kCGBlendModeSoftLight),
      MIJSONValueBlendModeHardLight : @(kCGBlendModeHardLight),
      MIJSONValueBlendModeDifference : @(kCGBlendModeDifference),
      MIJSONValueBlendModeExclusion : @(kCGBlendModeExclusion),
      MIJSONValueBlendModeHue : @(kCGBlendModeHue),
      MIJSONValueBlendModeSaturation : @(kCGBlendModeSaturation),
      MIJSONValueBlendModeColor : @(kCGBlendModeColor),
      MIJSONValueBlendModeLuminosity : @(kCGBlendModeLuminosity),
      MIJSONValueBlendModeClear : @(kCGBlendModeClear),
      MIJSONValueBlendModeCopy : @(kCGBlendModeCopy),
      MIJSONValueBlendModeSourceIn : @(kCGBlendModeSourceIn),
      MIJSONValueBlendModeSourceOut : @(kCGBlendModeSourceOut),
      MIJSONValueBlendModeSourceAtop : @(kCGBlendModeSourceAtop),
      MIJSONValueBlendModeDestinationOver : @(kCGBlendModeDestinationOver),
      MIJSONValueBlendModeDestinationIn : @(kCGBlendModeDestinationIn),
      MIJSONValueBlendModeDestinationAtop : @(kCGBlendModeDestinationAtop),
      MIJSONValueBlendModeXOR : @(kCGBlendModeXOR),
      MIJSONValueBlendModePlusDarker : @(kCGBlendModePlusDarker),
      MIJSONValueBlendModePlusLighter : @(kCGBlendModePlusLighter)
    };
}

static CGBlendMode GetBlendModeFromString(NSString *blendModeString)
{
    static NSDictionary *blendModeDict = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        blendModeDict = GetBlendModeDictionary();
    });
    
    CGBlendMode theMode = kCGBlendModeNormal;
    
    NSNumber *theBlendModeRef = blendModeDict[blendModeString];
    if (theBlendModeRef)
    {
        NSInteger theBlendMode = [theBlendModeRef integerValue];
        theMode = (CGBlendMode)theBlendMode;
    }
    return theMode;
}

static NSDictionary *GetTextAlignmentDictionary()
{
    return
    @{
        MIJSONValueTextAlignLeft : @(kCTTextAlignmentLeft),
        MIJSONValueTextAlignRight : @(kCTTextAlignmentRight),
        MIJSONValueTextAlignCenter : @(kCTTextAlignmentCenter),
        MIJSONValueTextAlignJustified : @(kCTTextAlignmentJustified),
        MIJSONValueTextAlignNatural : @(kCTTextAlignmentNatural)
    };
}

static CTTextAlignment GetTextAlignmentFromString(NSString *textAlignString)
{
    static NSDictionary *textAlignDictionary = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        textAlignDictionary = GetTextAlignmentDictionary();
    });

    CTTextAlignment textAlign = kCTTextAlignmentLeft;
    
    NSNumber *textAlignRef = textAlignDictionary[textAlignString];
    if (textAlignRef && [textAlignRef isKindOfClass:[NSNumber class]])
    {
        textAlign = (CTTextAlignment)[textAlignRef integerValue];
    }
    return textAlign;
}

static NSDictionary *GetInterpolationQualityDictionary()
{
    return
    @{
        MIJSONValueInterpolationDefault : @(kCGInterpolationDefault),
        MIJSONValueInterpolationNone : @(kCGInterpolationNone),
        MIJSONValueInterpolationLow : @(kCGInterpolationLow),
        MIJSONValueInterpolationMedium : @(kCGInterpolationMedium),
        MIJSONValueInterpolationHigh : @(kCGInterpolationHigh)
    };
}

static CGInterpolationQuality GetInterpolationQualityFromString(
                                        NSString *interpolationQualityString)
{
    static NSDictionary *interpolationQualityDict = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        interpolationQualityDict = GetInterpolationQualityDictionary();
    });

    CGInterpolationQuality interpQual = kCGInterpolationDefault;

    NSNumber *interpQualRef;
    interpQualRef = interpolationQualityDict[interpolationQualityString];
    if (interpQualRef && [interpQualRef isKindOfClass:[NSNumber class]])
        interpQual = (CGInterpolationQuality)[interpQualRef integerValue];

    return interpQual;
}

static CGLineCap GetLineCapFromString(NSString *lineCapString)
{
    CGLineCap lineCapValue = kCGLineCapButt;
    if (!lineCapString)
        return lineCapValue;

    if ([lineCapString isEqualToString:MIJSONValueLineCapButt])
        lineCapValue = kCGLineCapButt;
    else if ([lineCapString isEqualToString:MIJSONValueLineCapRound])
        lineCapValue = kCGLineCapRound;
    else if ([lineCapString isEqualToString:MIJSONValueLineCapSquare])
        lineCapValue = kCGLineCapSquare;

    return lineCapValue;
}

static CGLineJoin GetLineJoinFromString(NSString *lineJoinString)
{
    CGLineJoin lineJoinValue = kCGLineJoinMiter;
    if (!lineJoinString)
        return lineJoinValue;

    if ([lineJoinString isEqualToString:MIJSONValueLineJoinMiter])
        lineJoinValue = kCGLineJoinMiter;
    else if ([lineJoinString isEqualToString:MIJSONValueLineJoinRound])
        lineJoinValue = kCGLineJoinRound;
    else if ([lineJoinString isEqualToString:MIJSONValueLineJoinBevel])
        lineJoinValue = kCGLineJoinBevel;
    
    return lineJoinValue;
}

#pragma mark - CLASS MICGContext implementation.

@implementation MICGContext

#pragma mark Object init and dealloc methods

-(instancetype)initWithCGContext:(CGContextRef)ctxt
                       miContext:(MIContext *)miContext
                           owner:(id)theOwner
{
    if (!ctxt)
        return nil;

    self = [super init];
    if (self)
    {
        self->__owner = theOwner;
        self->__miContext = miContext ? miContext : [MIContext defaultContext];
        self->_context = CGContextRetain(ctxt);
    }
    return self;
}

-(void)dealloc
{
    CGContextRelease(self->_context);
}

#pragma mark Public Class methods

+(NSArray *)blendModes
{
    NSArray *result;

    result = @[MIJSONValueBlendModeNormal, MIJSONValueBlendModeMultiply,
               MIJSONValueBlendModeScreen, MIJSONValueBlendModeOverlay,
               MIJSONValueBlendModeDarken, MIJSONValueBlendModeLighten,
               MIJSONValueBlendModeColorDodge, MIJSONValueBlendModeColorBurn,
               MIJSONValueBlendModeSoftLight, MIJSONValueBlendModeHardLight,
               MIJSONValueBlendModeDifference, MIJSONValueBlendModeExclusion,
               MIJSONValueBlendModeHue, MIJSONValueBlendModeSaturation,
               MIJSONValueBlendModeColor, MIJSONValueBlendModeLuminosity,
               MIJSONValueBlendModeClear, MIJSONValueBlendModeCopy,
               MIJSONValueBlendModeSourceIn, MIJSONValueBlendModeSourceOut,
               MIJSONValueBlendModeSourceAtop,
               MIJSONValueBlendModeDestinationOver,
               MIJSONValueBlendModeDestinationIn,
               MIJSONValueBlendModeDestinationOut,
               MIJSONValueBlendModeDestinationAtop, MIJSONValueBlendModeXOR,
               MIJSONValueBlendModePlusDarker, MIJSONValueBlendModePlusLighter];
    
    return result;
}

#pragma mark Public Methods for handling drawing commands.

-(BOOL)handleDictionaryDrawElementCommand:(NSDictionary *)command
{
    self.elementErrorDebugName = nil;
    MIContext *theContext = self._miContext;
    if (theContext)
        self.variables = theContext.variables;

    BOOL result = [self _handleDictionaryDrawElementCommand:command];
    self.variables = nil;

    if (!result)
    {
        MILogBreak();
    }
    return result;
}

#pragma mark Private Methods

-(NSNumber *)_moveTo:(NSDictionary *)pointDict inPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint point;
    BOOL gotPoint = MIUtilityGetCGPointFromDictionary(pointDict[MIJSONKeyPoint],
                                                      &point, kMILog,
                                                      self.variables);
    if (!gotPoint)
        result = NO;
    
    if (result)
    {
        CGPathMoveToPoint(thePath, NULL, point.x, point.y);
    }
    else
    {
        MILog(@"Move to dictionary.", pointDict);
    }
    return @(result);
}

-(NSNumber *)_addLine:(NSDictionary *)lineDict
               toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint point;
    BOOL gotPoint = MIUtilityGetCGPointFromDictionary(lineDict[MIJSONKeyEndPoint],
                                                      &point, kMILog,
                                                      self.variables);
    if (!gotPoint)
        result = NO;
    
    if (result)
    {
        CGPathAddLineToPoint(thePath, NULL, point.x, point.y);
    }
    else
    {
        MILog(@"Add line dictionary.", lineDict);
    }

    return @(result);
}

-(NSNumber *)_addBezierCurve:(NSDictionary *)bezierDict
                toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint endPoint, controlPoint1, controlPoint2;
    BOOL gotEndPoint = MIUtilityGetCGPointFromDictionary(
                                            bezierDict[MIJSONKeyEndPoint],
                                            &endPoint, kMILog,
                                            self.variables);
    BOOL gotControlPoint1 = MIUtilityGetCGPointFromDictionary(
                                            bezierDict[MIJSONKeyControlPoint1],
                                            &controlPoint1, kMILog,
                                            self.variables);
    BOOL gotControlPoint2 = MIUtilityGetCGPointFromDictionary(
                                            bezierDict[MIJSONKeyControlPoint2],
                                            &controlPoint2, kMILog,
                                            self.variables);

    if (!gotEndPoint || !gotControlPoint1 || !gotControlPoint2)
        result = NO;
    
    if (result)
    {
        CGPathAddCurveToPoint(thePath, NULL, controlPoint1.x, controlPoint1.y,
                              controlPoint2.x, controlPoint2.y,
                              endPoint.x, endPoint.y);
    }
    else
    {
        MILog(@"Bezier curve dictionary.", bezierDict);
    }
    
    return @(result);
}

-(NSNumber *)_addQuadraticCurve:(NSDictionary *)quadDict
                   toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint endPoint, controlPoint1;
    BOOL gotEndPoint = MIUtilityGetCGPointFromDictionary(
                                            quadDict[MIJSONKeyEndPoint],
                                            &endPoint, kMILog, self.variables);
    BOOL gotControlPoint1 = MIUtilityGetCGPointFromDictionary(
                                            quadDict[MIJSONKeyControlPoint1],
                                            &controlPoint1, kMILog,
                                            self.variables);
    
    if (!gotEndPoint || !gotControlPoint1)
        result = NO;
    
    if (result)
    {
        CGPathAddQuadCurveToPoint(thePath, NULL, controlPoint1.x,
                                  controlPoint1.y, endPoint.x, endPoint.y);
    }
    else
    {
        MILog(@"Quad curve dictionary.", quadDict);
    }
    return @(result);
}

-(NSNumber *)_addArc:(NSDictionary *)arcDict toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint center;
    CGFloat radius, startAngle, endAngle;
    BOOL clockwise;
    result &= MIUtilityGetCGPointFromDictionary(arcDict[MIJSONKeyCenterPoint],
                                                &center, kMILog,
                                                self.variables);
    result &= MIUtilityGetCGFloatFromDictionary(arcDict, MIJSONKeyRadius,
                                                &radius,
                                                self.variables);
    result &= MIUtilityGetCGFloatFromDictionary(arcDict, MIJSONKeyStartAngle,
                                                &startAngle,
                                                self.variables);
    result &= MIUtilityGetCGFloatFromDictionary(arcDict, MIJSONKeyEndAngle,
                                                &endAngle,
                                                self.variables);
    clockwise = MIUtilityGetBOOLPropertyWithDefault(arcDict,
                                                      MIJSONKeyDrawArcClockwise,
                                                      NO);
    if (result)
    {
        CGPathAddArc(thePath, NULL, center.x, center.y, radius, startAngle,
                     endAngle, clockwise);
    }
    else
    {
        MILog(@"Add arc dictionary.", arcDict);
    }
    return @(result);
}

-(NSNumber *)_addArcToPoint:(NSDictionary *)arcPointDict
               toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGPoint tangentPoint1, tangentPoint2;
    CGFloat radius;
    
    result &= MIUtilityGetCGPointFromDictionary(
                                            arcPointDict[MIJSONKeyTangentPoint1],
                                            &tangentPoint1, kMILog,
                                            self.variables);
    
    result &= MIUtilityGetCGPointFromDictionary(
                                            arcPointDict[MIJSONKeyTangentPoint2],
                                            &tangentPoint2, kMILog,
                                            self.variables);
    
    result &= MIUtilityGetCGFloatFromDictionary(arcPointDict, MIJSONKeyRadius,
                                                &radius,
                                                self.variables);
    
    if (result)
    {
        CGPathAddArcToPoint(thePath, NULL, tangentPoint1.x, tangentPoint1.y,
                               tangentPoint2.x, tangentPoint2.y, radius);
    }
    else
    {
        MILog(@"Add arc to point dictionary.", arcPointDict);
    }
    return @(result);
}

-(NSNumber *)_assignRectangle:(NSDictionary *)rectangleDict
                 toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGRect theRect;
    BOOL gotRect = MIUtilityGetCGRectFromDictionary(rectangleDict[MIJSONKeyRect],
                                                    &theRect,
                                                    kMILog, self.variables);
    if (!gotRect)
        result = NO;
    
    if (result)
    {
        CGPathAddRect(thePath, NULL, theRect);
    }
    else
    {
        MILog(@"Assign rectangle dictionary.", rectangleDict);
    }

    return @(result);
}

-(NSNumber *)_assignRoundedRectangle:(NSDictionary *)roundedRectangleDict
                              toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGRect theRect;
    BOOL gotRect = MIUtilityGetCGRectFromDictionary(
                                            roundedRectangleDict[MIJSONKeyRect],
                                            &theRect, kMILog, self.variables);
    CGFloat radiuses[4];
    BOOL gotRadiuses = MIUtilityGetRadiusesFromDictionary(
                                            roundedRectangleDict, radiuses,
                                                          self.variables);

    if (!gotRect || !gotRadiuses)
        result = NO;
    
    if (result)
    {
        CGPathMoveToPoint(thePath, NULL, theRect.origin.x + radiuses[3],
                             theRect.origin.y);

        CGPoint arcCentre = CGPointMake(
                            theRect.origin.x + theRect.size.width - radiuses[0],
                            theRect.origin.y + radiuses[0]);

        CGPathAddArc(thePath, NULL, arcCentre.x, arcCentre.y, radiuses[0],
                    -M_PI_2, 0.0, 0);
        
        arcCentre = CGPointMake(
                        theRect.origin.x + theRect.size.width - radiuses[1],
                        theRect.origin.y + theRect.size.height - radiuses[1]);

        CGPathAddArc(thePath, NULL, arcCentre.x, arcCentre.y, radiuses[1], 0.0,
                    M_PI_2, 0);
        
        arcCentre = CGPointMake(theRect.origin.x + radiuses[2],
                        theRect.origin.y + theRect.size.height - radiuses[2]);

        CGPathAddArc(thePath, NULL, arcCentre.x, arcCentre.y, radiuses[2],
                        M_PI_2, M_PI, 0);
        
        arcCentre = CGPointMake(theRect.origin.x + radiuses[3],
                                theRect.origin.y + radiuses[3]);

        CGPathAddArc(thePath, NULL, arcCentre.x, arcCentre.y, radiuses[3],
                    M_PI, M_PI + M_PI_2, 0);

        CGPathCloseSubpath(thePath);
    }
    else
    {
        MILog(@"Assign rounded rectangle.", roundedRectangleDict);
    }
    
    return @(result);
}

-(NSNumber *)_assignOval:(NSDictionary *)ovalDict
                  toPath:(CGMutablePathRef)thePath
{
    BOOL result = YES;
    CGRect theRect;
    BOOL gotRect = MIUtilityGetCGRectFromDictionary(ovalDict[MIJSONKeyRect],
                                                    &theRect, kMILog,
                                                    self.variables);
    if (!gotRect)
        result = NO;
    
    if (result)
    {
        CGPathAddEllipseInRect(thePath, NULL, theRect);
    }
    else
    {
        MILog(@"Assign oval dictionary.", ovalDict);
    }

    return @(result);
}

-(NSNumber *)_close:(NSDictionary *)closeDict subPath:(CGMutablePathRef)thePath
{
    CGPathCloseSubpath(thePath);
    return @(YES);
}

-(CGPathRef)_createPathFromSVGPath:(NSString *)svgPath
{
    const char *pathString = [svgPath cStringUsingEncoding:NSUTF8StringEncoding];
    return CGCreatePathFromSVGPath(pathString);
}

-(CGPathRef)_createPathUsingPathElementsInArray:(NSArray *)elements
                                     startPoint:(CGPoint)startPoint
{
    BOOL success = YES;
    CGMutablePathRef mutablePath = CGPathCreateMutable();
    CGPathMoveToPoint(mutablePath, NULL, startPoint.x, startPoint.y);
    for (NSDictionary *element in elements)
    {
        if (![element isKindOfClass:[NSDictionary class]])
        {
            success = NO;
            break;
        }
        
        NSString *elementType = element[MIJSONKeyElementType];
        if (!(elementType && [elementType isKindOfClass:[NSString class]]))
        {
            MILog(@"Path element type not specified.", element);
            success = NO;
            break;
        }
        
        SEL theSelector = (SEL)0;
        theSelector = GetPathCreationSelectorFromElementType(elementType);
        
        if (theSelector == (SEL)0)
        {
            MILog(@"Path element type unknown.", element);
            success = NO;
            break;
        }
        
        @try
        {
            NSNumber *theNum;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            theNum = [self performSelector:theSelector
                                withObject:element
                                withObject:(__bridge id)mutablePath];
#pragma clang diagnostic pop
            success = [theNum boolValue];
        }
        @catch (NSException *exception)
        {
            NSString *name = exception.name;
            NSString *reason = exception.reason;
            NSString *eMsg;
            eMsg = [[NSString alloc] initWithFormat:@"Name: %@, \n Reason: %@",
                    name, reason];
            MILog(@"Exception thrown creating path. ", eMsg);
            success = NO;
        }

        if (!success)
        {
            break;
        }
    }
    
    CGPathRef finalPath = NULL;
    if (success)
    {
        finalPath = CGPathCreateCopy(mutablePath);
    }

    CGPathRelease(mutablePath);
    return finalPath;
}

-(BOOL)_applyTransformations:(NSArray *)transformations
{
    BOOL result = NO;
    CGAffineTransform affineTransform;
    if (MIMakeCGAffineTransformFromArray(transformations, &affineTransform,
                                self.variables))
    {
        CGContextConcatCTM(self.context, affineTransform);
        result = YES;
    }
    return result;
}

-(BOOL)_applyAffineTranform:(NSDictionary *)affineTransform
{
    CGAffineTransform transform;
    BOOL result = MIMakeCGAffineTransformFromDictionary(affineTransform,
                                                        &transform);
    if (result)
    {
        CGContextConcatCTM(self.context, transform);
    }
    else
    {
        MILog(@"Missing or invalid affine transform dict: ",
                             affineTransform);
    }
    return result;
}

-(BOOL)_applyShadow:(NSDictionary *)shadowDict
{
    BOOL result = YES;
    CGColorRef shadowColour = MIUtilityCreateColor(shadowDict[MIJSONKeyFillColor],
                                                   self.variables);
    CGSize offset;
    BOOL gotOffset = MIUtilityGetCGSizeFromDictionary(
                                              shadowDict[MIJSONKeyShadowOffset],
                                              &offset, kMILog, self.variables);
    CGFloat blur;
    BOOL gotBlur = MIUtilityGetCGFloatFromDictionary(shadowDict, MIJSONKeyBlur,
                                                     &blur, self.variables);
    result = gotBlur && gotOffset && shadowColour;
    if (result)
        CGContextSetShadowWithColor(self.context, offset, blur, shadowColour);
    
    if (shadowColour)
        CGColorRelease(shadowColour);
    
    if (!result)
    {
        MILog(@"Missing or invalid shadow dict.", shadowDict);
    }
    
    return result;
}

/*
 The image to be used as a mask must be in the gray color space. No alpha
 channel. The image behaves as a alpha channel. 
*/
-(BOOL)_applyImageAsMask:(NSDictionary *)maskDict
{
    BOOL result = YES;
    CGRect destRect;

    result &= MIUtilityGetCGRectFromDictionary(
                                        maskDict[MIJSONKeyDestinationRectangle],
                                        &destRect, kMILog, self.variables);

    MICGImage *maskImage = MICGImageFromDictionary(self._miContext,
                                            maskDict, self._owner);
    if (maskImage)
    {
        CGContextClipToMask(self.context, destRect, maskImage.CGImage);
    }
    else
    {
        result = NO;
    }
    
    return result;
}

-(CGPathRef)_createPath:(NSDictionary *)pathDict
{
    NSString *svgPathString = pathDict[MIJSONKeySVGPath];
    CGPathRef thePath = NULL;
    
    if (svgPathString)
    {
        thePath = [self _createPathFromSVGPath:svgPathString];
        if (!thePath) {
            MILog(@"Failed to CGPath from SVGPath.", svgPathString);
        }
    }
    else
    {
        CGPoint startPoint;
        BOOL hasStartPoint = MIUtilityGetCGPointFromDictionary(
                                            pathDict[MIJSONKeyStartPoint],
                                            &startPoint, kMILog, self.variables);
        
        NSArray *pathElementsArray = pathDict[MIJSONKeyArrayOfPathElements];
        if (hasStartPoint && pathElementsArray)
        {
            thePath = [self _createPathUsingPathElementsInArray:pathElementsArray
                                                     startPoint:startPoint];
        }
        else
        {
            MILog(@"Missing start point or path array.", pathDict);
        }
    }
    return thePath;
}

-(BOOL)_applyClipping:(NSDictionary *)clippingDict
{
    BOOL result = YES;
    BOOL isEvenOddClipping = NO;
    NSString *clippingRule = clippingDict[MIJSONKeyClippingRule];
    if (clippingRule &&
                [clippingRule isEqualToString:MIJSONValueEvenOddClippingRule])
    {
        isEvenOddClipping = YES;
    }

    CGPathRef thePath = [self _createPath:clippingDict];
    if (!thePath)
        result = NO;

    if (result)
    {
        CGContextAddPath(self.context, thePath);
        CGPathRelease(thePath);
        if (isEvenOddClipping)
            CGContextEOClip(self.context);
        else
            CGContextClip(self.context);
    }
    return result;
}

-(void)_assignLineDashSegments:(NSArray *)dashSegments phase:(NSNumber *)dashPhase
{
    if (dashSegments && [dashSegments isKindOfClass:[NSArray class]] && dashSegments.count > 1)
    {
        CGFloat lengths[dashSegments.count];
        int i = 0;
        for (NSNumber *number in dashSegments)
        {
            CGFloat length = 1;
            if (!MIUtilityGetCGFloatFromID(number, &length, self.variables))
            {
                MILog(@"Line dash segment lengths are not numbers: ", dashSegments);
            }
            lengths[i++] = length;
        }
        CGFloat phase = 0.0;
        MIUtilityGetCGFloatFromID(dashPhase, &phase, self.variables);
        CGContextSetLineDash(self.context, phase, lengths, dashSegments.count);
    }
    else
    {
        MILog(@"Should have an array of number/string values: ", dashSegments);
    }
}

// Returns true if the stack is pushed. Ignores possible problems.

-(BOOL)_setOptionalContextValuesAndPushState:(NSDictionary *)elementDict
{
    BOOL pushedState = FALSE;
    CGColorRef fillColour = MIUtilityCreateColor(elementDict[MIJSONKeyFillColor],
                                                 self.variables);

    CGColorRef strokeColour = MIUtilityCreateColor(elementDict[MIJSONKeyStrokeColor],
                                                   self.variables);
    CGFloat lineWidth = 1.0;
    BOOL gotLineWidth = MIUtilityGetLineWidthFromDictionary(elementDict,
                                                            &lineWidth,
                                                            self.variables);
    NSString *blendModeString = elementDict[MIJSONKeyBlendMode];
    NSDictionary *affineTransformation = elementDict[MIJSONKeyAffineTransform];
    NSArray *transformationArray = elementDict[MIJSONKeyContextTransformation];
    NSDictionary *shadowDict = elementDict[MIJSONKeyShadow];
    NSDictionary *clippingDict = elementDict[MIJSONKeyClippingpath];
    NSDictionary *maskDict = elementDict[MIJSONKeyApplyImageMask];
    NSNumber *contextAlpha = elementDict[MIJSONKeyContextAlpha];
    NSArray *dashSegments = elementDict[MIJSONKeyLineDashArray];
    NSNumber *dashPhase = elementDict[MIJSONKeyLineDashPhase];

    NSString *lineCapString = elementDict[MIJSONKeyLineCap];
    NSString *joinString = elementDict[MIJSONKeyLineJoin];
    CGFloat miter = 1.0;
    BOOL gotMiter = MIUtilityGetCGFloatFromDictionaryNoLog(elementDict,
                                                           MIJSONKeyMiter,
                                                           &miter, self.variables);

    NSString *interpolationQualityString = NULL;
    interpolationQualityString = elementDict[MIJSONKeyInterpolationQuality];
    
    // We shouldn't have both an affineTransformation and a transformationArray
    // defined. If that is the case then scrub the affineTransform for now.
    if (affineTransformation && transformationArray)
    {
        MILog(@"Both transformation array and affine transform defined",
              elementDict);
        affineTransformation = nil;
    }
    
    pushedState = fillColour || strokeColour || gotLineWidth ||
                  blendModeString || transformationArray || shadowDict ||
                  interpolationQualityString || affineTransformation ||
                  clippingDict || maskDict || contextAlpha ||
                  dashSegments || lineCapString || joinString || gotMiter;
    
    if (pushedState)
    {
        CGContextSaveGState(self.context);
        
        if (transformationArray)
            [self _applyTransformations:transformationArray];
        
        if (affineTransformation)
            [self _applyAffineTranform:affineTransformation];
        
        if (shadowDict)
            [self _applyShadow:shadowDict];
        
        if (clippingDict)
            [self _applyClipping:clippingDict];

        if (maskDict)
            [self _applyImageAsMask:maskDict];

        if (fillColour)
            CGContextSetFillColorWithColor(self.context, fillColour);
        
        if (strokeColour)
            CGContextSetStrokeColorWithColor(self.context, strokeColour);
        
        if (gotLineWidth)
            CGContextSetLineWidth(self.context, lineWidth);
        
        if (dashSegments)
            [self _assignLineDashSegments:dashSegments phase:dashPhase];

        if (lineCapString)
            CGContextSetLineCap(self.context,GetLineCapFromString(lineCapString));
        
        if (joinString)
            CGContextSetLineJoin(self.context, GetLineJoinFromString(joinString));
        
        if (gotMiter)
            CGContextSetMiterLimit(self.context, miter);

        if (blendModeString)
            CGContextSetBlendMode(self.context,
                                  GetBlendModeFromString(blendModeString));
        
        if (contextAlpha && [contextAlpha isKindOfClass:[NSNumber class]])
        {
            CGFloat alpha = contextAlpha.doubleValue;
            CGContextSetAlpha(self.context, alpha);
        }
        
        if (interpolationQualityString)
        {
            CGInterpolationQuality interpQual;
            interpQual = GetInterpolationQualityFromString(
                                                    interpolationQualityString);
            CGContextSetInterpolationQuality(self.context, interpQual);
        }
    }
    CGColorRelease(fillColour);
    CGColorRelease(strokeColour);
    return pushedState;
}

#pragma mark Private Methods for handling drawing commands.

-(NSNumber *)_handleDrawStrokeRectangleElement:(NSDictionary *)rectDict
{
    BOOL result = YES;
    CGRect theRect;
    result &= MIUtilityGetCGRectFromDictionary(rectDict[MIJSONKeyRect],
                                               &theRect, kMILog,
                                               self.variables);
    if (result)
    {
        BOOL stackPushed= [self _setOptionalContextValuesAndPushState:rectDict];
        CGContextStrokeRect(self.context, theRect);

        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Rectangle dictionary.", rectDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawStrokeRoundedRectangle:(NSDictionary *)rectDict
{
    BOOL result = YES;
    CGFloat radiuses[4];
    CGRect theRect;
    result &= MIUtilityGetCGRectFromDictionary(rectDict[MIJSONKeyRect],
                                               &theRect, kMILog, self.variables);
    result &= MIUtilityGetRadiusesFromDictionary(rectDict, radiuses,
                                                 self.variables);
    if (result)
    {
        BOOL pushStack = [self _setOptionalContextValuesAndPushState:rectDict];
        CGMutablePathRef thePath = CGPathCreateMutable();
        NSNumber *pathResult = [self _assignRoundedRectangle:rectDict
                                                      toPath:thePath];
        result = pathResult.boolValue;
        
        if (result)
        {
            CGContextAddPath(self.context, thePath);
            CGContextStrokePath(self.context);
        }
        else
        {
            MILog(@"Failed to create rounded rect path", rectDict);
        }

        if (thePath)
            CGPathRelease(thePath);

        if (pushStack)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Rectangle dictionary.", rectDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawFillRoundedRectangle:(NSDictionary *)rectDict
{
    BOOL result = YES;
    CGFloat radiuses[4];
    CGRect theRect;
    result &= MIUtilityGetCGRectFromDictionary(rectDict[MIJSONKeyRect],
                                               &theRect, kMILog, self.variables);
    result &= MIUtilityGetRadiusesFromDictionary(rectDict, radiuses,
                                                 self.variables);
    if (result)
    {
        BOOL stackPushed = [self _setOptionalContextValuesAndPushState:rectDict];
        CGMutablePathRef thePath = CGPathCreateMutable();
        NSNumber *pathResult = [self _assignRoundedRectangle:rectDict
                                                      toPath:thePath];
        result = pathResult.boolValue;
        
        if (result)
        {
            CGContextAddPath(self.context, thePath);
            CGContextFillPath(self.context);
        }
        else
        {
            MILog(@"Failed to create rounded rect path", rectDict);
        }
        
        if (thePath)
            CGPathRelease(thePath);
        
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Rectangle dictionary.", rectDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawFillRectangleElement:(NSDictionary *)rectDict
{
    BOOL result = YES;
    CGRect theRect;
    result &= MIUtilityGetCGRectFromDictionary(rectDict[MIJSONKeyRect],
                                              &theRect, kMILog, self.variables);
    if (result)
    {
        BOOL stackPushed= [self _setOptionalContextValuesAndPushState:rectDict];
        CGContextFillRect(self.context, theRect);
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Rectangle dictionary.", rectDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawStrokeOvalElement:(NSDictionary *)ovalDict
{
    BOOL result = YES;
    CGRect theRect;
    result &= MIUtilityGetCGRectFromDictionary(ovalDict[MIJSONKeyRect],
                                               &theRect, kMILog, self.variables);
    if (result)
    {
        BOOL stackPushed = [self _setOptionalContextValuesAndPushState:ovalDict];
        CGContextStrokeEllipseInRect(self.context, theRect);
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Oval dictionary.", ovalDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawFillOvalElement:(NSDictionary *)ovalDict
{
    BOOL result = YES;
    CGRect theRect;
    result &=MIUtilityGetCGRectFromDictionary(ovalDict[MIJSONKeyRect],
                                              &theRect, kMILog, self.variables);
    
    if (result)
    {
        BOOL stackPushed= [self _setOptionalContextValuesAndPushState:ovalDict];
        CGContextFillEllipseInRect(self.context, theRect);
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Oval dictionary.", ovalDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawLine:(NSDictionary *)lineDict
{
    BOOL result = YES;
    
    MMCGLine theLine;
    result = MIUtilityGetMMCGLineFromDictionary(lineDict[MIJSONKeyLine],
                                                &theLine, self.variables);
    if (result)
    {
        BOOL stackPushed= [self _setOptionalContextValuesAndPushState:lineDict];
		NSString *lineCapString = lineDict[MIJSONKeyLineCap];
		if (lineCapString)
		{
			stackPushed = YES;
            CGContextSetLineCap(self.context,GetLineCapFromString(lineCapString));
		}

        CGContextBeginPath(self.context);
        CGContextMoveToPoint(self.context, theLine.start.x, theLine.start.y);
        CGContextAddLineToPoint(self.context, theLine.end.x, theLine.end.y);
        CGContextClosePath(self.context);
        CGContextStrokePath(self.context);
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
       MILog(@"Line dict: ", lineDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawStrokePathElement:(NSDictionary *)pathDict
{
    BOOL result = YES;

    if (result)
    {
        BOOL stackPushed= [self _setOptionalContextValuesAndPushState:pathDict];

        CGPathRef thePath = [self _createPath:pathDict];

        if (!thePath)
            result = NO;
        
        if (result)
        {
            CGContextAddPath(self.context, thePath);
            CGContextStrokePath(self.context);
        }
        
        if (thePath)
            CGPathRelease(thePath);
        
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Path dictionary.", pathDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawFillPathElement:(NSDictionary *)pathDict
{
    BOOL result = YES;

    CGPathRef thePath = [self _createPath:pathDict];
    if (thePath)
    {
        BOOL stackPushed = [self _setOptionalContextValuesAndPushState:pathDict];
        if (result)
        {
            CGContextAddPath(self.context, thePath);
            NSString *fillRule = pathDict[MIJSONKeyClippingRule];
            if (fillRule && [fillRule isEqualToString:MIJSONValueEvenOddClippingRule]) {
                CGContextEOFillPath(self.context);
            }
            else {
                CGContextFillPath(self.context);
            }
        }
        
        if (thePath)
            CGPathRelease(thePath);

        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    else
    {
        MILog(@"Path dictionary.", pathDict);
        result = NO;
    }
    return @(result);
}

-(NSNumber *)_handleDrawFillPathAddInnerShadow:(NSDictionary *)fillPathInnerShadow
{
    NSDictionary *innerShadow = fillPathInnerShadow[MIJSONKeyInnerShadow];
    
    CGSize shadowOffset;
    if (!MIUtilityGetCGSizeFromDictionary(innerShadow[MIJSONKeyShadowOffset],
                                          &shadowOffset, kMILog, self.variables))
    {
        return @NO;
    }
    
    CGFloat blurRadius;
    if (!MIUtilityGetCGFloatFromDictionary(innerShadow, MIJSONKeyBlur,
                                           &blurRadius, self.variables))
    {
        return @NO;
    }

    CGColorRef shadowColor = MIUtilityCreateColor(innerShadow[MIJSONKeyFillColor],
                                                  self.variables);
    
    if (!shadowColor)
    {
        return @NO;
    }
    
    // From this point must manage any error, rather than bailing.

    BOOL result = YES;
    
    BOOL stackPushed = [self _setOptionalContextValuesAndPushState:
                       fillPathInnerShadow];
    
    if (!stackPushed)
    {
        CGContextSaveGState(self.context);
    }

    CGPathRef thePath = [self _createPath:fillPathInnerShadow];
    if (!thePath)
        result = NO;

    if (result)
    {
        CGContextAddPath(self.context, thePath);
        CGContextFillPath(self.context);
        
        // Code taken from:
        // http://blog.helftone.com/demystifying-inner-shadows-in-quartz/
        // Also see paintcode.
        CGContextAddPath(self.context, thePath);
        CGContextClip(self.context);

        CGContextSetShadowWithColor(self.context, CGSizeZero, 0, NULL);

        CGColorRef opaqueShadow = CGColorCreateCopyWithAlpha(shadowColor, 1.0);
        CGContextSetAlpha(self.context, CGColorGetAlpha(shadowColor));
        CGContextBeginTransparencyLayer(self.context, NULL);
        {
            CGContextSetShadowWithColor(self.context, shadowOffset,
                                        blurRadius, opaqueShadow);
            CGContextSetBlendMode(self.context, kCGBlendModeSourceOut);
            CGContextSetFillColorWithColor(self.context, opaqueShadow);
            CGContextAddPath(self.context, thePath);
            CGContextFillPath(self.context);
        }
        CGContextEndTransparencyLayer(self.context);
        CGColorRelease(opaqueShadow);
    }
    
    if (thePath)
        CGPathRelease(thePath);

    CGContextRestoreGState(self.context);
    
    CGColorRelease(shadowColor);

    return @(result);
}

-(NSNumber *)_handleDrawFillStrokePathElement:(NSDictionary *)pathDict
{
    BOOL result = YES;
    BOOL stackPushed = [self _setOptionalContextValuesAndPushState:pathDict];
    CGPathRef thePath = [self _createPath:pathDict];
    
    if (!thePath)
        result = NO;
    
    BOOL evenOddFillRule = NO;
    NSString *fillRule = pathDict[MIJSONKeyClippingRule];
    if (fillRule && [fillRule isEqualToString:MIJSONValueEvenOddClippingRule]) {
        evenOddFillRule = YES;
    }
    CGPathDrawingMode mode = evenOddFillRule ? kCGPathEOFillStroke : kCGPathFillStroke;
    if (result)
    {
        CGContextAddPath(self.context, thePath);
        CGContextDrawPath(self.context, mode);
    }
    
    if (thePath)
        CGPathRelease(thePath);
    
    if (stackPushed)
        CGContextRestoreGState(self.context);

    if (!result)
    {
        MILog(@"FillAndStroke Path dictionary.", pathDict);
    }
    return @(result);

}

-(NSNumber *)_handleDrawLinearGradientFill:(NSDictionary *)gradientFillDict
{
    BOOL result = YES;
    MMCGLine line;
    result &= MIUtilityGetMMCGLineFromDictionary(gradientFillDict[MIJSONKeyLine],
                                                 &line, self.variables);
    NSArray *arrayOfColorDicts = NULL;
    NSArray *arrayOfLocations = NULL;
    if (result)
    {
        arrayOfColorDicts = gradientFillDict[MIJSONKeyArrayOfColors];
        arrayOfLocations = gradientFillDict[MIJSONKeyArrayOfLocations];
        
        if (!arrayOfColorDicts || !arrayOfLocations ||
            ![arrayOfColorDicts isKindOfClass:[NSArray class]] ||
            ![arrayOfLocations isKindOfClass:[NSArray class]] ||
            [arrayOfColorDicts count] == 0 ||
            [arrayOfColorDicts count] != [arrayOfLocations count])
            result = NO;
    }
    
    // Need to bail here, just so that having a c-array of locations work from
    // this point on.
    if (!result)
    {
        MILog(@"Gradient fill dictionary.", gradientFillDict);
        return @(result);
    }
    
    CGFloat locations[arrayOfLocations.count];
    size_t i = 0;
    for (NSNumber *theNumber in arrayOfLocations)
    {
        if (![theNumber isKindOfClass:[NSNumber class]])
        {
            result = NO;
            break;
        }
        locations[i++] = [theNumber doubleValue];
    }

    BOOL hasShadow = gradientFillDict[MIJSONKeyShadow] ? YES : NO;
    BOOL hasContextAlpha = gradientFillDict[MIJSONKeyContextAlpha] ? YES : NO;
    BOOL requiresTransparencyLayer = hasShadow || hasContextAlpha;
    BOOL stackPushed = [self _setOptionalContextValuesAndPushState:
                        gradientFillDict];

    if (requiresTransparencyLayer)
        CGContextBeginTransparencyLayer(self.context, NULL);

    BOOL evenOddFillRule = NO;
    
    NSString *fillRule = gradientFillDict[MIJSONKeyClippingRule];
    if (fillRule && [fillRule isEqualToString:MIJSONValueEvenOddClippingRule]) {
        evenOddFillRule = YES;
    }
    
    NSMutableArray *arrayOfColors = NULL;
    if (result)
    {
        arrayOfColors = [[NSMutableArray alloc] initWithCapacity:0];
        for (id theColor in arrayOfColorDicts)
        {
            CGColorRef theColorRef = NULL;
            theColorRef = MIUtilityCreateColor(theColor, self.variables);
            if (!theColorRef)
            {
                result = NO;
                break;
            }
            [arrayOfColors addObject:(__bridge id)theColorRef];
            CGColorRelease(theColorRef);
        }
    }
    
    CGGradientRef theGradient = NULL;
    if (result)
    {
        theGradient = CGGradientCreateWithColors(
                 CGColorGetColorSpace((__bridge CGColorRef)(arrayOfColors[0])),
                 (__bridge CFArrayRef)(arrayOfColors),
                 locations);
        if (!theGradient)
            result = NO;
    }
    
    CGPathRef thePath = [self _createPath:gradientFillDict];

    if (!thePath)
        result = NO;

    if (result)
    {
        CGContextSaveGState(self.context);
        CGContextAddPath(self.context, thePath);
        if (evenOddFillRule) {
            CGContextEOClip(self.context);
        }
        else {
            CGContextClip(self.context);
        }

        CGContextDrawLinearGradient(self.context, theGradient,
                                    line.start, line.end, 0);
        CGContextRestoreGState(self.context);
    }
    
    if (thePath)
        CGPathRelease(thePath);

    if (theGradient)
        CGGradientRelease(theGradient);

    if (requiresTransparencyLayer)
        CGContextEndTransparencyLayer(self.context);

    if (stackPushed)
    {
        CGContextRestoreGState(self.context);
    }

    if (!result)
    {
        MILog(@"Gradient fill dict: ", gradientFillDict);
    }
    
    return @(result);
}

-(NSNumber *)_handleDrawRadialGradientFill:(NSDictionary *)gradientFillDict
{
    BOOL result = YES;

    CGPoint centerPoint1, centerPoint2;
    result &= MIUtilityGetCGPointFromDictionary(
                                        gradientFillDict[MIJSONKeyCenterPoint],
                                        &centerPoint1, kMILog, self.variables);

    result &= MIUtilityGetCGPointFromDictionary(
                                        gradientFillDict[MIJSONKeyCenterPoint2],
                                        &centerPoint2, kMILog, self.variables);

    CGFloat radius1, radius2;
    result &= MIUtilityGetCGFloatFromDictionary(gradientFillDict,
                                                MIJSONKeyRadius,
                                                &radius1, self.variables);

    result &= MIUtilityGetCGFloatFromDictionary(gradientFillDict,
                                                MIJSONKeyRadius2,
                                                &radius2, self.variables);

    CGGradientDrawingOptions drawOptions = 0;
    NSArray *optionsList = gradientFillDict[MIJSONKeyGradientDrawOptions];
    if (optionsList && [optionsList isKindOfClass:[NSArray class]] &&
        optionsList.count > 0)
    {
        for (NSString *option in optionsList)
        {
            if ([option isKindOfClass:[NSString class]])
            {
                if ([option isEqualToString:MIJSONValueGradientDrawBeforeStart])
                {
                    drawOptions |= kCGGradientDrawsBeforeStartLocation;
                }
                else if ([option isEqualToString:MIJSONValueGradientDrawAfterEnd])
                {
                    drawOptions |= kCGGradientDrawsAfterEndLocation;
                }
            }
        }
    }
    
    NSArray *arrayOfColorDicts = NULL;
    NSArray *arrayOfLocations = NULL;
    if (result)
    {
        arrayOfColorDicts = gradientFillDict[MIJSONKeyArrayOfColors];
        arrayOfLocations = gradientFillDict[MIJSONKeyArrayOfLocations];
        
        if (!arrayOfColorDicts || !arrayOfLocations ||
            ![arrayOfColorDicts isKindOfClass:[NSArray class]] ||
            ![arrayOfLocations isKindOfClass:[NSArray class]] ||
            [arrayOfColorDicts count] == 0 ||
            [arrayOfColorDicts count] != [arrayOfLocations count])
            result = NO;
    }
    
    // Need to bail here, just so that having a c-array of locations work from
    // this point on.
    if (!result)
    {
        MILog(@"Gradient fill dictionary.", gradientFillDict);
        return @(result);
    }

    CGFloat locations[[arrayOfLocations count]];
    size_t i = 0;
    for (NSNumber *theNumber in arrayOfLocations)
    {
        if (![theNumber isKindOfClass:[NSNumber class]])
        {
            result = NO;
            break;
        }
        locations[i++] = [theNumber doubleValue];
    }

    BOOL hasShadow = gradientFillDict[MIJSONKeyShadow] ? YES : NO;
    BOOL stackPushed = [self _setOptionalContextValuesAndPushState:
                        gradientFillDict];
    
    if (hasShadow)
        CGContextBeginTransparencyLayer(self.context, NULL);
    

    NSMutableArray *arrayOfColors = NULL;
    if (result)
    {
        arrayOfColors = [[NSMutableArray alloc] initWithCapacity:0];
        for (id theColor in arrayOfColorDicts)
        {
            CGColorRef theColorRef = NULL;
            theColorRef = MIUtilityCreateColor(theColor, self.variables);
            if (!theColorRef)
            {
                result = NO;
                break;
            }
            [arrayOfColors addObject:(__bridge id)theColorRef];
            CGColorRelease(theColorRef);
        }
    }
    
    CGGradientRef theGradient = NULL;
    if (result)
    {
        theGradient = CGGradientCreateWithColors(
                CGColorGetColorSpace((__bridge CGColorRef)(arrayOfColors[0])),
                (__bridge CFArrayRef)(arrayOfColors),
                locations);
        if (!theGradient)
            result = NO;
    }

    if (result)
    {
        CGContextDrawRadialGradient(self.context, theGradient,
                                    centerPoint1, radius1,
                                    centerPoint2, radius2,
                                    drawOptions);
    }

    if (theGradient)
        CGGradientRelease(theGradient);
    
    if (hasShadow)
        CGContextEndTransparencyLayer(self.context);
    
    if (stackPushed)
    {
        CGContextRestoreGState(self.context);
    }
    

    if (!result)
    {
        MILog(@"Gradient fill dict: ", gradientFillDict);
    }

    return @(result);
}

-(NSNumber *)_handleDrawImage:(NSDictionary *)drawImageDict
{
    BOOL result = YES;

    CGRect destinationRect;
    
    result &= MIUtilityGetCGRectFromDictionary(
                                drawImageDict[MIJSONKeyDestinationRectangle],
                                &destinationRect, kMILog, self.variables);

    if (result && CGRectIsEmpty(destinationRect))
    {
        result = NO;
        MILog(@"Empty destination rect",
                                drawImageDict[MIJSONKeyDestinationRectangle]);
    }
    
    MICGImage *image;
    if (result)
    {
        image = MICGImageFromDictionary(self._miContext, drawImageDict,
                                        self._owner);
        if (!image)
        {
            MILog(@"Failed to get image", drawImageDict);
            result = NO;
        }
    }

    if (result)
    {
        CGRect sourceRect;
        BOOL gotSourceRect = MIUtilityGetCGRectFromDictionary(
                                        drawImageDict[MIJSONKeySourceRectangle],
                                        &sourceRect, kMINoLog, self.variables);

        BOOL pushedState = FALSE;
        pushedState = [self _setOptionalContextValuesAndPushState:drawImageDict];

        // If we have a source rect then we are going
        // to need to generate an intermediate image before drawing to the
        // graphics context.
        
        if (gotSourceRect)
        {
            // first check to see if sourceRect contains all the original
            // image and shrink any dimension if any are larger than original.
            size_t imageHeight = CGImageGetHeight(image.CGImage);
            size_t imageWidth = CGImageGetWidth(image.CGImage);
            
            CGRect sourceImageRect;
            sourceImageRect = CGRectMake(0.0, 0.0, imageWidth, imageHeight);
            CGRect intersectRect;
            intersectRect = CGRectIntersection(sourceRect, sourceImageRect);
            
            BOOL intersectEmpty = CGRectIsEmpty(intersectRect);
            BOOL intersectEqualImageRect = CGRectEqualToRect(sourceImageRect,
                                                             intersectRect);
            if (!(intersectEmpty || intersectEqualImageRect))
            {
                // ok so now need to create the new image.
                CGImageRef subImage = CGImageCreateWithImageInRect(image.CGImage,
                                                                intersectRect);
                if (subImage)
                {
                    image = [[MICGImage alloc] initWithCGImage:subImage];
                    CGImageRelease(subImage);
                }
            }
        }

        // SaveCGImageToAPNGFile(imageRef, @"MICGContext__handleDrawImage.png");
        // If no errors do the drawing.
        CGContextDrawImage(self.context, destinationRect, image.CGImage);
        
        // We've done the work. Cleanup.
        if (pushedState)
            CGContextRestoreGState(self.context);
    }
    
    return @(result);
}

-(NSNumber *)_handleDrawLinesFromPoints:(NSArray *)points
{
    BOOL result = YES;
    BOOL gotPoint;
    NSInteger numPoints = points.count;
    CGPoint thePoints[numPoints];
    int i = 0;
    for (NSDictionary *pointDict in points)
    {
        gotPoint = MIUtilityGetCGPointFromDictionary(pointDict, &thePoints[i],
                                                     kMILog, self.variables);
        if (!gotPoint)
            result = NO;
        
        i++;
        if (!result)
            break;
    }
    if (result)
    {
        CGContextStrokeLineSegments(self.context, thePoints, numPoints);
    }
    else
    {
        MILog(@"Points.", points);
    }

    return @(result);
}

-(NSNumber *)_handleDrawLines:(NSDictionary *)linesDict
{
    BOOL result = YES;
    NSArray *points = linesDict[MIJSONKeyPoints];
    if (!points || ![points isKindOfClass:[NSArray class]])
        result = NO;

    if (result)
    {
        BOOL stackPushed=[self _setOptionalContextValuesAndPushState:linesDict];

        result = [[self _handleDrawLinesFromPoints:points] boolValue];
        if (stackPushed)
            CGContextRestoreGState(self.context);
    }
    
    if (!result)
    {
        MILog(@"Lines dictionary.", linesDict);
    }
    return @(result);
}

-(NSNumber *)_handleDrawBasicString:(NSDictionary *)stringDict
{
    NSDictionary *innerShadow = stringDict[MIJSONKeyInnerShadow];
    CGSize shadowOffset;
    CGFloat blurRadius = 4;
    CGColorRef shadowColor = NULL;
    
    if (innerShadow)
    {
        if (!MIUtilityGetCGSizeFromDictionary(innerShadow[MIJSONKeyShadowOffset],
                                              &shadowOffset,
                                              kMILog, self.variables))
        {
            return @NO;
        }
        
        if (!MIUtilityGetCGFloatFromDictionary(innerShadow, MIJSONKeyBlur,
                                               &blurRadius, self.variables))
        {
            return @NO;
        }
        
        shadowColor = MIUtilityCreateColor(innerShadow[MIJSONKeyFillColor],
                                           self.variables);
        
        if (!shadowColor)
        {
            return @NO;
        }
    }
    
    BOOL result = YES;
    
    NSString *theString = stringDict[MIJSONKeyStringText];
    NSString *substitution = stringDict[MIJSONKeyStringTextSubstitution];
    if (substitution)
    {
        NSString *subString = self.variables[substitution];
        if (subString)
            theString = subString;
    }

    CGPoint thePoint;
    BOOL gotPoint;
    gotPoint = MIUtilityGetCGPointFromDictionary(stringDict[MIJSONKeyPoint],
                                                 &thePoint, kMILog,
                                                 self.variables);
    
    NSString *postScriptFontName;
    postScriptFontName = stringDict[MIJSONKeyStringPostscriptFontName];
    
    NSString *uiFontTypeString = stringDict[MIJSONKeyUIFontType];
    CTFontUIFontType theUIFontType = kCTFontUIFontNone;
    CGFloat fontSize = 0.0;
    BOOL gotFontSize = NO;
    if (uiFontTypeString)
    {
        theUIFontType = MIUtilityGetUserInterfaceFontType(uiFontTypeString);
        gotFontSize = MIUtilityGetCGFloatFromDictionaryNoLog(stringDict,
                                MIJSONKeyStringFontSize, &fontSize, self.variables);
    }
    else
    {
        gotFontSize = MIUtilityGetCGFloatFromDictionary(stringDict,
                                MIJSONKeyStringFontSize, &fontSize, self.variables);
    }

    CTFontRef font = NULL;
    if (postScriptFontName && gotFontSize)
    {
        font = CTFontCreateWithName((__bridge CFStringRef)(postScriptFontName),
                                    fontSize, NULL);
    }
    else if (theUIFontType != kCTFontUIFontNone)
    {
        font = CTFontCreateUIFontForLanguage(theUIFontType, fontSize, NULL);
    }

    if (!theString || !gotPoint || !font)
    {
        NSString *missingText, *missingPoint, *missingFont, *missingFontSize;
        if (!theString)
            missingText = @"Text to draw is missing. ";
        
        if (!gotPoint)
            missingPoint = @"Point to draw text from is missing. ";
        
        if (!font)
        {
            if (postScriptFontName && !gotFontSize)
                missingFontSize = @"Font size to draw with is missing.";
            else if (!postScriptFontName && !theUIFontType)
                missingFont = @"Missing one of postscript or ui font.";
            else
                missingFont = @"Couldn't create font with font dictionary.";
        }
        NSMutableString *messageString = [[NSMutableString alloc] init];
        [messageString appendString:@"Missing or invalid draw string info. "];
        if (missingText)
            [messageString appendString:missingText];
        if (missingPoint)
            [messageString appendString:missingPoint];
        if (missingFontSize)
            [messageString appendString:missingFontSize];
        if (missingFont)
            [messageString appendString:missingFont];

        MILog(messageString, stringDict);

        result = NO;
    }
    
    if (result)
    {
        // We have the necessary information to display the string therefore
        // try and get any of the optional string drawing parameters.
        CGColorRef foreColor = MIUtilityCreateColor(stringDict[MIJSONKeyFillColor],
                                                    self.variables);
        CGColorRef strokeColor = MIUtilityCreateColor(
                                        stringDict[MIJSONKeyStrokeColor],
                                        self.variables);
        NSArray *arrayOfPathElements = stringDict[MIJSONKeyArrayOfPathElements];
        NSString *blendModeString = stringDict[MIJSONKeyBlendMode];
        NSArray *transformationArray;
        transformationArray = stringDict[MIJSONKeyContextTransformation];
        NSDictionary *affineTransform = stringDict[MIJSONKeyAffineTransform];

        // If both transformationArray and affineTransform are defined then
        // because we only want to apply one of them, we will scrub the
        // affine transform. I'm also going to output a message.
        if (transformationArray && affineTransform)
        {
            MILog(
                     @"Both transformation array and affine transform defined",
                     stringDict);
            affineTransform = nil;
        }
        NSNumber *strokeWidth = stringDict[MIJSONKeyStringStrokeWidth];
        
        NSString *alignmentString = stringDict[MIJSONKeyTextAlignment];
        
        CGContextSaveGState(self.context);
        if (blendModeString)
            CGContextSetBlendMode(self.context,
                                  GetBlendModeFromString(blendModeString));

        if (transformationArray)
            [self _applyTransformations:transformationArray];

        if (affineTransform)
            [self _applyAffineTranform:affineTransform];

        NSDictionary *shadowDict = stringDict[MIJSONKeyShadow];
        if (shadowDict)
            [self _applyShadow:shadowDict];
        
        NSDictionary *clippingDict = stringDict[MIJSONKeyClippingpath];
        if (clippingDict)
            [self _applyClipping:clippingDict];
        
        NSDictionary *maskDict = stringDict[MIJSONKeyApplyImageMask];
        if (maskDict)
            [self _applyImageAsMask:maskDict];

        CGPathRef thePath = NULL;
        if (arrayOfPathElements)
        {
            thePath = [self _createPathUsingPathElementsInArray:arrayOfPathElements
                                                     startPoint:thePoint];
            if (!thePath)
                result = NO;
        }
        NSMutableDictionary *attributesDict = [[NSMutableDictionary alloc]
                                                        initWithCapacity:0];
        [attributesDict setObject:(__bridge id)(font)
                            forKey:(__bridge NSString *)(kCTFontAttributeName)];
        if (foreColor)
            [attributesDict setObject:(__bridge id)(foreColor)
                forKey:(__bridge NSString *)(kCTForegroundColorAttributeName)];
        else
        {
            [attributesDict setObject:(__bridge id)(kCFBooleanTrue)
                               forKey:(__bridge NSString *)
                                    kCTForegroundColorFromContextAttributeName];
        }
        
        if (alignmentString)
        {
            CTTextAlignment alignment;
            alignment = GetTextAlignmentFromString(alignmentString);
            CTParagraphStyleSetting settings[] = {
                                {   kCTParagraphStyleSpecifierAlignment,
                                    sizeof(alignment),
                                    &alignment} };
            CTParagraphStyleRef paragraphStyle = NULL;
            paragraphStyle = CTParagraphStyleCreate(settings,
                                        sizeof(settings) / sizeof(settings[0]));
            [attributesDict setObject:(__bridge id)paragraphStyle
                            forKey:(__bridge id)kCTParagraphStyleAttributeName];
            CFRelease(paragraphStyle);
        }

        if (strokeColor)
            [attributesDict setObject:(__bridge id)(strokeColor)
                forKey:(__bridge NSString *)kCTStrokeColorAttributeName];
        
        if (strokeWidth)
            [attributesDict setObject:strokeWidth
                               forKey:(__bridge id)kCTStrokeWidthAttributeName];
        
        NSAttributedString *theAttrString = NULL;
        theAttrString = [[NSAttributedString alloc] initWithString:theString
                                                    attributes:attributesDict];
        
        if (!theAttrString)
        {
            MILog(@"Could not create attributed string.",
                                 stringDict);
            result = NO;
        }
/*
        CGContextSetAllowsFontSubpixelPositioning(self.context, true);
        CGContextSetShouldSubpixelPositionFonts(self.context, true);
        CGContextSetAllowsFontSubpixelQuantization(self.context, true);
        CGContextSetShouldSubpixelQuantizeFonts(self.context, true);
*/
        if (result)
        {
            result = [self _drawAttributedString:theAttrString
                                          inPath:thePath
                                       orAtPoint:thePoint];
        }

        if (result && innerShadow)
        {
            CGColorRef opaqueShadow = CGColorCreateCopyWithAlpha(shadowColor, 1.0);
            NSMutableDictionary *shadowAttribs = attributesDict.mutableCopy;
            [shadowAttribs setObject:(__bridge id)(shadowColor)
                forKey:(__bridge NSString *)(kCTForegroundColorAttributeName)];
            [shadowAttribs setObject:@NO
            forKey:(__bridge NSString *)(kCTForegroundColorFromContextAttributeName)];
            NSAttributedString *shadowString = [[NSAttributedString alloc]
                                                initWithString:theString
                                                    attributes:shadowAttribs];
            CGContextSetShadowWithColor(self.context, CGSizeZero, 0, NULL);
            CGContextSetAlpha(self.context, CGColorGetAlpha(shadowColor));

            CGContextBeginTransparencyLayer(self.context, NULL);
            {
                CGContextSetShadowWithColor(self.context, shadowOffset,
                                            blurRadius, opaqueShadow);
                CGContextSetBlendMode(self.context, kCGBlendModeSourceOut);
                // CGContextSetFillColorWithColor(self.context, opaqueShadow);
                CGContextBeginTransparencyLayer(self.context, NULL);
                result = [self _drawAttributedString:shadowString
                                              inPath:thePath
                                           orAtPoint:thePoint];
                CGContextEndTransparencyLayer(self.context);
            }
            CGContextEndTransparencyLayer(self.context);
            CGColorRelease(opaqueShadow);
        }
        CGContextRestoreGState(self.context); // added.

        if (foreColor)
            CGColorRelease(foreColor);
        if (strokeColor)
            CGColorRelease(strokeColor);
        if (thePath)
            CGPathRelease(thePath);
        if (shadowColor)
            CGColorRelease(shadowColor);
    }
    if (font)
        CFRelease(font);

    if (!result)
    {
        MILog(@"Draw string dictionary: ", stringDict);
    }
    return @(result);
}

-(BOOL)_drawAttributedString:(NSAttributedString *)attrStr
                      inPath:(CGPathRef)thePath
                   orAtPoint:(CGPoint)thePoint
{
    BOOL result = YES;
    CFAttributedStringRef attrString = (__bridge CFAttributedStringRef)attrStr;
    if (thePath)
    {
        // draw text with the path.
        CTFramesetterRef frameSetter;
        frameSetter = CTFramesetterCreateWithAttributedString(attrString);
        if (frameSetter)
        {
            CTFrameRef theFrame = CTFramesetterCreateFrame(frameSetter,
                            CFRangeMake(0, [attrStr length]), thePath, NULL);
            CFRelease(frameSetter);
            CTFrameDraw(theFrame, self.context);
            CFRelease(theFrame);
        }
        else
        {
            MILog(@"Failed to create frame setter.", attrStr);
            result = NO;
        }
    }
    else
    {
        CTLineRef line;
        line = CTLineCreateWithAttributedString(attrString);
        if (line)
        {
            CGContextSetTextPosition(self.context,
                                     thePoint.x,
                                     thePoint.y);
            CTLineDraw(line, self.context);
            CFRelease(line);
        }
        else
        {
            MILog(@"Failed to create core text line.", attrStr);
            result = NO;
        }
    }
    return result;
}

-(NSNumber *)_handleDrawArrayOfElements:(NSDictionary *)elements
{
    BOOL result = YES;

    NSArray *arrayOfElements = elements[MIJSONKeyArrayOfElements];
    if (arrayOfElements && [arrayOfElements isKindOfClass:[NSArray class]])
    {
        BOOL hasShadow = elements[MIJSONKeyShadow] ? YES : NO;
        BOOL pushStack = [self _setOptionalContextValuesAndPushState:elements];

        if (hasShadow)
            CGContextBeginTransparencyLayer(self.context, NULL);
        
        for (NSDictionary *elementDict in arrayOfElements)
        {
            if (![elementDict isKindOfClass:[NSDictionary class]])
                result = NO;
            
            if (result)
                result = [self _handleDictionaryDrawElementCommand:elementDict];
            
            if (!result)
                break;
        }

        if (hasShadow)
            CGContextEndTransparencyLayer(self.context);

        if (pushStack)
            CGContextRestoreGState(self.context);
    }
    else
    {
        result = NO;
    }
    
    if (!result)
    {
        // If there has been a problem. Note that here, but don't output
        // the elements. At this point it could be large and the output will
        // overwhelm the log file.
        MILog(@"Drawing error.", nil);
    }
    
    return @(result);
}

-(BOOL)_handleDictionaryDrawElementCommand:(NSDictionary *)command
{
    BOOL result = YES;
    @autoreleasepool
    {
        NSString *elementType = [command objectForKey:MIJSONKeyElementType];
        if (!(elementType && [elementType isKindOfClass:[NSString class]]))
        {
            MILog(@"Draw element type not specified.", command);
            result = NO;
        }
        
        SEL theSelector = (SEL)0;
        if (result)
            theSelector = GetSelectorFromElementType(elementType);
        
        if (theSelector == (SEL)0 && result)
        {
            MILog(@"Draw element type unknown.", command);
            result = NO;
        }
        
        if (result == NO)
            return result;

        @try
        {
            NSNumber *theNum;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            theNum = [self performSelector:theSelector withObject:command];
#pragma clang diagnostic pop
            result = [theNum boolValue];
        }
        @catch (NSException *exception)
        {
            NSString *name = exception.name;
            NSString *reason = exception.reason;
            NSString *eMsg;
            eMsg = [[NSString alloc] initWithFormat:@"Name: %@, \n Reason: %@",
                                                                name, reason];
            MILog(@"Exception thrown while drawing. ", eMsg);
            result = NO;
        }

        if (!result)
        {
            // If there has been a problem. Note that here, but don't output
            // the command. At this point it could be large and the output will
            // overwhelm the log file.
            MILog(@"Drawing error.", nil);
            // Drawing failed, see if the element that we've tried to draw
            // had a debug name, if so set that name. Note that this method
            // can be called recursively and we want to know the lowest level
            // element that caused the problem so if elementErrorDebugName has
            // already been set, don't reset. This also means that
            // elementErrorDebugName needs to be cleared before this method
            // gets called for the first time.
            if (!self.elementErrorDebugName)
            {
                NSString *elementName;
                elementName = [command objectForKey:MIJSONKeyElementDebugName];
                if (elementName)
                    self.elementErrorDebugName = elementName;
            }
        }
    }
    
    return result;
}

@end
