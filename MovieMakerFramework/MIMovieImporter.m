//  MIMovieImporter.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "AVMetadataItem+THAdditions.h"
#import "MIAVFoundationUtilities.h"
#import "MIBaseObject.h"
#import "MIBaseObject_Protected.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MICMSampleBuffer.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIHandleCommands.h"
#import "MIHandleCommands+Internal.h"
#import "MIJSONConstants.h"
#import "MIMovieImporter.h"
#import "MIMovieVideoSampleAccessor.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

@import AVFoundation;

#pragma mark -
#pragma mark MIMovieImporter Private Interface

@interface MIMovieImporter ()

@property (strong) dispatch_queue_t _sampleQueue;

/// Path to the movie file as a NSURL/CFURLRef
@property (nonatomic, readonly) NSString *_moviePath;

/// The movie asset that this class wraps
@property (strong) AVURLAsset *_movieAsset;

/// The video tracks frame accessor.
@property (strong) MIMovieVideoSampleAccessor *_frameAccessor;

/// Get the number of tracks based on conforming to dictionary properties.
-(NSInteger)_getNumberOfTracks:(NSDictionary *)commandDict;

/// Convert a array of tracks identified by dictionaries into array of AssetTracks
-(NSArray *)_assetVideoTracksFromTrackDictionaries:(NSArray *)trackDictionaries;

/// Create a CGImage from a list of tracks and frame time.
-(CGImageRef)_createImageFromTracks:(NSArray *)tracks atTime:(CMTime)time
                                                        CF_RETURNS_RETAINED;

/// Create a CGImage at the next sample time from the list of tracks.
-(CGImageRef)_createNextSampleImage:(NSArray *)tracks CF_RETURNS_RETAINED;

/// Get a property of a track.
-(NSDictionary *)_getTrackProperty:(NSDictionary *)commandDict;

/// Get the simple properties of a track
-(NSDictionary *)_getTrackProperties:(NSDictionary *)commandDict;

/// Get minimum frame duration from tracks.
-(CMTime)_frameDurationFromTracks:(NSArray *)tracks;

/// Process the frames. Return the result as a MIReplyDictionary. Valid inputs.
-(NSDictionary *)_processFrames:(NSArray *)instructions
                        context:(MIContext *)context
                         tracks:(NSArray *)tracks
                imageIdentifier:(NSString *)identifier
   lastFrameAccessedDurationKey:(NSString *)lastFrameAccessedDurationKey;

@end


NSDictionary *CreateDictionaryFromMetadataItem(AVMetadataItem *item)
{
    NSMutableDictionary *itemDict = [NSMutableDictionary new];
    itemDict[@"keyspace"] = item.keySpace;
    // itemDict[@"identifier"] = item.identifier;
    itemDict[@"key"] = item.keyString;

    if (item.commonKey)
    {
        itemDict[@"commonkey"] = item.commonKey;
    }
    if (item.stringValue)
    {
        itemDict[@"stringValue"] = item.stringValue;
    }
    if (item.numberValue)
    {
        itemDict[@"numberValue"] = item.numberValue;
    }
    if (item.extendedLanguageTag)
    {
        itemDict[@"extendedLanguageTag"] = item.extendedLanguageTag;
    }
    if (item.dateValue)
    {
        itemDict[@"dateValue"] = item.dateValue.description;
    }
    return itemDict.copy;
}

#pragma mark -
#pragma mark MIMovieImporter Implementation

@implementation MIMovieImporter

#pragma mark Public Class MIBaseObjectSemiPublicInterface protocol methods

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    context = context ? context : [MIContext defaultContext];
    NSString *filePath = MIUtilityGetFilePathFromCommandAndVariables(commandDict,
                                                            context.variables);

    if (filePath)
    {
        NSString *objectName = commandDict[MIJSONKeyObjectName];
        
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        NSDictionary *options = @{
        AVURLAssetPreferPreciseDurationAndTimingKey: @YES,
        AVURLAssetReferenceRestrictionsKey:
                                @(AVAssetReferenceRestrictionForbidNone)
                                  };
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL
                                                    options:options];

        if (asset && asset.readable && asset.exportable && asset.composable)
        {
            MIMovieImporter *object;
            object = [[MIMovieImporter alloc] initWithAVURLAsset:asset
                                                            name:objectName
                                                       inContext:context];
            if (object)
            {
                MIBaseReference objectReference = [object reference];
                NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                       (long)objectReference];
                replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                          MIReplyErrorNoError,
                                                          @(objectReference));
            }
            else
            {
                MILog(@"Failed to create movie importer",
                                     commandDict);
                NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                         @"Error: Failed to movie create importer with path: ",
                         filePath];
                replyDict = MIMakeReplyDictionary(message,
                                                  MIReplyErrorOptionValueInvalid);
            }
        }
        else
        {
            if (asset)
            {
                MILog(@"Failed to create movie importer",
                                     commandDict);
                NSString *readable = asset.readable ? @"YES" : @"NO";
                NSString *exportable = asset.exportable ? @"YES" : @"NO";
                NSString *composable = asset.composable ? @"YES" : @"NO";
                NSString *string = [NSString stringWithFormat:
                        @"Readable: %@ - Exportable: %@ - Composable: %@",
                        readable, exportable, composable];
                MILog(string, nil);
                NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                        @"Error: Movie file with path not readable: ",
                        filePath];
                replyDict = MIMakeReplyDictionary(message,
                                            MIReplyErrorOptionValueInvalid);
            }
            else
            {
                MILog(@"Couldn't load movie file", commandDict);
                NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                        @"Error: Failed to create movie importer with path: ",
                        filePath];
                replyDict = MIMakeReplyDictionary(message,
                                            MIReplyErrorOptionValueInvalid);
            }
        }
    }
    else
    {
        MILog(@"Missing path to movie file or track dictionary",
                             commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MIMovieImporterKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    context = context ? context : [MIContext defaultContext];
    if (propertyKey == NULL)
    {
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingProperty);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieImportTypes])
    {
        NSArray *importTypes = [AVURLAsset audiovisualTypes];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                    importTypes);
        replyDict = MIMakeReplyDictionary(resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieImportMIMETypes])
    {
        NSArray *importTypes = [AVURLAsset audiovisualMIMETypes];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                importTypes);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieMediaCharacteristics])
    {
        NSArray *mc = @[
                     AVMediaCharacteristicAudible,
                     AVMediaCharacteristicContainsOnlyForcedSubtitles,
                     AVMediaCharacteristicDescribesMusicAndSoundForAccessibility,
                     AVMediaCharacteristicDescribesVideoForAccessibility,
                     AVMediaCharacteristicEasyToRead,
                     AVMediaCharacteristicFrameBased,
                     AVMediaCharacteristicIsAuxiliaryContent,
                     AVMediaCharacteristicIsMainProgramContent,
                     AVMediaCharacteristicLegible,
                     AVMediaCharacteristicTranscribesSpokenDialogForAccessibility,
                     AVMediaCharacteristicVisual ];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ", mc);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
        
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieMediaTypes])
    {
        NSArray *mt = @[
                        AVMediaTypeAudio,
                        AVMediaTypeClosedCaption,
                        AVMediaTypeMetadata,
                        AVMediaTypeMuxed,
                        AVMediaTypeSubtitle,
                        AVMediaTypeText,
                        AVMediaTypeTimecode,
                        AVMediaTypeVideo ];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ", mt);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
        
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MIMovieImporterKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(
                                      @"Error: MIMovieImporter unknown property",
                                      MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

#pragma mark Private method implementation

-(NSArray *)_assetVideoTracksFromTrackDictionaries:(NSArray *)trackDictionaries
{
    if (!trackDictionaries)
        return nil;

    NSArray *assetTracks;
    if (!([trackDictionaries isKindOfClass:[NSArray class]]))
    {
        MILog(@"Track dictionaries not an array.",
                             trackDictionaries);
        return nil;
    }

    NSMutableArray *temp = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSDictionary *trackD in trackDictionaries)
    {
        AVAssetTrack *track;
        track = MIAVGetTrackFromAVAssetWithDictionary(self._movieAsset, trackD);
        if (track && [[track mediaType] isEqualToString:AVMediaTypeVideo])
            [temp addObject:track];
    }
    if (temp.count)
    {
        assetTracks = temp.copy;
    }
    return assetTracks;
}

-(CGImageRef)_createImageFromTracks:(NSArray *)tracks atTime:(CMTime)time
{
    if (self._frameAccessor && [self._frameAccessor equalTracks:tracks])
    {
        if (CMTimeCompare(self._frameAccessor.currentTime, time) == 0)
        {
            MICMSampleBuffer *buffer = self._frameAccessor.currentBuffer;
            if (buffer)
            {
                return MIAVCreateCGImageFromSampleBuffer(buffer);
            }
        }
    }
    
    // If we don't already have a movie frame accessor, or the one we have
    // is broken, or it has a different list of tracks create one.
    if (!(self._frameAccessor && !self._frameAccessor.isBroken &&
          [self._frameAccessor equalTracks:tracks]))
    {
        self._frameAccessor = [[MIMovieVideoSampleAccessor alloc]
                               initWithMovie:self._movieAsset
                             firstSampleTime:time
                                      tracks:tracks
                               videoSettings:nil
                            videoComposition:nil];
    }
    
    if (!self._frameAccessor)
    {
        MILog(@"Could not create a movie frame sample accessor",
            CFBridgingRelease(CMTimeCopyAsDictionary(time, kCFAllocatorDefault)));
        return NULL;
    }

    MICMSampleBuffer *buffer = [self._frameAccessor sampleBufferAtTime:time];
    if (!buffer)
    {
        MILog(@"Failed to create sample buffer.",
            CFBridgingRelease(CMTimeCopyAsDictionary(time, kCFAllocatorDefault)));
        return NULL;
    }

    return MIAVCreateCGImageFromSampleBuffer(buffer);
}

-(CGImageRef)_createNextSampleImage:(NSArray *)tracks
{
    // If we don't already have a movie frame accessor, or the one we have
    // is broken, or it has a different list of tracks.
    BOOL makeFrameAccessor = NO;
    if (self._frameAccessor && self._frameAccessor.isBroken)
    {
        self._frameAccessor = nil;
    }

    if (!self._frameAccessor)
    {
        makeFrameAccessor = YES;
    }
    else if (tracks && ![self._frameAccessor equalTracks:tracks])
    {
        makeFrameAccessor = YES;
    }

    if (makeFrameAccessor)
    {
        // We assume that if we are creating a new sample accessor
        // and we are calling create next sample image, then we are starting
        // at the beginning.
        self._frameAccessor = [[MIMovieVideoSampleAccessor alloc]
                                   initWithMovie:self._movieAsset
                                 firstSampleTime:kCMTimeZero
                                          tracks:tracks
                                   videoSettings:nil
                                videoComposition:nil];
    }
    
    if (!self._frameAccessor)
    {
        MILog(@"Could not create a movie frame sample accessor",
                             nil);
        return NULL;
    }
    
    MICMSampleBuffer *buffer = [self._frameAccessor nextSampleBuffer];
    if (!buffer)
    {
        MILog(@"Failed to create sample buffer.",
                             nil);
        return NULL;
    }
/* Used to determine the actual frame duration which turns out to be 3002/90000
    CMTime presentationTime = CMSampleBufferGetPresentationTimeStamp(
                                                    buffer.CMSampleBuffer);
    NSDictionary *pT = CFBridgingRelease(CMTimeCopyAsDictionary(presentationTime,
                                                        kCFAllocatorDefault));
    MILog(@"Sample buffer presentation time: ", pT);
 */
    return MIAVCreateCGImageFromSampleBuffer(buffer);
}

-(NSInteger)_getNumberOfTracks:(NSDictionary *)commandDict
{
    NSInteger numTracks = -1;

    NSString *mediaType = commandDict[MIJSONPropertyMovieMediaType];
    NSArray *tracks = nil;
    
    if (mediaType)
    {
        tracks = [self._movieAsset tracksWithMediaType:mediaType];
    }
    
    if (!tracks)
    {
        NSString *mediaCharacteristic =
        commandDict[MIJSONPropertyMovieMediaCharacteristic];
        if (mediaCharacteristic)
        {
            tracks = [self._movieAsset
                      tracksWithMediaCharacteristic:mediaCharacteristic];
        }
        else
        {
            tracks = self._movieAsset.tracks;
        }
    }
    
    if (tracks)
    {
        numTracks = tracks.count;
    }
    return numTracks;
}

-(NSDictionary *)_getTrackProperties:(NSDictionary *)commandDict
{
    AVAssetTrack *track;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    track = MIAVGetTrackFromAVAssetWithDictionary(self._movieAsset, trackDict);
    if (!track)
    {
        MILog(@"Failed to get a track to get properties from",
                             commandDict);
        return MIMakeReplyDictionary(
                         @"Error: Failed to get a track to get properties from.",
                         MIReplyErrorOperationFailed);
    }
    NSDictionary *propertiesDict = MIAVGetTrackProperties(track, commandDict);
    // default return to reply dict;
    return MIUtilitySaveDictionaryToDestination(commandDict,
                                                         propertiesDict, NO);
}

-(NSDictionary *)_getTrackProperty:(NSDictionary *)commandDict
{
    AVAssetTrack *track;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    track = MIAVGetTrackFromAVAssetWithDictionary(self._movieAsset, trackDict);
    return MIAVGetMovieTrackProperty(track, commandDict);
}

-(CMTime)_frameDurationFromTracks:(NSArray *)tracks
{
    AVAssetTrack *first = tracks.firstObject;
    Float64 nominalRate = first.nominalFrameRate;
    for (AVAssetTrack *track in tracks)
    {
        Float64 tempRate = track.nominalFrameRate;
        if (tempRate > nominalRate)
        {
            nominalRate = tempRate;
        }
    }
    Float64 durationInSeconds = 1.0 / nominalRate;
    CMTime minDuration = CMTimeMakeWithSeconds(durationInSeconds, 9000);
    return minDuration;
}

-(NSDictionary *)_processFrames:(NSArray *)instructions
                        context:(MIContext *)context
                         tracks:(NSArray *)tracks
                imageIdentifier:(NSString *)identifier
   lastFrameAccessedDurationKey:(NSString *)lastFrameAccessedDurationKey
{
    NSDictionary *result;
    // We are expecting that the frame times are in chronological order.
    // So we want the time range over which to get the frames.
    
    // I'll do plenty of checking of types before I actually enter looping
    // through the frames, but once in the loop there'll be less checks.
    
    // There is no dispatch_sync, etc. needed for the process frames command
    // as the command makes it own AVAssetReader and AVAssetReaderOutput.
    
    NSString *message;
    NSDictionary *first = instructions.firstObject;
    
    if (![first isKindOfClass:[NSDictionary class]])
    {
        message = @"Error: First instruction isn't dictionaries";
        MILog(message, first);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidProperty);
        return result;
    }

    NSDictionary *timeDict = first[MIJSONPropertyMovieFrameTime];
    
    CMTime firstTime = kCMTimeInvalid;
    if (timeDict)
    {
        if ([timeDict isKindOfClass:[NSDictionary class]])
        {
            if (!MIAVMakeACMTimeFromDictionary(context, timeDict,
                                               6000, &firstTime))
            {
                message = @"Error: Invalid frame time dictionary in first object";
                MILog(message, first);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorInvalidProperty);
                return result;
            }
        }
        else if ([timeDict isKindOfClass:[NSString class]])
        {
            NSString *nextString = (NSString *)timeDict;
            if ([nextString isEqualToString:MIJSONValueMovieNextSample])
            {
                firstTime = kCMTimeZero;
            }
        }
    }
    else
    {
        firstTime = kCMTimeZero;
    }
    
    if (CMTIME_IS_INVALID(firstTime))
    {
        message = @"Error: Start time not specified.";
        MILog(message, first);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidProperty);
        return result;
    }

    // Float64 durationInSeconds = CMTimeGetSeconds(duration);
    MIMovieVideoSampleAccessor *sampleAccessor;
    sampleAccessor = [[MIMovieVideoSampleAccessor alloc]
                                            initWithMovie:self._movieAsset
                                          firstSampleTime:firstTime
                                                   tracks:tracks
                                            videoSettings:nil
                                         videoComposition:nil];
    
    if (!sampleAccessor)
    {
        message = @"Error: Could not create the sample buffer accessor.";
        MILog(message, first);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return result;
    }
    CMTime frameTime;
    for (NSDictionary *frameDict in instructions)
    {
        @autoreleasepool {
            NSArray *commands = frameDict[MIJSONKeyCommands];
            if (!(commands && [commands isKindOfClass:[NSArray class]]))
            {
                message = @"Error: No commands for applying to the image.";
                MILog(message, frameDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorMissingOption);
                break;
            }
            
            NSDictionary *timeDict = frameDict[MIJSONPropertyMovieFrameTime];
            BOOL nextFrame = NO;
            
            if (timeDict)
            {
                if ([timeDict isKindOfClass:[NSDictionary class]])
                {
                    if (!MIAVMakeACMTimeFromDictionary(self.miContext, timeDict,
                                                       6000, &frameTime))
                    {
                        message = @"Error: Invalid time specified.";
                        MILog(message, timeDict);
                        result = MIMakeReplyDictionary(message,
                                                MIReplyErrorInvalidOption);
                        break;
                    }
                }
                else
                {
                    NSString *timeString = (NSString *)timeDict;
                    if ([timeString isKindOfClass:[NSString class]] &&
                         [timeString isEqualToString:MIJSONValueMovieNextSample])
                    {
                        nextFrame = YES;
                    }
                    else
                    {
                        message = @"Error: Invalid time specification.";
                        MILog(message, timeDict);
                        result = MIMakeReplyDictionary(message,
                                                       MIReplyErrorInvalidOption);
                        break;
                    }
                }
            }
            else
            {
                message = @"Error: Missing frame time dictionary.";
                MILog(message, frameDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorInvalidOption);
                break;
            }
            MICMSampleBuffer *buffer;
            if (nextFrame)
            {
                buffer = [sampleAccessor nextSampleBuffer];
            }
            else
            {
                buffer = [sampleAccessor sampleBufferAtTime:frameTime];
            }

            if (!buffer)
            {
                message = @"Failed to get a CMSampleBuffer from reader";
                MILog(message, frameDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorOperationFailed);
                break;
            }

            NSDictionary *varDict;
            if (lastFrameAccessedDurationKey)
            {
                CMTime frameDuration;
                frameDuration = CMSampleBufferGetDuration(buffer.CMSampleBuffer);
                if (CMTIME_IS_VALID(frameDuration))
                {
                    NSDictionary *durDict;
                    durDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                            frameDuration, kCFAllocatorDefault));
                    varDict = @{ lastFrameAccessedDurationKey : durDict };
                    [context appendVariables:varDict];
                }
            }

            NSString *localIdent = frameDict[MIJSONPropertyImageIdentifier];
            localIdent = localIdent ? localIdent : identifier;

            if (!localIdent)
            {
                if (varDict)
                {
                    [context dropVariablesDictionary:varDict];
                }
                message = @"Error: No image identifier specified.";
                MILog(message, frameDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorOperationFailed);
                break;
            }

            CGImageRef theImage = MIAVCreateCGImageFromSampleBuffer(buffer);
            if (theImage)
            {
                [context assignCGImage:theImage identifier:localIdent];
                CGImageRelease(theImage);
                // Now on to perform the commands. ========================
                NSDictionary *replyDict;
                replyDict = MIPerformCommands(context, commands);
                // ========================================================
                if (MIGetErrorCodeFromReplyDictionary(replyDict) !=
                                                        MIReplyErrorNoError)
                {
                    result = replyDict;
                    break;
                }
            }
            else
            {
                message = @"Error: Could not make a CGImage.";
                MILog(message, frameDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorOperationFailed);
            }
            if (varDict)
            {
                [context dropVariablesDictionary:varDict];
            }
        }
    }

    if (!result)
    {
        result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    return result;
}

#pragma mark Private property implementation

-(NSString *)_moviePath
{
    return [self._movieAsset.URL path];
}

#pragma mark Public methods

-(instancetype)initWithAVURLAsset:(AVURLAsset *)avAsset
                             name:(NSString *)theName
                     	inContext:(MIContext *)context
{
    if (!avAsset)
    {
        self = nil;
        return self;
    }

    if (!theName || theName.length == 0)
    {
        theName = [avAsset.URL path];
    }
    
    self = [super initWithBaseObjectType:MIMovieImporterKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        dispatch_queue_t defaultPriorityQueue;
        defaultPriorityQueue = dispatch_get_global_queue(
                                            DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_queue_t myQueue;
        NSString *queueName = [NSString stringWithFormat:
                               @"com.zukini.mimovieimporter.%p", self];
        myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(myQueue, defaultPriorityQueue);
        self->__sampleQueue = myQueue;

        self._movieAsset = avAsset;
    }
    
    return self;
}

-(void)close
{
    [super baseObjectClose];
}

#pragma mark Public MIBaseObjectSemiPublicInterface protocol methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }
    
    if (![propertyType isKindOfClass:[NSString class]])
    {
        MILog(@"Property type is not a string.", commandDict);
        return MIMakeReplyDictionary(@"Error: Property type is not a string",
                                     MIReplyErrorMissingProperty);
    }

    // First check to see if we are requesting a property of a track.
    // If there is a property of the command dict that is a track specification
    // then assume we are wanting a track property.
    
    if (commandDict[MIJSONPropertyMovieTrack])
    {
        return [self _getTrackProperty:commandDict];
    }

    NSDictionary *resultsDict;
    resultsDict = [super handleCommonGetPropertyCommand:commandDict];
    if (resultsDict)
    {
        return resultsDict;
    }

    if ([propertyType isEqualToString:MIJSONPropertyMovieCurrentTime])
    {
        CMTime currentTime = kCMTimeZero;
        if (self._frameAccessor && !self._frameAccessor.isBroken)
        {
            currentTime = self._frameAccessor.currentTime;
        }
        NSDictionary *cTDict;
        cTDict = CFBridgingRelease(CMTimeCopyAsDictionary(currentTime,
                                                          kCFAllocatorDefault));
        NSString *stringRep = MIUtilityCreateStringRepresentationOfObject(cTDict,
                                                                          0);
        NSNumber *currentTimeInSecs = @(CMTimeGetSeconds(currentTime));
        resultsDict = MIMakeReplyDictionaryWithNumericValue(stringRep,
                                        MIReplyErrorNoError, currentTimeInSecs);
    }
    else
    {
        resultsDict = MIAVGetMovieAssetProperty(self._movieAsset, commandDict);
    }

    return resultsDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }

    // First check to see if we are requesting a property of a track.
    // If there is a property of the command dict that is a track specification
    // then assume we are wanting a track property.
    
    if (commandDict[MIJSONPropertyMovieTrack])
    {
        return [self _getTrackProperties:commandDict];
    }

    NSDictionary *propDict;
    propDict = MIAVGetMovieAssetProperties(self, self._movieAsset, commandDict);
    replyDict = MIUtilitySaveDictionaryToDestination(commandDict, propDict, NO);
    return replyDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleProcessFramesCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    
    // Get the various different command lists & at least confirm they're arrays.
    NSArray *instructions = commandDict[MIJSONPropertyMovieProcessInstructions];
    if (!(instructions && [instructions isKindOfClass:[NSArray class]]))
    {
        NSString *message;
        message = @"Error: frame processing instructions missing or wrong type";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }

    NSArray *preProcessCommands = commandDict[MIJSONPropertyMoviePreProcess];
    NSArray *postProcessCommands = commandDict[MIJSONPropertyMoviePostProcess];
    if ((preProcessCommands &&
                    ![preProcessCommands isKindOfClass:[NSArray class]]) ||
        (postProcessCommands &&
                    ![postProcessCommands isKindOfClass:[NSArray class]]))
    {
        NSString *message;
        message = @"Error: Pre or post processing commands object is not an array";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }

    NSArray *cleanupCommands = commandDict[MIJSONKeyCleanupCommands];
    if (cleanupCommands && ![cleanupCommands isKindOfClass:[NSArray class]])
    {
        NSString *message;
        message = @"Error: Cleanup commands object is not an array";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }

    // Determine the context the commands should be run in.
    BOOL localContext = MIUtilityGetBOOLPropertyWithDefault(commandDict,
                                        MIJSONPropertyMovieLocalContext, NO);
    MIContext *theContext = localContext ? [MIContext new] : self.miContext;

    // Get the tracks that the image should be generated from.
    NSArray *tracksD = commandDict[MIJSONPropertyMovieTracks];
    NSArray *tracks;
    if (tracksD)
    {
        if ([tracksD isKindOfClass:[NSArray class]])
        {
            tracks = [self _assetVideoTracksFromTrackDictionaries:tracksD];
        }
        else
        {
            NSString *message = @"Error: Track list is not an array";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
            return result;
        }
    }
    else
    {
        tracks = [self._movieAsset tracksWithMediaType:AVMediaTypeVideo];
    }

    if (!(tracks && tracks.count))
    {
        NSString *message = @"Error: No tracks to create image frame from";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }

    //===============================================================
    // From this point on we can't bail if an error occurs.
    // Basic checks that the inputs are correct have been done.
    NSString *message;

    MIReplyErrorEnum error = MIReplyErrorNoError;

    if (preProcessCommands)
    {
        result = MIPerformCommands(theContext, preProcessCommands);
        error = MIGetErrorCodeFromReplyDictionary(result);
        if (error != MIReplyErrorNoError)
        {
            message = @"Performing pre processing commands failed.";
            MILog(message, preProcessCommands);
        }
    }
    
    // Do the actual processing of the movie frames.
    if (error == MIReplyErrorNoError)
    {
        NSString *frameDurKey;
        frameDurKey = commandDict[MIJSONPropertyMovieLastAccessedFrameDurationKey];
        result = [self _processFrames:instructions
                              context:theContext
                               tracks:tracks
                      imageIdentifier:commandDict[MIJSONPropertyImageIdentifier]
         lastFrameAccessedDurationKey:frameDurKey];
        if (result)
        {
            error = MIGetErrorCodeFromReplyDictionary(result);
            if (error != MIReplyErrorNoError)
            {
                message = @"Performing frame processing instructions failed";
                MILog(message, instructions);
            }
        }
    }
    
    // Now perform the post processing if there has not been an error.
    if (error == MIReplyErrorNoError && postProcessCommands)
    {
        result = MIPerformCommands(theContext, postProcessCommands);
        error = MIGetErrorCodeFromReplyDictionary(result);
        if (error != MIReplyErrorNoError)
        {
            message = @"Error: Performing post processing commands failed";
            MILog(message, postProcessCommands);
        }
    }
    
    if (cleanupCommands)
    {
        MIPerformCleanupCommands(theContext, cleanupCommands);
    }
    return result;
}

-(NSDictionary *)handleAssignImageToCollectionCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *ident = commandDict[MIJSONPropertyImageIdentifier];
    NSDictionary *options = commandDict[MIJSONKeyImageOptions];
    if (options && ![options isKindOfClass:[NSDictionary class]])
    {
        NSString *message = @"Error: Image creation options should be a dict";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    else if (ident && options)
    {
        
        CGImageRef cgImage = [self createCGImageWithOptions:options];
        MIContext *context = self.miContext;
        if (cgImage && context)
        {
            MICGImage *image = [[MICGImage alloc] initWithCGImage:cgImage];
            [context assignImage:image identifer:ident];
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            message = cgImage ? @"Error: No context" : @"Error: No image";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }

        CGImageRelease(cgImage);
    }
    else
    {
        NSString *message = @"Error: Assign image command dict missing values.";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return result;
}

#pragma mark Implementing MICreateCGImageInterface protocol methods.

-(CGImageRef)createCGImageWithOptions:(NSDictionary *)options
{
    __block CGImageRef image = nil;
    if (options)
    {
        NSDictionary *fTD = options[MIJSONPropertyMovieFrameTime];
        NSArray *tracksAD = options[MIJSONPropertyMovieTracks];
        NSArray *tracks;
        if (tracksAD)
        {
            tracks = [self _assetVideoTracksFromTrackDictionaries:tracksAD];
            if (!(tracks && tracks.count))
            {
                MILog(
                @"Array of track dictionaries resulted in no tracks", options);
                return nil;
            }
        }

        if (!tracks)
        {
            tracks = [self._movieAsset tracksWithMediaType:AVMediaTypeVideo];
            if (!(tracks && tracks.count))
            {
                MILog(@"No video tracks in movie asset.", nil);
                return nil;
            }
        }

        if (fTD)
        {
            if ([fTD isKindOfClass:[NSDictionary class]])
            {
                CMTime frameTime;
                if (MIAVMakeACMTimeFromDictionary(self.miContext, fTD,
                                                  9000, &frameTime))
                {
                    dispatch_sync(self._sampleQueue, ^{
                        image = [self _createImageFromTracks:tracks
                                                      atTime:frameTime];
                    });
                }
            }
            else if ([fTD isKindOfClass:[NSString class]])
            {
                NSString *timeString = (NSString *)fTD;
                if ([timeString isEqualToString:MIJSONValueMovieNextSample])
                {
                    dispatch_sync(self._sampleQueue, ^{
                        image = [self _createNextSampleImage:tracks];
                    });
                }
            }
        }
    }
    //    SaveCGImageToAPNGFile(image, @"time08.png");
    if (!image)
    {
        MILog(@"Couldn't create movie frame image.", options);
    }
    return image;
}

-(void)passMICGImageTo:(MICGImageWrapper *)imageWrapper
          usingOptions:(NSDictionary *)options
{
    CGImageRef cgImage = [self createCGImageWithOptions:options];
    if (cgImage)
    {
        MICGImage *image = [[MICGImage alloc] initWithCGImage:cgImage];
        CGImageRelease(cgImage);
        [imageWrapper setMICGImageStrong:image];
    }
}

#pragma mark Implementing MIAVAssetTrackProviderInterface protocol methods

-(AVAssetTrack *)trackWithIdentifier:(NSDictionary *)trackIdentifier
{
    return MIAVGetTrackFromAVAssetWithDictionary(self._movieAsset,
                                                 trackIdentifier);
}

@end
