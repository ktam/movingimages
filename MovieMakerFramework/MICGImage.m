//  MICGImage.m
//  MovieMaker
//
//  Copyright (c) 2014  Kevin Meaney. All rights reserved.

#import "MICGImage.h"

@implementation MICGImage
{
    CGImageRef _ownedImage;
}

#pragma mark MICGImage initialization, copying and dealloc methods

-(instancetype)initWithMICGImage:(MICGImage *)image
{
    self = [super init];
    if (self)
    {
        self->_ownedImage = CGImageRetain(image.CGImage);
    }
    return self;
}

-(instancetype)initWithCGImage:(CGImageRef)image
{
    self = [super init];
    if (self)
    {
        self->_ownedImage = CGImageRetain(image);
    }
    return self;
}

-(instancetype)copy
{
    return [[MICGImage alloc] initWithMICGImage:self];
}

-(void)dealloc
{
    CGImageRelease(self->_ownedImage);
}

# pragma mark Manual property implementation

-(CGImageRef)CGImage
{
    return self->_ownedImage;
}

#pragma mark Conforming to NSCopying protocol methods

-(instancetype)copyWithZone:(NSZone *)zone
{
    return [self copy];
}

@end
