//  MISafeMutableArray.h
//  MovieMaker
//
//  Copyright (c) 2014 Kevin Meaney.
// MIT license.
// Written after reading the dispatch barrier section by Mike Ash.
// Perhaps over the top for what it does, but I'm not sure of any other way
// to get the same results.
// https://www.mikeash.com/pyblog/friday-qa-2011-10-14-whats-new-in-gcd.html

#import <Foundation/Foundation.h>

typedef  BOOL (^ArrayObjectPredicate)(id, NSUInteger, BOOL *stop);

@interface MISafeMutableArray : NSObject

// The queue name must be convertible to ASCII encoding.
-(instancetype)initWithQueueName:(NSString *)queueName;
-(id)objectAtIndex:(NSInteger)index;
-(void)removeObject:(id)object;
-(void)removeObjectAtIndex:(NSInteger)index;
-(void)addObject:(id)object;
-(NSInteger)count;
-(void)removeAllObjects;
-(id)firstObjectPassingTest:(ArrayObjectPredicate)predicate;
-(NSArray *)immutableArray;

@end
