//  MICreateCGImageInterface.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
@import CoreGraphics;

@class MICGImageWrapper;

/**
 @brief Proptocol MICreateCGImageInterface
 @discussion Three different types of base objects conform to this protocol.
 Objects of type MICGBitmapContext and MINSGraphicsContext don't
 have a image index, when the methods which conform to the
 MICreateCGImageInterface protocol on these objects are called the index value
 is ignored.
*/

@protocol MICreateCGImageInterface <NSObject>

/// Create a cgimage using the properties of the options dictionary.
-(CGImageRef)createCGImageWithOptions:(NSDictionary *)options CF_RETURNS_RETAINED;

/// Create a cgimage if necessary and then pass it to the image wrapper.
-(void)passMICGImageTo:(MICGImageWrapper *)imageWrapper
          usingOptions:(NSDictionary *)options;

@end
