//  MIAVFoundationUtilities.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
@import AVFoundation;

@class MIContext;
@class MICGImage;
@class MICMSampleBuffer;
@class MIBaseObject;

/**
 @brief Get a track from an AVAsset using dictionary properties.
 @discussion The trackIdent dictionary should contain properties that identifies
 the dictionary. The dictionary can contain a persistent track id property
 or a track index, optionally with a media type or characteristic.
 @param asset The movie asset to get the track from.
 @param trackIdent A dictionary with properties identifying a track.
 @result Returns a AVAssetTrack object or nil.
*/
AVAssetTrack *MIAVGetTrackFromAVAssetWithDictionary(AVAsset *asset,
                                                    NSDictionary *trackIdent);

/**
 @brief Creates a dictionary of AVFoundation mapping of time ranges from a
 source to a target time range. AVFoundation provides CMCopyTimeRangeAsDictionary
 function but doesn't supply a time mapping version of the copy as dictionary.
 This fixes that gap.
*/
NSDictionary *MIAVCopyTimeMappingAsDictionary(CMTimeMapping mapping);

/**
 @brief Take a dictionary and convert it to a CMTime struct.
 @param movieTime   Dictionary containing CMTime properties or single "time" prop
 @param defaultTimeScale    used if movieTime has only the "time" property.
 @param time    A pointer to a CMTime struct that the result is put in.
 @result    Returns YES on success otherwise NO.
*/
BOOL MIAVMakeACMTimeFromDictionary(MIContext *context, NSDictionary *movieTime,
                                   CMTimeScale defaultTimeScale, CMTime *time);

/**
 @brief Take a dictionary and convert it to a CMTimeRange struct.
 @param timeRangeDict   Dictionary containing a time and a duration dictionary.
 @param defaultTimeScale    used if time or duration uses "time" property.
 @param timeRange    A pointer to a CMTimeRange struct that the result is put in.
 @result    Returns YES on success otherwise NO.
 */
BOOL MIAVMakeACMTimeRangeFromDictionary(MIContext *context,
                                   NSDictionary *timeRangeDict,
                                   CMTimeScale defaultScale,
                                   CMTimeRange *timeRange);
/**
 @brief Create a CGImage from a CVPixelBuffer.
 @discussion This function will attempt to create a CGImage from the pixel
 buffer but will fail if the pixel format is not ARGB32 or that the pixel buffer
 base address couldn't be locked.
 @param pixBuffer The pixel buffer to create the image from.
 @result    Returns a CGImage if successful otherwise nil.
*/
CGImageRef MIAVCreateCGImageFromPixelBuffer(CVPixelBufferRef pixBuffer)
                                                            CF_RETURNS_RETAINED;

/**
 @brief Create a CGImage from a MICMSampleBuffer.
 @discussion This function will attempt to create a CGImage from the sample
 buffer but will fail if we couldn't get a image buffer from the sampel buffer.
 Calls MIAVCreateCGImageFromPixelBuffer which can also fail.
 @param pixBuffer The pixel buffer to create the image from.
 @result    Returns a CGImage if successful otherwise nil.
*/
CGImageRef MIAVCreateCGImageFromSampleBuffer(MICMSampleBuffer *buffer)
                                                            CF_RETURNS_RETAINED;

/**
 @brief Get property specified in the command dictionary and return in reply dict.
 @discussion Get the desired property from the movie track and return in a
 reply dictionary. If the property couldn't be returned then the reply dicts
 error value is non zero and reply string contains a short error description.
*/
NSDictionary *MIAVGetMovieTrackProperty(AVAssetTrack *track,
                                        NSDictionary *commandDict);

/**
 @brief Get the number of tracks from a movie asset.
 @discussion This function determines the number of tracks in a movie asset
 depending on properties of the command dictionary like media type or
 media characteristic which are used to filter the list of all tracks in
 in the movie asset.
*/
NSInteger MIAVGetNumberOfTracks(AVAsset *movieAsset, NSDictionary *commandDict);

/// Get properties of a movie track and return them as specified in commandDict
NSDictionary *MIAVGetTrackProperties(AVAssetTrack *track,
                                     NSDictionary *commandDict);

/// Get the properties of a movie and return them as specified in commandDict
NSDictionary *MIAVGetMovieAssetProperties(MIBaseObject *obj, AVAsset *movie,
                                          NSDictionary *commandDict);

/// Get a property of a movie and return it as a reply dictionary.
NSDictionary *MIAVGetMovieAssetProperty(AVAsset *movie,
                                                    NSDictionary *commandDict);

/**
 @brief Draw a cgimage into a pixel buffer.
 @discussion This function will only draw an image which has the same dimensions
 as the pixel buffer, it won't try to resize, crop or pad. The pixel buffer is
 assumed to have 32 bit pixels. 4 components of 8 bits each in ARGB/BGRA order.
*/
BOOL MIAVDrawCGImageIntoPixelBuffer(MICGImage *image, CVPixelBufferRef pixelBuf);

/// Create a CGImage with dimensions 800x600 of a AV composition map.
CGImageRef MIAVCreateImageOfCompositionMap(AVComposition *composition,
                                           AVVideoComposition *videoComposition,
                                           AVAudioMix *audioMix);

/// Save the composition layout as a png file.
void MIAVSaveCompositionMapToAPNGFile(AVComposition *composition,
                                      AVVideoComposition *videoComposition,
                                      AVAudioMix *audioMix,
                                 	  NSString *filePath);
