//  MIBaseObject_Protected.h
//  MovieMaker
//
//  Copyright (c) 2014 Zukini Ltd.

#import "MIBaseObject.h"

@interface MIBaseObject ()

/**
 @brief Handles getting a property that is common to all base objects.
 @discussion The handle getproperty command will pass the property request
 to this base object method to see if it can handle the property request. Unlike
 the handlers in the derived classes this one will return nil if it hasn't
 handled the request.
 */
-(NSDictionary *)handleCommonGetPropertyCommand:(NSDictionary *)command;

@end
