//  MIFilterInputImageHandler.h
//  Moving Images
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

@class MICGImageWrapper;
@class CIFilter;

@interface MIFilterInputImageHandler : NSObject

/// Force the image to be held as a strong reference or not.
@property (assign) BOOL makeCopy; // Clears old image. New image will be a copy

/**
 @brief Initialize a MIFilterInputImageHandler object. Designated.
 @discussion This object manages one of the images needed by a filter.
 The imageWrapper object shouldn't initially hold the CGImage but will request
 it when needed. If the filter is used repeatedly then the imageWrapper object
 knows if the image needs to obtained again or not, and if so then a new image
 needs to assigned to the filter object.
 @param filter  The filter object that will have the image applied to it.
 @param filterImageInputKey Key used to assign the image to the filter object.
 @param imageWrapper    The object managing the image to be assigned to filter.
*/
-(instancetype)initWithCIFilter:(CIFilter *)filter
            filterInputImageKey:(NSString *)filterImageInputKey
                   imageWrapper:(MICGImageWrapper *)imageWrapper;

/**
 @brief Initialize a MIFilterInputImageHandler object. Designated.
 @discussion This object manages one of the images needed by a filter.
 The imageWrapper object shouldn't initially hold the CGImage but will request
 it when needed. If the filter is used repeatedly then the imageWrapper object
 knows if the image needs to obtained again or not, and if so then a new image
 needs to assigned to the filter object.
 @param filter  The filter object that will have the image applied to it.
 @param filterImageInputKey Key used to assign the image to the filter object.
 @param inputFilter The filter to pull the CIImage from as input for filter.
 */
-(instancetype)initWithCIFilter:(CIFilter *)filter
            filterInputImageKey:(NSString *)filterImageInputKey
                    inputFilter:(CIFilter *)inputFilter;

/**
 @brief If the image the filter holds is stale or not applied then apply image.
 @discussion If the _inputImage object already holds an image, then that means
 that the image has already been applied to the filter chain. If however it
 doesn't hold an image then, it should get the image and then apply it to the
 filter chain. QUESTION: Does a CIImage hold a strong reference to a CGImage.
 I don't think it does, there is no comment about it in the documentation.
 Also the MICGImage object doesn't care whether the CGImage has has been
 retained by some other object, it will still follow arc rules. This design
 should not be leaking CGImages even if a CIImage holds a strong reference
 because when the CIImage object is destroyed it should release the CGImage.
*/
-(BOOL)applyImageToFilterIfNecessary;

/// If we are holding onto an image then clear it. Also clears weak reference.
-(void)clearImage;

/// Returns YES if this handler manages the source image for the filter and key.
-(BOOL)managesImageForFilter:(CIFilter *)theFilter key:(NSString *)theKey;

@end
