//  MIMovieVideoFramesWriter.m
//  MovingImagesFramework
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIAVFoundationUtilities.h"
#import "MIBaseObject_Protected.h"
#import "MICGImage.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIHandleCommands.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIMovieVideoFramesWriter.h"
#import "MIUtilities.h"

@import AVFoundation;

#pragma mark CLASS MIMovieVideoFramesWriter Private Interface

@interface MIAVAssetWriterInputState : NSObject

@property (strong) AVAssetWriterInputPixelBufferAdaptor *adaptor;
@property (assign) CGSize size; // The dimensions of the video track created.
@property (assign) CMTime currentTime; // Next frame sample will be added.
@property (assign) CMTime duration; // frame duration time.

@end

@implementation MIAVAssetWriterInputState

@end


@interface MIMovieVideoFramesWriter ()

@property (strong) AVAssetWriter *_videoFrameWriter;

// An array of MIAVAssetWriterInputState objects.
// Currently only allow one AVAssetWriterInput, and only one pixel buffer adaptor.
@property (strong) NSMutableArray *_writerInputsState;

// All access and mutation goes through this queue.
@property (strong) dispatch_queue_t _writerQueue;

@property (readonly) BOOL _canWrite;
@property (readonly) BOOL _canNoLongerAddFrames;
@property (assign) BOOL _hasStarted;
@property (assign) BOOL _allDone;
@property (assign) BOOL _hasFinalizeBeenCalled; // Has finalized been called.

// Private designated initializer.
-(instancetype)initWithAssetWriter:(AVAssetWriter *)assetWriter
                       writerQueue:(dispatch_queue_t)writerQueue
                              name:(NSString *)theName
                         inContext:(MIContext *)context;

-(NSDictionary *)_addInputToMovieFrameWriter:(NSDictionary *)commandDict;
-(NSDictionary *)_ensureWriterCanAddImages:(NSDictionary *)commandDict;
-(NSDictionary *)_addImageSampleToWriter:(NSDictionary *)commandDict;
-(void)_finishWritingFrames;

// The following two properties are for manage waiting for the video input
// to be read for more data.
@property (assign) BOOL _isWaitingForInputReady;
@property (strong) dispatch_semaphore_t _writeSemaphore;
-(void)_waitTillReadyForMediaData;

@end

#pragma mark Local Helper functions

static dispatch_queue_t CreateVideoFramesWriterQueue(NSString *queueName)
{
    dispatch_queue_t defaultQueue;
    defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                             0);
    dispatch_queue_t myQueue;
    myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
    dispatch_set_target_queue(myQueue, defaultQueue);
    return myQueue;
}



/*
 The video settings listed here were selected after suffering a lot of pain
 with combinations of settings that just didn't work. This function returns a
 dictionary of dictionaries.
*/
static NSDictionary *PresetDictionaries()
{
    static NSDictionary *presetsDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
        presetsDict =
        @{
            MIJSONValueMovieVideoWriterPresetH264_HD :
            @{
                AVVideoCodecKey : AVVideoCodecH264,
                AVVideoCompressionPropertiesKey : @{
                    AVVideoAllowFrameReorderingKey : @1,
                    AVVideoExpectedSourceFrameRateKey : @30,
                    AVVideoAverageBitRateKey : @15585760,
                    AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
                    AVVideoMaxKeyFrameIntervalKey : @30,
                    AVVideoProfileLevelKey : AVVideoProfileLevelH264HighAutoLevel
                }
            },
            MIJSONValueMovieVideoWriterPresetH264_SD :
            @{
                AVVideoCodecKey : AVVideoCodecH264,
                AVVideoCompressionPropertiesKey : @{
//                  AVVideoAllowFrameReorderingKey : @1,
                    AVVideoExpectedSourceFrameRateKey : @30,
                    AVVideoAverageBitRateKey : @3145728,
                    AVVideoMaxKeyFrameIntervalKey : @30,
                    AVVideoProfileLevelKey :
                                        AVVideoProfileLevelH264BaselineAutoLevel
                }
            },
            MIJSONValueMovieVideoWriterPresetJPEG :
            @{
                AVVideoCodecKey : AVVideoCodecJPEG,
                AVVideoCompressionPropertiesKey : @{
                    AVVideoQualityKey : @0.85
                }
            },
#if !TARGET_OS_IPHONE
            MIJSONValueMovieVideoWriterPresetProRes4444 :
            @{
                AVVideoCodecKey : AVVideoCodecAppleProRes4444,
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspect,
                  
                AVVideoColorPropertiesKey : @{
                    AVVideoColorPrimariesKey : AVVideoColorPrimaries_ITU_R_709_2,
                    AVVideoTransferFunctionKey :
                                        AVVideoTransferFunction_ITU_R_709_2,
                    AVVideoYCbCrMatrixKey : AVVideoYCbCrMatrix_ITU_R_709_2
                }
            },
            MIJSONValueMovieVideoWriterPresetProRes422 :
            @{
                AVVideoScalingModeKey : AVVideoScalingModeResizeAspect,
                AVVideoColorPropertiesKey : @{
                    AVVideoColorPrimariesKey : AVVideoColorPrimaries_ITU_R_709_2,
                    AVVideoTransferFunctionKey :
                                            AVVideoTransferFunction_ITU_R_709_2,
                    AVVideoYCbCrMatrixKey : AVVideoYCbCrMatrix_ITU_R_709_2
                },
                AVVideoCodecKey : AVVideoCodecAppleProRes422
            }
#endif
        };
    });
    return presetsDict;
}

static NSArray *PresetList()
{
    NSDictionary *presets = PresetDictionaries();
    return presets.allKeys;
}

static NSDictionary *DictionaryFromPreset(NSString *preset)
{
    NSDictionary *presetsDict = PresetDictionaries();
    return presetsDict[preset];
}

NSDictionary *CopyVideoSettingsFromPreset(NSString *preset,
                                          size_t width, size_t height,
                                          NSDictionary *cleanCapture,
                                          NSString *scalingMode)
{
    NSDictionary *settings = DictionaryFromPreset(preset);
    if (!settings)
    {
        return settings;
    }

    NSMutableDictionary *mutableSettings = settings.mutableCopy;
    mutableSettings[AVVideoWidthKey] = @(width);
    mutableSettings[AVVideoHeightKey] = @(height);
    if (cleanCapture && [cleanCapture isKindOfClass:[NSDictionary class]])
    {
        mutableSettings[AVVideoCleanApertureKey] = cleanCapture;
    }
    
    if (scalingMode && [scalingMode isKindOfClass:[NSString class]])
    {
        mutableSettings[AVVideoScalingModeKey] = scalingMode;
    }

    return mutableSettings.copy;
}

NSDictionary *PixelBufferAttributes(size_t width, size_t height)
{
    size_t bytesPerRow = width * 4;
    if (bytesPerRow % 16)
    {
        bytesPerRow += 16 - bytesPerRow % 16;
    }
    return @{
             (id)kCVPixelBufferCGImageCompatibilityKey : @YES,
             (id)kCVPixelBufferCGBitmapContextCompatibilityKey : @YES,
#if TARGET_OS_IPHONE
             (id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32BGRA),
#else
             (id)kCVPixelBufferPixelFormatTypeKey:@(kCVPixelFormatType_32ARGB),
#endif
             (id)kCVPixelBufferWidthKey : @(width),
             (id)kCVPixelBufferHeightKey : @(height),
             (id)kCVPixelBufferBytesPerRowAlignmentKey : @(bytesPerRow)
            };
}

AVAssetWriterInputPixelBufferAdaptor *CreateAdaptor(AVAssetWriterInput *input,
                                                    size_t width,
                                                    size_t height)
{
    NSDictionary *pixelBufferAttributes = PixelBufferAttributes(width, height);

    AVAssetWriterInputPixelBufferAdaptor *adaptor;
    adaptor = [[AVAssetWriterInputPixelBufferAdaptor alloc]
                  initWithAssetWriterInput:input
               sourcePixelBufferAttributes:pixelBufferAttributes];
    return adaptor;
}

@implementation MIMovieVideoFramesWriter

#pragma mark Public Class methods for Protocol MIBaseObjectSemiPublicInterface

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    // context = context ? context : [MIContext defaultContext];
    if (propertyKey == nil)
    {
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingProperty);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieExportTypes])
    {
        NSString *resultStr = [[NSString alloc] initWithFormat:@"%@ %@ %@",
                               AVFileTypeQuickTimeMovie, AVFileTypeMPEG4,
                               AVFileTypeAppleM4V];
        replyDict = MIMakeReplyDictionary(resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieVideoWriterPresets])
    {
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                PresetList());
        replyDict = MIMakeReplyDictionary(resultStr, 0); /* **success** */
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(
                            @"Error: MIMovieVideoFramesWriter unknown property",
                            MIReplyErrorOptionValueInvalid);
    }
    
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict;
    NSString *message;
    NSString *objectName = commandDict[MIJSONKeyObjectName];
    context = context ? context : [MIContext defaultContext];
    NSString *filePath = MIUtilityGetFilePathFromCommandAndVariables(commandDict,
                                                              context.variables);
    
    if (!filePath)
    {
        message = @"Error: Missing file path for video frames writer";
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return replyDict;
    }

    NSString *exportFileType = commandDict[MIJSONPropertyFileType];
    if (!exportFileType)
    {
        message = @"Error: Missing export file type for video frames writer";
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return replyDict;
    }
    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    // Make sure directory that the video file is to be saved to exists.
    
    NSString *dirPath = [filePath stringByDeletingLastPathComponent];
    BOOL isDir;
    if (![fm fileExistsAtPath:dirPath isDirectory:&isDir])
    {
        message = [[NSString alloc] initWithFormat:
                   @"Error: Folder to save video doesn't exists: %@", dirPath];
    }
    
    if (!isDir)
    {
        message = [[NSString alloc] initWithFormat:
                   @"Error: Location to save video is not directory: %@", dirPath];
    }
    
    if (message)
    {
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return replyDict;
    }
    
    if ([fm fileExistsAtPath:filePath])
    {
        [fm removeItemAtPath:filePath error:nil];
    }
    
    AVAssetWriter *videoFrameWriter;
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath
                                            isDirectory:NO];
    NSError *error;
    videoFrameWriter = [[AVAssetWriter alloc] initWithURL:fileURL
                                                 fileType:exportFileType
                                                    error:&error];
    if (!videoFrameWriter)
    {
        if (error)
        {
            message = [[NSString alloc]
            initWithFormat:@"Error: Creating video frame writer failed: %@",
                       error.localizedDescription];
        }
        else
        {
            message = @"Error: Creating video frame writer failed: ";
        }
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return replyDict;
    }

    MIMovieVideoFramesWriter *writer;
    writer = [[MIMovieVideoFramesWriter alloc]
                                        initWithAssetWriter:videoFrameWriter
                                                       name:objectName
                                                  inContext:context];

    if (writer)
    {
        MIBaseReference objectReference = [writer reference];
        NSString *ref;
        ref = [[NSString alloc] initWithFormat:@"%ld", (long)objectReference];

        replyDict = MIMakeReplyDictionaryWithNumericValue(ref,
                                                          MIReplyErrorNoError,
                                                          @(objectReference));
    }
    else
    {
        message = @"Error: Failed to create movie frame writer";
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message,
                                          MIReplyErrorOptionValueInvalid);
    }

    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MIMovieEditorKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Private Object methods

-(instancetype)initWithAssetWriter:(AVAssetWriter *)assetWriter
                       writerQueue:(dispatch_queue_t)writerQueue
                              name:(NSString *)theName
                         inContext:(MIContext *)context
{
    if (!(assetWriter && writerQueue))
    {
        self = nil;
        return self;
    }
    
    self = [super initWithBaseObjectType:MIMovieVideoFramesWriterKey
                                    name:theName
                               inContext:context];
    if (!self)
    {
        return self;
    }
    
    self->__videoFrameWriter = assetWriter;
    self->__writerInputsState = [[NSMutableArray alloc] initWithCapacity:0];
    self->__writerQueue = writerQueue;
    return self;
}

// Currently only one writer input allowed. This could change.
-(NSDictionary *)_addInputToMovieFrameWriter:(NSDictionary *)commandDict
{
    // Required MIJSONKeySize.
    // Required MIJSONPropertyMovieVideoWriterPreset
    
    // Optional
    // AVVideoCleanApertureKey - dict with following keys that numbers.
    //   AVVideoCleanApertureWidthKey
    //   AVVideoCleanApertureHeightKey
    //   AVVideoCleanApertureHorizontalOffsetKey
    //   AVVideoCleanApertureVerticalOffsetKey

    // Optional
    // AVVideoScalingModeKey - takes one of the following values.
    //   AVVideoScalingModeFit
    //   AVVideoScalingModeResize
    //   AVVideoScalingModeResizeAspect
    //   AVVideoScalingModeResizeAspectFill

    NSDictionary *result;
    NSString *message;
    
    if (self._allDone)
    {
        // Video writer already marked as completed.
        message = @"Error: Can't add a writer input. Video writer marked as done.";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }
    
    if (self._writerInputsState.count)
    {
        // only one adaptor and asset writer input currently allowed.
        message = @"Error: Already have an AVAssetWriterInput. Only 1 allowed.";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        return result;
    }
    
    CGSize inputSize;
    MIContext *context = self.miContext;
    BOOL gotInputSize = MIUtilityGetCGSizeFromDictionary(
                                                commandDict[MIJSONKeySize],
                                                &inputSize, kMILog,
                                                context.variables);
    
    if (!gotInputSize)
    {
        message = @"Error: Missing size property for asset writer input";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return result;
    }

    NSDictionary *cleanCapture = commandDict[AVVideoCleanApertureKey];
    NSString *scalingMode = commandDict[AVVideoScalingModeKey];
    
    NSDictionary *duration = commandDict[MIJSONPropertyMovieFrameDuration];
    if (!duration)
    {
        message = @"Error: Missing frame duration property for asset writer input";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return result;
    }
    
    CMTime frameDuration;
    if (!MIAVMakeACMTimeFromDictionary(context, duration, 6000, &frameDuration))
    {
        message = @"Error: Invalid frame duration property for asset writer input";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return result;
    }

    NSString *preset = commandDict[MIJSONPropertyMovieVideoWriterPreset];
    
    if (!preset)
    {
        message = @"Error: Missing required preset command option.";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return result;
    }

    NSDictionary *videoSettings;
    videoSettings = CopyVideoSettingsFromPreset(preset, inputSize.width,
                                                inputSize.height,
                                                cleanCapture, scalingMode);
    
    if (!videoSettings)
    {
        // Video settings failed to be generated from preset.
        message = @"Error: No video writer settings generated for preset";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return result;
    }
    
    // Inputs checked, now attempting to setup the assetWriterInput
    // Creating the asset writer input can throw an exception with invalid
    // or missing inputs. I'm catching this exception and treating it as
    // a failed operation.
    AVAssetWriterInput *assetWriterInput;
    @try
    {
        assetWriterInput = [AVAssetWriterInput
                            assetWriterInputWithMediaType:AVMediaTypeVideo
                            outputSettings:videoSettings];
    }
    @catch (NSException *exc)
    {
        NSString *msg = @"Error: Creating an asset writer input failed: ";
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"%@%@", msg, exc.reason];
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    if (!result)
    {
        AVAssetWriterInputPixelBufferAdaptor *adaptor;
        adaptor = CreateAdaptor(assetWriterInput, inputSize.width,
                                inputSize.height);
        if ([self._videoFrameWriter canAddInput:assetWriterInput])
        {
            assetWriterInput.mediaTimeScale = frameDuration.timescale;
            [self._videoFrameWriter addInput:assetWriterInput];
            MIAVAssetWriterInputState *inputState;
            inputState = [MIAVAssetWriterInputState new];
            inputState.adaptor = adaptor;
            inputState.size = inputSize;
            inputState.currentTime = CMTimeMake(0, frameDuration.timescale);
            inputState.duration = frameDuration;
            
            [self._writerInputsState addObject:inputState];
            self._videoFrameWriter.movieTimeScale = frameDuration.timescale;
            
            self._writeSemaphore = dispatch_semaphore_create(0);
            // Add this class as an observer for the writer input
            // and whether writer input is ready for more media data.
            [assetWriterInput addObserver:self
                               forKeyPath:@"readyForMoreMediaData"
                                  options:0
                                  context:NULL];

            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            message = @"Error: Failed adding a new input to an asset writer";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    return result;
}

// returns null if everything a-ok, otherwise returns a reply dictionary.
-(NSDictionary *)_ensureWriterCanAddImages:(NSDictionary *)commandDict
{
    NSDictionary *reply;
    NSString *message;

    if (self._canNoLongerAddFrames)
    {
        if (self._videoFrameWriter.status == AVAssetWriterStatusFailed &&
            self._videoFrameWriter.error)
        {
            NSError *error = self._videoFrameWriter.error;
            message = [[NSString alloc] initWithFormat:
                       @"Error: code: %ld, description: %@, \nreason: %@",
                       (long)error.code, error.localizedDescription,
                       error.localizedFailureReason];
            MILog(message, commandDict);
            reply = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
        else
        {
            message = @"Error: Can't add image to the video frames writer";
            MILog(message, commandDict);
            reply = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
        return reply;
    }
    
    if (!self._hasStarted)
    {
        @try {
            if ([self._videoFrameWriter startWriting])
            {
                [self._videoFrameWriter startSessionAtSourceTime:kCMTimeZero];
                self._hasStarted = YES;
            }
            else
            {
                self._allDone = YES;
                NSError *error = self._videoFrameWriter.error;
                if (error)
                {
                    message = [[NSString alloc] initWithFormat:
                           @"%@ Description:  %@ Failure reason: %@",
                           @"Error: Failed to prep writer for accepting frames.",
                           error.localizedDescription,
                           error.localizedFailureReason];
                    MILog(message, commandDict);
                    reply = MIMakeReplyDictionary(message,
                                                  MIReplyErrorOperationFailed);
                }
                else
                {
                message = @"Error: Couldn't prep AVAssetWriter for taking frames";
                    MILog(message, commandDict);
                    reply = MIMakeReplyDictionary(message,
                                                  MIReplyErrorOperationFailed);
                }
            }
        }
        @catch (NSException *exception) {
            self._allDone = YES;
            message = @"Error: Prepping AVAssetWriter threw exception.";
            reply = MIMakeReplyDictionary(message,
                                          MIReplyErrorOperationFailed);
            // NSLog(@"Exception name: %@", exception.name);
            // NSLog(@"Exception reason: %@", exception.reason);
            MILog(@"Error: Start writing AV exception name: ",
                                 exception.name);
            MILog(@"Error: Start writing AV exception reason: ",
                                 exception.reason);
        }
    }
    
    if (!self._canWrite)
    {
        message = @"Error: Could not add to video frames writer";
        MILog(message, commandDict);
        reply = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return reply;
}

-(void)observeValueForKeyPath:(NSString *)keyPath
                     ofObject:(id)object
                       change:(NSDictionary *)change
                      context:(void *)context
{
    if ([keyPath isEqualToString:@"readyForMoreMediaData"])
    {
        AVAssetWriterInput *input = self._videoFrameWriter.inputs[0];
        if (self._isWaitingForInputReady && input.isReadyForMoreMediaData)
        {
            self._isWaitingForInputReady = NO;
            // MILog(@"readyForMoreMediaData changed.", nil);
            dispatch_semaphore_signal(self._writeSemaphore);
        }
    }
}

-(void)_waitTillReadyForMediaData
{
    self._isWaitingForInputReady = YES;
    // Set the wait time to 1 second. If we have to wait for more than
    // a second then something more serious is wrong.
    dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
    dispatch_semaphore_wait(self._writeSemaphore, time);

    self._isWaitingForInputReady = NO;
}

-(NSDictionary *)_addImageSampleToWriter:(NSDictionary *)commandDict
{
    NSDictionary *reply = [self _ensureWriterCanAddImages:commandDict];
    if (reply)
    {
        return reply;
    }

    NSString *message;
    
    AVAssetWriterInput *input = self._videoFrameWriter.inputs[0];
    
    if (!input.readyForMoreMediaData)
    {
        [self _waitTillReadyForMediaData];
    }
    
    if (input.readyForMoreMediaData)
    {
        // First lets get the image to be added.
        MIContext *theContext = self.miContext;
        MICGImage *miImage = MICGImageFromDictionary(theContext,
                                                     commandDict, nil);

        // Get the the video input writer buffer adaptor.
        AVAssetWriterInputPixelBufferAdaptor *adaptor;
        MIAVAssetWriterInputState *state;
        state = self._writerInputsState[0];
        adaptor = state.adaptor;
        
        // Get the optional frame duration time from the command dict.
        NSDictionary *localDuration;
        NSString *durKey;
        
        // Precedence of where to get frame duration from.
        // 1. From the variables dictionary in the context.
        // 2. If that fails from the dictionary for this command.
        // 3. If that fails from the state object when new input was created.
        CMTime duration = kCMTimeInvalid;
        durKey = commandDict[MIJSONPropertyMovieLastAccessedFrameDurationKey];
        NSDictionary *variablesDuration;
        if (durKey)
        {
            variablesDuration = theContext.variables[durKey];
            if (variablesDuration)
            {
                MIAVMakeACMTimeFromDictionary(theContext, variablesDuration,
                                              6000, &duration);
            }
        }
        
        localDuration = commandDict[MIJSONPropertyMovieFrameDuration];
        if (CMTIME_IS_INVALID(duration) && localDuration)
        {
            CMTime temp;
            if (MIAVMakeACMTimeFromDictionary(theContext, localDuration,
                                              6000, &temp))
            {
                duration = temp;
            }
        }
        
        if (CMTIME_IS_INVALID(duration))
        {
            duration = state.duration;
        }
        
        // Get the cv pixel buffer pool from the adaptor.
        CVPixelBufferPoolRef pixelBufferPool = adaptor.pixelBufferPool;
        if (!pixelBufferPool)
        {
			if (self._videoFrameWriter.error)
			{
				NSError *err = self._videoFrameWriter.error;
				NSString *str = @"Error: No pixel buffer pool, error code:";
				message = [[NSString alloc] initWithFormat:
						   @"%@ %ld reason: %@ description: %@", str,
						   (long)err.code, err.localizedFailureReason,
						   err.localizedDescription];
				MILog(message, commandDict);
				reply = MIMakeReplyDictionary(message,
											  MIReplyErrorOperationFailed);
			}
			else
			{
				message = @"Error: Failed to get a pixel buffer pool";
				MILog(message, commandDict);
				reply = MIMakeReplyDictionary(message,
											  MIReplyErrorOperationFailed);
			}
        }
        
        // prepare the pixel buffer
        CVPixelBufferRef pixelBuffer = NULL;
		
        if (!reply)
        {
			CVReturn createdBuffer;
			createdBuffer = CVPixelBufferPoolCreatePixelBuffer(
														   kCFAllocatorDefault,
														   pixelBufferPool,
														   &pixelBuffer);

			if (createdBuffer != kCVReturnSuccess)
            {
                message = [[NSString alloc] initWithFormat:
                           @"Error: Failed to create pixel buffer. Code: %d",
                           createdBuffer];
                MILog(message, commandDict);
                reply = MIMakeReplyDictionary(message,
                                              MIReplyErrorOperationFailed);
            }
        }

        if (!reply)
        {
            BOOL drewImage = MIAVDrawCGImageIntoPixelBuffer(miImage,
                                                            pixelBuffer);
            if (!drewImage)
            {
                message = @"Error: Failed to draw into CV pixel buffer";
                MILog(message, commandDict);
                reply = MIMakeReplyDictionary(message,
                                              MIReplyErrorOperationFailed);
            }
        }
        
        if (!reply)
        {
            if ([adaptor appendPixelBuffer:pixelBuffer
                      withPresentationTime:state.currentTime])
            {
                state.currentTime = CMTimeAdd(state.currentTime, duration);
            }
            else
            {
                long status = self._videoFrameWriter.status;
				NSError *err = self._videoFrameWriter.error;
				if (err)
				{
					// If we get errors coming in here check VTErrors.h in
					// the VideoToolbox framework.
					NSString *str = @"Error: Failed to append sample. Status:";
					message = [[NSString alloc] initWithFormat:
							   @"%@ %ld, Reason: %@, Description: %@, code: %ld",
							   str, status, err.localizedFailureReason,
							   err.localizedDescription, (long)err.code];
					MILog(message, commandDict);
					reply = MIMakeReplyDictionary(message,
												  MIReplyErrorOperationFailed);
				}
				else
				{
					message = [[NSString alloc] initWithFormat:
							   @"Error: Failed to append video sample. Code: %ld",
							   status];
					MILog(message, commandDict);
					reply = MIMakeReplyDictionary(message,
												  MIReplyErrorOperationFailed);
				}
            }
        }
        if (pixelBuffer)
        {
            CVPixelBufferRelease(pixelBuffer);
        }
    }
    else
    {
        message = @"Error: Writer not ready for more video data";
        MILog(message, commandDict);
        reply = MIMakeReplyDictionary(message,
                                      MIReplyErrorOperationFailed);
    }
    
    if (!reply)
    {
        reply = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }

    return reply;
}

-(void)_finishWritingFrames
{
    if (self._hasFinalizeBeenCalled)
    {
        return;
    }

    self._hasFinalizeBeenCalled = YES;
    AVAssetWriterInput *input = nil;
    if (self._videoFrameWriter.inputs.count)
    {
        input = self._videoFrameWriter.inputs[0];
        [input removeObserver:self forKeyPath:@"readyForMoreMediaData"];
    }

    if (self._allDone)
    {
        return;
    }
    self._allDone = YES;

    AVAssetWriterStatus status = self._videoFrameWriter.status;
    if (status == AVAssetWriterStatusFailed ||
        status == AVAssetWriterStatusUnknown)
    {
        // NSLog(@"AVAssetWriterStatusFailed or unknown.");
        return;
    }

    if (input)
    {
        [input markAsFinished];

        // Need to wait until frames have been written before returning.
        dispatch_semaphore_t waitSemaphore = dispatch_semaphore_create(0);
        
        void (^completion)(void) = ^(void)
        {
            // NSLog(@"AVAssetWriterStatus: %ld.", self._videoFrameWriter.status);
            dispatch_semaphore_signal(waitSemaphore);
        };
        
        // finish writing the frames.
        [self._videoFrameWriter finishWritingWithCompletionHandler:completion];
        
        dispatch_time_t dispatchTime = DISPATCH_TIME_FOREVER;
        dispatch_semaphore_wait(waitSemaphore, dispatchTime);
    }
}

#pragma mark Private readonly property implementation.

-(BOOL)_canWrite
{
    BOOL canWrite = YES;
    AVAssetWriterStatus status = self._videoFrameWriter.status;
    canWrite &= (status != AVAssetWriterStatusCompleted);
    canWrite &= (status != AVAssetWriterStatusFailed);
    canWrite &= (status != AVAssetWriterStatusCancelled);
    canWrite &= (self._videoFrameWriter.inputs.count != 0);
    canWrite &= (!self._allDone);
    return canWrite;
}

-(BOOL)_canNoLongerAddFrames
{
    AVAssetWriterStatus status = self._videoFrameWriter.status;
    BOOL noLonger = (status == AVAssetWriterStatusCompleted);
    noLonger |= (status == AVAssetWriterStatusFailed);
    noLonger |= (status == AVAssetWriterStatusCancelled);
    noLonger |= self._allDone;
    return noLonger;
}

#pragma mark Public Object methods

-(instancetype)initWithAssetWriter:(AVAssetWriter *)assetWriter
                              name:(NSString *)theName
                         inContext:(MIContext *)context
{
    if (!assetWriter)
    {
        self = nil;
        return self;
    }
    NSString *queueName = [NSString stringWithFormat:
                           @"com.zukini.mivideoframewriter.%p", self];
    return [self initWithAssetWriter:assetWriter
                         writerQueue:CreateVideoFramesWriterQueue(queueName)
                                name:theName
                           inContext:context];
}

-(void)close
{
    [self finalize];
    [super baseObjectClose];
}

-(void)finalize
{
    dispatch_sync(self._writerQueue, ^{
        [self _finishWritingFrames];
    });
}

#pragma mark Public methods for Protocol MIBaseObjectSemiPublicInterface

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }

    NSDictionary *resultsDict;
    resultsDict = [super handleCommonGetPropertyCommand:commandDict];
    if (resultsDict)
    {
        return resultsDict;
    }
    
    __block NSDictionary *temp;
    dispatch_sync(self._writerQueue, ^
    {
        NSString *message;
        if ([propertyType
             isEqualToString:MIJSONPropertyMovieVideoWriterCanWriteFrames])
        {
            BOOL canWriteFrames = (self._canWrite);
            NSNumber *numericVal = @(canWriteFrames);
            NSString *stringRes = canWriteFrames ? @"YES" : @"NO";
            temp = MIMakeReplyDictionaryWithNumericValue(stringRes,
                                                MIReplyErrorNoError, numericVal);
        }
        else if ([propertyType
                        isEqualToString:MIJSONPropertyMovieVideoWriterStatus])
        {
            NSNumber *numericVal = @(self._videoFrameWriter.status);
            NSString *stringRes = numericVal.stringValue;
            temp = MIMakeReplyDictionaryWithNumericValue(stringRes,
                                                         MIReplyErrorNoError,
                                                         numericVal);
        }
        else if ([propertyType isEqualToString:MIJSONPropertyFile])
        {
            message = self._videoFrameWriter.outputURL.path;
            temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
        }
        else if ([propertyType isEqualToString:MIJSONPropertyFileType])
        {
            message = self._videoFrameWriter.outputFileType;
            temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
        }
        else if (self._videoFrameWriter.inputs.count)
        {
            MIAVAssetWriterInputState *state = self._writerInputsState[0];
            if ([propertyType
                        isEqualToString:MIJSONPropertyMovieVideoWriterSettings])
            {
                AVAssetWriterInput *input = self._videoFrameWriter.inputs[0];
                NSDictionary *settings = input.outputSettings;
                message = MIUtilityConvertDictionaryToJSONString(settings);
                temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
            }
            else if ([propertyType
                              isEqualToString:MIJSONPropertyMovieFrameDuration])
            {
                NSDictionary *durationDict;
                durationDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                        state.duration, kCFAllocatorDefault));
                message = MIUtilityConvertDictionaryToJSONString(durationDict);
                temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
            }
            else if ([propertyType isEqualToString:MIJSONPropertyMovieTime])
            {
                NSDictionary *timeDict;
                timeDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                    state.currentTime, kCFAllocatorDefault));
                message = MIUtilityConvertDictionaryToJSONString(timeDict);
                temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
            }
            else if ([propertyType isEqualToString:MIJSONKeySize])
            {
                NSDictionary *sizeDict;
                sizeDict =  @{ MIJSONKeyWidth : @(state.size.width),
                               MIJSONKeyHeight : @(state.size.height) };
                message = MIUtilityConvertDictionaryToJSONString(sizeDict);
                temp = MIMakeReplyDictionary(message, MIReplyErrorNoError);
            }
        }
        else
        {
            message = @"Error: Unknown property requested";
            MILog(message, commandDict);
            temp = MIMakeReplyDictionary(message, MIReplyErrorUnknownProperty);
        }
    });
    resultsDict = temp;
    
    return resultsDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }
    
    __block NSDictionary *propDict;
    dispatch_sync(self._writerQueue, ^
    {
        NSString *string;
        NSMutableDictionary *fileDict = [[NSMutableDictionary alloc]
                                                            initWithCapacity:0];
        fileDict[MIJSONPropertyFileType] = self._videoFrameWriter.outputFileType;
        string = self._videoFrameWriter.outputURL.path;
        fileDict[MIJSONPropertyFile] = string;
        fileDict[MIJSONKeyObjectName] = self.name;
        fileDict[MIJSONKeyObjectType] = self.baseObjectType;
        NSNumber *status = @(self._videoFrameWriter.status);
        fileDict[MIJSONPropertyMovieVideoWriterStatus] = status;
        fileDict[MIJSONPropertyMovieVideoWriterCanWriteFrames] = @(self._canWrite);
        if (self._videoFrameWriter.inputs.count)
        {
            NSDictionary *tempDict;
            MIAVAssetWriterInputState *state = self._writerInputsState[0];
            AVAssetWriterInput *input = self._videoFrameWriter.inputs[0];
            // tempDict = input.outputSettings;
            // string = MIUtilityConvertDictionaryToJSONString(tempDict);
            // fileDict[MIJSONPropertyMovieVideoWriterSettings] = string;
            fileDict[MIJSONPropertyMovieVideoWriterSettings] =
                                                            input.outputSettings;
            tempDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                            state.duration, kCFAllocatorDefault));
            // string = MIUtilityConvertDictionaryToJSONString(tempDict);
            fileDict[MIJSONPropertyMovieFrameDuration] = tempDict;
            tempDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                    state.currentTime, kCFAllocatorDefault));
            // string = MIUtilityConvertDictionaryToJSONString(tempDict);
            fileDict[MIJSONPropertyMovieTime] = tempDict;
            tempDict =  @{ MIJSONKeyWidth : @(state.size.width),
                           MIJSONKeyHeight : @(state.size.height) };
            // string = MIUtilityConvertDictionaryToJSONString(tempDict);
            fileDict[MIJSONKeySize] = tempDict;
        }
        propDict = fileDict.copy;
    });
    
    replyDict = MIUtilitySaveDictionaryToDestination(commandDict, propDict, NO);
    return replyDict;
}

// Currently only one writer input allowed. This could change.
-(NSDictionary *)handleAddInputToMovieFrameWriter:(NSDictionary *)commandDict
{
    __block NSDictionary *replyDict;
    dispatch_sync(self._writerQueue, ^{
        replyDict = [self _addInputToMovieFrameWriter:commandDict];
    });
    return replyDict;
}

-(NSDictionary *)handleFinishWritingFrames:(NSDictionary *)commandDict
{
    dispatch_sync(self._writerQueue, ^{
        [self _finishWritingFrames];
    });
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

-(NSDictionary *)handleCancelWritingFrames:(NSDictionary *)commandDict
{
    dispatch_sync(self._writerQueue, ^{
        [self._videoFrameWriter cancelWriting];
    });
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

-(NSDictionary *)handleAddImageSampleToWriter:(NSDictionary *)commandDict
{
    __block NSDictionary *replyDict;
    dispatch_sync(self._writerQueue, ^
    {
        replyDict = [self _addImageSampleToWriter:commandDict];
    });
    return replyDict;
}

@end
