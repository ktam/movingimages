//  MIConstants.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIConstants.h"

NSString *const MIImageImporterKey = @"imageimporter";
NSString *const MIImageExporterKey = @"imageexporter";
NSString *const MIImageFilterKey = @"imagefilterchain";
NSString *const MICGBitmapContextKey = @"bitmapcontext";
NSString *const MICGPDFContextKey = @"pdfcontext";
NSString *const MIMovieImporterKey = @"movieimporter";
// NSString *const MIMovieFrameIteratorKey = @"movieframeiterator";
NSString *const MIMovieEditorKey = @"movieeditor";
NSString *const MIMovieVideoFramesWriterKey = @"videoframeswriter";
NSString *const MINSGraphicContextKey = @"nsgraphicscontext";

/*
 Define the presets for the colorspace/bit depth/alpha channel combos
 that have been specifically mentioned as being supported
*/
NSString *const MIAlphaOnly8bpc8bppInteger = @"AlphaOnly8bpcInt";
NSString *const MIGray8bpc8bppInteger = @"Gray8bpcInt";
NSString *const MIGray16bpc16bppInteger = @"Gray16bpcInt";
NSString *const MIGray32bpc32bppFloat = @"Gray32bpcFloat";
NSString *const MIAlphaSkipFirstRGB8bpc32bppInteger =
                                                @"AlphaSkipFirstRGB8bpcInt";
NSString *const MIAlphaSkipLastRGB8bpc32bppInteger = @"AlphaSkipLastRGB8bpcInt";
NSString *const MIAlphaPreMulFirstRGB8bpc32bppInteger =
                                                @"AlphaPreMulFirstRGB8bpcInt";
NSString *const MIAlphaPreMulLastRGB8bpc32bppInteger =
                                                @"AlphaPreMulLastRGB8bpcInt";
NSString *const MIAlphaPreMulLastRGB16bpc64bppInteger =
                                                @"AlphaPreMulLastRGB16bpcInt";
NSString *const MIAlphaSkipLastRGB16bpc64bppInteger =
                                                @"AlphaSkipLastRGB16bpcInt";
NSString *const MIAlphaSkipLastRGB32bpc128bppFloat =
                                                @"AlphaSkipLastRGB32bpcFloat";
NSString *const MIAlphaPreMulLastRGB32bpc128bppFloat =
                                                @"AlphaPreMulLastRGB32bpcFloat";
NSString *const MICMYK8bpc32bppInteger = @"CMYK8bpcInt";
NSString *const MICMYK16bpc64bppInteger = @"CMYK16bpcInt";
NSString *const MICMYK32bpc128bppFloat = @"CMYK32bpcFloat";

NSString *const MIAlphaPreMulBGRA8bpc32bppInteger = @"AlphaPreMulBGRA8bpcInt";

NSString *const MIPlatformDefaultBitmapContext = @"PlatformDefaultBitmapContext";

NSString *const MIErrorMessageKey = @"errorMessage";

#if TARGET_OS_IPHONE

NSArray *MICGBitmapGetPresetList()
{
    static NSArray *array = NULL;
    if (array == NULL)
    {
        array = @[
                  MIAlphaOnly8bpc8bppInteger,
                  MIGray8bpc8bppInteger,
                  MIGray16bpc16bppInteger,
                  MIGray32bpc32bppFloat,
                  MIAlphaSkipFirstRGB8bpc32bppInteger,
                  MIAlphaSkipLastRGB8bpc32bppInteger,
                  MIAlphaPreMulFirstRGB8bpc32bppInteger,
                  MIAlphaPreMulBGRA8bpc32bppInteger,
                  MIAlphaPreMulLastRGB8bpc32bppInteger,
                  MIPlatformDefaultBitmapContext
                  ];
    }
    return array;
}

#else

NSArray *MICGBitmapGetPresetList()
{
    static NSArray *array = NULL;
    if (array == NULL)
    {
        array = @[
                MIAlphaOnly8bpc8bppInteger,
                MIGray8bpc8bppInteger,
                MIGray16bpc16bppInteger,
                MIGray32bpc32bppFloat,
                MIAlphaSkipFirstRGB8bpc32bppInteger,
                MIAlphaSkipLastRGB8bpc32bppInteger,
                MIAlphaPreMulFirstRGB8bpc32bppInteger,
                MIAlphaPreMulBGRA8bpc32bppInteger,
                MIAlphaPreMulLastRGB8bpc32bppInteger,
                MIAlphaPreMulLastRGB16bpc64bppInteger,
                MIAlphaSkipLastRGB16bpc64bppInteger,
                MIAlphaSkipLastRGB32bpc128bppFloat,
                MIAlphaPreMulLastRGB32bpc128bppFloat,
                MICMYK8bpc32bppInteger,
                MICMYK16bpc64bppInteger,
                MICMYK32bpc128bppFloat,
                MIPlatformDefaultBitmapContext
            ];
    }
    return array;
}

#endif