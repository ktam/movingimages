//  MIAVFoundationUtilities.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "AVMetadataItem+THAdditions.h"
#import "MIAVFoundationUtilities.h"
#import "MIBaseObject.h"
#import "MICGImage.h"
#import "MICMSampleBuffer.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

AVAssetTrack *MIAVGetTrackFromAVAssetWithDictionary(AVAsset *asset,
                                                    NSDictionary *trackIdent)
{
    AVAssetTrack *track;
    if (!(trackIdent && [trackIdent isKindOfClass:[NSDictionary class]]))
    {
        MILog(@"Track ident is null or not a dict", trackIdent);
        return nil;
    }
    
    NSNumber *trackID = trackIdent[MIJSONPropertyMovieTrackID];
    NSNumber *trackIndex = trackIdent[MIJSONPropertyMovieTrackIndex];
    if (trackID)
    {
        if ([trackID isKindOfClass:[NSNumber class]])
        {
            CMPersistentTrackID tID = (CMPersistentTrackID)trackID.integerValue;
            track = [asset trackWithTrackID:tID];
        }
    }
    else if (trackIndex)
    {
        if ([trackIndex isKindOfClass:[NSNumber class]])
        {
            NSInteger tI = trackIndex.integerValue;
            if (tI >= 0)
            {
                NSString *mediaType = trackIdent[MIJSONPropertyMovieMediaType];
                if (mediaType)
                {
                    if ([mediaType isKindOfClass:[NSString class]])
                    {
                        NSArray *tracks = [asset tracksWithMediaType:mediaType];
                        if (tracks && tI >= 0 && tI < tracks.count)
                            track = tracks[tI];
                    }
                }
                else
                {
                    NSString *mC =
                    trackIdent[MIJSONPropertyMovieMediaCharacteristic];
                    if (mC)
                    {
                        if ([mC isKindOfClass:[NSString class]])
                        {
                            NSArray *tracks;
                            tracks = [asset tracksWithMediaCharacteristic:mC];
                            if (tracks && tI >= 0 && tI < tracks.count)
                                track = tracks[tI];
                        }
                    }
                    else
                    {
                        NSArray *tracks = asset.tracks;
                        if (tI < tracks.count)
                            track = tracks[tI];
                    }
                }
            }
        }
    }
    return track;
}

BOOL MIAVMakeACMTimeFromDictionary(MIContext *context, NSDictionary *movieTime,
                                   CMTimeScale defaultTimeScale, CMTime *time)
{
    if (!movieTime)
    {
        // MILog(@"Missing movie time dictionary.", nil);
        return NO;
    }

    *time = CMTimeMakeFromDictionary((__bridge CFDictionaryRef)movieTime);
    if (CMTIME_IS_VALID(*time))
    {
        return YES;
    }

    // Since the dictionary didn't represent a CMTime structure then we need a
    // defaultTimeScale value
    if (defaultTimeScale <= 0)
    {
        MILog(@"Default time scale is negative.", nil);
        return NO;
    }

    CGFloat floatVal;
    if (MIUtilityGetCGFloatFromDictionary(movieTime,
                                          MIJSONPropertyMovieTimeInSeconds,
                                          &floatVal, context.variables))
    {
        time->epoch = 0;
        time->flags = 1;
        time->timescale = defaultTimeScale;
        time->value = floatVal * defaultTimeScale;
        return YES;
    }
    return NO;
}

BOOL MIAVMakeACMTimeRangeFromDictionary(MIContext *context,
                                        NSDictionary *timeRangeDict,
                                        CMTimeScale defaultScale,
                                        CMTimeRange *timeRange)
{
    if (!timeRangeDict)
    {
        MILog(@"Missing time range dictionary.", nil);
        return NO;
    }

    if (defaultScale <= 0)
    {
        MILog(@"Default time scale is negative.", nil);
        return NO;
    }

    *timeRange = CMTimeRangeMakeFromDictionary(
                                    (__bridge CFDictionaryRef)timeRangeDict);
    if (CMTIMERANGE_IS_VALID((*timeRange)))
    {
        return YES;
    }
    
    CMTime start;
    CMTime duration;
    NSDictionary *startDict = timeRangeDict[MIJSONPropertyMovieTimeRangeStart];
    NSDictionary *durationD = timeRangeDict[MIJSONPropertyMovieTimeRangeDuration];
    
    if (!(startDict && durationD))
    {
        MILog(@"Missing start time or duration.", timeRangeDict);
        return NO;
    }

    if (MIAVMakeACMTimeFromDictionary(context, startDict, defaultScale, &start))
    {
        if (MIAVMakeACMTimeFromDictionary(context, durationD, defaultScale,
                                          &duration))
        {
            timeRange->duration = duration;
            timeRange->start = start;
            return YES;
        }
    }
    return NO;
}

NSDictionary *MIAVCopyTimeMappingAsDictionary(CMTimeMapping mapping)
{
    NSDictionary *sourceDict = CFBridgingRelease(
             CMTimeRangeCopyAsDictionary(mapping.source, kCFAllocatorDefault));
    NSDictionary *targetDict = CFBridgingRelease(
             CMTimeRangeCopyAsDictionary(mapping.target, kCFAllocatorDefault));
    
    NSDictionary *result;
    result = [[NSDictionary alloc] initWithObjectsAndKeys:sourceDict,
              MIJSONPropertyMovieSourceTimeRange, targetDict,
              MIJSONPropertyMovieTargetTimeRange, nil];
    return result;
}

#if TARGET_OS_IPHONE
CGImageRef MIAVCreateCGImageFromPixelBuffer(CVPixelBufferRef pixBuffer)
{
    CGImageRef image = NULL;
    CVReturn result = CVPixelBufferLockBaseAddress(pixBuffer, 0);
    if (result == kCVReturnSuccess)
    {
        //        CVBufferSetAttachment kCVImageBufferCGColorSpaceKey
        OSType pixType = CVPixelBufferGetPixelFormatType(pixBuffer);
        if (kCVPixelFormatType_32BGRA == pixType)
        {
            CGColorSpaceRef colorSpace;
            colorSpace = (CGColorSpaceRef)CVBufferGetAttachment(pixBuffer,
                                        kCVImageBufferCGColorSpaceKey, NULL);
            
            if (colorSpace)
            {
                CGColorSpaceRetain(colorSpace);
            }
            else
            {
                colorSpace = CGColorSpaceCreateDeviceRGB();
            }
            
            size_t width = CVPixelBufferGetWidth(pixBuffer);
            size_t height = CVPixelBufferGetHeight(pixBuffer);
            size_t bPR = CVPixelBufferGetBytesPerRow(pixBuffer);
            void *backing = CVPixelBufferGetBaseAddress(pixBuffer);
            CGBitmapInfo info;
            info = (CGBitmapInfo)kCGImageAlphaPremultipliedFirst |
                                 kCGBitmapByteOrder32Little;
            CGContextRef bitmap;
            bitmap = CGBitmapContextCreate(backing, width, height, 8,
                                           bPR, colorSpace, info);
            if (bitmap)
            {
                image = CGBitmapContextCreateImage(bitmap);
                CGContextRelease(bitmap);
            }
            
            CGColorSpaceRelease(colorSpace);
        }
        else
        {
            MILog(@"CVPixelBuffer format is not 32BGRA.", nil);
        }
        CVPixelBufferUnlockBaseAddress(pixBuffer, 0);
    }
    else
    {
        MILog(@"Couldn't lock pixel buffer base address.", nil);
    }
    
    return image;
}
#else
CGImageRef MIAVCreateCGImageFromPixelBuffer(CVPixelBufferRef pixBuffer)
{
    CGImageRef image = NULL;
    CVReturn result = CVPixelBufferLockBaseAddress(pixBuffer, 0);
    if (result == kCVReturnSuccess)
    {
        //        CVBufferSetAttachment kCVImageBufferCGColorSpaceKey
        OSType pixType = CVPixelBufferGetPixelFormatType(pixBuffer);
        if (kCVPixelFormatType_32ARGB == pixType)
        {
            CGColorSpaceRef colorSpace;
            colorSpace = (CGColorSpaceRef)CVBufferGetAttachment(pixBuffer,
                                        kCVImageBufferCGColorSpaceKey, NULL);
            
            if (colorSpace)
            {
                CGColorSpaceRetain(colorSpace);
            }
            else
            {
                colorSpace = CGColorSpaceCreateDeviceRGB();
            }

            size_t width = CVPixelBufferGetWidth(pixBuffer);
            size_t height = CVPixelBufferGetHeight(pixBuffer);
            size_t bPR = CVPixelBufferGetBytesPerRow(pixBuffer);
            void *backing = CVPixelBufferGetBaseAddress(pixBuffer);
            CGContextRef bitmap;
            bitmap = CGBitmapContextCreate(
                               backing, width, height, 8, bPR, colorSpace,
                               (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
            if (bitmap)
            {
                image = CGBitmapContextCreateImage(bitmap);
                CGContextRelease(bitmap);
            }

            CGColorSpaceRelease(colorSpace);
        }
        else
        {
            MILog(@"CVPixelBuffer format is not 32ARGB.", nil);            
        }
        CVPixelBufferUnlockBaseAddress(pixBuffer, 0);
    }
    else
    {
        MILog(@"Couldn't lock pixel buffer base address.", nil);
    }

    return image;
}
#endif

#if TARGET_OS_IPHONE
BOOL MIAVDrawCGImageIntoPixelBuffer(MICGImage *inputImage,
                                    CVPixelBufferRef pixelBuf)
{
    if (!inputImage || !pixelBuf)
    {
        return NO;
    }
    CGImageRef image = inputImage.CGImage;
    
    size_t width = CVPixelBufferGetWidth(pixelBuf);
    size_t height = CVPixelBufferGetHeight(pixelBuf);
    if (CGImageGetWidth(image) != width ||
        CGImageGetHeight(image) != height)
    {
        return NO;
    }
    
    if (CVPixelBufferGetPixelFormatType(pixelBuf) != kCVPixelFormatType_32BGRA)
    {
        return NO;
    }
    
    // No more early bailing out from this point on.
    BOOL result = YES;
    
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuf);
    CGColorSpaceRef colorSpace = (CGColorSpaceRef)CVBufferGetAttachment(pixelBuf,
                                            kCVImageBufferCGColorSpaceKey, NULL);
    
    if (!colorSpace)
    {
        colorSpace = CGImageGetColorSpace(inputImage.CGImage);
        CVBufferSetAttachment(pixelBuf, kCVImageBufferCGColorSpaceKey, colorSpace,
                              kCVAttachmentMode_ShouldNotPropagate);
    }
    
    CGBitmapInfo info;
    info = kCGImageAlphaPremultipliedFirst | kCGBitmapByteOrder32Little;
    CVPixelBufferLockBaseAddress(pixelBuf, 0);  // ***
    void *pixelBufferBaseAddr = CVPixelBufferGetBaseAddress(pixelBuf);
    CGContextRef theContext = CGBitmapContextCreate(pixelBufferBaseAddr,
                                                    width, height, 8, bytesPerRow,
                                                    colorSpace, info);
    if (theContext)
    {
        CGRect rect = CGRectMake(0, 0, width, height);
        CGContextDrawImage(theContext, rect, image);
        CGContextRelease(theContext); // *
    }
    else
    {
        result = NO;
    }
    CVPixelBufferUnlockBaseAddress(pixelBuf, 0); // *
    
    return result;
}
#else
BOOL MIAVDrawCGImageIntoPixelBuffer(MICGImage *inputImage,
                                    CVPixelBufferRef pixelBuf)
{
    if (!inputImage || !pixelBuf)
    {
        return NO;
    }
    CGImageRef image = inputImage.CGImage;

    size_t width = CVPixelBufferGetWidth(pixelBuf);
    size_t height = CVPixelBufferGetHeight(pixelBuf);
    if (CGImageGetWidth(image) != width ||
        CGImageGetHeight(image) != height)
    {
        MILog(@"CVPixelBuffer and CGImage dimensions dont match", nil);
        return NO;
    }

    if (CVPixelBufferGetPixelFormatType(pixelBuf) != kCVPixelFormatType_32ARGB)
    {
        MILog(@"CVPixelBuffer pixel format is not 32ARGB", nil);
        return NO;
    }
    
    // No more early bailing out from this point on.
    BOOL result = YES;
    
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(pixelBuf);
    CGColorSpaceRef colorSpace = (CGColorSpaceRef)CVBufferGetAttachment(
                                pixelBuf, kCVImageBufferCGColorSpaceKey, NULL);

    if (!colorSpace)
    {
        colorSpace = CGImageGetColorSpace(inputImage.CGImage);
        CVBufferSetAttachment(pixelBuf, kCVImageBufferCGColorSpaceKey, colorSpace,
                              kCVAttachmentMode_ShouldNotPropagate);
    }

    CGBitmapInfo bitMapInfo = (CGBitmapInfo)kCGImageAlphaPremultipliedFirst;
    CVPixelBufferLockBaseAddress(pixelBuf, 0);  // ***
    void *pixelBufferBaseAddr = CVPixelBufferGetBaseAddress(pixelBuf);
    CGContextRef theContext = CGBitmapContextCreate(pixelBufferBaseAddr,
                                                width, height, 8, bytesPerRow,
                                                colorSpace, bitMapInfo); // ***
    // CGColorSpaceRelease(colorSpace); // *
    if (theContext)
    {
        CGRect rect = CGRectMake(0, 0, width, height);
        CGContextDrawImage(theContext, rect, image);
        CGContextRelease(theContext); // *
    }
    else
    {
        result = NO;
    }
    CVPixelBufferUnlockBaseAddress(pixelBuf, 0); // *
    
    return result;
}
#endif

CGImageRef MIAVCreateCGImageFromSampleBuffer(MICMSampleBuffer *buffer)
{
    CVImageBufferRef imageBuffer;
    imageBuffer = CMSampleBufferGetImageBuffer(buffer.CMSampleBuffer);
    if (imageBuffer)
    {
        return MIAVCreateCGImageFromPixelBuffer(imageBuffer);
    }
    else
    {
        MILog(@"Couldn't get pixel buffer from sample buffer.", nil);
    }
    return NULL;
}

NSInteger MIAVGetNumberOfTracks(AVAsset *movieAsset, NSDictionary *commandDict)
{
    NSInteger numTracks = -1;
    
    NSString *mediaType = commandDict[MIJSONPropertyMovieMediaType];
    NSArray *tracks = nil;
    
    if (mediaType)
    {
        tracks = [movieAsset tracksWithMediaType:mediaType];
    }
    
    if (!tracks)
    {
        NSString *characteristic;
        characteristic = commandDict[MIJSONPropertyMovieMediaCharacteristic];
        if (characteristic)
        {
            tracks = [movieAsset tracksWithMediaCharacteristic:characteristic];
        }
        else
        {
            tracks = movieAsset.tracks;
        }
    }
    
    if (tracks)
    {
        numTracks = tracks.count;
    }
    return numTracks;
}

static NSDictionary *CreateDictionaryFromMetadataItem(AVMetadataItem *item)
{
    NSMutableDictionary *itemDict = [NSMutableDictionary new];
    itemDict[@"keyspace"] = item.keySpace;
    // itemDict[@"identifier"] = item.identifier;
    itemDict[@"key"] = item.keyString;
    
    if (item.commonKey)
    {
        itemDict[@"commonkey"] = item.commonKey;
    }
    if (item.stringValue)
    {
        itemDict[@"stringValue"] = item.stringValue;
    }
    if (item.numberValue)
    {
        itemDict[@"numberValue"] = item.numberValue;
    }
    if (item.extendedLanguageTag)
    {
        itemDict[@"extendedLanguageTag"] = item.extendedLanguageTag;
    }
    if (item.dateValue)
    {
        itemDict[@"dateValue"] = item.dateValue.description;
    }
    return itemDict.copy;
}

NSDictionary *MIAVGetMovieTrackProperty(AVAssetTrack *track,
                                        NSDictionary *commandDict)
{
    if (!track)
    {
        MILog(@"No track to get properties from", commandDict);
        return MIMakeReplyDictionary(@"Error: No track to get a property from.",
                                     MIReplyErrorOperationFailed);
    }
    
    NSDictionary *resultsDict;
    NSString *resultString;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if ([propertyType isEqualToString:MIJSONPropertyMovieTrackID])
    {
        CMPersistentTrackID trackID = track.trackID;
        resultString = [[NSString alloc] initWithFormat:@"%d", trackID];
        resultsDict = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                            MIReplyErrorNoError,
                                                            @(trackID));
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieMediaType])
    {
        NSString *mediaType = track.mediaType;
        resultsDict = MIMakeReplyDictionary(mediaType, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieMetadataFormats])
    {
        NSArray *formats = track.availableMetadataFormats;
        if (formats)
        {
            resultString = MIUtilityCreateStringFromArrayOfStrings(@" ", formats);
            resultsDict = MIMakeReplyDictionary(resultString,MIReplyErrorNoError);
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieTrackEnabled])
    {
        resultString = track.enabled ? @"YES" : @"NO";
        resultsDict = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                            MIReplyErrorNoError,
                                                            @(track.enabled));
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieTimeRange])
    {
        CMTimeRange theRange = track.timeRange;
        NSDictionary *timeRange = CFBridgingRelease(
                    CMTimeRangeCopyAsDictionary(theRange, kCFAllocatorDefault));
        resultString = MIUtilityCreateStringRepresentationOfObject(timeRange, 0);
        resultsDict = MIMakeReplyDictionary(resultString, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieLanguageCode])
    {
        resultString = track.languageCode;
        resultString = resultString ? track.languageCode : @"";
        resultsDict = MIMakeReplyDictionary(resultString, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:
                                        MIJSONPropertyMovieExtendedLanguageTag])
    {
        resultString = track.extendedLanguageTag;
        resultString = resultString ? track.extendedLanguageTag:@"";
        resultsDict = MIMakeReplyDictionary(resultString, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieTrackNaturalSize])
    {
        if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
        {
            CGSize nSize = track.naturalSize;
            NSDictionary *sizeDict;
            sizeDict = MIUtilityCreateSizeDictionaryFromCGSize(nSize);
            resultsDict = MIMakeReplyDictionaryWithDictionaryValue(sizeDict, YES);
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
                            @"Error: Non visual tracks can't have a size.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:MIJSONKeyAffineTransform])
    {
        if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
        {
            CGAffineTransform transform = track.preferredTransform;
            NSDictionary *transDict;
            transDict = MIUtilityCreateCGAffineTransformDictionary(transform);
            resultsDict = MIMakeReplyDictionaryWithDictionaryValue(transDict,
                                                                   YES);
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
                            @"Error: Non visual tracks can't have a transform.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:
              MIJSONKeyMovieTrackPreferredVolume])
    {
        if ([track hasMediaCharacteristic:AVMediaCharacteristicAudible])
        {
            CGFloat volume = track.preferredVolume;
            resultString = [[NSString alloc] initWithFormat:@"%f", volume];
            resultsDict = MIMakeReplyDictionaryWithNumericValue(resultString,
                                        MIReplyErrorNoError, @(volume));
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
                    @"Error: Non audible tracks can't have a preferred volume.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:
              MIJSONPropertyMovieTrackNominalFrameRate])
    {
        if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
        {
            float rate = track.nominalFrameRate;
            resultString = [[NSString alloc] initWithFormat:@"%f", rate];
            resultsDict = MIMakeReplyDictionaryWithNumericValue(
                                                            resultString,
                                                            MIReplyErrorNoError,
                                                            @(rate));
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
                          @"Error: Non visual tracks can't have a transform.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:
              MIJSONPropertyMovieTrackMinFrameDuration])
    {
        if ([track respondsToSelector:NSSelectorFromString(@"minFrameDuration")])
        {
            if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
            {
                CMTime minDur = track.minFrameDuration;
                NSDictionary *durDict = CFBridgingRelease(
                            CMTimeCopyAsDictionary(minDur, kCFAllocatorDefault));
                resultString = MIUtilityCreateStringRepresentationOfObject(
                                                                durDict, 0);
                resultsDict = MIMakeReplyDictionary(resultString,
                                                    MIReplyErrorNoError);
            }
            else
            {
                resultsDict = MIMakeReplyDictionary(
                @"Error: Non visual tracks dont have a frame duration.",
                                                    MIReplyErrorInvalidProperty);
            }
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
            @"Error: Property not available with this version of AVFoundation.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:
              MIJSONPropertyMovieTrackRequiresFrameReordering])
    {
        if ([track respondsToSelector:
             NSSelectorFromString(@"requiresFrameReordering")])
        {
            if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
            {
                resultString = track.requiresFrameReordering ? @"YES" : @"NO";
                resultsDict = MIMakeReplyDictionaryWithNumericValue(
                                                resultString,
                                                MIReplyErrorNoError,
                                                @(track.requiresFrameReordering));
            }
            else
            {
                resultsDict = MIMakeReplyDictionary(
                @"Error: Frame reordering not available for non visual tracks",
                                                    MIReplyErrorInvalidProperty);
            }
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(
            @"Error: Property not available with this version of AVFoundation.",
                                                MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:
              MIJSONPropertyMovieTrackSegmentMappings])
    {
        NSArray *segments = track.segments;
        NSMutableArray *segmentDicts;
        segmentDicts = [[NSMutableArray alloc] initWithCapacity:0];
        if (segments)
        {
            for (AVAssetTrackSegment *segment in segments)
            {
                if (!segment.empty)
                {
                    NSDictionary *mappingDict = MIAVCopyTimeMappingAsDictionary(
                                                        segment.timeMapping);
                    [segmentDicts addObject:mappingDict];
                }
            }
        }
        resultString = MIUtilityCreateStringRepresentationOfObject(segmentDicts,
                                                                   0);
        resultsDict = MIMakeReplyDictionary(resultString, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieMetadata])
    {
        NSString *format = commandDict[MIJSONPropertyMovieMetadataFormats];
        NSArray *metadata;
        if (format)
        {
            metadata = [track metadataForFormat:format];
            if (!metadata)
                return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            metadata = track.metadata;
        }
        NSMutableArray *theMetadata = [[NSMutableArray alloc]
                                       initWithCapacity:metadata.count];
        for (AVMetadataItem *item in metadata)
        {
            [theMetadata addObject:CreateDictionaryFromMetadataItem(item)];
        }
        NSString *stringRep = MIUtilityCreateStringRepresentationOfObject(
                                                            theMetadata, 0);
        resultsDict = MIMakeReplyDictionary(stringRep, MIReplyErrorNoError);
    }
    else
    {
        MILog(@"Requested unknown property for movie track",
                          commandDict);
        
        resultsDict = MIMakeReplyDictionary(
                    @"Error: Requested unknown property from  movie track",
                                            MIReplyErrorUnknownProperty);
    }
    
    return resultsDict;
}

NSDictionary *MIAVGetTrackProperties(AVAssetTrack *track,
                                     NSDictionary *commandDict)
{
    NSMutableDictionary *properties;
    properties = [[NSMutableDictionary alloc] initWithCapacity:0];
    properties[MIJSONPropertyMovieTrackID] = @(track.trackID);
    properties[MIJSONPropertyMovieMediaType] = track.mediaType;
    properties[MIJSONPropertyMovieTrackEnabled] = @(track.enabled);
    NSDictionary *timeRange;
    timeRange = CFBridgingRelease(CMTimeRangeCopyAsDictionary(track.timeRange,
                                                        kCFAllocatorDefault));
    properties[MIJSONPropertyMovieTimeRange] = timeRange;
    NSString *temp = track.languageCode ? track.languageCode : @"";
    properties[MIJSONPropertyMovieLanguageCode] = temp;
    temp = track.extendedLanguageTag ? track.extendedLanguageTag : @"";
    properties[MIJSONPropertyMovieExtendedLanguageTag] = temp;
    if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
    {
        CGSize nSize = track.naturalSize;
        NSDictionary *sizeDict;
        sizeDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                    @(nSize.width), MIJSONKeyWidth,
                    @(nSize.height), MIJSONKeyHeight,
                    nil];
        properties[MIJSONPropertyMovieTrackNaturalSize] = sizeDict;
        CGAffineTransform transform = track.preferredTransform;
        NSDictionary *transDict;
        transDict = MIUtilityCreateCGAffineTransformDictionary(transform);
        properties[MIJSONKeyAffineTransform] = transDict;
        
        if ([track respondsToSelector:
             NSSelectorFromString(@"requiresFrameReordering")])
        {
            BOOL nRO = track.requiresFrameReordering;
            properties[MIJSONPropertyMovieTrackRequiresFrameReordering] = @(nRO);
        }
        float nFR = track.nominalFrameRate;
        properties[MIJSONPropertyMovieTrackNominalFrameRate] = @(nFR);
        if ([track respondsToSelector:
             NSSelectorFromString(@"minFrameDuration")])
        {
            CMTime frameDur = track.minFrameDuration;
            NSDictionary *durDict;
            durDict = CFBridgingRelease(CMTimeCopyAsDictionary(frameDur,
                                                        kCFAllocatorDefault));
            properties[MIJSONPropertyMovieTrackMinFrameDuration] = durDict;
        }
    }
    else if ([track hasMediaCharacteristic:AVMediaCharacteristicAudible])
    {
        float volume = track.preferredVolume;
        properties[MIJSONKeyMovieTrackPreferredVolume] = @(volume);
    }
    return properties.copy;
}

NSDictionary *MIAVGetMovieAssetProperties(MIBaseObject *obj, AVAsset *movie,
                                          NSDictionary *commandDict)
{
    NSMutableDictionary *propertyDict = [NSMutableDictionary new];
    propertyDict[MIJSONKeyObjectType] = obj.baseObjectType;
    propertyDict[MIJSONKeyObjectName] = obj.name;
    propertyDict[MIJSONKeyObjectReference] = @(obj.reference);
    NSNumber *numTracks = @(movie.tracks.count);
    propertyDict[MIJSONPropertyMovieNumberOfTracks] = numTracks;
    if ([movie isKindOfClass:[AVURLAsset class]])
    {
        AVURLAsset *asset = (AVURLAsset *)movie;
        NSString *path = asset.URL ? asset.URL.path : nil;
        if (path)
        {
            propertyDict[MIJSONPropertyFile] = path;
        }
    }
    NSDictionary *durDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                            movie.duration, kCFAllocatorDefault));
    propertyDict[MIJSONPropertyMovieDuration] = durDict;
    NSArray *metadataFormats = movie.availableMetadataFormats;
    if (metadataFormats)
    {
        NSString *formats;
        formats = MIUtilityCreateStringFromArrayOfStrings(@" ", metadataFormats);
        propertyDict[MIJSONPropertyMovieMetadataFormats] = formats;
    }
    return propertyDict.copy;
}

NSDictionary *MIAVGetMovieAssetProperty(AVAsset *movie,
                                        NSDictionary *commandDict)
{
    NSString *resultString;
    NSDictionary *resultsDict;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if ([propertyType isEqualToString:MIJSONPropertyMovieNumberOfTracks])
    {
        NSInteger numTracks;
        numTracks = MIAVGetNumberOfTracks(movie, commandDict);
        if (numTracks >= 0)
        {
            resultString = [[NSString alloc] initWithFormat:@"%ld",
                            (long)numTracks];
            resultsDict = MIMakeReplyDictionaryWithNumericValue(resultString,
                                            MIReplyErrorNoError, @(numTracks));
        }
        else
        {
            MILog(@"Failed to get a list of tracks", commandDict);
            resultsDict = MIMakeReplyDictionary(
                        @"Error: Failed to get a list of tracks from movie.",
                                                MIReplyErrorOperationFailed);
        }
    }
    else if([propertyType isEqualToString:MIJSONPropertyMovieMetadata])
    {
        NSString *format = commandDict[MIJSONPropertyMovieMetadataFormats];
        NSArray *metadata;
        if (format)
        {
            metadata = [movie metadataForFormat:format];
            if (!metadata)
                return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            // Only available on 10.10. Not sure what to do.
            if ([movie respondsToSelector:NSSelectorFromString(@"metadata")])
            {
                metadata = movie.metadata;
            }
            else
            {
                metadata = [movie metadataForFormat:@"com.apple.quicktime.mdta"];
            }
        }
        NSMutableArray *theMetadata = [[NSMutableArray alloc]
                                       initWithCapacity:metadata.count];
        for (AVMetadataItem *item in metadata)
        {
            [theMetadata addObject:CreateDictionaryFromMetadataItem(item)];
        }
        NSString *stringRep = MIUtilityCreateStringRepresentationOfObject(
                                                           theMetadata, 0);
        resultsDict = MIMakeReplyDictionary(stringRep, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieCommonMetadata])
    {
        NSArray *cMD = [movie commonMetadata];
        NSMutableArray *theMetadata;
        theMetadata = [[NSMutableArray alloc] initWithCapacity:cMD.count];
        for (AVMetadataItem *item in cMD)
        {
            [theMetadata addObject:CreateDictionaryFromMetadataItem(item)];
        }
        NSString *stringRep = MIUtilityCreateStringRepresentationOfObject(
                                                            theMetadata, 0);
        resultsDict = MIMakeReplyDictionary(stringRep, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyFile])
    {
        if ([movie isKindOfClass:[AVURLAsset class]])
        {
            AVURLAsset *asset = (AVURLAsset*)movie;
            NSString *path = asset.URL ? asset.URL.path : nil;
            resultsDict = MIMakeReplyDictionary(path, MIReplyErrorNoError);
        }
        else
        {
            resultString = @"Error: Path to movie file not available.";
            MILog(resultString, commandDict);
            resultsDict = MIMakeReplyDictionary(resultString,
                                        MIReplyErrorInvalidProperty);
        }
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieDuration])
    {
        NSDictionary *durationDict = CFBridgingRelease(CMTimeCopyAsDictionary(
                                        movie.duration, kCFAllocatorDefault));
        NSString *stringRep = MIUtilityCreateStringRepresentationOfObject(
                                                            durationDict, 0);
        NSNumber *durationInSecs = @(CMTimeGetSeconds(movie.duration));
        resultsDict = MIMakeReplyDictionaryWithNumericValue(stringRep,
                                            MIReplyErrorNoError, durationInSecs);
    }
    else if ([propertyType isEqualToString:MIJSONPropertyMovieMetadataFormats])
    {
        NSArray *formats = movie.availableMetadataFormats;
        if (formats)
        {
            NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                        formats);
            resultsDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
        }
        else
        {
            resultsDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
    }
    return resultsDict;
}

void MIAVSaveCompositionMapToAPNGFile(AVComposition *composition,
                                      AVVideoComposition *videoComposition,
                                      AVAudioMix *audioMix,
                                      NSString *filePath)
{
    CGImageRef image = MIAVCreateImageOfCompositionMap(composition,
                                                       videoComposition,
                                                       audioMix);
    MISaveCGImageToAPNGFileWithPath(image, filePath);
    CGImageRelease(image);
}
