//  MIFilterInputImageHandler.m
//  Moving Images
//
//  Copyright (c) 2015 Zukini Ltd

#import "MIFilterInputImageHandler.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIUtilities.h"

@import QuartzCore;

#if TARGET_OS_IPHONE
@import CoreImage;
#endif

@interface MIFilterInputImageHandler ()

@property (readonly, weak) CIFilter *_imageFilter;
@property (readonly, copy) MICGImageWrapper *_inputImage;
@property (readonly, strong) NSString *_filterImageInputKey;
@property (readonly, weak) CIFilter *_inputFilter;

@end

@implementation MIFilterInputImageHandler

-(BOOL)makeCopy
{
    return self._inputImage.makeCopy;
}

-(void)setMakeCopy:(BOOL)makeCopy
{
    if (self._inputImage)
    {
        [self._inputImage setMakeCopy:makeCopy];
        // The make copy option is not about copying the current image if there
        // is one, but instead that when an image is next obtained as input
        // for a filter in a filter chain a copy will be made. 
        [self clearImage];
    }
}

-(instancetype)initWithCIFilter:(CIFilter *)filter
            filterInputImageKey:(NSString *)filterImageInputKey
                   imageWrapper:(MICGImageWrapper *)imageWrapper
{
    self = [super init];
    if (!filter || !filterImageInputKey || !imageWrapper)
        self = nil;

    if (self)
    {
        self->__imageFilter = filter;
        self->__filterImageInputKey = filterImageInputKey.copy;
        self->__inputImage = imageWrapper;
    }
    return self;
}

-(instancetype)initWithCIFilter:(CIFilter *)filter
            filterInputImageKey:(NSString *)filterImageInputKey
                    inputFilter:(CIFilter *)inputFilter
{
    self = [super init];
    if (!filter || !filterImageInputKey || !inputFilter)
        self = nil;
    
    if (self)
    {
        self->__imageFilter = filter;
        self->__filterImageInputKey = filterImageInputKey.copy;
        self->__inputFilter = inputFilter;
    }
    return self;
}

-(BOOL)applyImageToFilterIfNecessary
{
    CIFilter *theFilter = self._imageFilter;
    if (!theFilter)
        return NO;

    BOOL result = YES;
    CIFilter *inputFilter = self._inputFilter;
    if (inputFilter)
    {
        @try
        {
            [theFilter setValue:[inputFilter outputImage]
                         forKey:self._filterImageInputKey];
        }
        @catch (NSException *exception)
        {
            result = NO;
        }
        if (!result)
        {
            MILog(@"Invalid filter key ",
                                 self._filterImageInputKey);
        }
        return result;
    }
    
    MICGImage *theImage = [self._inputImage getImageDontGenerate];
    if (theImage)
    {
        // Since we already have the image assume that it has been applied.
        return result;
    }
    
    // Lets obtain the image.
    theImage = [self._inputImage image];
    if (!theImage)
        result = NO;
    
    if (result)
    {
        CIImage *theCoreImage;
        // SaveCGImageToAPNGFile([theImage CGImage],
        // @"MIFilterInputImageHandler_applyImageToFilterIfNecessary.png");
        theCoreImage = [[CIImage alloc] initWithCGImage:[theImage CGImage]];
        if (theCoreImage && theFilter)
        {
            @try
            {
                [theFilter setValue:theCoreImage
                             forKey:self._filterImageInputKey];
            }
            @catch (NSException *exception)
            {
                result = NO;
            }
            if (!result)
            {
                MILog(@"Invalid filter key ",
                                     self._filterImageInputKey);
            }
        }
        else
            result = NO;
    }
    return result;
}

-(void)clearImage
{
    if (self._inputImage)
    {
        [self._inputImage clearImage];
    }
}

-(BOOL)managesImageForFilter:(CIFilter *)theFilter key:(NSString *)theKey
{
    return ((theFilter == self._imageFilter) &&
            [self._filterImageInputKey isEqualToString:theKey]);
}

@end
