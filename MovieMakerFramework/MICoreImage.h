//  MICoreImage.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"

@import Foundation;

@class CIFilter;

/**
 @brief A wrapper for a CoreImage filter chain, the CIImages and CIContext.
 @discussion A MICoreImage object wraps up everything needed for performing
 the complete filter operation. The only thing missing will be the input image.
 This will be supplied in the processImage message which will return an output
 image. Unlike MICGContext and MICGBitmapContext a MICoreImage object does not
 hold onto a copy of the generated image after rendering the filter chain. 
 MICGBitmapContext can provide the backing store for the CGImageRef there is
 no such possibility for MICoreImage. We don't gain enough to make the memory
 used worthwhile.
*/
@interface MICoreImage : MIBaseObject <MIBaseObjectSemiPublicInterface>

@property (readonly, assign) BOOL softwareRender;
@property (assign) BOOL useSRGBColorSpace;

/// Apply the value to the filter using the key. Return YES on success else NO.
+(BOOL)filter:(CIFilter *)theFilter setValue:(id)value forKey:(NSString *)key;

/**
 @brief Initializes a MICoreImage object.
 @param theName     The name to initialize the object with.
 @param inContext   The context that manages the object.
 @param renderDestination   Where the filter chain renders to.
 @param filterList  List of dictionaries describing filters in filter chain.
 @param softwareRender  Should the filter chain be rendered in software not GPU.
 @param srgbRender  Should render be forced to happen in the rgb color space.
 @discussion This is the designated initializer for a MICoreImage object.
*/
-(instancetype)initWithName:(NSString *)theName
                  inContext:(MIContext *)context
          renderDestination:(id <MICIRenderDestinationInterface>)dest
                 filterList:(NSArray *)filterList
             softwareRender:(BOOL)softwareRender
                 srgbRender:(BOOL)srgbRender;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end



