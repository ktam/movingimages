//  MICGBitmapContext.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"
#import "MICIRenderDestinationInterface.h"

@class MICGContext;

/**
 @class MICGBitmapContext
 @abstract A bit map graphics context defined by a preset.
 @discussion The MICGBitmapContext class is a graphics context used for drawing
 bit map images like jpeg and png files to and can be used for generating bit
 map images. Quartz drawing commands modify the pixel data.
*/
@interface MICGBitmapContext : MIBaseObject <MICreateCGImageInterface,
                                             MIBaseObjectSemiPublicInterface,
                                             MICIRenderDestinationInterface>


/**
 @brief Initialize a MICGBitmapContext object with a graphics context.
 @discussion Each preset defines a number of parameters used to create the
 bitmap context.
*/
-(instancetype)initWithPreset:(NSString *)preset
                         name:(NSString *)theName
                bitmapContext:(CGContextRef)cgContext
                    inContext:(MIContext *)context;

/// Remove this object from lists. If no other strong refs it will be deleted
-(void)close;

@end
