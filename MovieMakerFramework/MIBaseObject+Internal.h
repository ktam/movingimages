//
//  MIBaseObject_MIBaseObjext_Internal_h.h
//  MovieMaker
//
//  Created by Kevin Meaney on 15/12/2014.
//  Copyright (c) 2014 Apple Inc. All rights reserved.
//

#import "MIBaseObject.h"

@interface MIBaseObject ()

/// Finalize does close stuff except from removing from object lists.
-(void)finalize;

@end
