//  MIUtility.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

// #import "DDLog.h"

#import "DDMathParser.h"

#import "MIUtilities.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"

#if !TARGET_OS_IPHONE
#import "NSAffineTransform+DictionaryRepresentation.h"
#endif

@import QuartzCore;
@import ImageIO;

#if TARGET_OS_IPHONE
@import CoreImage;
@import MobileCoreServices;
@import UIKit;
#endif

const NSInteger MIUtilityInvalidImageIndex = -1;

BOOL MIUtilityGetIntegerFromString(NSString *string, NSInteger *value,
                                   NSDictionary *variablesDict)
{
    NSError *error = nil;
    NSNumber *result;
    result = [string numberByEvaluatingStringWithSubstitutions:variablesDict
                                                         error:&error];
    if (!result)
        return NO;
    
    if (error)
        return NO;
    
    *value = [result integerValue];
    return YES;
}

BOOL MIUtilityGetFloatFromString(NSString *string, CGFloat *value,
                                   NSDictionary *variablesDict)
{
    NSError *error = nil;
    NSNumber *result;
    result = [string numberByEvaluatingStringWithSubstitutions:variablesDict
                                            error:&error];
    if (error)
    {
        MILog(@"Error evaluating equation:", string);
        MILog(@"Variables dictionary:", variablesDict);
        MILog(@"Error description:", error.localizedDescription);
        return NO;
    }

    if (!result)
        return NO;
    
    *value = [result doubleValue];
    if (!isfinite(*value))
    {
        if (isnan(*value))
        {
            MILog(@"Result is not a number.", string);
        }
        else
        {
            MILog(@"Likely divide by zero. Result overflowed", string);
        }
        return NO;
    }

    return YES;
}

BOOL MIUtilityGetCGFloatFromID(id floatValue, CGFloat *value, NSDictionary *vars)
{
    if (!(floatValue && value))
    {
        return NO;
    }
    
    if ([floatValue isKindOfClass:[NSNumber class]])
    {
        *value = [(NSNumber *)floatValue doubleValue];
        return YES;
    }
    else if ([floatValue isKindOfClass:[NSString class]])
    {
        return MIUtilityGetFloatFromString((NSString *)floatValue, value, vars);
    }
    return NO;
}

/*
 Apple's CoreGraphic functions like CGPointMakeWithDictionaryRepresentation()
 don't define the keys used in the dictonary. I've therefore not decided
 to use them here as the lack of documentation detail implies that apple
 wants to keep implementation details private so apple can change them as it
 sees fit in future.
 
 If the following code proves to be slow, it can all be replaced with the
 equivalent corefoundation calls.
*/

BOOL MIUtilityGetCGFloatFromDictionaryNoLog(NSDictionary *dict, // in
                                            NSString *key,      // in
                                            CGFloat *floatVal,  // out
                                            NSDictionary *variablesDict) // in
{
    if (!floatVal || !key || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        return NO;
    }
    
    return MIUtilityGetCGFloatFromID(dict[key], floatVal, variablesDict);
}

BOOL MIUtilityGetCGFloatFromDictionary(NSDictionary *dict, // in
                                       NSString *key,      // in
                                       CGFloat *floatVal,  // out
                                       NSDictionary *variablesDict)  // in
{
    BOOL result = MIUtilityGetCGFloatFromDictionaryNoLog(dict, key,
                                                         floatVal, variablesDict);
    
    if (!result)
    {
        MILog(@"Missing or invalid dict.", dict);
        MILog(@"Key: ", key);
    }

    return result;
}

BOOL MIUtilityGetIntegerFromDictionaryNoLog(NSDictionary *dict, // in
                                            NSString *key,      // in
                                            NSInteger *intVal,  // out
                                            NSDictionary *variablesDict) // in
{
    if (!intVal || !key || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        return NO;
    }
    BOOL result = YES;
    NSNumber *numValue = dict[key];
    if (!numValue)
    {
        result = NO;
    }
    else if ([numValue isKindOfClass:[NSNumber class]])
    {
        *intVal = [numValue integerValue];
    }
    else if (variablesDict && [numValue isKindOfClass:[NSString class]])
    {
        NSString *string = (NSString *)numValue;
        NSError *error = nil;
        NSNumber *result;
        result = [string numberByEvaluatingStringWithSubstitutions:variablesDict
                                                             error:&error];
        if (!result)
            return NO;
        
        if (error)
            return NO;
        
        *intVal = [result integerValue];
    }
    else
    {
        result = NO;
    }
    return result;
}

BOOL MIUtilityGetImageIndexWithKey(NSDictionary *dict,
                                     NSString *key,
                                     NSInteger *imageIndex,
                                     size_t numImages)
{
    BOOL hasImageIndex = NO;
    NSNumber *imageIndexNSNum;
    imageIndexNSNum = dict[key];

    if (imageIndexNSNum)
        hasImageIndex = YES;
    
    if (hasImageIndex)
    {
        *imageIndex = MIUtilityInvalidImageIndex;
        if ([imageIndexNSNum isKindOfClass:[NSNumber class]])
        {
            NSInteger index = [imageIndexNSNum integerValue];
            if (index >= 0 && index < numImages)
                *imageIndex = index;
        }
    }
    return hasImageIndex;
}

BOOL MIUtilityDoesPropertyTypeBeginWithDictionary(NSString *propertyKey)
{
    NSRange dictRange;
    dictRange = [propertyKey rangeOfString:MIJSONPropertyDictionary];
    return dictRange.location == 0; // 0 is the beginning of the string.
}

BOOL MIUtilityGetIntegerFromDictionary(NSDictionary *dict, // in
                                       NSString *key,      // in
                                       NSInteger *intVal,  // out
                                       NSDictionary *variablesDict)
{
    BOOL result = MIUtilityGetIntegerFromDictionaryNoLog(dict, key,
                                                         intVal, variablesDict);
    if (!result)
    {
        NSString *message  = [[NSString alloc] initWithFormat:
                @"Failed to get integer from dictionary with key: %@", key];
        MILog(message, dict);
    }
    return result;
}

BOOL MIUtilityGetConstrainedCGFloatFromDictionary(NSDictionary *dict,
                                                  NSString *key,
                                                  CGFloat minVal,
                                                  CGFloat maxVal,
                                                  CGFloat *floatVal,
                                                  NSDictionary *variablesDict)
{
    BOOL result = MIUtilityGetCGFloatFromDictionaryNoLog(dict, key, floatVal,
                                                         variablesDict);
    if (!result)
        return result;
    
    if (*floatVal > maxVal)
        *floatVal = maxVal;
    
    if (*floatVal < minVal)
        *floatVal = minVal;
    return result;
}

BOOL MIUtilityGetMIBaseReferenceFromDictionary(NSDictionary *dict,
                                               MIBaseReference *baseRef)
{
    // If MIBaseReference changes to something other than a typedef to NSInteger
    // then we will have to change this function. This is an attempt to limit
    // how many places an assumption is made about what type a MIBaseReference
    // really is.
    return MIUtilityGetIntegerFromDictionaryNoLog(dict,
                        MIJSONKeyObjectReference, (NSInteger *)baseRef, nil);
}

BOOL MIUtilityGetLineWidthFromDictionary(NSDictionary *dict, CGFloat *lineWidth,
                                         NSDictionary *variablesDict)
{
    return MIUtilityGetCGFloatFromDictionaryNoLog(dict,
                                                  MIJSONKeyLineWidth,
                                                  lineWidth,
                                                  variablesDict);
}

BOOL MIUtilityGetCGSizeFromDictionary(NSDictionary *dict, CGSize *size,
                                      MI_LOG log, NSDictionary *variablesDict)
{
    if (!size || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        if (log != kMINoLog)
        {
            MILog(@"Expecting dictionary.", dict);
        }
        return NO;
    }
    
    BOOL result = YES;
    CGFloat width, height;

    if (log != kMINoLog)
    {
        result &= MIUtilityGetCGFloatFromDictionary(dict, MIJSONKeyWidth,
                                                    &width, variablesDict);
        result &= MIUtilityGetCGFloatFromDictionary(dict, MIJSONKeyHeight,
                                                    &height, variablesDict);
    }
    else
    {
        result &= MIUtilityGetCGFloatFromDictionaryNoLog(dict, MIJSONKeyWidth,
                                                         &width, variablesDict);
        result &= MIUtilityGetCGFloatFromDictionaryNoLog(dict, MIJSONKeyHeight,
                                                         &height, variablesDict);
    }
    if (result)
    {
        *size = CGSizeMake(width, height);
    }
    else
    {
        if (log != kMINoLog)
        {
            MILog(@"Failed getting size. { width, height }.", dict);
        }
    }

    return result;
}

BOOL MIUtilityGetCGSizeFromPropertyValue(id propertyValue, CGSize *size,
                                         NSDictionary *variables)
{
    if (!(propertyValue && size))
    {
        MILog(@"Missing property value or size.", nil);
        return NO;
    }

    if ([propertyValue isKindOfClass:[NSDictionary class]])
    {
        return MIUtilityGetCGSizeFromDictionary((NSDictionary *)propertyValue,
                                                size, kMILog, variables);
    }
    else if ([propertyValue isKindOfClass:[NSString class]])
    {
        // Assume the string is a json string.
        NSDictionary *dict;
        dict = MIUtilityCreateDictionaryFromJSONString(propertyValue);
        if (dict)
        {
            return MIUtilityGetCGSizeFromDictionary(dict, size, kMILog, variables);
        }
        else
        {
            // String value could not treated as a json string.
            MILog(@"Could not convert size dict into a size",
                              propertyValue);
            return NO;
        }
    }
    else
    {
        // Property value is not a known type for a CGSize
        MILog(@"Could not create a size object from property",
                          propertyValue);
        return NO;
    }
}

BOOL MIUtilityGetCGPointFromDictionary(NSDictionary *dict, CGPoint *point,
                                       MI_LOG log, NSDictionary *variablesDict)
{
    if (!point || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        if (log != kMINoLog)
        {
            MILog(@"Expecting dictionary.", dict);
        }
        return NO;
    }

    BOOL result = YES;
    CGFloat x, y;
    
    if (log == kMINoLog)
    {
        result &= MIUtilityGetCGFloatFromDictionaryNoLog(dict, MIJSONKeyX, &x,
                                                         variablesDict);
        result &= MIUtilityGetCGFloatFromDictionaryNoLog(dict, MIJSONKeyY, &y,
                                                         variablesDict);
    }
    else
    {
        result &= MIUtilityGetCGFloatFromDictionary(dict, MIJSONKeyX, &x,
                                                    variablesDict);
        result &= MIUtilityGetCGFloatFromDictionary(dict, MIJSONKeyY, &y,
                                                    variablesDict);
    }
    
    if (result)
    {
        *point = CGPointMake(x, y);
    }
    else
    {
        if (log != kMINoLog)
        {
            MILog(@"Failed getting point {x, y}.", dict);
        }
        return NO;
    }
    
    return result;
}

BOOL MIUtilityGetCGRectFromDictionary(NSDictionary *dict, CGRect *rect,
                                      MI_LOG log, NSDictionary *variablesDict)
{
    if (!rect || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        if (log != kMINoLog)
        {
            MILog(@"Expecting dictionary: ", dict);
        }
        return NO;
    }

    BOOL result = YES;
    CGPoint origin;
    CGSize size;
    
    NSDictionary *pointDict = dict[MIJSONKeyOrigin];
    NSDictionary *sizeDict = dict[MIJSONKeySize];
    
    result &= MIUtilityGetCGPointFromDictionary(pointDict, &origin,
                                                log, variablesDict);
    result &= MIUtilityGetCGSizeFromDictionary(sizeDict, &size,
                                               log, variablesDict);
    if (result)
    {
        rect->origin = origin;
        rect->size = size;
    }
    else
    {
        if (log != kMINoLog)
        {
            MILog(@"Failed getting rect {origin, size}.", dict);
        }
    }
    return result;
}

BOOL MIUtilityGetMMCGLineFromDictionary(NSDictionary *dict,
                                        MMCGLine *line,
                                        NSDictionary *variablesDict)
{
    if (!line || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        MILog(@"Expecting dictionary.", dict);
        return NO;
    }
    
    BOOL result = YES;
    CGPoint startPoint;
    CGPoint endPoint;
    
    NSDictionary *startPointDict = dict[MIJSONKeyStartPoint];
    NSDictionary *endPointDict = dict[MIJSONKeyEndPoint];
    
    result &= MIUtilityGetCGPointFromDictionary(startPointDict, &startPoint,
                                                kMILog, variablesDict);
    result &= MIUtilityGetCGPointFromDictionary(endPointDict, &endPoint,
                                                kMILog, variablesDict);
    if (result)
    {
        line->start = startPoint;
        line->end = endPoint;
    }
    else
    {
        MILog(@"Failed getting line {point, point}.", dict);
    }
    return result;
}

BOOL MIUtilityGetRadiusesFromDictionary(NSDictionary *dict, CGFloat *radiuses,
                                        NSDictionary *variablesDict)
{
    if (!radiuses || !dict || ![dict isKindOfClass:[NSDictionary class]])
    {
        MILog(@"Expecting dictionary.", dict);
        return NO;
    }
    
    BOOL result = YES;
    CGFloat value;
    result = MIUtilityGetCGFloatFromDictionaryNoLog(dict, MIJSONKeyRadius,
                                                    &value, variablesDict);
    if (result)
    {
        if (value < 1.0)
            value = 1.0;

        for (int i=0 ; i < 4 ; ++i)
            radiuses[i] = value;
    }
    else
    {
        result = YES;
        NSArray *radiusVals = dict[MIJSONKeyRadiuses];
        if (radiusVals && ([radiusVals count] == 4))
        {
            int i=0;
            for (id value in radiusVals)
            {
                if ([value isKindOfClass:[NSNumber class]])
                {
                    NSNumber *numVal = (NSNumber *)value;
                    radiuses[i] = [numVal doubleValue];
                    if (radiuses[i] < 1.0)
                        radiuses[i] = 1.0;
                }
                else if ([value isKindOfClass:[NSString class]])
                {
                    CGFloat floatVal;
                    if (MIUtilityGetFloatFromString((NSString *)value,
                                                &floatVal, variablesDict))
                    {
                        if (floatVal < 1.0)
                            floatVal = 1.0;
                        radiuses[i] = floatVal;
                    }
                    else
                    {
                        result = NO;
                        break;
                    }
                }
                else
                {
                    MILog(@"Expecting number.", value);
                    result = NO;
                    break;
                }
                i++;
            }
        }
        else
        {
            MILog(@"Expecting array.",radiusVals);
            result = NO;
        }
    }
    return result;
}

#if TARGET_OS_IPHONE
static BOOL IsRGBColorSpace(NSString *colorSpaceName)
{
    //    return [colorSpaceName containsString:@"RGB"]; // 10.10, iOS 8 only.
    if ([colorSpaceName isEqualToString:MIJSONValueDeviceRGB])
        return YES;

    return [colorSpaceName rangeOfString:@"RGB"].length > 0;
}

static BOOL IsGrayColorSpace(NSString *colorSpaceName)
{
    //    return [colorSpaceName containsString:@"Gray"]; // 10.10, iOS 8 only.
    return [colorSpaceName rangeOfString:@"Gray"].length > 0;
}
#else
static BOOL IsRGBColorSpace(NSString *colorSpaceName)
{
    if ([colorSpaceName isEqualToString:MIJSONValueDeviceRGB])
        return YES;
    if ([colorSpaceName isEqualToString:(NSString *)kCGColorSpaceGenericRGB])
        return YES;
    if ([colorSpaceName isEqualToString:
                                    (NSString *)kCGColorSpaceGenericRGBLinear])
        return YES;
    if ([colorSpaceName isEqualToString:(NSString *)kCGColorSpaceAdobeRGB1998])
        return YES;
    if ([colorSpaceName isEqualToString:(NSString *)kCGColorSpaceSRGB])
        return YES;
    return NO;
}

static BOOL IsGrayColorSpace(NSString *colorSpaceName)
{
    return [colorSpaceName isEqualToString:(NSString *)kCGColorSpaceGenericGray]
    || [colorSpaceName isEqualToString:(NSString *)kCGColorSpaceGenericGrayGamma2_2];
}
#endif

static CGColorRef CreateGrayColor(NSDictionary *colourDict,
                                  NSDictionary *variablesDict)
{
    CGColorRef colourRef = NULL;
    CGColorSpaceRef colourSpace;
#if TARGET_OS_IPHONE
    // Currently ignoring color profiles on iOS.
    // TODO: iPhone color management.
    colourSpace = CGColorSpaceCreateDeviceGray();
#else
    NSString *profileName = colourDict[MIJSONKeyColorColorProfileName];
    colourSpace = CGColorSpaceCreateWithName((CFStringRef)profileName);
#endif
    if (colourSpace)
    {
        BOOL success = YES;
        CGFloat colour[2];
        colour[1] = 1.0; // Alpha channel, defaults to 1.0
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyGray,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[0],
                                                        variablesDict);
        // Don't need to determine if successfuly obtained the alpha value
        // as it has a default value;
        if (success)
        {
            MIUtilityGetConstrainedCGFloatFromDictionary(colourDict,
                                                         MIJSONKeyAlpha,
                                                         0.0, // max value
                                                         1.0, // min value
                                                         &colour[1],
                                                         variablesDict);
            colourRef = CGColorCreate(colourSpace, colour);
        }
        CGColorSpaceRelease(colourSpace);
    }
    
    if (!colourRef)
    {
        MILog(
             @"Failed to create Gray color. Missing or invalid dictionary: ",
                          colourDict);
    }

    return colourRef;
}

static CGColorRef CreateCMYKColor(NSDictionary *colourDict,
                                  NSDictionary *variablesDict)
{
    CGColorRef colourRef = NULL;
    CGColorSpaceRef colourSpace;
#if TARGET_OS_IPHONE
    // Currently ignoring color profiles on iOS.
    // TODO: iPhone color management.
    colourSpace = CGColorSpaceCreateDeviceCMYK();
#else
    NSString *profileName = colourDict[MIJSONKeyColorColorProfileName];
    colourSpace = CGColorSpaceCreateWithName((CFStringRef)profileName);
#endif
    if (colourSpace)
    {
        BOOL success = YES;
        CGFloat colour[5];
        colour[4] = 1.0; // Alpha channel, defaults to 1.0
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyCyan,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[0],
                                                        variablesDict);
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyMagenta,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[1],
                                                        variablesDict);
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyYellow,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[2],
                                                        variablesDict);
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyCMYKBlack,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[3],
                                                        variablesDict);
        // Don't need to determine if successfuly obtained the alpha value
        // as it has a default value;
        if (success)
        {
            MIUtilityGetConstrainedCGFloatFromDictionary(colourDict,
                                                         MIJSONKeyAlpha,
                                                         0.0, // max value
                                                         1.0, // min value
                                                         &colour[4],
                                                         variablesDict);
            colourRef = CGColorCreate(colourSpace, colour);
        }
        CGColorSpaceRelease(colourSpace);
    }

    if (!colourRef)
    {
        MILog(
               @"Failed to create CMYK color. Missing or invalid dictionary: ",
                          colourDict);
    }

    return colourRef;
}

static CGColorRef CreateRGBColor(NSDictionary *colourDict,
                                 NSDictionary *variablesDict)
{
    CGColorRef colourRef = NULL;
    CGColorSpaceRef colourSpace;
#if TARGET_OS_IPHONE
    // Currently ignoring color profiles on iOS.
    // TODO: iPhone color management.
    colourSpace = CGColorSpaceCreateDeviceRGB();
#else
    NSString *profileName = colourDict[MIJSONKeyColorColorProfileName];
    if ([profileName isEqualToString:MIJSONValueDeviceRGB])
    {
        colourSpace = CGColorSpaceCreateDeviceRGB();
    }
    else
    {
        colourSpace = CGColorSpaceCreateWithName((CFStringRef)profileName);
    }
#endif
    if (colourSpace)
    {
        BOOL success = YES;
        CGFloat colour[4];
        colour[3] = 1.0; // Alpha channel, defaults to 1.0
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyRed,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[0],
                                                        variablesDict);
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyGreen,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[1],
                                                        variablesDict);
        success &= MIUtilityGetConstrainedCGFloatFromDictionary(
                                                        colourDict,
                                                        MIJSONKeyBlue,
                                                        0.0, // max value
                                                        1.0, // min value
                                                        &colour[2],
                                                        variablesDict);

        // Don't need to determine if successfuly obtained the alpha value
        // as it has a default value;
        if (success)
        {
            MIUtilityGetConstrainedCGFloatFromDictionary(colourDict,
                                                         MIJSONKeyAlpha,
                                                         0.0, // max value
                                                         1.0, // min value
                                                         &colour[3],
                                                         variablesDict);
            colourRef = CGColorCreate(colourSpace, colour);
        }
        CGColorSpaceRelease(colourSpace);
    }
    
    if (!colourRef)
    {
        MILog(
        @"Failed to create RGB color. Missing or invalid dictionary: ",
                          colourDict);
    }
    
    return colourRef;
}

CGColorRef MIUtilityCreateColorFromDictionary(NSDictionary *colorDict,
                                              NSDictionary *variablesDict)
{
    if (!colorDict || ![colorDict isKindOfClass:[NSDictionary class]])
        return NULL;

    CGColorRef colorRef = NULL;
    NSString *profileName = colorDict[MIJSONKeyColorColorProfileName];
    if (IsRGBColorSpace(profileName))
    {
        colorRef = CreateRGBColor(colorDict, variablesDict);
    }
    else if (IsGrayColorSpace(profileName))
    {
        colorRef = CreateGrayColor(colorDict, variablesDict);
    }
    else if ([profileName rangeOfString:@"CMYK"].length > 0)
    {
        colorRef = CreateCMYKColor(colorDict, variablesDict);
    }
    return colorRef;
}

CGColorRef MIUtilityCreateColorFromString(NSString *colorString)
{
    if (!colorString || ![colorString isKindOfClass:[NSString class]])
        return NULL;

    if (colorString.length != 7 && [colorString characterAtIndex:0] != '#')
    {
        MILog(@"Badly formatted hex color string", colorString);
        return NULL;
    }

    CGColorRef colorRef = NULL;
    NSString *subString = [colorString substringFromIndex:1];
    NSScanner *scanner = [NSScanner scannerWithString:subString];
    unsigned int baseColor;
    
    [scanner setCharactersToBeSkipped:[NSCharacterSet symbolCharacterSet]];
    if (![scanner scanHexInt:&baseColor])
    {
        MILog(@"Failed to create RGB color. Missing or invalid hex color string: ",
              colorString);
        return NULL;
    }
    
    CGFloat colour[4];
    colour[3] = 1.0;
    colour[0] = ((baseColor & 0xFF0000) >> 16) / 255.0f;
    colour[1] = ((baseColor & 0x00FF00) >>  8) / 255.0f;
    colour[2]  =  (baseColor & 0x0000FF) / 255.0f;
    CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceSRGB);
    colorRef = CGColorCreate(colorSpace, colour);
    CGColorSpaceRelease(colorSpace);
    return colorRef;
}

CGColorRef MIUtilityCreateColor(id colorRep, NSDictionary *variablesDict)
{
    if (!colorRep)
        return NULL;
    
    if ([colorRep isKindOfClass:[NSString class]])
        return MIUtilityCreateColorFromString(colorRep);
    else if ([colorRep isKindOfClass:[NSDictionary class]])
        return MIUtilityCreateColorFromDictionary(colorRep, variablesDict);

    return NULL;
}

NSDictionary *MIUtilityCalculateGraphicSizeOfTest(NSDictionary *command,
                                                  NSDictionary *variables)
{
    NSDictionary *textDictionary;
    textDictionary = MIUtilityCreateDictionaryFromCommand(command, variables);
	// Assumes that this is a dictionary of key values suitable for determining
    // the size of text.
    NSDictionary *replyDict;
    NSString *theString = textDictionary[MIJSONKeyStringText];
    
    NSString *postScriptFontName;
    postScriptFontName = textDictionary[MIJSONKeyStringPostscriptFontName];
    NSNumber *fontSizeRef = textDictionary[MIJSONKeyStringFontSize];
    
    NSString *uiFontTypeString = textDictionary[MIJSONKeyUIFontType];
    CTFontUIFontType theUIFontType = kCTFontUIFontNone;
    if (uiFontTypeString)
        theUIFontType = MIUtilityGetUserInterfaceFontType(uiFontTypeString);
    
    // If we have a postscript font name, then we also need a font size ref.
    // The UIFont type doesn't need a font size, as it has an inbuilt default
    // size. Therefore only make sure we have a font size if we also have a
    // a postscript font.
    if (postScriptFontName && !fontSizeRef)
    {
        MILog(@"Missing font size or postscript font name", command);
        replyDict = MIMakeReplyDictionary(
                            @"Error: Missing font size or postscript font name",
                            MIReplyErrorMissingOption);
        return replyDict;
    }
    
    if (!theString)
    {
        MILog(@"Missing text to calculate size of", command);
        replyDict = MIMakeReplyDictionary(
                          @"Error: Missing text to calculate size of",
                          MIReplyErrorMissingOption);
        return replyDict;
    }
    
    // We have the necessary information to display the string therefore
    // try and get any of the optional string drawing parameters.
    CGFloat fontSize = 0.0;
    NSNumber *strokeWidth = textDictionary[MIJSONKeyStringStrokeWidth];
    CTFontRef font = NULL;
    if (postScriptFontName)
    {
        fontSize = [fontSizeRef doubleValue];
        font = CTFontCreateWithName((__bridge CFStringRef)
                                    (postScriptFontName), fontSize, NULL);
    }
    else if (theUIFontType != kCTFontUIFontNone)
    {
        if (fontSizeRef)
            fontSize = [fontSizeRef doubleValue];
        
        font = CTFontCreateUIFontForLanguage(theUIFontType, fontSize, NULL);
    }
    
    if (!font)
    {
        MILog(@"Couldn't create font", command);
        replyDict = MIMakeReplyDictionary(
                                  @"Error: Couldn't create font",
                                  MIReplyErrorOperationFailed);
        return replyDict;
    }
    NSMutableDictionary *attributesDict = [[NSMutableDictionary alloc]
                                           initWithCapacity:0];
    [attributesDict setObject:(__bridge id)(font)
                       forKey:(__bridge NSString *)(kCTFontAttributeName)];
    if (strokeWidth)
        [attributesDict setObject:strokeWidth
                           forKey:(__bridge id)kCTStrokeWidthAttributeName];
    
    NSAttributedString *theAttrString = NULL;
    theAttrString = [[NSAttributedString alloc] initWithString:theString
                                                    attributes:attributesDict];
    // CTLineGetTypographicBounds - works on OS X but not on iOS.
    // CTLineGetBoundsWithOptions
    CTLineRef line = CTLineCreateWithAttributedString(
								(__bridge CFAttributedStringRef)theAttrString);
    if (line)
    {
        CGFloat width, height;
        width = CTLineGetTypographicBounds(line, &height, NULL, NULL);
        replyDict = @{ MIJSONKeyWidth : @(width), MIJSONKeyHeight : @(height) };
        CFRelease(line);
    }
    CFRelease(font);
    return MIUtilitySaveDictionaryToDestination(command, replyDict, NO);
}

/**
 This will return the string representation of an object. For a NSString
 object it just returns the string, for an NSNumber just request it's string
 value. For NSArray and NSDictionary this function will return a json
 representation.
*/
NSString *MIUtilityCreateStringRepresentationOfObject(id object,
                                                      NSJSONWritingOptions opts)
{
    if ([object isKindOfClass:[NSString class]])
        return object;
    else if ([object isKindOfClass:[NSNumber class]])
        return [object stringValue];
    else if ([object isKindOfClass:[NSArray class]])
    {
        NSArray *array = object;
        BOOL canJSONSerialize = [NSJSONSerialization isValidJSONObject:array];
        if (canJSONSerialize)
        {
            NSData *data;
            data = [NSJSONSerialization dataWithJSONObject:array
                                                   options:opts
                                                     error:NULL];
            return [[NSString alloc] initWithData:data
                                         encoding:NSUTF8StringEncoding];
        }
        else
            return [array description];
    }
    else if ([object isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dict = object;
        BOOL canJSONSerialize = [NSJSONSerialization isValidJSONObject:dict];
        if (canJSONSerialize)
        {
            NSData *data;
            data = [NSJSONSerialization dataWithJSONObject:dict
                                                   options:opts
                                                     error:NULL];
            return [[NSString alloc] initWithData:data
                                         encoding:NSUTF8StringEncoding];
        }
        else
            return [dict description];
    }
    return [object description];
}

NSString *MIUtilityCreateStringFromArrayOfStrings(NSString *delimiter,
                                                  NSArray *strings)
{
    // Concatenate the NSArray result into a single string.
    NSMutableString *result = [[NSMutableString alloc] initWithCapacity:0];
    BOOL notFirst = NO;
    for (NSString *type in strings)
    {
        if (notFirst && delimiter && delimiter.length)
            [result appendString:delimiter];
        
        [result appendString:type];
        notFirst = YES;
    }
    NSString *resultStr = [[NSString alloc] initWithString:result];
    return resultStr;
}

NSString *MIUtilityGetDictionaryPropertyWithKey(NSDictionary *dict,
                                                NSString *key)
{
    NSString *resultString = NULL;
    if (!dict || !key)
    {
        MILog(@"Expecting dictionary.", dict);
        return resultString;
    }

    NSCharacterSet *dotSet;
    dotSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
    
    NSArray *elements;
    elements = [key componentsSeparatedByCharactersInSet:dotSet];
    
    if ([elements count] == 1)
        return MIUtilityCreateStringRepresentationOfObject(dict, 0);
    
    id tempObj;
    NSDictionary *tempDict = dict;
    NSArray *tempArray = NULL;
    NSInteger lastElement = elements.count - 1;
    for (int i = 1 ; i <= lastElement ; ++i)
    {
        if (tempDict)
        {
            tempObj = [tempDict objectForKey:elements[i]];
            if (tempObj == NULL)
                break;
        }
        else if (tempArray)
        {
            // We need to convert the element in the dictionary path into
            // an integer index into the array. If we fail to do that or
            // the value is beyond the array bounds then we should bail.
            NSString *element = elements[i];
            NSScanner *scanner = [[NSScanner alloc] initWithString:element];
            NSInteger index;
            BOOL gotIndex = [scanner scanInteger:&index];
            if (gotIndex == FALSE)
                break;
            if (index < 0 || index >= tempArray.count)
                break;
            tempObj = tempArray[index];
        }
        if (i == lastElement)
        {
            resultString = MIUtilityCreateStringRepresentationOfObject(tempObj,
                                                                       0);
        }
        else
        {
            if ([tempObj isKindOfClass:[NSDictionary class]])
            {
                tempDict = (NSDictionary *)tempObj;
                tempArray = NULL;
            }
            else if ([tempObj isKindOfClass:[NSArray class]])
            {
                tempArray = (NSArray *)tempObj;
                tempDict = NULL;
            }
            else
                break;
        }
    }
    
    if (!resultString)
    {
        NSString *message = [[NSString alloc] initWithFormat:
                   @"No matching key \"%@\" in dictionary.", key];
        MILog(message, dict);
    }
    return resultString;
}

NSDictionary *MIUtilityGetUserInterfaceFontTypeDictionary()
{
    static NSDictionary *userInterfaceFontDictionary = NULL;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        userInterfaceFontDictionary = @{
        MIJSONValueUIFontUser: @(kCTFontUIFontUser),
        MIJSONValueUIFontFixedPitch : @(kCTFontUIFontUserFixedPitch),
        MIJSONValueUIFontSystem : @(kCTFontUIFontSystem),
        MIJSONValueUIFontEmphasizedSystem : @(kCTFontUIFontEmphasizedSystem),
        MIJSONValueUIFontSmallSystem : @(kCTFontUIFontSmallSystem),
        MIJSONValueUIFontSmallEmphasizedSytem :
                                        @(kCTFontUIFontSmallEmphasizedSystem),
        MIJSONValueUIFontMiniSystem : @(kCTFontUIFontMiniSystem),
        MIJSONValueUIFontMiniEmphasizedSystem :
                                        @(kCTFontUIFontMiniEmphasizedSystem),
        MIJSONValueUIFontViews : @(kCTFontUIFontViews),
        MIJSONValueUIFontApplication : @(kCTFontUIFontApplication),
        MIJSONValueUIFontLabel : @(kCTFontUIFontLabel),
        MIJSONValueUIFontMenuTitle : @(kCTFontUIFontMenuTitle),
        MIJSONValueUIFontMenuItem : @(kCTFontUIFontMenuItem),
        MIJSONValueUIFontWindowTitle : @(kCTFontUIFontWindowTitle),
        MIJSONValueUIFontPushButton : @(kCTFontUIFontWindowTitle),
        MIJSONValueUIFontSystemDetail : @(kCTFontUIFontSystemDetail),
        MIJSONValueUIFontEmphasizedSystemDetail :
            @(kCTFontUIFontEmphasizedSystemDetail),
        MIJSONValueUIFontToolbar : @(kCTFontUIFontToolbar),
        MIJSONValueUIFontSmallToolbar : @(kCTFontUIFontSmallToolbar),
        MIJSONValueUIFontMessage : @(kCTFontUIFontMessage),
        MIJSONValueUIFontToolTip : @(kCTFontUIFontToolTip),
        MIJSONValueUIFontControlContent : @(kCTFontUIFontControlContent)
        };
    });

    return userInterfaceFontDictionary;
}

CTFontUIFontType MIUtilityGetUserInterfaceFontType(NSString *uiFontTypeString)
{
    NSDictionary *userInterfaceFontDictionary;
    userInterfaceFontDictionary = MIUtilityGetUserInterfaceFontTypeDictionary();
    CTFontUIFontType uiFontType = kCTFontUIFontNone;
    NSNumber *uiFontTypeRef = userInterfaceFontDictionary[uiFontTypeString];
    if (uiFontTypeRef)
    {
        NSInteger fontType = [uiFontTypeRef integerValue];
        uiFontType = (CTFontUIFontType)fontType;
    }
    else
    {
        MILog(@"UIFontName: ", uiFontTypeString);
    }
    return uiFontType;
}

NSArray *MIUtilityGetListOfUserInterfaceFonts()
{
    NSDictionary *userInterfaceFontDict;
    userInterfaceFontDict = MIUtilityGetUserInterfaceFontTypeDictionary();
    NSArray *keys = userInterfaceFontDict.allKeys;
    return keys;
}

NSString *MIUtilityConvertDictionaryToJSONString(NSDictionary *theDict)
{
    NSString *jsonString;
    if ([NSJSONSerialization isValidJSONObject:theDict])
    {
        NSData *data;
        data = [NSJSONSerialization dataWithJSONObject:theDict
                                               options:0
                                                 error:NULL];
        NSString *temp = [[NSString alloc] initWithData:data
                                          encoding:NSUTF8StringEncoding];
        jsonString = [NSString stringWithString:temp];
    }
    
    if (!jsonString)
    {
        MILog(@"Expecting dictionary: ", theDict);
    }
    return jsonString;
}

void MIUtilityMakeSureParentDirectoryOfFilePathExists(NSString *filePath)
{
    NSString *dirPath = [filePath stringByDeletingLastPathComponent];
    [[NSFileManager defaultManager] createDirectoryAtPath:dirPath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:nil];
}

BOOL MIUtilityWriteDictionaryToPLISTFile(NSDictionary *dictionary,
                                             NSString *path)
{
    BOOL result = NO;
    NSString *fullPath = [path stringByExpandingTildeInPath];
    if (fullPath)
    {
        MIUtilityMakeSureParentDirectoryOfFilePathExists(fullPath);
        result = [dictionary writeToFile:fullPath atomically:YES];
    }
    
    return result;
}

BOOL MIUtilityWriteDictionaryToJSONFile(NSDictionary *dictionary,
                                            NSString *path)
{
    BOOL result = NO;
    
    NSString *fullPath = [path stringByExpandingTildeInPath];
    
    if (fullPath && [NSJSONSerialization isValidJSONObject:dictionary])
    {
        MIUtilityMakeSureParentDirectoryOfFilePathExists(fullPath);
        NSOutputStream *outStream;
        outStream = [[NSOutputStream alloc] initToFileAtPath:fullPath append:NO];
        [outStream open];
        NSInteger bytesWritten = [NSJSONSerialization
                                  writeJSONObject:dictionary
                                         toStream:outStream
                                          options:NSJSONWritingPrettyPrinted
                                            error:NULL];
        [outStream close];
        if (bytesWritten)
            result = YES;
    }
    
    return result;
}

NSDictionary *MIUtilityCreateDictionaryFromJSONString(NSString *jsonString)
{
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *theDict = NULL;
    theDict =[NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    
    if (theDict == NULL || ![theDict isKindOfClass:[NSDictionary class]])
        theDict = NULL;
    
    return theDict;
}

NSDictionary *MIUtilityCreateSizeDictionaryFromCGSize(CGSize size)
{
    return @{
             MIJSONKeyWidth : @(size.width),
             MIJSONKeyHeight : @(size.height)
           };
}

BOOL MakeCGAffineTransformFromDictionary(NSDictionary *transformDict,
                                         CGAffineTransform *affine)
{
    CGAffineTransform transform;
    NSNumber *tempVal;
    tempVal = transformDict[MIJSONKeyAffineTransformM11];
    if (!tempVal || ![tempVal isKindOfClass:[NSNumber class]])
        return NO;
    
    transform.a = [tempVal doubleValue];
    tempVal = transformDict[MIJSONKeyAffineTransformM12];
    if (tempVal && [tempVal isKindOfClass:[NSNumber class]])
        transform.b = [tempVal doubleValue];
    else
        return NO;
    
    tempVal = transformDict[MIJSONKeyAffineTransformM21];
    if (tempVal && [tempVal isKindOfClass:[NSNumber class]])
        transform.c = [tempVal doubleValue];
    else
        return NO;
    
    tempVal = transformDict[MIJSONKeyAffineTransformM22];
    if (tempVal && [tempVal isKindOfClass:[NSNumber class]])
        transform.d = [tempVal doubleValue];
    else
        return NO;
    
    tempVal = transformDict[MIJSONKeyAffineTransformtX];
    if (tempVal && [tempVal isKindOfClass:[NSNumber class]])
        transform.tx = [tempVal doubleValue];
    else
        return NO;
    
    tempVal = transformDict[MIJSONKeyAffineTransformtY];
    if (tempVal && [tempVal isKindOfClass:[NSNumber class]])
        transform.ty = [tempVal doubleValue];
    else
        return NO;
    
    *affine = transform;
    return YES;
}

BOOL MIMakeCGAffineTransformFromDictionary(NSDictionary *transformDict,
                                           CGAffineTransform *affine)
{
    if (!(transformDict && [transformDict isKindOfClass:[NSDictionary class]]))
    {
        MILog(@"Transformation dict missing or not a dictionary.",
                          transformDict);
        return NO;
    }
    
    return MakeCGAffineTransformFromDictionary(transformDict, affine);
}

BOOL MakeCGAffineTransformFromArray(NSArray *transformations,
                                    CGAffineTransform *affine,
                                    NSDictionary *variables)
{
    CGAffineTransform localT = CGAffineTransformIdentity;
    for (NSDictionary *transform in transformations)
    {
        if (![transform isKindOfClass:[NSDictionary class]])
        {
            MILog(@"Transform missing or not a dictionary.",
                              transform);
            return NO;
        }
        
        NSString *transformType = transform[MIJSONKeyTransformationType];
        if ([transformType isEqualToString:MIJSONValueTranslate])
        {
            NSDictionary *translateDict = transform[MIJSONKeyTranslation];
            CGPoint translate;
            BOOL gotVal = MIUtilityGetCGPointFromDictionary(translateDict,
                                                            &translate, kMILog,
                                                            variables);
            if (gotVal)
            {
                localT = CGAffineTransformTranslate(localT, translate.x,
                                                    translate.y);
            }
            else
            {
                MILog(@"Invalid translate transform dictionary.",
                                  transform);
                return NO;
            }
        }
        else if ([transformType isEqualToString:MIJSONValueScale])
        {
            NSDictionary *scaleDict = transform[MIJSONKeyScale];
            CGPoint scale;
            BOOL gotVal = MIUtilityGetCGPointFromDictionary(scaleDict,
                                                            &scale, kMILog,
                                                            variables);
            if (gotVal)
            {
                localT = CGAffineTransformScale(localT, scale.x, scale.y);
            }
            else
            {
                MILog(@"Invalid scale transform dictionary.",
                                  transform);
                return NO;
            }
        }
        else if ([transformType isEqualToString:MIJSONValueRotate])
        {
            CGFloat value;
            BOOL gotVal = MIUtilityGetCGFloatFromDictionary(transform,
                                                            MIJSONKeyRotation,
                                                            &value,
                                                            variables);
            if (gotVal)
            {
                localT = CGAffineTransformRotate(localT, value);
            }
            else
            {
                MILog(@"Invalid rotate transform dictionary.",
                                  transform);
                return NO;
            }
        }
        else
        {
            MILog(@"Invalid transform specified.",
                              transform);
            return NO;
        }
    }
    *affine = localT;
    return YES;
}

BOOL MIMakeCGAffineTransformFromArray(NSArray *transformations,
                                      CGAffineTransform *affine,
                                      NSDictionary *variables)
{
    if (!(transformations && [transformations isKindOfClass:[NSArray class]]))
    {
        MILog(@"Transformations missing or not an array.",
                          transformations);
        return NO;
    }
    return MakeCGAffineTransformFromArray(transformations, affine, variables);
}

BOOL MIMakeCGAffineTransform(id inputTransformData,
                             CGAffineTransform *affine,
                             NSDictionary *variables)
{
    if (!inputTransformData)
    {
        // MILog(@"inputTransformData is null.", NULL);
        return NO;
    }
    
    if ([inputTransformData isKindOfClass:[NSDictionary class]])
    {
        return MakeCGAffineTransformFromDictionary(inputTransformData, affine);
    }
    else if ([inputTransformData isKindOfClass:[NSArray class]])
    {
        return MakeCGAffineTransformFromArray(inputTransformData, affine,
                                              variables);
    }

    MILog(@"inputTransformData is not an Array or dictionary.",
                      inputTransformData);
    return NO;
}

NSDictionary *MIUtilityCreateCGAffineTransformDictionary(
                                                        CGAffineTransform trans)
{
    NSMutableDictionary *newDict;
    newDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    newDict[MIJSONKeyAffineTransformM11] = @(trans.a);
    newDict[MIJSONKeyAffineTransformM12] = @(trans.b);
    newDict[MIJSONKeyAffineTransformM21] = @(trans.c);
    newDict[MIJSONKeyAffineTransformM22] = @(trans.d);
    newDict[MIJSONKeyAffineTransformtX] = @(trans.tx);
    newDict[MIJSONKeyAffineTransformtY] = @(trans.ty);
    return [[NSDictionary alloc] initWithDictionary:newDict];
}

NSArray *MIUtilityCreateArraySuitableForPlistOrJSON(NSArray *inputArray)
{
    if (!inputArray || inputArray.count == 0)
        return inputArray;
    
    NSMutableArray *newArray = [[NSMutableArray alloc] initWithCapacity:0];
    for (NSObject *object in inputArray)
    {
        NSObject *value = object;
        if ([value isKindOfClass:[NSDictionary class]])
            value = MIUtilityCreateDictionarySuitableForPlistOrJSON(
                                                        (NSDictionary *)value);
        else if ([value isKindOfClass:[NSArray class]])
            value = MIUtilityCreateArraySuitableForPlistOrJSON(
                                                            (NSArray *)value);
        else if ([value isKindOfClass:[CIVector class]])
            value = [(CIVector *)value stringRepresentation];
        else if ([value isKindOfClass:[NSURL class]])
            value = [(NSURL *)value absoluteString];
        else if ([value isKindOfClass:[CIColor class]])
            value = [(CIColor *)value stringRepresentation];
        else if ([value isKindOfClass:[NSData class]])
            value = [(NSData *)value base64EncodedStringWithOptions:0];
#if TARGET_OS_IPHONE
        else if ([value isKindOfClass:[NSNumber class]])
            value = value;
        else if ([value isKindOfClass:[NSValue class]])
        {
            NSValue *theValue = (NSValue *)value;
            const char *affineEncode = @encode(CGAffineTransform);
            if (strcmp([theValue objCType], affineEncode) == 0)
            {
                CGAffineTransform theTrans;
                [theValue getValue:&theTrans];
                value = MIUtilityCreateCGAffineTransformDictionary(theTrans);
            }
        }
#elif TARGET_OS_MAC
        else if ([value isKindOfClass:[NSAffineTransform class]])
            value = [(NSAffineTransform *)value dictionaryRepresentation];
#endif
        [newArray addObject:value];
    }
    return [[NSArray alloc] initWithArray:newArray];
}

NSDictionary *MIUtilityCreateDictionarySuitableForPlistOrJSON(
                                                NSDictionary *inputDictionary)
{
    // If an empty dictionary then just return the input dictionary.
    if (!inputDictionary || inputDictionary.count == 0)
        return inputDictionary;
    
    NSMutableDictionary *newDict;
    newDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    @autoreleasepool
    {
        NSArray *allKeys = [inputDictionary allKeys];
        
        for (NSString *key in allKeys)
        {
            NSObject *value = [inputDictionary objectForKey:key];
            if ([value isKindOfClass:[NSDictionary class]])
                value = MIUtilityCreateDictionarySuitableForPlistOrJSON(
                                                        (NSDictionary *)value);
            else if ([value isKindOfClass:[NSArray class]])
                value = MIUtilityCreateArraySuitableForPlistOrJSON(
                                                            (NSArray *)value);
            else if ([value isKindOfClass:[CIVector class]])
                value = [(CIVector *)value stringRepresentation];
            else if ([value isKindOfClass:[NSURL class]])
                value = [(NSURL *)value absoluteString];
            else if ([value isKindOfClass:[CIColor class]])
                value = [(CIColor *)value stringRepresentation];
            else if ([value isKindOfClass:[NSData class]])
                value = [(NSData *)value base64EncodedStringWithOptions:0];
#if TARGET_OS_IPHONE
            else if ([value isKindOfClass:[NSNumber class]])
                value = value;
            else if ([value isKindOfClass:[NSValue class]])
            {
                NSValue *theValue = (NSValue *)value;
                const char *affineEncode = @encode(CGAffineTransform);
                if (strcmp([theValue objCType], affineEncode) == 0)
                {
                    CGAffineTransform trans;
                    [theValue getValue:&trans];
                    value = MIUtilityCreateCGAffineTransformDictionary(trans);
                }
            }
#elif TARGET_OS_MAC
            else if ([value isKindOfClass:[NSAffineTransform class]])
                value = [(NSAffineTransform *)value dictionaryRepresentation];
#endif

            newDict[key] = value;
        }
    }
    
    return [[NSDictionary alloc] initWithDictionary:newDict];
}

BOOL MIUtilityGetBOOLValue(NSObject *value, BOOL *isError)
{
    if (isError)
    {
        *isError = NO;
    }
    
    if (value)
    {
        if ([value isKindOfClass:[NSString class]])
        {
            return [(NSString *)value isEqualToString:MIJSONValueYes];
        }
        else if ([value isKindOfClass:[NSNumber class]])
        {
            return [(NSNumber *)value boolValue];
        }
        else if (isError)
        {
            *isError = YES;
        }
    }
    else if (isError)
    {
        *isError = YES;
    }
    return NO;
}

BOOL MIUtilityGetBOOLPropertyWithDefault(NSDictionary *dict, NSString *key,
                                           BOOL theDefault)
{
    NSString *propVal = dict[key];
    if (propVal)
    {
        if ([propVal isKindOfClass:[NSNumber class]])
        {
            return [propVal boolValue];
        }
        else if ([propVal isKindOfClass:[NSString class]])
        {
            if (theDefault)
            {
                if ([propVal isEqualToString:MIJSONValueNo])
                    return NO;
                else
                    return YES;
            }
            else
            {
                if ([propVal isEqualToString:MIJSONValueYes])
                    return YES;
                else
                    return NO;
            }
        }
        else
        {
            return theDefault;
        }
    }
    else
    {
        return theDefault;
    }
}

NSString *MIUtilityDoesCommandDictContainDestination(NSDictionary *commandDict)
{
    NSString *saveResultsType = commandDict[MIJSONKeySaveResultsType];
    NSString *errorString;
    if (!saveResultsType)
    {
        errorString = @"Missing saveresultstype";
        //        MILog(@"Missing saveresultstype", commandDict);
        return errorString;
    }

    NSString *propertyPath;
    NSString *jsonPath;
    BOOL hasJSONString = NO;
    BOOL dictionaryObject = NO;

    if ([saveResultsType isEqualToString:MIJSONPropertyDictionaryObject])
    {
        dictionaryObject = YES;
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyPropertyFilePath])
    {
        propertyPath = commandDict[MIJSONKeySaveResultsTo];
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyJSONFilePath])
    {
        jsonPath = commandDict[MIJSONKeySaveResultsTo];
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyJSONString])
    {
        hasJSONString = YES;
    }

    if (!(propertyPath || jsonPath || hasJSONString || dictionaryObject))
    {
        errorString = @"Save location unspecified";
        // MILog(@"Save location unspecified.", commandDict);
        return errorString;
    }
    return nil;
}

NSString *MIUtilityGetFilePathFromCommandWithKeyAndContext(
                                                    NSDictionary *commandDict,
                                                    NSString *pathKey,
                                                    NSDictionary *variables)
{
    NSString *filePath = commandDict[pathKey];
    NSString *substitution = commandDict[MIJSONPropertyPathSubstitution];
    if (substitution)
    {
        NSString *temp = variables[substitution];
        if (!temp)
        {
            MILog(@"Path substitution key failed:", substitution);
            MILog(@"Variables dictionary:", variables);
        }
        filePath = temp;
    }
    if (!(filePath && [filePath isKindOfClass:[NSString class]]))
    {
        return nil;
    }
    return [filePath stringByExpandingTildeInPath];
}

NSString *MIUtilityGetFilePathFromCommandAndVariables(NSDictionary *commandDict,
                                                    NSDictionary *variables)
{
    return MIUtilityGetFilePathFromCommandWithKeyAndContext(commandDict,
                                                            MIJSONPropertyFile,
                                                            variables);
}

NSDictionary *MIUtilityCreateDictionaryFromCommand(NSDictionary *commandDict,
                                                   NSDictionary *variables)
{
    NSString *getDataFromType = commandDict[MIJSONKeyGetDataType];
    if (!getDataFromType || ![getDataFromType isKindOfClass:[NSString class]])
        return nil;

    NSDictionary *result;
    if ([getDataFromType isEqualToString:MIJSONPropertyDictionaryObject])
    {
        result = commandDict[MIJSONPropertyValue];
        if (!result)
            result = commandDict[MIJSONKeyInputData];
    }
    else if ([getDataFromType isEqualToString:MIJSONPropertyJSONString])
    {
        NSString *jsonString = commandDict[MIJSONPropertyValue];
        if (!jsonString)
            jsonString = commandDict[MIJSONKeyInputData];
        
        result = MIUtilityCreateDictionaryFromJSONString(jsonString);
    }
    else if ([getDataFromType isEqualToString:MIJSONPropertyPropertyFilePath])
    {
        NSString *filePath = MIUtilityGetFilePathFromCommandWithKeyAndContext(
                                commandDict, MIJSONPropertyValue, variables);
        if (!filePath)
        {
            filePath = MIUtilityGetFilePathFromCommandWithKeyAndContext(
                        commandDict, MIJSONPropertyPropertyFilePath, variables);
        }
        
        if (filePath)
        {
            result = [NSDictionary dictionaryWithContentsOfFile:filePath];
        }
    }
    else if ([getDataFromType isEqualToString:MIJSONPropertyJSONFilePath])
    {
        NSString *filePath = MIUtilityGetFilePathFromCommandWithKeyAndContext(
                                    commandDict, MIJSONPropertyValue, variables);
        if (!filePath)
        {
            filePath = MIUtilityGetFilePathFromCommandWithKeyAndContext(
                        commandDict, MIJSONPropertyJSONFilePath, variables);
        }

        if (filePath)
        {
            NSFileManager *fileManager = [NSFileManager defaultManager];
            BOOL isDirectory;
            BOOL fileExists = [fileManager fileExistsAtPath:filePath
                                                isDirectory:&isDirectory];
            
            id container;
            if (fileExists)
            {
                NSInputStream *inStream;
                inStream = [[NSInputStream alloc] initWithFileAtPath:filePath];
                [inStream open];
                container = [NSJSONSerialization JSONObjectWithStream:inStream
                                                              options:0
                                                                error:nil];
                if (container && [container isKindOfClass:[NSDictionary class]])
                    result = container;
            }
        }
    }
    return result;
}

NSDictionary *MIUtilitySaveDictionaryToDestination(NSDictionary *commandDict,
                                                     NSDictionary *resultDict,
                                                     BOOL defaultToReplyDict)
{
    NSDictionary *replyDict;
    if (!resultDict)
    {
        MILog(@"Missing results dictionary.", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing results dictionary",
                                          MIReplyErrorOperationFailed);
        return replyDict;
    }
    
    NSString *saveResultsType = commandDict[MIJSONKeySaveResultsType];
    
    NSString *propertyPath;
    NSString *jsonPath;
    BOOL hasJSONString = NO;
    BOOL dictionaryObject = NO;
    
    if ([saveResultsType isEqualToString:MIJSONPropertyDictionaryObject])
    {
        dictionaryObject = YES;
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyPropertyFilePath])
    {
        propertyPath = commandDict[MIJSONKeySaveResultsTo];
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyJSONFilePath])
    {
        jsonPath = commandDict[MIJSONKeySaveResultsTo];
    }
    else if ([saveResultsType isEqualToString:MIJSONPropertyJSONString])
    {
        hasJSONString = YES;
    }
    else if (defaultToReplyDict)
    {
        dictionaryObject = YES;
    }
    else
    {
        hasJSONString = YES;
    }
    
    if (dictionaryObject)
    {
        replyDict = MIMakeReplyDictionaryWithDictionaryValue(resultDict, NO);
    }
    else if (hasJSONString)
    {
        if ([NSJSONSerialization isValidJSONObject:resultDict])
        {
            NSData *data;
            data = [NSJSONSerialization dataWithJSONObject:resultDict
                                                   options:0
                                                     error:NULL];
            NSString *resString;
            resString = [[NSString alloc]
                         initWithData:data
                         encoding:NSUTF8StringEncoding];
            replyDict = MIMakeReplyDictionary(resString,
                                              MIReplyErrorNoError);
        }
        else
        {
            MILog(@"Invalid JSON object", resultDict);
            replyDict = MIMakeReplyDictionary(@"Error: invalid JSON object",
                                              MIReplyErrorInvalidOption);
        }
    }
    else
    {
        BOOL written = NO;
        if (propertyPath)
        {
            written = MIUtilityWriteDictionaryToPLISTFile(resultDict,
                                                          propertyPath);
        }
        else
        {
            written = MIUtilityWriteDictionaryToJSONFile(resultDict,
                                                         jsonPath);
        }
        if (written)
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        else
        {
            MILog(@"Failed to save properties", commandDict);
            replyDict = MIMakeReplyDictionary(@"Error: Failed to save properties",
                                              MIReplyErrorInvalidOption);
        }
    }

    return replyDict;
}

BOOL MISaveCGImageToAPNGFileWithPath(CGImageRef theImage, NSString *filePath)
{
    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
    if (!fileURL)
        return NO;
    
    CGImageDestinationRef exporter = CGImageDestinationCreateWithURL(
                                                    (__bridge CFURLRef)fileURL,
                                                    kUTTypePNG, 1, NULL);
    if (!exporter)
        return NO;
    
    CGImageDestinationAddImage(exporter, theImage, nil);
    BOOL result = CGImageDestinationFinalize(exporter);
    CFRelease(exporter);
    return result;
}

#if DEBUG

#if TARGET_OS_IPHONE
@import ImageIO;
@import MobileCoreServices;
#endif

void SaveCGImageToAPNGFile(CGImageRef theImage, NSString *fileName)
{
    NSString *df = @"~/Desktop/";
    NSString *destination = [NSString stringWithFormat:@"%@/%@",
                             [df stringByExpandingTildeInPath], fileName];
    
    MISaveCGImageToAPNGFileWithPath(theImage, destination);
}

void SaveCGBitmapContextToAPNGFile(CGContextRef context, NSString *fileName)
{
    CGImageRef image = CGBitmapContextCreateImage(context);
    SaveCGImageToAPNGFile(image, fileName);
    CGImageRelease(image);
}

#endif