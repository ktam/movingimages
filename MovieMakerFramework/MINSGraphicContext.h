//  MINSGraphicContext.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"
#import "MICIRenderDestinationInterface.h"

@class MICGContext;
@class NSWindow;

/**
 @class MINSGraphicContext
 @abstract A wrapper for the graphics context of a window.
 @discussion The MINSGraphicContext class is a wrapper for a window context 
 which is used for handling the draw commands going to a window.
*/
@interface MINSGraphicContext : MIBaseObject <MICreateCGImageInterface,
											  MIBaseObjectSemiPublicInterface,
											  MICIRenderDestinationInterface>

/// initialize. Set up the list of MICGBitmapContext objects.
// +(void)initialize;

/**
 @brief Initialize a MINSGraphicContext object with a window.
 @discussion The NSGraphicContext object of the NSWindow will be obtained,
 and once obtained the graphicsPort will be requested, type cast to a
 CGContextRef and passed to the instantiation of a MICGContext object
 and that object then does any requested drawing to the window context.
*/
-(instancetype)initWithWindow:(NSWindow *)window
                         name:(NSString *)theName
                    inContext:(MIContext *)context;

/// Remove this object from lists. If no other strong refs it will be deleted
-(void)close;


@end
