//  MICoreImage.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import QuartzCore;

#if TARGET_OS_IPHONE
@import CoreImage;
#endif

#import "MIBaseObject.h"
#import "MIBaseObject+Internal.h"
#import "MICGBitmapContext.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MICIRenderDestinationInterface.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MICoreImage.h"
#import "MICoreImageJSONConstants.h"
#import "MIFilterInputImageHandler.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#if !TARGET_OS_IPHONE
#import "NSAffineTransform+DictionaryRepresentation.h"
#endif

#import "YVSChromaKeyFilter.h"

#import "MIBaseObject_Protected.h"

#pragma mark CLASS MICoreImage Private Interface

@interface MICoreImage ()

/// Each MICoreImage object has it's own dispatch queue that it renders on.
@property (strong) dispatch_queue_t _renderQueue;

// At the moment the only destination for rendering is a MICGBitmapContext
// object. This may change in the future so the render destination conforms
// to a MICIRenderDestinationInteface protocol.
@property (weak) id <MICIRenderDestinationInterface> _renderDestination;

/**
 @brief An array of MIFilterInputImageHandler objects
 @discussion Each MIFilterInputImageHandler object either contains the image
 or information about how to obtain the image. It also knows which CIFilter
 object the image is needed by, and which key is used to apply the image to
 the filter.
*/
@property (strong) NSMutableArray *_filterSourceImageHandlers;

/// An array of CIFilter objects. This is list of filters that forms chain.
@property (strong) NSMutableArray *_filterList;

/// Should software render. This overrides the readonly public property.
@property (assign) BOOL softwareRender;

/// Obtain a list of the built in filters.
+(NSArray *)_listOfCoreImageFilters;

/// Obtain a list of the filters in category.
+(NSArray *)_listOfCoreImageFiltersInCategory:(NSString *)category;

/// Obtain the filter attributes for a particular filter.
+(NSDictionary *)_filterAttributes:(NSString *)filterName;

/**
 @brief Return the filter from our filter list identified by the dictionary.
 @discussion The dictionary should contain one of two keys, a filter index key
 or a filter name identification key. The index value is an index into the
 list of filters owned by the MICoreImage object. The name identification value
 is a identification name of one of the filters in the filter list and should be
 unique within this filter chain. (In all the filters owned by the MICoreImage
 object).
*/
-(CIFilter *)_findFilterFromDictionary:(NSDictionary *)dict;

/**
 @brief Apply the property defined by the dictionary to the filter.
 @discussion The key value pairs in the dictionary define the type of object
 to be assigned to the filter, the object value to be assigned and the key
 to use to assign the property.
 @result YES if successful NO if failed.
*/
-(BOOL)_applyProperty:(NSDictionary *)propsDict
             toFilter:(CIFilter *)filter
//       previousFilter:(CIFilter *)previousFilter
 buildSourceImageList:(BOOL)buildImage;

/**
 @brief Build the CIFilter chain from the list of filter descriptions.
 @param filterList  A list of NSDictionary objects describing each filter.
 @discussion Each CIFilter object after being initialized will be set to
 its default values before the values in the NSDictionary are assigned to
 the filter.
*/
-(BOOL)_buildFilterChain:(NSArray *)filterList;

/// Apply the images accessed via the list of image handlers to the filters.
-(BOOL)_applyImagesToFilters;

/// Pull the output image from the last filter in the filter chain.
-(CIImage *)_ciFilterChainOutputImage;

-(MIFilterInputImageHandler *)_findHandlerForFilter:(CIFilter *)filter
                                           inputKey:(NSString *)inputKey;

@end

/// Create the queue for rendering the filter chain & setting filter attributes
static dispatch_queue_t CreateMICoreImageQueue(MICoreImage *object)
{
    dispatch_queue_t defaultPriorityQueue = dispatch_get_global_queue(
                                              DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                              0);
    dispatch_queue_t myQueue;
    NSString *queueName;
    queueName = [NSString stringWithFormat:@"com.zukini.micoreimage.%p", object];
    myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
    dispatch_set_target_queue(myQueue, defaultPriorityQueue);
    return myQueue;
}

static id AffineTransformDictToCIFilterValue(NSDictionary *affineDict)
{
#if TARGET_OS_IPHONE
    CGFloat a, b, c, d, tx, ty;
    
    NSNumber *theNum;
    theNum = affineDict[MIJSONKeyAffineTransformM11];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    a = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM12];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    b = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM21];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    c = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM22];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    d = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformtX];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    tx = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformtY];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    ty = [theNum doubleValue];
    
    CGAffineTransform theTransform = CGAffineTransformMake(a, b, c, d, tx, ty);
    return [NSValue valueWithBytes:&theTransform
                          objCType:@encode(CGAffineTransform)];
#elif TARGET_OS_MAC
    return [NSAffineTransform transformWithDictionary:affineDict];
#endif
}

static id ConvertStringOrDictToCIFilterValue(id input, NSString *className,
                                             NSDictionary *variables)
{
    id theVal;
    if ([className isEqualToString:@"CIColor"])
    {
        if ([input isKindOfClass:[NSString class]])
            theVal = [CIColor colorWithString:input];
        else if ([input isKindOfClass:[NSDictionary class]])
        {
            CGColorRef color = MIUtilityCreateColorFromDictionary(input,
                                                                  variables);
            if (color)
            {
                theVal = [CIColor colorWithCGColor:color];
                CGColorRelease(color);
            }
        }
    }
    else if ([className isEqualToString:@"AffineTransform"])
    {
        if ([input isKindOfClass:[NSDictionary class]])
            theVal = AffineTransformDictToCIFilterValue(input);
    }
    else if ([className isEqualToString:@"NSData"])
    {
        // TODO: CIColorCube
        MILog(@"CIColor cube not implemented.", input);
    }
    else if ([className isEqualToString:@"CIVector"])
    {
        if ([input isKindOfClass:[NSString class]])
            theVal = [CIVector vectorWithString:input];
        else if ([input isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *rect = input[MIJSONKeyRect];
            NSDictionary *point = input[MIJSONKeyPoint];
            if (rect)
            {
                CGRect theRect;
                BOOL gotRect = MIUtilityGetCGRectFromDictionary(rect,
                                                                &theRect,
                                                                kMILog,
                                                                variables);
                if (gotRect)
                {
                    theVal = [CIVector vectorWithCGRect:theRect];
                }
            }
            else if (point)
            {
                CGPoint thePoint;
                BOOL gotPoint = MIUtilityGetCGPointFromDictionary(point,
                                                                  &thePoint,
                                                                  kMILog,
                                                                  variables);
                if (gotPoint)
                {
                    theVal = [CIVector vectorWithCGPoint:thePoint];
                }
            }
        }
    }
    return theVal;
}

#pragma mark - CLASS MICoreImage Implementation

@implementation MICoreImage

#pragma mark Public Class methods

+(NSArray *)_listOfCoreImageFilters
{
    NSMutableArray *filterArray;
    filterArray = [[NSMutableArray alloc]
            initWithArray:[CIFilter filterNamesInCategory:kCICategoryBuiltIn]];
#if !TARGET_OS_IPHONE
    [filterArray addObject:YVSChromaKeyFilterName];
#endif
    return [NSArray arrayWithArray:filterArray];
}

+(NSArray *)_listOfCoreImageFiltersInCategory:(NSString *)category;
{
    NSArray *filtersInCat = [CIFilter filterNamesInCategory:category];
#if !TARGET_OS_IPHONE
    if (IsYVSChromaKeyFilterInCategory(category))
    {
        NSMutableArray *filters = [NSMutableArray arrayWithArray:filtersInCat];
        [filters addObject:YVSChromaKeyFilterName];
        filtersInCat = [NSArray arrayWithArray:filters];
    }
#endif
    return filtersInCat;
}

+(NSDictionary *)_filterAttributes:(NSString *)filterName
{
    CIFilter *filter = [CIFilter filterWithName:filterName];
    if (filter)
        return [filter attributes];

    return nil;
}

+(BOOL)filter:(CIFilter *)theFilter setValue:(id)value forKey:(NSString *)key
{
    BOOL result = YES;
    @try
    {
        [theFilter setValue:value forKey:key];
    }
    @catch (NSException *exception)
    {
        result = NO;
    }
    return result;
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface class methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict;
    context = context ? context : [MIContext defaultContext];
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    if (propertyKey == NULL)
    {
        MILog(@"Missing property key.", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyImageFilters])
    {
        NSArray *imageFilterTypes;
        NSString *category = commandDict[MIJSONPropertyFilterCategory];
        if (category)
        {
            imageFilterTypes = [MICoreImage
                                _listOfCoreImageFiltersInCategory:category];
        }
        else
        {
            imageFilterTypes = [MICoreImage _listOfCoreImageFilters];
        }

        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                               imageFilterTypes);
        replyDict = MIMakeReplyDictionary(resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyImageFilterAttributes])
    {
        NSString *filterName;
        if (!replyDict)
        {
            filterName = commandDict[MIJSONPropertyImageFilterName];
            if (!filterName)
            {
                MILog(@"Missing filter name.", commandDict);
                replyDict = MIMakeReplyDictionary(@"Error: Missing filter name.",
                                                  MIReplyErrorMissingOption);
            }
        }
        NSDictionary *filterAttributes;
        if (!replyDict)
        {
            filterAttributes = [MICoreImage _filterAttributes:filterName];
        }
        if (filterAttributes)
        {
            filterAttributes = MIUtilityCreateDictionarySuitableForPlistOrJSON(
                                                                filterAttributes);
            // DumpNSDictionaryByKeyIndex(filterAttributes);
            if (!replyDict)
            {
                replyDict = MIUtilitySaveDictionaryToDestination(
                           commandDict, // dictionary containing destination info.
                           filterAttributes, // dictionary to be saved.
                           YES);// Default to saving dict to reply dictionary.
            }
        }
        else
        {
            MILog(@"Failed to get attributes. Unknown filter?",
                                 commandDict);
            replyDict = MIMakeReplyDictionary(
                            @"Error: failed to get attributes. Unknown filter?",
                                              MIReplyErrorInvalidOption);
        }
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MIImageFilterKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"MICoreImage class unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: MICoreImage unknown property",
                                          MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSString *objName = commandDict[MIJSONKeyObjectName];
    NSDictionary *coreImageDict = commandDict[MIJSONPropertyImageFilterChain];
    if (!coreImageDict)
    {
        MILog(@"Missing the core image dictionary", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing core image dictionary",
                                     MIReplyErrorMissingProperty);
    }

    id <MICIRenderDestinationInterface> obj;
    NSDictionary *rDict = coreImageDict[MICoreImageJSONKeyRenderDestination];
    if (rDict)
    {
        obj = FindObjectFromDictionaryConformingToRenderDestinationProtocol(
                                                                context, rDict);
        if (!obj)
        {
            MILog(@"Couldn't find a render destination object.",
                                 rDict);
            return MIMakeReplyDictionary(
                        @"Error: Couldn't find a render destination object.",
                        MIReplyErrorInvalidOption);
        }
    }
    else
    {
        MILog(@"No destination dict with key: ",
                             MICoreImageJSONKeyRenderDestination);
        return MIMakeReplyDictionary(@"Error: No destination object dictionary.",
                                     MIReplyErrorMissingProperty);
    }
    
    NSArray *filterList = coreImageDict[MICoreImageJSONKeyCIFilterList];
    if (!filterList)
    {
        MILog(@"Missing filter list.", coreImageDict);
        return MIMakeReplyDictionary(@"Error: Missing filter list.",
                                     MIReplyErrorMissingProperty);
    }
    
    BOOL softwareRender = MIUtilityGetBOOLPropertyWithDefault(coreImageDict,
                                            MIJSONPropertySoftwareRender, NO);
    
    BOOL srgbRender = MIUtilityGetBOOLPropertyWithDefault(coreImageDict,
                                          MIJSONPropertyUseSRGBColorSpace, NO);

    MICoreImage *newObj = [[MICoreImage alloc] initWithName:objName
                                                  inContext:context
                                          renderDestination:obj
                                                 filterList:filterList
                                             softwareRender:softwareRender
                                                 srgbRender:srgbRender];

    NSDictionary *result;
    
    if (newObj)
    {
        MIBaseReference objectReference = [newObj reference];
        NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                               (long)objectReference];
        result = MIMakeReplyDictionaryWithNumericValue(refString,
                                                       MIReplyErrorNoError,
                                                       @(objectReference));
    }
    else
    {
        result = MIMakeReplyDictionary(
                                   @"Error: Failed to create core image object",
                                   MIReplyErrorOptionValueInvalid);
    }
    return result;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    [context removeAllObjectsWithType:MIImageFilterKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Public methods

-(instancetype)initWithName:(NSString *)theName
                  inContext:(MIContext *)context
          renderDestination:(id <MICIRenderDestinationInterface>)dest
                 filterList:(NSArray *)filterList
             softwareRender:(BOOL)softwareRender
                 srgbRender:(BOOL)srgbRender
{
    self = [super initWithBaseObjectType:MIImageFilterKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        self->__renderQueue = CreateMICoreImageQueue(self);
        self->__renderDestination = dest;
        self->__filterSourceImageHandlers = [NSMutableArray new];
        self->__filterList = [NSMutableArray new];
        self->_softwareRender = softwareRender;
        self->_useSRGBColorSpace = srgbRender;
        // Building the filter list will also determine all the objects that
        // supply images to the filter chain. Therefore these objects will
        // need to have been created before the filter chain is created.
        __block BOOL success = YES;
        dispatch_sync(self._renderQueue, ^
        {
            @try
            {
                if (![self _buildFilterChain:filterList])
                    success = NO;
            }
            @catch (NSException *exception) {
                success = NO;
            }
        });
        if (!success)
        {
            MILog(@"Failed to build filter chain.", filterList);
            [context removeObjectWithReference:self.reference];
            self = nil;
        }
        
        if (!self)
        {
            MILogBreak();
        }
    }
    return self;
}

-(void)dealloc
{
    id <MICIRenderDestinationInterface> dest = self._renderDestination;
    if (dest)
        [dest clearCIContextRelatingTo:self];
}

-(void)finalize
{
    self._filterList = nil;
    self._filterSourceImageHandlers = nil;
    [super finalize];
}

-(void)close
{
    [self finalize];
    [super baseObjectClose];
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface instance methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *reply;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    NSString *stringVal;
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }
    
    if ([propertyType isEqualToString:MIJSONPropertySoftwareRender])
    {
        stringVal = self.softwareRender ? @"YES" : @"NO";
        reply = MIMakeReplyDictionaryWithNumericValue(stringVal,
                                                        MIReplyErrorNoError,
                                                        @(self.softwareRender));
    }
    else if ([propertyType isEqualToString:MIJSONPropertyUseSRGBColorSpace])
    {
        stringVal = self.useSRGBColorSpace ? @"YES" : @"NO";
        reply = MIMakeReplyDictionaryWithNumericValue(stringVal,
                                                      MIReplyErrorNoError,
                                                      @(self.useSRGBColorSpace));
    }
    else
    {
        reply = [super handleCommonGetPropertyCommand:commandDict];
        if (!reply)
        {
            MILog(
                            @"Requested unknown property from imagefilterchain",
                            commandDict);
            reply = MIMakeReplyDictionary(
                        @"Error: Requested unknown property from image file",
                        MIReplyErrorUnknownProperty);
        }
    }
    return reply;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *errorDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        errorDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return errorDict;
    }
    NSDictionary *replyDict;
    NSMutableDictionary *propertyDict;
    propertyDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    propertyDict[MIJSONPropertyUseSRGBColorSpace] = @(self.useSRGBColorSpace);
    propertyDict[MIJSONPropertySoftwareRender]= @(self.softwareRender);
    propertyDict[MIJSONKeyObjectReference] = @(self.reference);
    propertyDict[MIJSONKeyObjectName] = self.name;
    propertyDict[MIJSONKeyObjectType] = self.baseObjectType;
    replyDict = MIUtilitySaveDictionaryToDestination(
           commandDict,// dictionary containing info where to save propertyDict
           propertyDict, // dictionary to be saved.
           YES);// Default to replyDict.
    return replyDict;
}

-(NSDictionary *)handleSetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    NSString *propertyValue = commandDict[MIJSONPropertyValue];

    if (!propertyKey || !propertyValue)
    {
        MILog(@"Missing property key or value", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property key or value",
                                     MIReplyErrorMissingProperty);
    }
    __block NSDictionary *result = NULL;
    dispatch_sync(self._renderQueue, ^
    {
        BOOL isError = NO;
        BOOL boolValue;
        if ([propertyKey isEqualToString:MIJSONPropertySoftwareRender])
        {
            boolValue = MIUtilityGetBOOLValue(propertyValue, &isError);
            if (isError)
            {
                MILog(@"Missing or invalid property value",
                                     commandDict);
                result = MIMakeReplyDictionary(
                                    @"Error: Missing or invalid property value",
                                    MIReplyErrorInvalidOption);
            }
            else
            {
                [self setSoftwareRender:boolValue];
                result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
            }
        }
        else if ([propertyKey isEqualToString:MIJSONPropertyUseSRGBColorSpace])
        {
            boolValue = MIUtilityGetBOOLValue(commandDict[MIJSONPropertyValue],
                                                &isError);
            if (isError)
            {
                MILog(@"Missing or invalid property value",
                                     commandDict);
                result = MIMakeReplyDictionary(
                                   @"Error: Missing or invalid property value",
                                   MIReplyErrorInvalidOption);
            }
            else
            {
                [self setUseSRGBColorSpace:boolValue];
                result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
            }
        }
        else
        {
            NSString *message = [[NSString alloc] initWithFormat:@"%@ %@",
                                 @"Unknown property: ", propertyKey];
            MILog(message, commandDict);
            NSString *errorMsg = [[NSString alloc] initWithFormat:@"%@ %@",
                                 @"Error: ", message];
            result = MIMakeReplyDictionary(errorMsg, MIReplyErrorUnknownProperty);
        }
    });
    return result;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleRenderFilterChainCommand:(NSDictionary *)command
{
    __block NSDictionary *replyDict;
    dispatch_sync(self._renderQueue, ^
    {
        BOOL result = YES;
        NSArray *propertiesArray;
        NSDictionary *instructions;
        instructions = command[MIJSONPropertyRenderInstructions];

        if (instructions)
            propertiesArray = instructions[MICoreImageJSONKeyCIFilterProperties];

        MIContext *miContext = self.miContext;
        NSDictionary *variablesDict = miContext.variables;
        
        if (propertiesArray && propertiesArray.count)
        {
            // We have an array of properties to be modified. We can identify
            // which filter the property should be applied to by either the
            // index into the list of filters in the filter chain or by the
            // filters identification name.
            for (NSDictionary *property in propertiesArray)
            {
                CIFilter *filter = [self _findFilterFromDictionary:property];
                if (!filter)
                {
                    MILog(@"Unknown filter: ", property);
                    replyDict = MIMakeReplyDictionary(@"Error: Unknown filter",
                                                    MIReplyErrorInvalidOption);
                    break;
                }
                
                id value = property[MICoreImageJSONKeyCIFilterValue];
                NSString *key = property[MICoreImageJSONKeyCIFIlterValueKey];
                NSString *className;
                className = property[MICoreImageJSONKeyCIFIlterValueClass];
                // Most of the inputs are NSNumber and that is usual assumed
                // class. There is just the 1 className equal to NSString and
                // this is in the filter CIQRCodeGenerator
                if (!className || ([className isEqualToString:@"NSNumber"] ||
                                   [className isEqualToString:@"NSString"]))
                {
                    // If the class name is NSNumber or NSString or doesn't
                    // exist then assume that we can just assign the value
                    // to the filter using the key without having to do any
                    // converting of data types.
                    if ([className isEqualToString:@"NSNumber"] &&
                        [value isKindOfClass:[NSString class]])
                    {
                        CGFloat numericValue;
                        if (MIUtilityGetCGFloatFromID(value, &numericValue,
                                                    variablesDict))
                        {
                            double val = numericValue;
                            value = [[NSNumber alloc] initWithDouble:val];
                        }
                    }
                    
                    if (key && value)
                    {
                        result = [MICoreImage filter:filter
                                            setValue:value
                                              forKey:key];
                        if (!result)
                        {
                            MILog(@"Invalid filter key.", key);
                            replyDict = MIMakeReplyDictionary(
                                                @"Error: Invalid filter key",
                                                MIReplyErrorInvalidOption);
                            break;
                        }
                    }
                    else
                    {
                        MILog(@"Missing key or value.", property);
                        replyDict = MIMakeReplyDictionary(
                                                  @"Error: Missing key or value",
                                                  MIReplyErrorInvalidOption);
                        result = NO;
                        break;
                    }
                }
                else if ([className isEqualToString:@"CIImage"])
                {
                    // Deal with the image related props. This relates to
                    // static images or force immediate update.
                    NSNumber *keepStatic = property[
                                MICoreImageJSONKeyCIFilterSourceImageKeepStatic];
                    NSNumber *forceSourceUpdate = property[
                                MICoreImageJSONKeyCIFilterForceSourceImageUpdate];
                    MIFilterInputImageHandler *handler;
                    handler = [self _findHandlerForFilter:filter inputKey:key];
                    if (keepStatic)
                    {
                        BOOL keep = [keepStatic boolValue];
                        [handler setMakeCopy:keep];
                    }
                    if (forceSourceUpdate)
                    {
                        // By clearing the image that is held, the next time an
                        // image is requested a new image will be generated.
                        BOOL forceUpdate = [forceSourceUpdate boolValue];
                        if (forceUpdate)
                            [handler clearImage];
                    }
                }
                else
                {
                    // Deal with CIVector, NSAffineTransform, CIColor etc.
                    // classes, which we will need to convert our value from a
                    // NSString or an NSDictionary into.
                    id oldValue = value;
                    value = ConvertStringOrDictToCIFilterValue(value,
                                                               className,
                                                               variablesDict);
                    if (value)
                    {
                        result = [MICoreImage filter:filter
                                            setValue:value
                                              forKey:key];
                        if (!result)
                        {
                            replyDict = MIMakeReplyDictionary(
                                                  @"Error: Invalid filter key",
                                                  MIReplyErrorInvalidOption);
                            MILog(@"Invalid filter key.", key);
                            break;
                        }
                    }
                    else
                    {
                        NSString *message;
                        message = [[NSString alloc] initWithFormat:
                                   @"Couldn't convert object to class: %@",
                                   className];
                        MILog(message, oldValue);

                        NSString *errorMessage;
                        errorMessage = [[NSString alloc] initWithFormat:
                                        @"Error: %@", message];
                        replyDict = MIMakeReplyDictionary(errorMessage,
                                                    MIReplyErrorInvalidOption);
                        result = NO;
                        break;
                    }
                }
            }
        }
        
        // If no fromRect is supplied, the drawCoreImage method will check whether
        // fromRect is equal to the zero rect and if so, it will pull the source
        // rect from the ciImage's extent.
        CGRect fromRect = CGRectZero;
        if (instructions)
            MIUtilityGetCGRectFromDictionary(
                                        instructions[MIJSONKeySourceRectangle],
                                        &fromRect, kMINoLog, variablesDict);
        
        // if no toRect is supplied, the drawCoreImage method will check whether
        // toRect is equal to the zero rect and if so, it will make the
        // destination equal to the bounds of the bitmap context.
        CGRect toRect = CGRectZero;
        
        if (instructions)
            MIUtilityGetCGRectFromDictionary(
                                    instructions[MIJSONKeyDestinationRectangle],
                                    &toRect, kMINoLog, variablesDict);
        
        if (result)
        {
            result = [self _applyImagesToFilters];
            CIImage *ciImage = nil;
            if (!result)
            {
                // Couldn't apply input images to filters.
                replyDict = MIMakeReplyDictionary(
                                @"Error: Couldn't apply input images to filters",
                                MIReplyErrorInvalidOption);
                MILog(@"Couldn't apply input images to filters",
                                     instructions);
            }
            else
            {
                id <MICIRenderDestinationInterface> dest;
                dest = self._renderDestination;
                ciImage = [self _ciFilterChainOutputImage];
                if (dest && ciImage)
                {
                    //  result = [dest drawCoreImage:ciImage
                    BOOL createImage = MIUtilityGetBOOLPropertyWithDefault(
                                        command, MIJSONPropertyCreateImage, NO);
                    [dest drawCoreImage:ciImage
                        fromFilterChain:self
                               fromRect:fromRect
                                 toRect:toRect
                      shouldCreateImage:createImage];
                }
                else
                {
                    if (!dest)
                    {
                        replyDict = MIMakeReplyDictionary(
                                        @"Error: Invalid or missing destination",
                                              MIReplyErrorInvalidOption);
                        MILog(@"Invalid or missing destination.",
                                             nil);
                    }
                    if (!ciImage)
                    {
                        replyDict = MIMakeReplyDictionary(
                                      @"Error: Couldn't generate output CIImage",
                                      MIReplyErrorOperationFailed);
                        MILog(
                                      @"Couldn't generate output CIImage.",
                                             instructions);
                    }
                    // result = NO;
                }
            }
        }
    });
    if (replyDict)
    {
        MILogBreak();
    }
    else
    {
        replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    return replyDict;
}

#pragma mark Private methods

-(CIImage *)_ciFilterChainOutputImage
{
    CIFilter *lastFilter = self._filterList.lastObject[@"filter"];
    return [lastFilter valueForKey:kCIOutputImageKey];
}

-(MIFilterInputImageHandler *)_findHandlerForFilter:(CIFilter *)filter
                                           inputKey:(NSString *)inputKey
{
    for (MIFilterInputImageHandler *handler in self._filterSourceImageHandlers)
    {
        if ([handler managesImageForFilter:filter key:inputKey])
            return handler;
    }
    return nil;
}

-(CIFilter *)_findFilterFromDictionary:(NSDictionary *)dict
{
    // First see if we have an index reference to the filter. If we do but
    // it is bad for any reason then return nil.
    NSNumber *theIndex = dict[MICoreImageJSONKeyCIFilterIndex];
    if (theIndex)
    {
        CIFilter *theFilter;
        if ([theIndex isKindOfClass:[NSNumber class]])
        {
            NSUInteger index = [theIndex unsignedIntegerValue];
            if (index < self._filterList.count)
                theFilter = self._filterList[index][@"filter"];
        }
        return theFilter;
    }

    // We don't have a filter index, so lets try a filter name.
    NSString *filterName = dict[MICoreImageJSONKeyMIFilterName];
    if (filterName && [filterName isKindOfClass:[NSString class]])
    {
        for (NSDictionary *theFilter in self._filterList)
        {
            if ([filterName isEqualToString:theFilter[@"name"]])
                return theFilter[@"filter"];
        }
    }
        
    return nil;
}

-(BOOL)_applyProperty:(NSDictionary *)propDict
             toFilter:(CIFilter *)filter
 buildSourceImageList:(BOOL)buildSourceImage
{
    // If buildSourceImageList is true, then the source image list is still being
    // built so if the propsDict is about an image then we need to build the
    // image list including connecting output and input images of filter chain.
    BOOL result = YES;
    // min info needed is the value to be assigned and key to assign it with.
    id theVal = propDict[MICoreImageJSONKeyCIFilterValue];
    NSString *filterKey = propDict[MICoreImageJSONKeyCIFIlterValueKey];
    if (!theVal || !(filterKey && [filterKey isKindOfClass:[NSString class]]))
    {
        MILog(
            @"CIFilter property dictionary missing key or value.", propDict);
        return NO;
    }
    NSString *valueClass = propDict[MICoreImageJSONKeyCIFIlterValueClass];
    if (valueClass)
    {
        MIContext *context = self.miContext;
        if ([valueClass isEqualToString:@"CIImage"])
        {
            // The CIImage case is more complex so I've seperated it out from
            // the remaining cases below. First need to ask if we are building
            // the source image list. If it's already been built then we don't
            // need to do anything with input images. They should not be
            // in the list of filter properties to be updated.
            if (buildSourceImage)
            {
                NSDictionary *sourceDict;
                if ([theVal isKindOfClass:[NSDictionary class]])
                    sourceDict = theVal;
                
                // The dictionary will supply us with either a filter or a
                // a base object from which we can obtain a base object ref
                // or an image identifier for an image that is in the image
                // collection.
                // If not then we are missing something.
                CIFilter *theFilter;
                MIBaseReference baseReference = kMIInvalidElementReference;
                NSString *imageID;
                if (sourceDict)
                {
                    MIBaseObject *baseObject;
                    theFilter = [self _findFilterFromDictionary:sourceDict];
                    imageID = sourceDict[MIJSONPropertyImageIdentifier];
                    baseObject = MIGetObjectFromDictionary(context, sourceDict);
                    if (baseObject && [baseObject conformsToProtocol:
                                       @protocol(MICreateCGImageInterface)])
                        baseReference = [baseObject reference];
                }
                
                if (imageID)
                {
                    MICGImageWrapper *wrapper;
                    wrapper = [[MICGImageWrapper alloc]
                               initWithImageIdentifier:imageID
                                           withContext:context];

                    NSNumber *kS;
                    kS = propDict[MICoreImageJSONKeyCIFilterSourceImageKeepStatic];
                    wrapper.makeCopy = kS && [kS boolValue] ? YES : NO;

                    MIFilterInputImageHandler *imageHandler;
                    imageHandler = [[MIFilterInputImageHandler alloc]
                                           initWithCIFilter:filter
                                        filterInputImageKey:filterKey
                                               imageWrapper:wrapper];
                    // Force the image to be obtained and applied to the
                    // filter during construction. This way the rule is
                    // that for images held strongly the image can be in the
                    // image collection when the filter is created or when the
                    // filter chain is first rendered and the image is needed.
                    result = [imageHandler applyImageToFilterIfNecessary];
                    [self._filterSourceImageHandlers addObject:imageHandler];
                    if (!result)
                    {
                        MILog(@"Not applied image", propDict);
                    }
                    
                    // Setting result to yes here because the image might well
                    // exist by the time we ask the filter chain to render, so
                    // a failure to get the image now can be ignored.
                    result = YES;
                }
                else if (theFilter)
                {
                    CIImage *sImage = [theFilter valueForKey:kCIOutputImageKey];
                    if (sImage)
                    {
                        result = [MICoreImage filter:filter
                                            setValue:sImage
                                              forKey:filterKey];
                        if (result)
                        {
                            MIFilterInputImageHandler *imageHandler;
                            imageHandler = [[MIFilterInputImageHandler alloc]
                                                    initWithCIFilter:filter
                                                 filterInputImageKey:filterKey
                                                         inputFilter:theFilter];
                            [self._filterSourceImageHandlers addObject:imageHandler];
                        }
                        else
                        {
                            MILog(@"Incorrect key.", filterKey);
                        }
                    }
                    else
                    {
                        MILog(
                                        @"Failure to get CIFilter's CIImage.",
                                        sourceDict);
                        result = NO;
                    }
                }
                else if (baseReference != kMIInvalidElementReference)
                {
                    NSDictionary *options = sourceDict[MIJSONKeyImageOptions];
                    if (options && ![options isKindOfClass:[NSDictionary class]])
                    {
                        MILog(
                            @"Image options should be a dictionary.", sourceDict);
                        options = nil;
                    }
                    MICGImageWrapper *wrapper;
                    wrapper = [[MICGImageWrapper alloc]
                                   initWithObjectReference:baseReference
                                                   options:options
                                               withContext:context];
                    BOOL makeCopy = NO;
                    NSNumber *fSRef = propDict[
                                MICoreImageJSONKeyCIFilterSourceImageKeepStatic];
                    if (fSRef && [fSRef boolValue])
                        makeCopy = YES;
                    
                    wrapper.makeCopy = makeCopy;
                    MIFilterInputImageHandler *imageHandler;
                    imageHandler = [[MIFilterInputImageHandler alloc]
                                        initWithCIFilter:filter
                                     filterInputImageKey:filterKey
                                            imageWrapper:wrapper];
                    
                    // Force the image to be obtained and applied to the
                    // filter during construction. This way the rule is
                    // that for images held strongly the base object they
                    // come from only needs to exist at the time the core
                    // image exists. Other sources need to be created b4
                    // the CoreImage object is created and detroyed after
                    // the coreimage object is destroyed.
                    result = [imageHandler applyImageToFilterIfNecessary];
                    if (result)
                    {
                        [self._filterSourceImageHandlers addObject:imageHandler];
                    }
                }
                else
                {
                    MILog(@"Missing info to get CIImage.",
                                           sourceDict);
                    result = NO;
                }
            }
        }
        else if ([valueClass isEqualToString:@"NSNumber"])
        {
            result = [MICoreImage filter:filter setValue:theVal forKey:filterKey];
            if (!result)
            {
                MILog(@"Unknown filter key: ", filterKey);
            }
        }
        else
        {
            id oldVal = theVal;
            theVal = ConvertStringOrDictToCIFilterValue(theVal, valueClass,
                                                        context.variables);
            if (!theVal)
            {
                MILog(@"Unknown property value type.",
                                       oldVal);
                result = NO;
            }

            if (result)
            {
                result = [MICoreImage filter:filter
                                    setValue:theVal
                                      forKey:filterKey];

                if (!result)
                {
                    MILog(@"Unknown filter key: ", filterKey);
                }
            }
        }
    }
    else
    {
        // If theVal is a NSNumber object then just assign it straight to
        // the filter using the key. If theVal is some other object then
        // it possibly isn't correct. All conversions from dictionaries or
        // arrays have already been dealt with above so the only remaining
        // other option is an assignment of an NSString object. As of OS X10.9
        // and iOS 7 the only filter to take a NSString object is a
        // CIQRCodeGenerator filter. The logging code below will output to the
        // log file except for this one case if theVal is not a NSNumber object.
        id theVal = propDict[MICoreImageJSONKeyCIFilterValue];
        if (![theVal isKindOfClass:[NSNumber class]])
        {
            NSString *filterClassName = NSStringFromClass([filter class]);
            if (!([filterClassName isEqualToString:@"CIQRCodeGenerator"] &&
                  [filterKey isEqualToString:@"inputCorrectionLevel"] &&
                  [theVal isKindOfClass:[NSString class]]))
            {
                // Just warn that we've an object which belongs to an unexpected
                // class. Don't set result to NO in this case.
                MILog(@"Object with unexpected class: ", theVal);
                MILog(@"For filter Key: ", filterKey);
                MILog(@"As part of filter dictionary: ", propDict);
                MILogBreak();
            }
        }
        result = [MICoreImage filter:filter setValue:theVal forKey:filterKey];
        if (!result)
        {
            MILog(@"Unknown filter key: ", filterKey);
        }
    }
    
    return result;
}

-(BOOL)_buildFilterChain:(NSArray *)listOfFilters
{
    BOOL result = YES;
    for (NSDictionary *filterDict in listOfFilters)
    {
        NSString *filterName = filterDict[MICoreImageJSONKeyCIFilterName];
        NSArray *filterProps = filterDict[MICoreImageJSONKeyCIFilterProperties];
        CIFilter *filter;
        
        if (filterName)
        {
            filter = [CIFilter filterWithName:filterName];
            if (!filter)
            {
                MILog(@"Couldn't create filter: ", filterName);
            }
        }

        if (!filter || !filterProps)
        {
            MILog(@"Missing or invalid properties.", filterDict);
            result = NO;
            break;
        }

        [filter setDefaults];

        NSString *identName = filterDict[MICoreImageJSONKeyMIFilterName];
        if (!identName)
            identName = @"";
        
        NSDictionary *filterDict = @{ @"filter" : filter, @"name" : identName };

        BOOL shouldBail = NO;
        [self._filterList addObject:filterDict];
        for (NSDictionary *filterProp in filterProps)
        {
            result = [self _applyProperty:filterProp
                                 toFilter:filter
                     buildSourceImageList:YES];
            if (!result)
            {
                MILog(@"Missing or invalid properties.", filterProp);
                shouldBail = YES;
                break;
            }
        }
        if (shouldBail)
        {
            break;
        }
    }
    return result;
}

-(BOOL)_applyImagesToFilters
{
    BOOL result = YES;
    for (MIFilterInputImageHandler *handler in self._filterSourceImageHandlers)
    {
        result &= [handler applyImageToFilterIfNecessary];
    }
    return result;
}

@end

