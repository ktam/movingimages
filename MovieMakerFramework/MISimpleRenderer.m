//  MISimpleRenderer.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MISimpleRenderer.h"

@import QuartzCore;

#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIRenderer.h"

#import "MICGContext.h"

@interface MISimpleRenderer ()

@property (readonly, strong) MIContext *_miContext;

@end

@implementation MISimpleRenderer

-(instancetype)initWithMIContext:(MIContext *)miContext
{
    self = [super init];
    if (self)
    {
        self->__miContext = miContext;
    }
    return self;
}

-(instancetype)init
{
    return [self initWithMIContext:MIContext.new];
}

-(void)drawDictionary:(NSDictionary *)drawDict intoCGContext:(CGContextRef)context
{
    @autoreleasepool {
        MICGContext *theContext;
        theContext = [[MICGContext alloc] initWithCGContext:context
                                                  miContext:self._miContext
                                                      owner:self];
        [self._miContext appendVariables:self.variables];
        [theContext handleDictionaryDrawElementCommand:drawDict];
        [self._miContext dropVariablesDictionary:self.variables];
    }
}

-(void)assignImage:(CGImageRef)image withIdentifier:(NSString *)identifier
{
    [self._miContext assignCGImage:image identifier:identifier];
}

-(void)removeImageWithIdentifier:(NSString *)identifier
{
    [self._miContext removeImageWithIdentifier:identifier];
}

@end
