//  MMImageImporter.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import CoreGraphics;
@import ImageIO;
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIImageImporter.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#import "MIBaseObject_Protected.h"

@interface MIImageImporter ()

/// If the image file format supports it, allow image to be represented by floats
@property (assign) BOOL _allowFloatingPointImages;

@property (readonly) CGImageSourceRef _imageSource;
@property (assign) size_t _dictionaryImageIndex;
@property (strong) NSDictionary *_dictionaryAtIndex;

/// Path to the image file as a NSURL/CFURLRef
@property (nonatomic, copy) NSURL *_imagePathURL;

/// Obtain specific property info from the property dictionary.
-(NSString *)_getDictionaryProperty:(NSString *)propertyKey
                     withImageIndex:(NSInteger)imageIndex;

/// Create a dictionary with all the file related image properties
-(NSDictionary *)_createImageFilePropertyDictionary;

/// Returns the status of the image source object.
-(CGImageSourceStatus)_imageSourceStatus;

/// Returns the image file type.
-(NSString *)_imageSourceType;

/// Returns the status for a particular image at index in the file.
-(CGImageSourceStatus)_imageSourceImageStatusAtIndex:(size_t)index;

@end

@implementation MIImageImporter

#pragma mark Public Class MIBaseObjectSemiPublicInterface methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    context = context ? context : [MIContext defaultContext];
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    if (propertyKey == NULL)
    {
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingProperty);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyImageImportTypes])
    {
        NSArray *importTypes;
        importTypes = CFBridgingRelease(CGImageSourceCopyTypeIdentifiers());
        
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                    importTypes);
        replyDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MIImageImporterKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(
                                    @"Error: MIImageImporter unknown property",
                                    MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *filePath = MIUtilityGetFilePathFromCommandAndVariables(commandDict,
                                                            context.variables);

    if (filePath)
    {
        NSString *objectName = commandDict[MIJSONKeyObjectName];
        
        filePath = [filePath stringByExpandingTildeInPath];
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        MIImageImporter *object;
        object = [[MIImageImporter alloc] initWithFileURL:fileURL
                                                     name:objectName
                                                inContext:context];
        if (object)
        {
            NSNumber *allowFloats;
            allowFloats = commandDict[MIJSONPropertyAllowFloatingPointImages];
            if (allowFloats && [allowFloats boolValue])
                object._allowFloatingPointImages = YES;

            MIBaseReference objectReference = [object reference];
            NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                   (long)objectReference];
            replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                              MIReplyErrorNoError,
                                                              @(objectReference));
        }
        else
        {
            MILog(@"Failed to create importer", commandDict);
            NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                                 @"Error: Failed to create importer with path: ",
                                 filePath];
            replyDict = MIMakeReplyDictionary(message,
                                              MIReplyErrorOptionValueInvalid);
        }
    }
    else
    {
        replyDict = MIMakeReplyDictionary(@"Error: Missing option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MIImageImporterKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Public methods

-(instancetype)initWithFileURL:(NSURL *)fileURL
                          name:(NSString *)theName
                     inContext:(MIContext *)context
{
    if (!theName || theName.length == 0)
        theName = [fileURL path];

	self = [super initWithBaseObjectType:MIImageImporterKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        self->__imagePathURL = fileURL.copy;
        self->__dictionaryImageIndex = -1;
        self->__imageSource = CGImageSourceCreateWithURL(
                                        (__bridge CFURLRef)fileURL, nil);
        if (self->__imageSource)
        {
            size_t numImages = CGImageSourceGetCount(self->__imageSource);
            if (numImages == 0)
            {
                CFRelease(self->__imageSource);
                self->__imageSource = nil;
            }
        }
        
        if (!self->__imageSource)
        {
            [super baseObjectClose];
            MILog(@"Failed to create CGImageSource with path.",
                                 fileURL.path);
            self = nil;
        }
    }

    return self;
}

-(void)close
{
    [super baseObjectClose];
}

-(void)dealloc
{
    if (self->__imageSource)
        CFRelease(self->__imageSource);
}

#pragma mark Private instance methods.

-(NSDictionary *)_createImageFilePropertyDictionary
{
    NSDictionary *sourceDict = [self copyImageSourcePropertyDictionary];
    
    NSMutableDictionary *imageFileDict;
    if (sourceDict)
        imageFileDict = [[NSMutableDictionary alloc]
                         initWithDictionary:@{@"dictionary":sourceDict}];
    else
        imageFileDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    imageFileDict[MIJSONPropertyNumberOfImages] = @(self.numberOfImages);
    imageFileDict[MIJSONPropertyFile] = [self._imagePathURL path];
    imageFileDict[MIJSONPropertyFileType] = self._imageSourceType;
    imageFileDict[MIJSONPropertyImageSourceStatus] = @(self._imageSourceStatus);
    imageFileDict[MIJSONKeyObjectName] = self.name;
    imageFileDict[MIJSONKeyObjectType] = self.baseObjectType;
    imageFileDict[MIJSONPropertyAllowFloatingPointImages] =
                                                @(self._allowFloatingPointImages);
    
    return [[NSDictionary alloc] initWithDictionary:imageFileDict];
}

#pragma mark Query the CGImageSourceRef for info.

-(size_t)numberOfImages
{
    return CGImageSourceGetCount(self._imageSource);
}

-(CGImageSourceStatus)_imageSourceStatus
{
    return CGImageSourceGetStatus(self._imageSource);
}

-(NSString *)_imageSourceType
{
    CFStringRef imageType = CGImageSourceGetType(self._imageSource);
    return (__bridge NSString *)imageType;
}

-(CGImageSourceStatus)_imageSourceImageStatusAtIndex:(size_t)index
{
    return CGImageSourceGetStatusAtIndex(self._imageSource, index);
}

#pragma mark Get image file and image properties

-(NSDictionary *)copyImageSourcePropertyDictionary
{
    CFDictionaryRef theDict;
    theDict = CGImageSourceCopyProperties(self._imageSource, NULL);
    return CFBridgingRelease(theDict);
}

-(NSDictionary *)imagePropertyDictionaryAtIndex:(size_t)index
{
    if (index != self._dictionaryImageIndex)
    {
        CFDictionaryRef theDict;
        theDict = CGImageSourceCopyPropertiesAtIndex(self._imageSource, index,
                                                                        NULL);
        if (theDict)
            self._dictionaryImageIndex = index;
        else
            self._dictionaryImageIndex = -1;

        self._dictionaryAtIndex = CFBridgingRelease(theDict);
    }
    return self._dictionaryAtIndex;
}

-(NSString *)_getDictionaryProperty:(NSString *)propertyKey
                    withImageIndex:(NSInteger)index
{
    NSDictionary *theDict = [self imagePropertyDictionaryAtIndex:index];
    if (!theDict)
        return NULL;

    return MIUtilityGetDictionaryPropertyWithKey(theDict, propertyKey);
}

#pragma mark Implementing MICreateCGImageInterface protocol methods.

-(CGImageRef)createCGImageWithOptions:(NSDictionary *)options
{
    NSDictionary *imageoptions = @{ (__bridge id)kCGImageSourceShouldAllowFloat :
                                   @(self._allowFloatingPointImages) };
    CFDictionaryRef optionsDict = (__bridge CFDictionaryRef)imageoptions;
    NSInteger index = 0;
    if (options)
    {
        NSNumber *imageIndex = options[MIJSONKeyImageIndex];
        if (imageIndex && [imageIndex isKindOfClass:[NSNumber class]])
        {
            index = imageIndex.integerValue;
        }
    }
    return CGImageSourceCreateImageAtIndex(self._imageSource, index, optionsDict);
}

-(void)passMICGImageTo:(MICGImageWrapper *)imageWrapper
          usingOptions:(NSDictionary *)options
{
    CGImageRef cgImage = [self createCGImageWithOptions:options];
    if (cgImage)
    {
        MICGImage *image = [[MICGImage alloc] initWithCGImage:cgImage];
        CGImageRelease(cgImage);
        [imageWrapper setMICGImageStrong:image];
    }
    else
    {
        MILog(@"Failed to create CGImage with options.",
                             options);
    }
}

#pragma mark Implementing MIBaseObjectSemiPublicInterface public methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }

    if (![propertyType isKindOfClass:[NSString class]])
    {
        MILog(@"Property type key is not a string.", commandDict);
        return MIMakeReplyDictionary(@"Error: Property type key not a string",
                                     MIReplyErrorMissingProperty);
    }
    
    NSDictionary *resultsDict = NULL;
    NSInteger imageIndex;
    NSString *resultString = NULL;
    BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(commandDict,
                                                         MIJSONKeyImageIndex,
                                                         &imageIndex,
                                                         self.numberOfImages);
    if (hasImageIndex)
    {
        if (imageIndex == MIUtilityInvalidImageIndex)
        {
            resultsDict = MIMakeReplyDictionary(@"Error: Invalid image index",
                                                MIReplyErrorInvalidIndex);
        }
        // Need to obtain the property value about image at index not image file
        else if([propertyType isEqualToString:MIJSONPropertyImageSourceStatus])
        {
            NSInteger status;
            status = [self _imageSourceImageStatusAtIndex:imageIndex];
            resultString = [[NSString alloc] initWithFormat:@"%ld", (long)status];
            resultsDict=MIMakeReplyDictionary(resultString,MIReplyErrorNoError);
        }
        else if (MIUtilityDoesPropertyTypeBeginWithDictionary(propertyType))
        {
            NSString *resultString = [self _getDictionaryProperty:propertyType
                                                  withImageIndex:imageIndex];
            if (resultString)
                resultsDict = MIMakeReplyDictionary(resultString,
                                                    MIReplyErrorNoError);
            else
                resultsDict = MIMakeReplyDictionary(
                                            @"Error: Invalid dictionary path",
                                            MIReplyErrorUnknownProperty);
        }
        // Further properties to be handled by the image importer for the
        // image file will go here.
        else
            resultsDict = MIMakeReplyDictionary(
                                @"Error: Requested unknown property for image",
                                MIReplyErrorUnknownProperty);
    }
    else
    {
        // Getting a property about the image file not about a specific image.
        if ([propertyType isEqualToString:MIJSONPropertyNumberOfImages])
        {
            resultString = [[NSString alloc] initWithFormat:@"%ld",
                            self.numberOfImages];
            resultsDict = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                  MIReplyErrorNoError,
                                                  @(self.numberOfImages));
        }
        else if([propertyType isEqualToString:MIJSONPropertyImageSourceStatus])
        {
            NSInteger imageStatus = self._imageSourceStatus;
            resultString = [[NSString alloc] initWithFormat:@"%ld",
                            (long)imageStatus];
            resultsDict=MIMakeReplyDictionary(resultString,MIReplyErrorNoError);
        }
        else if([propertyType isEqualToString:MIJSONPropertyFileType])
        {
            resultsDict = MIMakeReplyDictionary(self._imageSourceType,
                                                MIReplyErrorNoError);
        }
        else if ([propertyType isEqualToString:MIJSONPropertyFile])
        {
            resultString = [self._imagePathURL path];
            resultsDict=MIMakeReplyDictionary(resultString,MIReplyErrorNoError);
        }
        else if ([propertyType isEqualToString:
                                    MIJSONPropertyAllowFloatingPointImages])
        {
            resultString = self._allowFloatingPointImages ? MIJSONValueYes :
                                                            MIJSONValueNo;
            MIMakeReplyDictionaryWithNumericValue(resultString,
                                              MIReplyErrorNoError,
                                              @(self._allowFloatingPointImages));
        }
        else if (MIUtilityDoesPropertyTypeBeginWithDictionary(propertyType))
        {
            NSString *resultString = NULL;
            resultString = MIUtilityGetDictionaryPropertyWithKey(
                                     [self copyImageSourcePropertyDictionary],
                                     propertyType);
            if (resultString)
                resultsDict = MIMakeReplyDictionary(resultString,
                                                    MIReplyErrorNoError);
            else
            {
                MILog(@"Invalid dictionary path", commandDict);
                resultsDict = MIMakeReplyDictionary(
                                            @"Error: Invalid dictionary path",
                                            MIReplyErrorUnknownProperty);
            }
        }
        else
        {
            resultsDict = [super handleCommonGetPropertyCommand:commandDict];
            if (!resultsDict)
            {
                MILog(
                    @"Requested unknown property for image file", commandDict);

                resultsDict = MIMakeReplyDictionary(
                            @"Error: Requested unknown property from image file",
                            MIReplyErrorUnknownProperty);
            }

        }
    }
    return resultsDict;
}

-(NSDictionary *)handleSetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    id propertyValue = commandDict[MIJSONPropertyValue];
    if ([propertyKey isEqualToString:MIJSONPropertyAllowFloatingPointImages])
    {
        if ([propertyValue isKindOfClass:[NSNumber class]])
        {
            self._allowFloatingPointImages = [(NSNumber *)propertyValue boolValue];
        }
        else if ([propertyValue isKindOfClass:[NSString class]])
        {
            NSString *valueString = (NSString *)propertyValue;
            self._allowFloatingPointImages =
                                    [valueString isEqualToString:MIJSONValueYes];
        }
        replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    else
    {
        NSString *message = @"Unknown property to be assigned";
        MILog(message, commandDict);
        NSString *errorMessage = [[NSString alloc] initWithFormat:@"%@%@",
                                                    @"Error: ", message];
        replyDict = MIMakeReplyDictionary(errorMessage,
                                          MIReplyErrorUnknownProperty);
    }
    return replyDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }

    NSInteger imageIndex = 0;
    BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(commandDict,
                                                         MIJSONKeyImageIndex,
                                                         &imageIndex,
                                                         self.numberOfImages);

    if (hasImageIndex && imageIndex == MIUtilityInvalidImageIndex)
    {
        MILog(@"Invalid image index", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Bad image index",
                                          MIReplyErrorOptionValueInvalid);
        return replyDict;
    }

    NSDictionary *imageDict;
    if (hasImageIndex)
        imageDict = [self imagePropertyDictionaryAtIndex:imageIndex];
    else
        imageDict = [self _createImageFilePropertyDictionary];
    
    if (!imageDict)
    {
        MILog(@"Couldn't create property dict ", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: No property dictionary",
                                          MIReplyErrorInvalidOption);
    }
    else
    {
        replyDict = MIUtilitySaveDictionaryToDestination(
                                        commandDict,
                                        imageDict,
                                        NO); // default return to reply dict;
    }
    return replyDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleAssignImageToCollectionCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *ident = commandDict[MIJSONPropertyImageIdentifier];
    NSDictionary *options = commandDict[MIJSONKeyImageOptions];
    if (options && ![options isKindOfClass:[NSDictionary class]])
    {
        NSString *message = @"Error: image creation options should be a dict";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    else if (ident)
    {
        
        CGImageRef cgImage = [self createCGImageWithOptions:options];
        MIContext *context = self.miContext;
        if (cgImage && context)
        {
            MICGImage *image = [[MICGImage alloc] initWithCGImage:cgImage];
            [context assignImage:image identifer:ident];
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            message = cgImage ? @"Error: No context" : @"Error: No image";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
        CGImageRelease(cgImage);
    }
    else
    {
        NSString *message = @"Error: Assign image command dict missing values.";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return result;
}

@end
