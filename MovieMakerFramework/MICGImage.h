//  MICGImage.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
@import CoreGraphics;

#pragma clang assume_nonnull begin
/**
 @brief A minimal CGImage wrapper object that will manage CGImage's lifetime
 @discussion This object is a lifetime managing object for a CGImageRef object
 within the context of ARC. The addtion of conforming to the NSCopying protocol
 and implementing the copy method is 
*/
@interface MICGImage : NSObject <NSCopying>

/// Copy initializer. Designated initializer.
-(instancetype)initWithMICGImage:(MICGImage *)image NS_DESIGNATED_INITIALIZER;

/// Initialize with a CGImageRef
-(instancetype)initWithCGImage:(CGImageRef)image NS_DESIGNATED_INITIALIZER;

/// Do not allow this class to be initialized using super's init.
-(instancetype)init NS_UNAVAILABLE;

/// copy this object method.
-(instancetype)copy;

/// Returns the CGImage.
-(nullable CGImageRef)CGImage CF_RETURNS_NOT_RETAINED;

@end

#pragma clang assume_nonnull end