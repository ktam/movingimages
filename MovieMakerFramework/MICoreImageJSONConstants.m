//  MICoreImageJSONConstants.m
//  Moving Images
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MICoreImageJSONConstants.h"

NSString *const MICoreImageJSONKeyCIFilterList = @"cifilterlist";

NSString *const MICoreImageJSONKeyRenderDestination = @"cirenderdestination";

NSString *const MICoreImageJSONKeyCIFilterName = @"cifiltername";

NSString *const MICoreImageJSONKeyCIFilterProperties = @"cifilterproperties";

NSString *const MICoreImageJSONKeyCIFilterValue = @"cifiltervalue";

NSString *const MICoreImageJSONKeyCIFIlterValueClass = @"cifiltervalueclass";

NSString *const MICoreImageJSONKeyCIFIlterValueKey = @"cifilterkey";

NSString *const MICoreImageJSONKeyCIFilterSourceImageKeepStatic =
                                                    @"cisourceimagekeepstatic";

NSString *const MICoreImageJSONKeyCIFilterIndex = @"cifilterindex";

NSString *const MICoreImageJSONKeyCIFilterForceSourceImageUpdate =
                                                    @"cisourceimageforceupdate";

NSString *const MICoreImageJSONKeyMIFilterName = @"mifiltername";
