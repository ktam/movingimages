//  MIContext.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MICGImage.h"
#import "MIBaseObject.h"
#import "MIBaseObject+Internal.h"
#import "MIBaseObjectArray.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIConstants.h"
#import "MIUtilities.h"

const MIBaseReference kMIInvalidElementReference = -1;
const MIBaseReference kMIWarehouseCapacity = 1000;

@interface MIContext () {
    NSDictionary *__variables;
}

#pragma mark Definitions of private properties

/// A concurrent queue for accessing/adding/removing items in the context.
@property (readonly, strong) dispatch_queue_t _providerQueue;

/// A concurrent queue for accessing/adding/removing variable dictionaries.
@property (readonly, strong) dispatch_queue_t _variablesQueue;

/// A dictionary with keys being object types and values as arrays of objects.
@property (strong) NSDictionary *_listsByType;

/// An array of dictionary of variables, to use to rebuild variables dict.
@property (strong) NSMutableArray *_listOfDictionaryVariables;

/// An dictionary of images.
@property (strong) NSMutableDictionary *_imagesCollection;

/**
 @brief An array of objects whose indexes are MIBaseReference
 @discussion Rather than items being deleted from the the list of objects
 when an object is deleted the item is replaced with a [NSNull null] entry.
 Rather than the list of objects growing indefinitely the maximum size is
 capped to kMMWarehouseCapacity and the size of the array reaches it's
 maximum capacity then base references (indexes) into the array are reused.
 If the list of objects contains more than half the capacity valid objects
 then as new objects are added a warning is emitted to the log file as there
 may be a memory leak where objects are not closed from the calling application.
*/
@property (strong) NSMutableArray *_elements;

/// Keeps track of last item where a base object was added to the list of objects.
@property (assign) MIBaseReference _lastItemPlus1;

#pragma mark Definitions of private instance methods

/// The following methods mirror the internal interface
-(MIBaseReference)_findNullEntry;
-(MIBaseReference)_addElement:(MIBaseObject *)object;
-(void)_removeElementWithReference:(MIBaseReference)objectReference;

/// Append a dictionary with keys for variable names & their associated values.
-(void)_appendVariables:(NSDictionary *)variables;

/// Drop the last variables dictionary added.
-(void)_dropVariablesDictionary:(NSDictionary *)dictToDrop;

/// Regenerate the list of dictionary variables after addition/deletion collection
-(void)_generateNewDictionaryOfVariables;

/**
 @brief Assign an image to collection with identifier. Returns YES on success.
 @discussion Will remove a previous image if it used the same identifier
*/

-(BOOL)_assignImage:(MICGImage *)theImage identifier:(NSString *)identifier;

/// Remove identified image from image collection. Returns YES if no error.
-(BOOL)_removeImageWithIdentifier:(NSString *)identifier;

/// Get image from image collection with identifier.
-(MICGImage *)_getImageWithIdentifier:(NSString *)identifier;

/// Clear all image from collection.
-(void)_clearAllImages;

/// Create the provider queue and the variables dictionary queue.
-(void)_createQueues;

@end

static NSArray *CreateListOfObjectTypeKeys()
{
    static NSArray *objectTypes = nil;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
    {
        objectTypes = @[
            MICGBitmapContextKey,
            MICGPDFContextKey,
            MIImageImporterKey,
            MIImageExporterKey,
            MIImageFilterKey,
            MIMovieImporterKey,
            MIMovieEditorKey,
            MIMovieVideoFramesWriterKey,
#if !TARGET_OS_IPHONE
            MINSGraphicContextKey,
//             MIMovieFrameIteratorKey
#endif
        ];
    });
    return objectTypes;
}

@implementation MIContext

#pragma mark Public class methods

+(MIContext *)defaultContext
{
    static dispatch_once_t pred;
    static MIContext *context;
    dispatch_once(&pred, ^
    {
        context = [MIContext new];
    });
    
    return context;
}

#pragma mark Private instance methods implementation

-(MIBaseReference)_findNullEntry
{
    MIBaseReference result = kMIInvalidElementReference;
    if (self._elements.count >= kMIWarehouseCapacity)
    {
        NSRange range;
        size_t length = self._elements.count;
        range = NSMakeRange(self._lastItemPlus1, length - self._lastItemPlus1);
        result =[self._elements indexOfObjectIdenticalTo:[NSNull null]
                                                inRange:range];
        if (result == NSNotFound)
        {
            result = [self._elements indexOfObjectIdenticalTo:[NSNull null]];
            if (result == NSNotFound)
                result = kMIInvalidElementReference;
        }
        
        if (result != kMIInvalidElementReference)
            self._lastItemPlus1 = (result + 1) % kMIWarehouseCapacity;
    }
    return result;
}

-(MIBaseReference)_addElement:(MIBaseObject *)object
{
    MIBaseReference result = kMIInvalidElementReference;
    if (object == NULL)
        return result;
    
    MIBaseReference numEntries = [self._elements count];
    MIBaseReference nullEntry = [self _findNullEntry];
    if (nullEntry == kMIInvalidElementReference)
    {
        if (numEntries < kMIWarehouseCapacity)
        {
            [self._elements addObject:object];
            result = numEntries;
        }
    }
    else
    {
        [self._elements replaceObjectAtIndex:nullEntry withObject:object];
        result = nullEntry;
    }
    
    if (result != kMIInvalidElementReference)
        self->_numberOfObjects++;
    
    if (self.numberOfObjects > (kMIWarehouseCapacity / 2))
    {
        MILog(
                      @"Wharehouse is over half full. Are you leaking objects.",
                      @(self.numberOfObjects));
        MILogBreak();
    }
    return result;
}

-(void)_removeElementWithReference:(MIBaseReference)elementReference
{
    MIBaseObject *object = [self._elements objectAtIndex:elementReference];
    [object finalize];
    self->_numberOfObjects--;
    [self._elements replaceObjectAtIndex:elementReference
                              withObject:[NSNull null]];
}

-(void)_removeObject:(MIBaseObject *)object
{
    MIBaseObjectArray *array = self._listsByType[object.baseObjectType];
    [array removeObject:object];
    [self _removeElementWithReference:object.reference];
}

-(BOOL)_removeObjectWithReference:(MIBaseReference)objectReference
{
    // This will capture out of bounds object references.
    if (objectReference >= self._elements.count)
        return NO;
    
    MIBaseObject *object;
    object = [self._elements objectAtIndex:objectReference];
    
    MIBaseObjectArray *array = self._listsByType[object.baseObjectType];
    [array removeObject:object];
    
    [self _removeElementWithReference:objectReference];
    return YES;
}

-(void)_removeAllObjectsWithType:(NSString *)objectType
{
    MIBaseObjectArray *objects = self._listsByType[objectType];
    if (objects)
    {
        NSArray *copiedList = objects.immutableArray;
        [objects removeAllObjects];
        for (MIBaseObject *object in copiedList)
        {
            [self _removeElementWithReference:object.reference];
        }
    }
}

-(void)_generateNewDictionaryOfVariables
{
    if (self._listOfDictionaryVariables && self._listOfDictionaryVariables.count)
    {
        NSMutableDictionary *variables = [NSMutableDictionary new];
        for (NSDictionary *theDict in self._listOfDictionaryVariables)
        {
            [variables addEntriesFromDictionary:theDict];
        }
        self->__variables = [[NSDictionary alloc] initWithDictionary:variables];
    }
    else
    {
        self->__variables = [NSDictionary new];
    }
}

-(void)_createQueues
{
    dispatch_queue_t globalQueue;
    globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t myQueue;
    
    // Creating a concurrent queue. Using dispatch_barrier_sync to limit mutating
    // the array of elements in the context.
    myQueue = dispatch_queue_create("com.zukini.cicontext.provider",
                                    DISPATCH_QUEUE_CONCURRENT);
    dispatch_set_target_queue(myQueue, globalQueue);
    self->__providerQueue = myQueue;
    myQueue = dispatch_queue_create("com.zukini.cicontext.variables",
                                    DISPATCH_QUEUE_CONCURRENT);
    dispatch_set_target_queue(myQueue, globalQueue);
    self->__variablesQueue = myQueue;
}

-(void)_appendVariables:(NSDictionary *)variables
{
    if (!variables)
        return;
    
    if (!self._listOfDictionaryVariables)
    {
        self._listOfDictionaryVariables = [NSMutableArray new];
    }
    [self._listOfDictionaryVariables addObject:variables];
    [self _generateNewDictionaryOfVariables];
}

-(void)_dropVariablesDictionary:(NSDictionary *)dictToDrop
{
    if (!dictToDrop)
        return;
    
    if (!self._listOfDictionaryVariables)
        return;
    
    [self._listOfDictionaryVariables removeObject:dictToDrop];
    if (self._listOfDictionaryVariables.count == 0)
    {
        self._listOfDictionaryVariables = nil;
    }
    [self _generateNewDictionaryOfVariables];
}

-(void)_dropVariables:(nullable NSDictionary *)dropDict
      appendVariables:(nullable NSDictionary *)appendDict
{
    if (dropDict && self._listOfDictionaryVariables)
    {
        [self._listOfDictionaryVariables removeObject:dropDict];
    }
    
    if (appendDict) {
        if (!self._listOfDictionaryVariables)
        {
            self._listOfDictionaryVariables = [NSMutableArray new];
        }
        [self._listOfDictionaryVariables addObject:appendDict];
    }
    
    if (self._listOfDictionaryVariables &&
        self._listOfDictionaryVariables.count == 0)
    {
        self._listOfDictionaryVariables = nil;
    }
    
    [self _generateNewDictionaryOfVariables];
}

-(BOOL)_assignImage:(MICGImage *)theImage identifier:(NSString *)identifier
{
    BOOL result = NO;
    if (theImage && identifier && [identifier isKindOfClass:[NSString class]])
    {
        [self._imagesCollection setObject:theImage
                                   forKey:identifier];
        result = YES;
    }
    return result;
}

-(BOOL)_removeImageWithIdentifier:(NSString *)identifier
{
    BOOL result = NO;
    if (identifier)
    {
        [self._imagesCollection removeObjectForKey:identifier];
        result = YES;
    }
    return result;
}

-(MICGImage *)_getImageWithIdentifier:(NSString *)identifier
{
    MICGImage *theImage;
    if (identifier)
    {
        theImage = [self._imagesCollection objectForKey:identifier];
    }
    return theImage;
}

-(void)_clearAllImages
{
    [self._imagesCollection removeAllObjects];
}

#pragma mark Implementation of public properties

-(BOOL)isEmpty
{
    return self.numberOfObjects < 1;
}

-(NSDictionary *)variables
{
    __block NSDictionary *vars;
    dispatch_sync(self._variablesQueue, ^
    {
        vars = self->__variables;
    });
    return vars;
}

#pragma mark Public instance methods implementation

-(instancetype)init
{
    self = [super init];
    if (!self)
        return self;

    [self _createQueues];
    self._elements = [[NSMutableArray alloc] initWithCapacity:0];

    NSMutableDictionary *types = [[NSMutableDictionary alloc] initWithCapacity:0];
    NSArray *objectTypeKeys = CreateListOfObjectTypeKeys();
    for (NSString *key in objectTypeKeys)
    {
        types[key] = [MIBaseObjectArray new];
    }
    self._listsByType = [[NSDictionary alloc] initWithDictionary:types];
    self._lastItemPlus1 = 0;
    self._imagesCollection = [[NSMutableDictionary alloc] initWithCapacity:0];
    self->__variables = [NSDictionary new];
    return self;
}

-(void)dealloc
{
    [self _clearAllImages];
    [self removeAllObjects];
}

-(void)appendVariables:(NSDictionary *)variables
{
    if (!variables)
        return;
    
    dispatch_barrier_sync(self._variablesQueue, ^
    {
        [self _appendVariables:variables];
    });
}

-(void)dropVariablesDictionary:(NSDictionary *)dictToDrop
{
    if (!dictToDrop)
        return;

    dispatch_barrier_sync(self._variablesQueue, ^
    {
        [self _dropVariablesDictionary:dictToDrop];
    });
}

-(void)dropVariablesDictionary:(NSDictionary *)dropDict
           appendNewDictionary:(NSDictionary *)newDict
{
    dispatch_barrier_sync(self._variablesQueue, ^
    {
        [self _dropVariables:dropDict appendVariables:newDict];
    });
}

-(BOOL)assignCGImage:(CGImageRef)theImage identifier:(NSString *)identifier
{
    BOOL result = NO;
    if (theImage)
    {
        MICGImage *miImage = [[MICGImage alloc] initWithCGImage:theImage];
        result = [self assignImage:miImage identifer:identifier];
    }
    return result;
}

-(BOOL)assignImage:(MICGImage *)image identifer:(NSString *)identifier
{
    __block BOOL result = NO;
    dispatch_barrier_sync(self._variablesQueue, ^
    {
        result = [self _assignImage:image identifier:identifier];
    });
    return result;
}

-(BOOL)removeImageWithIdentifier:(NSString *)identifier
{
    __block BOOL result;
    dispatch_barrier_sync(self._variablesQueue, ^
    {
        result = [self _removeImageWithIdentifier:identifier];
    });
    return result;
}

-(MICGImage *)getImageWithIdentifier:(NSString *)identifier
{
    __block MICGImage *image;
    dispatch_sync(self._variablesQueue, ^
    {
        image = [self _getImageWithIdentifier:identifier];
    });
    return image;
}

-(CGImageRef)getCGImageWithIdentifier:(NSString *)identifier
{
    MICGImage *miImage = [self getImageWithIdentifier:identifier];
    return miImage.CGImage;
}

-(MIBaseReference)addObject:(MIBaseObject *)object withType:(NSString *)baseType
{
    __block MIBaseReference objectRef = kMIInvalidElementReference;
    MIBaseObjectArray *array = self._listsByType[baseType];
    if (!array)
        return kMIInvalidElementReference;

    dispatch_barrier_sync(self._providerQueue, ^
    {
        objectRef = [self _addElement:object];
        if (objectRef != kMIInvalidElementReference)
        {
            [array addObject:object];
        }
    });

    return objectRef;
}

-(BOOL)removeObjectWithReference:(MIBaseReference)objectReference
{
    // This will capture invalid object references including
    // kMIInvalidElementReference
    if (objectReference < 0)
        return NO;

    // This will capture out of bounds object references.
    if (objectReference >= self._elements.count)
    {
        MILog(@"objectReference is beyond bounds.", nil);
        return NO;
    }

    __block BOOL objectRemoved = NO;
    dispatch_barrier_sync(self._providerQueue, ^
    {
        MIBaseObject *object = [self._elements objectAtIndex:objectReference];
        if ((NSNull *)object != [NSNull null])
        {
            MIBaseObjectArray *array = self._listsByType[object.baseObjectType];
            [array removeObject:object];
            [self _removeElementWithReference:objectReference];
            objectRemoved = YES;
        }
    });
    return objectRemoved;
}

-(BOOL)removeObject:(MIBaseObject *)object
{
    if (!object)
        return NO;

    dispatch_barrier_sync(self._providerQueue, ^
    {
        [self _removeObject:object];
    });
    return YES;
}

-(void)removeAllObjectsWithType:(NSString *)objectType
{
    dispatch_barrier_sync(self._providerQueue, ^
    {
        [self _removeAllObjectsWithType:objectType];
    });
}

-(void)removeAllObjects
{
    dispatch_barrier_sync(self._providerQueue, ^
    {
        NSArray *allKeys = self._listsByType.allKeys;
        for (NSString *key in allKeys)
        {
            [self _removeAllObjectsWithType:key];
        }
    });
}

-(MIBaseObject *)objectWithReference:(MIBaseReference)reference
{
    // This will capture invalid object references including
    // kMIInvalidElementReference
    if (reference < 0)
        return nil;

    __block MIBaseObject *object;
    dispatch_sync(self._providerQueue, ^{
        if (reference < self._elements.count)
        {
            object = [self._elements objectAtIndex:reference];
            object = (object == (MIBaseObject *)[NSNull null]) ? nil : object;
        }
    });
    return object;
}

-(MIBaseObject *)objectWithType:(NSString *)type name:(NSString *)name
{
    MIBaseObject *object;
    MIBaseObjectArray *array = self._listsByType[type];
    if (!array)
        return nil;

    object = [array baseObjectWithName:name];
    return object;
}

-(MIBaseObject *)objectWithType:(NSString *)type index:(NSInteger)index
{
    MIBaseObject *object;
    MIBaseObjectArray *array = self._listsByType[type];
    if (!array)
        return nil;

    if (index < 0 || index >= array.count)
        return nil;
    
    object = [array objectAtIndex:index];
    return object;
}

-(NSInteger)numberOfObjectsOfType:(NSString *)objectType
{
    MIBaseObjectArray *array = self._listsByType[objectType];
    if (!array)
        return -1;
    
    return array.count;
}

@end
