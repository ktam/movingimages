//  MIMovieVideoFramesWriter.h
//  MovingImagesFramework
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"

@class AVAssetWriter;

/**
 The `MIMovieVideoFramesWriter` class is an Objective-C wrapper for an
 AVAssetWriter object. This is one of the MovingImages Framework base classes.
 For documentation on AVAssetWriter please refer to:
 AVFoundation Programming Guide.
 */
@interface MIMovieVideoFramesWriter :
                                MIBaseObject <MIBaseObjectSemiPublicInterface>


/**
 @brief Initializes the MIMovieImporter object.
 @param fileURL The file to create the CGImageSourceRef object from.
 @param name The name the object is given to use to refer to the object
 @param context The context within which to create image importer base object
*/
-(instancetype)initWithAssetWriter:(AVAssetWriter *)assetWriter
                              name:(NSString *)theName
                         inContext:(MIContext *)context;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end
