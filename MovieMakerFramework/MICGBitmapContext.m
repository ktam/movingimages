//  MICGBitmapContext.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MICGBitmapContext.h"
#import "MICGContext.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MICoreImage.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#import "MIBaseObject_Protected.h"

@import CoreGraphics;

#if TARGET_OS_IPHONE  // TARGET_OS_MAC
@import CoreImage;
#endif

@import QuartzCore;

@interface MICGBitmapContext ()

@property (readonly, strong) MICGContext *_miCGContext;
@property (strong) dispatch_queue_t _drawingQueue;

@property (strong) MICGImage *_image;
@property (strong) MICGImage *_snapShotImage;
@property (strong) CIContext *_ciContext;
@property (weak) MICoreImage *_requestingObject;
@property (assign) BOOL _softwareRender;
@property (assign) BOOL _useSRGBColorSpace;

/// The preset used for defining the properties for creating the graphic context
@property (nonatomic, readonly, strong) NSString *_preset;

/// Return the width of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _width;

/// Return the height of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _height;

/// Number of bits per channel in the pixel.
@property (nonatomic, readonly) size_t _bitsPerComponent;

/// Number of bits per pixel
@property (nonatomic, readonly) size_t _bitsPerPixel;

/// Number of bytes per row.
@property (nonatomic, readonly) size_t _bytesPerRow;

/// The color space of the graphic context
@property (nonatomic, readonly) CGColorSpaceRef _colorSpace;

/// The alpha channel info
@property (nonatomic, readonly) CGImageAlphaInfo _alphaInfo;

/// The bit map info.
@property (nonatomic, readonly) CGBitmapInfo _bitmapInfo;

/// Make an image from the full bitmap context.
-(MICGImage *)_makeImage;

/// _getImage Calls _makeImage and assigns the image to _image.
-(MICGImage *)_getImage;

/// _takeSnapshot Calls _makeImage and assigns the image to _snapShotImage.
-(void)_takeSnapshot;

-(void)_getCIContextFor:(MICoreImage *)requestingObject
                options:(NSDictionary *)options;

/**
 @brief This is the draw command handler.
 @discussion This method should only be called when running on the _drawingQueue.
 The public interface should always wrapp calling this method in a dispatch_sync
 or dispatch_async before it is called.
*/
-(BOOL)_handleDrawElementCommand:(NSDictionary *)commandDict;

/**
 @brief Get the pixel data within the rectangle inRect.
 @discussion This method returns a dictionary with two entries. The first is an
 array of column names, accessed by "columnNames" key. The second is the pixel
 data accessed by the "pixelData" key. The pixel data is an array of arrays.
 The outer pixel data array will have a maximum length of
 kMICGBitmapContextMaxPixelRequest because that is the largest amount of pixel
 data that can be requested in one go.
 
 The inside array will typically have 6 entries "x", "y", "red", "green",
 "blue", "alpha" but this can change depending on the format of the pixel buffer.
 The "x" and "y" values will always be there. The "alpha" data may not be there
 if the bitmap context doesn't process the alpha channel. The bitmap context
 might represent a monochrome (usually grayscale) image with or without an
 alpha channel. There is also a CMYK option which doesn't have an alpha channel
 but will have four colour components: "Cyan", "Magenta", "Yellow", "CMYKBlack".
 A grayscale bitmap with no alpha channel would have 3 columns of data. "x", "y",
 "gray".
*/
-(NSDictionary *)_getPixelDataInRect:(CGRect)inRect;

/// Return an array of string objects. Each string is a different blend mode.
+(NSArray *)blendModes;

@end

static const size_t kMICGBitmapContextMaxPixelRequest = 65536;

#pragma mark MICGBitmapContext Static (local) helper functions.

#if TARGET_OS_IPHONE

NSString *kMIAlphaOnly = @"AlphaOnly";
NSString *kMIGray = @"Gray";
NSString *kMIDeviceRGB = @"DeviceRGB";

static NSDictionary *GetCGBitmapContextDictionaryFromPreset(NSString *preset)
{
    NSDictionary *resultsDict = NULL;
    static NSDictionary *presetsDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
    {
        presetsDict =
        @{
            MIAlphaOnly8bpc8bppInteger :
            @{
                @"colorspace" : kMIAlphaOnly,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @8,
                @"alphainfo" : @(kCGImageAlphaOnly)
            },
            MIGray8bpc8bppInteger :
            @{
                @"colorspace" : kMIGray,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @8,
                @"alphainfo" : @(kCGImageAlphaNone)
            },
            MIGray32bpc32bppFloat :
            @{
                @"colorspace" : kMIGray,
                @"bitspercomponent" : @32,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNone |
                                 kCGBitmapFloatComponents |
                                 kCGBitmapByteOrder32Little)
            },
            MIAlphaSkipFirstRGB8bpc32bppInteger :
            @{
                @"colorspace" : kMIDeviceRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNoneSkipFirst)
            },
            MIAlphaSkipLastRGB8bpc32bppInteger :
            @{
                @"colorspace" : kMIDeviceRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNoneSkipLast)
            },
            MIAlphaPreMulFirstRGB8bpc32bppInteger :
            @{
                @"colorspace" : kMIDeviceRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedFirst)
            },
            MIAlphaPreMulBGRA8bpc32bppInteger :
            @{
                @"colorspace" : kMIDeviceRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedFirst |
                                 kCGBitmapByteOrder32Little)
            },
            MIAlphaPreMulLastRGB8bpc32bppInteger :
            @{
                @"colorspace" : kMIDeviceRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedLast)
            }
        };
    });
    
    resultsDict = presetsDict[preset];
    return resultsDict;
}

CGContextRef CreateCGBitmapContextFromPresetSize(NSString *preset,
                                                 CGSize size,
                                                 CGColorSpaceRef colorSpace)
{
    CGContextRef theContext = NULL;
    NSDictionary *theDict = GetCGBitmapContextDictionaryFromPreset(preset);
    if (theDict == NULL)
        return theContext;
    
    NSInteger bpc = [theDict[@"bitspercomponent"] integerValue];
    NSInteger bpp = [theDict[@"bitsperpixel"] integerValue];
    size_t width = size.width;
    size_t height = size.height;
    NSInteger alphaInfo = [theDict[@"alphainfo"] integerValue];
    CGBitmapInfo bitMapInfo = (CGBitmapInfo)alphaInfo;
    
    size_t bytesPerRow = width * bpp / 8;
    // For efficiency We need to make sure row bytes is a multiple of 16 bytes.
    if (bytesPerRow % 16)
        bytesPerRow += 16 - bytesPerRow % 16;
    
    CGColorSpaceRef localColorSpace = nil;
    if (!colorSpace)
    {
        NSString *colorSpaceName = theDict[@"colorspace"];
        if ([colorSpaceName isEqualToString:kMIGray])
        {
            localColorSpace = CGColorSpaceCreateDeviceGray();
        }
        else
        {
            localColorSpace = CGColorSpaceCreateDeviceRGB();
        }
        colorSpace = localColorSpace;
    }
    
    theContext = CGBitmapContextCreate(NULL, width, height, bpc, bytesPerRow,
                                       colorSpace, bitMapInfo);
    if (localColorSpace)
    {
        CGColorSpaceRelease(localColorSpace);
    }
    if (alphaInfo != kCGImageAlphaNone)
    {
        CGContextSetAlpha(theContext, 1.0);
    }
    
    CGFloat whiteOfGray[2] = { 1.0, 1.0 };
    CGColorSpaceRef deviceGray = CGColorSpaceCreateDeviceGray();
    CGColorRef white = CGColorCreate(deviceGray, whiteOfGray);
    CGColorSpaceRelease(deviceGray);
    CGContextSetFillColorWithColor(theContext, white);
    CGColorRelease(white);
    CGRect theRect = CGRectMake(0.0, 0.0, width, height);
    CGContextFillRect(theContext, theRect);
    return theContext;
}

#else

static NSDictionary *GetCGBitmapContextDictionaryFromPreset(NSString *preset)
{
    NSDictionary *resultsDict = NULL;
    static NSDictionary *presetsDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
    {
        presetsDict =
        @{
            MIAlphaOnly8bpc8bppInteger :
            @{
                @"colorspace" : (NSString *)[NSNull null],
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @8,
                @"alphainfo" : @(kCGImageAlphaOnly)
            },
            MIGray8bpc8bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceGenericGray,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @8,
                @"alphainfo" : @(kCGImageAlphaNone)
            },
            MIGray16bpc16bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceGenericGray,
                @"bitspercomponent" : @16,
                @"bitsperpixel" : @16,
                @"alphainfo" : @(kCGImageAlphaNone | kCGBitmapByteOrder16Little)
            },
            MIGray32bpc32bppFloat :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceGenericGray,
                @"bitspercomponent" : @32,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNone |
                    kCGBitmapFloatComponents |
                    kCGBitmapByteOrder32Little)
            },
            MIAlphaSkipFirstRGB8bpc32bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNoneSkipFirst)
            },
            MIAlphaSkipLastRGB8bpc32bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaNoneSkipLast)
            },
            MIAlphaPreMulFirstRGB8bpc32bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedFirst |
                                 kCGBitmapByteOrder32Big)
            },
            MIAlphaPreMulBGRA8bpc32bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedFirst |
                                 kCGBitmapByteOrder32Little)
            },
            MIAlphaPreMulLastRGB8bpc32bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @8,
                @"bitsperpixel" : @32,
                @"alphainfo" : @(kCGImageAlphaPremultipliedLast)
            },
            MIAlphaPreMulLastRGB16bpc64bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @16,
                @"bitsperpixel" : @64,
                @"alphainfo" : @(kCGImageAlphaPremultipliedLast |
                                 kCGBitmapByteOrder16Little)
            },
            MIAlphaSkipLastRGB16bpc64bppInteger :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @16,
                @"bitsperpixel" : @64,
                @"alphainfo" : @(kCGImageAlphaNoneSkipLast |
                    kCGBitmapByteOrder16Little)
            },
            MIAlphaSkipLastRGB32bpc128bppFloat :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @32,
                @"bitsperpixel" : @128,
                @"alphainfo" : @(kCGImageAlphaNoneSkipLast |
                    kCGBitmapFloatComponents |
                    kCGBitmapByteOrder32Little)
            },
            MIAlphaPreMulLastRGB32bpc128bppFloat :
            @{
                @"colorspace" : (NSString *)kCGColorSpaceSRGB,
                @"bitspercomponent" : @32,
                @"bitsperpixel" : @128,
                @"alphainfo" : @(kCGImageAlphaPremultipliedLast |
                    kCGBitmapFloatComponents |
                    kCGBitmapByteOrder32Little)
            }
        };
    });
    
    resultsDict = presetsDict[preset];
    return resultsDict;
}

CGContextRef CreateCGBitmapContextFromPresetSize(NSString *preset,
                                                 CGSize size,
                                                 CGColorSpaceRef colorSpace)
{
    CGContextRef theContext = NULL;
    NSDictionary *theDict = GetCGBitmapContextDictionaryFromPreset(preset);
    if (theDict == NULL)
        return theContext;
    
    NSInteger bpc = [theDict[@"bitspercomponent"] integerValue];
    NSInteger bpp = [theDict[@"bitsperpixel"] integerValue];
    size_t width = size.width;
    size_t height = size.height;
    NSInteger alphaInfo = [theDict[@"alphainfo"] integerValue];
    CGBitmapInfo bitMapInfo = (CGBitmapInfo)alphaInfo;
    
    size_t bytesPerRow = width * bpp / 8;
    // For efficiency We need to make sure row bytes is a multiple of 16 bytes.
    if (bytesPerRow % 16)
        bytesPerRow += 16 - bytesPerRow % 16;
    
    CGColorSpaceRef locallyOwnedColorSpace = NULL; // (CGColorSpaceRef)kCFNull;
    if (!colorSpace)
    {
        CFStringRef profileName = (__bridge CFStringRef)theDict[@"colorspace"];
        locallyOwnedColorSpace = CGColorSpaceCreateWithName(profileName);
        // locallyOwnedColorSpace = CGColorSpaceCreateDeviceRGB();
        colorSpace = locallyOwnedColorSpace;
    }
    
    theContext = CGBitmapContextCreate(NULL, width, height, bpc, bytesPerRow,
                                       colorSpace, bitMapInfo);
    if (alphaInfo != kCGImageAlphaNone)
        CGContextSetAlpha(theContext, 1.0);
    
    CGFloat whiteOfGray[] = { 1.0, 1.0 };
    CGColorSpaceRef deviceGray = CGColorSpaceCreateDeviceGray();
    CGColorRef white = CGColorCreate(deviceGray, whiteOfGray);
    CGColorSpaceRelease(deviceGray);
    CGContextSetFillColorWithColor(theContext, white);
    CGRect theRect = CGRectMake(0.0, 0.0, width, height);
    CGContextFillRect(theContext, theRect);
    CGColorRelease(white);
    CGColorSpaceRelease(locallyOwnedColorSpace);
    return theContext;
}

#endif

static NSArray *GetPixelDataFromBuffer(void *buffer,
                                       size_t x1, size_t x2,
                                       size_t y1, size_t y2,
                                       size_t rowBytes,
                                       size_t bytesPerPixel,
                                       size_t bytesPerComponent,
                                       CGColorSpaceModel colorModel,
                                       BOOL isValidAlphaChannel,
                                       BOOL hasAlphaChannel,
                                       BOOL isAlphaFirst,
                                       BOOL isAlphaLast,
                                       BOOL isFloat)
{
    NSArray *result;
    NSMutableArray *outerArray = [[NSMutableArray alloc] initWithCapacity:0];
    unsigned char *dataPtr = nil;
    UInt16 *uint16DataPtr = nil;
    float *floatDataPtr = nil;
    
    size_t width = x2 - x1;
    size_t height = y2 - y1;
    
    void *data = buffer + rowBytes * y1 + bytesPerPixel * x1;
    
    for (size_t y = 0 ; y < height ; ++y)
    {
        void *xData = data;
        for (size_t x = 0 ; x < width ; ++x)
        {
            NSMutableArray *innerArray;
            innerArray = [[NSMutableArray alloc] initWithCapacity:0];
            innerArray[0] = @(x + x1);
            innerArray[1] = @(y + y1);
            if (isFloat)
            {
                size_t numFloats = bytesPerPixel / sizeof(float);
                size_t index = 0;
                floatDataPtr = xData;
                if (hasAlphaChannel && isAlphaFirst)
                {
                    if (isValidAlphaChannel)
                        [innerArray addObject:[[NSNumber alloc]
                                    initWithFloat:floatDataPtr[index]]];
                    index++;
                }
                if (isAlphaLast && !isValidAlphaChannel)
                {
                    numFloats--;
                }
                
                for ( ; index < numFloats ; ++index)
                {
                    [innerArray addObject:[[NSNumber alloc]
                                    initWithFloat:floatDataPtr[index]]];
                }
            }
            else
            {
                if (bytesPerComponent == 1)
                {
                    dataPtr = xData;
                    size_t numComponents = bytesPerPixel;
                    size_t index = 0;
                    if (hasAlphaChannel && isAlphaFirst)
                    {
                        if (isValidAlphaChannel)
                            [innerArray addObject:[[NSNumber alloc]
                                    initWithUnsignedChar:dataPtr[index]]];
                        index++;
                    }
                    if (isAlphaLast && !isValidAlphaChannel)
                    {
                        numComponents--;
                    }
                    
                    for ( ; index < numComponents ; ++index)
                    {
                        [innerArray addObject:[[NSNumber alloc]
                                    initWithUnsignedChar:dataPtr[index]]];
                    }
                }
                else if (bytesPerComponent == 2)
                {
                    uint16DataPtr = xData;
                    size_t numComponents = bytesPerPixel / bytesPerComponent;
                    size_t index = 0;
                    if (hasAlphaChannel && isAlphaFirst)
                    {
                        if (isValidAlphaChannel)
                            [innerArray addObject:[[NSNumber alloc]
                                    initWithUnsignedShort:uint16DataPtr[index]]];
                        index++;
                    }
                    if (isAlphaLast && !isValidAlphaChannel)
                    {
                        numComponents--;
                    }
                    
                    for ( ; index < numComponents ; ++index)
                    {
                        [innerArray addObject:[[NSNumber alloc]
                                    initWithUnsignedShort:uint16DataPtr[index]]];
                    }
                }
                else
                {
                    // We should never get here.
                    return nil;
                }
            }
            [outerArray addObject:innerArray];
            xData = xData + bytesPerPixel;
        }
        data = data + rowBytes;
    }
    result = [[NSArray alloc] initWithArray:outerArray];
    return result;
}

#pragma mark -
#pragma mark CLASS MICGBitmapContext implementation.

@implementation MICGBitmapContext

#pragma mark Public methods

/**
 @brief Initialize a MICGBitmapContext object using the preset and dimensions.
 @discussion Each preset defines a number of parameters used to create the
 bitmap context.
*/
-(instancetype)initWithPreset:(NSString *)preset
                         name:(NSString *)theName
                bitmapContext:(CGContextRef)cgContext
                    inContext:(MIContext *)context
{
    if (!cgContext)
        return nil;

    context = context ? context : [MIContext defaultContext];
    self = [super initWithBaseObjectType:MICGBitmapContextKey
                                    name:theName
                               inContext:context];
    
    if (self)
    {
        dispatch_queue_t defaultPriorityQueue;
        defaultPriorityQueue = dispatch_get_global_queue(
                                         DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_queue_t myQueue;
        NSString *queueName = [NSString stringWithFormat:
                               @"com.zukini.micgbitmapcontext.%p", self];
        myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(myQueue, defaultPriorityQueue);
        
        self->__preset = preset;
        self->__miCGContext = [[MICGContext alloc] initWithCGContext:cgContext
                                                           miContext:context
                                                               owner:self];
        self->__drawingQueue = myQueue;
    }
    return self;
}

-(void)close
{
    [super baseObjectClose];
}

#pragma mark Public Class Methods

+(NSArray *)blendModes
{
    return [MICGContext blendModes];
}

#pragma mark Public Class MIBaseObjectSemiPublicInterface methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    context = context ? context : [MIContext defaultContext];
    if (propertyKey == NULL)
    {
        MILog(@"Missing property key", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property key",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyPresets])
    {
        NSArray *presetList = MICGBitmapGetPresetList();
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                      presetList);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyBlendModes])
    {
        NSArray *blendModes = [MICGBitmapContext blendModes];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                      blendModes);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyUserInterfaceFonts])
    {
        NSArray *uiFonts = MIUtilityGetListOfUserInterfaceFonts();
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                      uiFonts);
        replyDict = MIMakeReplyDictionary( resultStr, 0); /* **success** */
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MICGBitmapContextKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property requested.", commandDict);
        replyDict = MIMakeReplyDictionary(
                                @"Error: MICGBitmapContext unknown property",
                                MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *preset = commandDict[MIJSONPropertyPreset];

    if (!preset)
    {
        MILog(@"Missing preset option creating bitmap context.",
                             commandDict);
        replyDict = MIMakeReplyDictionary(
                        @"Error: Missing preset option creating bitmap context.",
                        MIReplyErrorMissingOption);
        return replyDict;
    }
    
    CGSize theSize;
    BOOL gotSize = MIUtilityGetCGSizeFromDictionary(
            commandDict[MIJSONKeySize], &theSize, kMILog, context.variables);
    
    if (!gotSize)
    {
        MILog(@"Missing bitmap size creating bitmap context.",
                             commandDict);
        replyDict = MIMakeReplyDictionary(
                        @"Error: Missing size creating bitmap context.",
                        MIReplyErrorMissingOption);
        return replyDict;
    }

    MICGBitmapContext *object;
    NSString *objectName, *colorSpaceName;
    objectName = commandDict[MIJSONKeyObjectName];
    colorSpaceName = commandDict[MIJSONPropertyColorProfile];
    CGContextRef cgContext;
    CGColorSpaceRef colorSpace = nil;
    
    if (colorSpaceName)
    {
        if ([colorSpaceName isEqualToString:MIJSONValueDeviceRGB])
        {
            colorSpace = CGColorSpaceCreateDeviceRGB();
        }
        else
        {
            colorSpace = CGColorSpaceCreateWithName((CFStringRef)colorSpaceName);
        }
    }

    if ([preset isEqualToString:MIPlatformDefaultBitmapContext])
    {
    #if TARGET_OS_IPHONE
        preset = MIAlphaPreMulBGRA8bpc32bppInteger;
    #else
        preset = MIAlphaPreMulFirstRGB8bpc32bppInteger;
    #endif
    }
    
    cgContext = CreateCGBitmapContextFromPresetSize(preset, theSize,
                                                    colorSpace);
    CGColorSpaceRelease(colorSpace);

    object = [[MICGBitmapContext alloc] initWithPreset:preset
                                                  name:objectName
                                         bitmapContext:cgContext
                                             inContext:context];
    CGContextRelease(cgContext);

    if (object)
    {
        MIBaseReference objectReference = [object reference];
        NSNumber *refNum = [[NSNumber alloc] initWithInteger:objectReference];
        NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                               (long)objectReference];
        replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                          MIReplyErrorNoError,
                                                          refNum);            
    }
    else
    {
        MILog(@"Failed to create bitmap context.", commandDict);
        replyDict = MIMakeReplyDictionary(
                                @"Error: Failed to create bitmap context",
                                MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MICGBitmapContextKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

+(NSDictionary *)handleCalculateGraphicSizeOfTextCommand:(NSDictionary *)dict
                                               inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    return MIUtilityCalculateGraphicSizeOfTest(dict, context.variables);
}

#pragma mark Manual implementation of MICGBitmapContext properties.

-(size_t)_width
{
    return CGBitmapContextGetWidth(self._miCGContext.context);
}

-(size_t)_height
{
    return CGBitmapContextGetHeight(self._miCGContext.context);
}

-(size_t)_bitsPerComponent
{
    return CGBitmapContextGetBitsPerComponent(self._miCGContext.context);
}

-(size_t)_bitsPerPixel
{
    return CGBitmapContextGetBitsPerPixel(self._miCGContext.context);
}

-(size_t)_bytesPerRow
{
    return CGBitmapContextGetBytesPerRow(self._miCGContext.context);
}

-(CGColorSpaceRef)_colorSpace
{
    return CGBitmapContextGetColorSpace(self._miCGContext.context);
}

-(CGImageAlphaInfo)_alphaInfo
{
    return CGBitmapContextGetAlphaInfo(self._miCGContext.context);
}

-(CGBitmapInfo)_bitmapInfo
{
    return CGBitmapContextGetBitmapInfo(self._miCGContext.context);
}

#pragma mark Private methods.

// Only call this method when already all on the drawing queue.
-(MICGImage *)_makeImage
{
    CGImageRef cgImage = CGBitmapContextCreateImage(self._miCGContext.context);
    MICGImage *newImage;
    if (cgImage)
    {
        newImage = [[MICGImage alloc] initWithCGImage:cgImage];
        CGImageRelease(cgImage);
    }
    return newImage;
}

-(MICGImage *)_getImage
{
    MICGImage *image = self._image;
    if (image)
        return image;

    __block MICGImage *theImage;
    dispatch_sync(self._drawingQueue, ^
    {
        theImage = [self _makeImage];
        self._image = theImage;
    });
    return theImage;
}

-(void)_takeSnapshot
{
    dispatch_sync(self._drawingQueue, ^
    {
        self._snapShotImage = [self _makeImage];
    });
}

-(BOOL)_handleDrawElementCommand:(NSDictionary *)commandDict
{
    BOOL success = NO;
    NSDictionary *drawDict = commandDict[MIJSONPropertyDrawInstructions];
    if (!drawDict)
    {
        return success;
    }

    @try
    {
        self._image = nil;
        // Reset the element error debug name before calling
        // handleDictionaryDrawElementCommand as if it is already set then
        // handleDictionaryDrawElementCommand wont set it again as
        // handleDictionaryDrawElementCommand can be called recursively and it
        // doesn't want to reset
        self._miCGContext.elementErrorDebugName = nil;
        success = [self._miCGContext handleDictionaryDrawElementCommand:drawDict];
        if (success)
        {
            BOOL createImage = MIUtilityGetBOOLPropertyWithDefault(
                                    commandDict, MIJSONPropertyCreateImage, NO);
            if (createImage)
            {
                self._image = [self _makeImage];
            }
        }
    }
    @catch (NSException *exception) {
        MILog(@"Exception thrown.", [exception reason]);
    }
    return success;
}

-(void)_getCIContextFor:(MICoreImage *)requestingObject
                options:(NSDictionary *)options
{
    CIContext *theContext;
    @autoreleasepool
    {
#if TARGET_OS_IPHONE
        theContext = [CIContext contextWithOptions:options];
#else
        theContext = [CIContext contextWithCGContext:self._miCGContext.context
                                             options:options];
#endif
    }
    if (theContext)
    {
        self._ciContext = theContext;
        self._requestingObject = requestingObject;
    }
}

-(NSDictionary *)_getPixelDataInRect:(CGRect)inRect
{
    __block NSDictionary *result;
    
    // First make sure that the rectangle that the pixel data is being requested
    // from makes sense within the bitmap context.
    if (CGRectIsEmpty(inRect))
    {
        result = @{ MIErrorMessageKey :
                        @"Error: Requested pixel data from empty rectangle" };
        return result;
    }
    
    if (inRect.origin.x < 0.0 || inRect.origin.y < 0.0)
    {
        result = @{ MIErrorMessageKey :
                    @"Error: Requested pixel data with negative coordinates." };
        return result;
    }
    
    CGRect contextRect = CGRectMake(0.0, 0.0, self._width, self._height);
    CGRect intersectRect = CGRectIntersection(inRect, contextRect);
    if (!CGRectEqualToRect(intersectRect, inRect))
    {
        result = @{ MIErrorMessageKey :
                @"Error: Requested pixel data beyond bitmap context bounds." };
        return result;
    }
    
    
    CGImageAlphaInfo alphaInfo = self._alphaInfo;
    CGBitmapInfo bitmapInfo = self._bitmapInfo;
    BOOL isFloat = ((bitmapInfo & kCGBitmapFloatComponents) == 0) ? NO : YES;
    BOOL isLittleEndian;
    isLittleEndian = ((bitmapInfo & kCGBitmapByteOrder32Little) == 0) ? NO : YES;
    
    // Now that we know it is a sensible area we will be getting pixel data
    // from convert everything into integer values for pixel data access.
    size_t x1 = inRect.origin.x;
    size_t x2 = x1 + inRect.size.width;
    size_t y1 = inRect.origin.y;
    size_t y2 = y1 + inRect.size.height;
    
    size_t numEntries = (x2 - x1) * (y2 - y1);
    
    // Do a final check to see if too much pixel data is being requested
    // in one go.
    if (numEntries > kMICGBitmapContextMaxPixelRequest)
        return result;
    
    size_t bytesPerComponent = self._bitsPerComponent / 8;
    if (self._bitsPerComponent % 8)
        bytesPerComponent++;
    
    size_t rowBytes = self._bytesPerRow;
    size_t bytesPerPixel = self._bitsPerPixel / 8;
    if (self._bitsPerPixel % 8)
        bytesPerPixel++;
    
    NSDictionary *contextDescription;
    contextDescription = GetCGBitmapContextDictionaryFromPreset(self._preset);
    
    dispatch_sync(self._drawingQueue, ^
    {
        void *buff = CGBitmapContextGetData(self._miCGContext.context);
        
        BOOL isValidAlphaChannel;
        if (alphaInfo == kCGImageAlphaNone ||
            alphaInfo == kCGImageAlphaNoneSkipFirst ||
            alphaInfo == kCGImageAlphaNoneSkipLast)
            isValidAlphaChannel = NO;
        else
            isValidAlphaChannel = YES;
        
        BOOL hasAlphaChannel = (alphaInfo != kCGImageAlphaNone) ? YES : NO;
        BOOL isAlphaFirst = ((alphaInfo == kCGImageAlphaNoneSkipFirst) ||
                             (alphaInfo == kCGImageAlphaPremultipliedFirst) ||
                             (alphaInfo == kCGImageAlphaFirst)) ? YES : NO;
        BOOL isAlphaLast = ((alphaInfo == kCGImageAlphaNoneSkipLast) ||
                            (alphaInfo == kCGImageAlphaPremultipliedLast) ||
                            (alphaInfo == kCGImageAlphaLast)) ? YES : NO;
        
        // Get the column titles.
        CGColorSpaceModel colorModel = CGColorSpaceGetModel(self._colorSpace);
        NSMutableArray *columnTitles = [[NSMutableArray alloc] initWithCapacity:0];
        columnTitles[0] = @"x";
        columnTitles[1] = @"y";
        if (colorModel == kCGColorSpaceModelMonochrome)
        {
            if (isValidAlphaChannel && isAlphaFirst)
                [columnTitles addObject:@"Alpha"];
            
            [columnTitles addObject:@"Gray"];
            
            if (isValidAlphaChannel && isAlphaLast)
                [columnTitles addObject:@"Alpha"];
        }
        else if (colorModel == kCGColorSpaceModelRGB)
        {
            if (isLittleEndian && !isFloat)
            {
                if (isValidAlphaChannel && isAlphaLast)
                    [columnTitles addObject:@"Alpha"];
                
                [columnTitles addObject:@"Blue"];
                [columnTitles addObject:@"Green"];
                [columnTitles addObject:@"Red"];
                
                if (isValidAlphaChannel && isAlphaFirst)
                    [columnTitles addObject:@"Alpha"];
            }
            else
            {
                if (isValidAlphaChannel && isAlphaFirst)
                    [columnTitles addObject:@"Alpha"];
                
                [columnTitles addObject:@"Red"];
                [columnTitles addObject:@"Green"];
                [columnTitles addObject:@"Blue"];
                
                if (isValidAlphaChannel && isAlphaLast)
                    [columnTitles addObject:@"Alpha"];
            }
        }
        else if (colorModel == kCGColorSpaceModelCMYK)
        {
            // CMYK doesn't do alpha.
            [columnTitles addObject:@"Cyan"];
            [columnTitles addObject:@"Magenta"];
            [columnTitles addObject:@"Yellow"];
            [columnTitles addObject:@"CMYKBlack"];
        }
        NSArray *pixelData = GetPixelDataFromBuffer(buff, x1, x2, y1, y2,
                                                    rowBytes, bytesPerPixel,
                                                    bytesPerComponent,
                                                    colorModel,
                                                    isValidAlphaChannel,
                                                    hasAlphaChannel,
                                                    isAlphaFirst,
                                                    isAlphaLast,
                                                    isFloat);
        
        if (pixelData && columnTitles && contextDescription)
            result = @{ @"columnnames" : columnTitles,
                        @"pixeldata" : pixelData,
                        @"contextinfo" : contextDescription };
    });
    return result;
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface instance methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict = NULL;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    NSString *resultString;
    NSNumber *resultNum;
    NSDictionary *resultDict;

    size_t resultVal;
    if ([propertyType isEqualToString:MIJSONKeyWidth])
    {
        resultVal = self._width;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeyHeight])
    {
        resultVal = self._height;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeySize])
    {
        resultDict = @{ MIJSONKeyWidth : @(self._width),
                        MIJSONKeyHeight : @(self._height) };
    }
    else if ([propertyType isEqualToString:MIJSONPropertyBitsPerComponent])
    {
        resultVal = self._bitsPerComponent;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld",resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONPropertyBitsPerPixel])
    {
        resultVal = self._bitsPerPixel;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld",resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONPropertyBytesPerRow])
    {
        resultVal = self._bytesPerRow;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld",resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONPropertyColorSpaceName])
    {
#if TARGET_OS_IPHONE
        CGColorSpaceModel colorModel = CGColorSpaceGetModel(self._colorSpace);
        if (colorModel == kCGColorSpaceModelRGB)
        {
            resultString = @"RGB";
        }
        else if (colorModel == kCGColorSpaceModelMonochrome)
        {
            resultString = @"Gray";
        }
        else
        {
            resultString = @"AlphaOnly";
        }
#else
        CFStringRef colorString = CGColorSpaceCopyName(self._colorSpace);
        resultString = CFBridgingRelease(colorString);
#endif
    }
    else if ([propertyType isEqualToString:MIJSONPropertyAlphaAndBitmapInfo])
    {
        resultVal = ((size_t)self._bitmapInfo) + ((size_t)self._alphaInfo);
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONPropertyPreset])
    {
        resultString = self._preset;
    }
    else
    {
        replyDict = [super handleCommonGetPropertyCommand:commandDict];
        if (!replyDict)
        {
            MILog(@"Requested unknown bitmap context property",
                                 commandDict);
            replyDict = MIMakeReplyDictionary(
                            @"Error: Requested unknown bitmap context property",
                            MIReplyErrorUnknownProperty);
        }
    }

    if (!replyDict && (resultString || resultDict))
    {
        if (resultDict)
        {
            replyDict = MIMakeReplyDictionaryWithDictionaryValue(resultDict, NO);
        }
        else
        {
            if (resultNum)
            {
                replyDict = MIMakeReplyDictionaryWithNumericValue(
                                                          resultString,
                                                          MIReplyErrorNoError,
                                                          resultNum);
            }
            else
            {
                replyDict = MIMakeReplyDictionary(resultString,
                                                  MIReplyErrorNoError);
            }
        }
    }
    return replyDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }

    NSNumber *resultVal;
    NSString *resString;
    
    NSMutableDictionary *propertyDict;
    propertyDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    propertyDict[MIJSONKeyObjectType] = self.baseObjectType;
    propertyDict[MIJSONKeyObjectName] = self.name;
    propertyDict[MIJSONKeyObjectReference] = @(self.reference);
    propertyDict[MIJSONKeyWidth] = @(self._width);
    propertyDict[MIJSONKeyHeight] = @(self._height);
    propertyDict[MIJSONPropertyBitsPerComponent] = @(self._bitsPerComponent);
    propertyDict[MIJSONPropertyBitsPerPixel] = @(self._bitsPerPixel);
    propertyDict[MIJSONPropertyBytesPerRow] = @(self._bytesPerRow);
#if TARGET_OS_IPHONE
    CGColorSpaceModel colorModel = CGColorSpaceGetModel(self._colorSpace);
    if (colorModel == kCGColorSpaceModelRGB)
    {
        resString = @"RGB";
    }
    else if (colorModel == kCGColorSpaceModelMonochrome)
    {
        resString = @"Gray";
    }
    else
    {
        resString = @"AlphaOnly";
    }
#else
    resString = CFBridgingRelease(CGColorSpaceCopyName(self._colorSpace));
#endif
    propertyDict[MIJSONPropertyColorSpaceName] = resString;
    resultVal = [[NSNumber alloc] initWithInteger:
                 (NSInteger)self._alphaInfo + (NSInteger)self._bitmapInfo];
    propertyDict[MIJSONPropertyAlphaAndBitmapInfo] = resultVal;
    propertyDict[MIJSONPropertyPreset] = self._preset;

    replyDict = MIUtilitySaveDictionaryToDestination(commandDict,
                                                       propertyDict,
                                                       NO);
    return replyDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleAssignImageToCollectionCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *ident = commandDict[MIJSONPropertyImageIdentifier];
    if (ident)
    {
        MICGImage *image = [self _getImage];
        MIContext *context = self.miContext;
        if (image && context)
        {
            [context assignImage:image identifer:ident];
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            message = image ? @"Error: No context" : @"Error: No image";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    else
    {
        NSString *message = @"Error: command dict doesn't contain image ident";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return result;
}

-(NSDictionary *)handleDrawElementCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict = NULL;
    __block BOOL success = NO;
    dispatch_sync(self._drawingQueue, ^
    {
        success = [self _handleDrawElementCommand:commandDict];
    });

    if (success)
    {
        replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    else
    {
        NSString *message;
        NSString *elementDebugName = self._miCGContext.elementErrorDebugName;
        if (elementDebugName)
        {
            message = [[NSString alloc] initWithFormat:@"%@%@",
                       @"Error: Badly formed element with name: ",
                       elementDebugName];
        }
        else
        {
            if (!commandDict[MIJSONPropertyDrawInstructions])
                message = @"Error: Missing drawing information dictionary.";
            else
                message = @"Error: Problem interpreting draw dictionary";
        }
        
        MILog(message, commandDict);
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return replyDict;
}

-(NSDictionary *)handleSnapshotCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *snapShotAction = commandDict[MIJSONPropertySnapshotAction];
    // Error checking.
    if (!snapShotAction)
    {
        MILog(@"Missing snapshot action.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing snapshot action",
                                     MIReplyErrorMissingOption);
    }
    
    if ([snapShotAction isEqualToString:MIJSONValueDrawSnapshot] &&
        !self._snapShotImage)
    {
        MILog(@"Snap to be drawn hasn't be taken", commandDict);
        return MIMakeReplyDictionary(@"Error: Snap to be drawn hasn't be taken",
                                     MIReplyErrorMissingOption);
    }
    
    // now perform the command.
    if ([snapShotAction isEqualToString:MIJSONValueTakeSnapshot])
    {
        [self _takeSnapshot];
    }
    else if ([snapShotAction isEqualToString:MIJSONValueDrawSnapshot])
    {
        dispatch_sync(self._drawingQueue, ^
        {
            self._image = nil;
            size_t width, height;
            width = CGImageGetWidth(self._snapShotImage.CGImage);
            height = CGImageGetHeight(self._snapShotImage.CGImage);
            CGRect theRect = CGRectMake(0.0, 0.0, width, height);
            CGContextDrawImage(self._miCGContext.context, theRect,
                               self._snapShotImage.CGImage);
        });
    }
    else if ([snapShotAction isEqualToString:MIJSONValueClearSnapshot])
    {
        self._snapShotImage = nil;
    }
    else
    {
        replyDict = MIMakeReplyDictionary(@"Error: Invalid snapshot action",
                                          MIReplyErrorInvalidOption);
    }
    if (!replyDict)
    {
        replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    return replyDict;
}

-(NSDictionary *)handleGetPixelDataCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *returnResultsType = commandDict[MIJSONKeySaveResultsType];
    NSString *propertyFilePath;
    NSString *jsonFilePath;
    BOOL saveResultsAsDictionaryObject = NO;
    MIContext *context = self.miContext;
    context = context ? context : [MIContext defaultContext];
    NSDictionary *variables = context.variables;
    NSDictionary *rectDict = MIUtilityCreateDictionaryFromCommand(commandDict,
                                                                  variables);
    if (!rectDict)
    {
        MILog(@"Mising or invalid input dict or jsonstring",
                             commandDict);
        replyDict = MIMakeReplyDictionary(
                             @"Error: Mising or invalid input dict or jsonstring",
                                          MIReplyErrorInvalidOption);
        return replyDict;
    }

    CGRect theRect;
    BOOL gotRect = MIUtilityGetCGRectFromDictionary(rectDict, &theRect, kMILog,
                                                    nil);
    if (!gotRect)
    {
        NSString *message = @"Error: Couldn't make rectangle";
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        MILog(@"Couldn't make JSON String into a rectangle",
                             commandDict);
        return replyDict;
    }
    size_t width = theRect.size.width;
    size_t height = theRect.size.height;
    
    BOOL onePixel = ((width == 1) && (height == 1));

    if (!(onePixel || returnResultsType))
    {
        MILog(@"Missing \"saveresultstype\" option",
                             commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing \"saveresults\" prop",
                                          MIReplyErrorMissingOption);
        return replyDict;
    }
    
    if (returnResultsType)
    {
        if ([returnResultsType isEqualToString:MIJSONPropertyPropertyFilePath])
        {
            propertyFilePath = commandDict[MIJSONKeySaveResultsTo];
        }
        else if ([returnResultsType isEqualToString:MIJSONPropertyJSONFilePath])
        {
            jsonFilePath = commandDict[MIJSONKeySaveResultsTo];
        }
        else if ([returnResultsType isEqualToString:MIJSONPropertyDictionaryObject])
        {
            saveResultsAsDictionaryObject = YES;
        }
    }

    if (!onePixel && !(propertyFilePath || jsonFilePath ||
                       saveResultsAsDictionaryObject))
    {
        NSString *message = @"Error: Missing destination for data.";
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        MILog(message, commandDict);
        return replyDict;
    }
    
    NSDictionary *pixelData = [self _getPixelDataInRect:theRect];
    if (pixelData)
    {
        BOOL success = NO;
        NSString *resString = nil;
        // If the errorMessage key exists in the pixel data dictionary it means
        // that an error occured and we have no pixel data.
        if (pixelData[MIErrorMessageKey])
        {
            MILog(pixelData[MIErrorMessageKey], commandDict);
            replyDict = MIMakeReplyDictionary(pixelData[MIErrorMessageKey],
                                              MIReplyErrorInvalidOption);
        }
        else if (propertyFilePath)
        {
            resString = @"";
            success = MIUtilityWriteDictionaryToPLISTFile(pixelData,
                                                          propertyFilePath);
            
        }
        else if (jsonFilePath)
        {
            resString = @"";
            success = MIUtilityWriteDictionaryToJSONFile(pixelData,
                                                         jsonFilePath);
        }
        else if (saveResultsAsDictionaryObject)
        {
            replyDict = MIMakeReplyDictionaryWithDictionaryValue(pixelData, NO);
            success = YES;
        }
        else if (onePixel)
        {
            // convert the NSDictionary into a json string to be returned.
            if ([NSJSONSerialization isValidJSONObject:pixelData])
            {
                NSData *data;
                data = [NSJSONSerialization dataWithJSONObject:pixelData
                                                       options:0
                                                         error:NULL];
                resString = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
                success = YES;
            }
            else
            {
                MILog(@"Not a valid JSON object.", commandDict);
                replyDict = MIMakeReplyDictionary(
                                              @"Error: Not a valid JSON object",
                                              MIReplyErrorInvalidOption);
            }
        }
        
        if (!replyDict)
        {
            if (success)
            {
                replyDict = MIMakeReplyDictionary(resString, MIReplyErrorNoError);
            }
            else
            {
                MILog(@"Failed to save pixel data.", commandDict);
                replyDict = MIMakeReplyDictionary(
                                @"Error: Failed to save pixel data to file.",
                                MIReplyErrorInvalidOption);
            }
        }
    }
    else
    {
        replyDict = MIMakeReplyDictionary(@"Error: ", MIReplyErrorInvalidOption);
    }
    return replyDict;
}

#pragma mark MICIRenderDestinationInterface protocol methods

-(BOOL)drawCoreImage:(CIImage *)ciImage
     fromFilterChain:(MICoreImage *)coreImage
            fromRect:(CGRect)fromRect
              toRect:(CGRect)toRect
   shouldCreateImage:(BOOL)createImage
{
    __block BOOL result = YES;
    dispatch_sync(self._drawingQueue, ^
    {
        self._image = nil;
        
        // Create a new ciContext if we don't have a ciContext, or the
        // configuration options for the ciContext have changed since we last
        // created a ciContext.
        if (!self._ciContext || (self._requestingObject != coreImage) ||
            (coreImage.softwareRender != self._softwareRender) ||
            (coreImage.useSRGBColorSpace != self._useSRGBColorSpace))
        {
            BOOL softwareRender = coreImage.softwareRender;
#if TARGET_OS_IPHONE
            NSDictionary *options;
            options = @{ kCIContextUseSoftwareRenderer : @(softwareRender)};
#else
            // The CIFilter native color space for processing images is
            // in the generic RGB color space. But the sRGB space is
            // more like what our eyes relate to so there may be times when
            // it will be better to be working in the sRGB color space. This
            // requires more work on the part of CoreImage but manageable.
            CFStringRef cSpaceName;
            if (coreImage.useSRGBColorSpace)
                cSpaceName = kCGColorSpaceSRGB;
            else
                cSpaceName = kCGColorSpaceGenericRGBLinear;

            CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName(cSpaceName);
            CGColorSpaceRef cgContextColorSpace =
                        CGBitmapContextGetColorSpace(self._miCGContext.context);
            NSDictionary *options = @{
                kCIContextUseSoftwareRenderer : @(softwareRender),
                  kCIContextWorkingColorSpace : (__bridge id)colorSpace,
                   kCIContextOutputColorSpace : (__bridge id)cgContextColorSpace };
            CGColorSpaceRelease(colorSpace);
#endif
            [self _getCIContextFor:coreImage options:options];
            self._softwareRender = coreImage.softwareRender;
            self._useSRGBColorSpace = coreImage.useSRGBColorSpace;
        }
        if (self._ciContext && ciImage)
        {
            CGRect localTo = toRect;
            if (CGRectIsEmpty(localTo))
            {
                localTo = CGRectMake(0.0, 0.0, (CGFloat)self._width,
                                   (CGFloat)self._height);
            }

            CGRect localFrom = fromRect;
            if (CGRectIsEmpty(localFrom))
            {
                localFrom = ciImage.extent;
                if (localFrom.size.width > 1.0e+6 ||
                    localFrom.size.height > 1.0e+6)
                {
                    localFrom = localTo;
                }
            }
            
            @try
            {
#if TARGET_OS_IPHONE
                CGImageRef cgImage = [self._ciContext createCGImage:ciImage
                                                           fromRect:localFrom];
                CGContextDrawImage(self._miCGContext.context,
                                   localTo, cgImage);
                CGImageRelease(cgImage);
#else
                [self._ciContext drawImage:ciImage inRect:localTo
                                  fromRect:localFrom];
#endif
            }
            @catch (NSException *exception)
            {
                MILog(@"Exception thrown.", [exception reason]);
                result = NO;
            }
        }
        else
          result = NO;
    });
    if (result && createImage)
        self._image = [self _makeImage];

    return result;
}

-(void)clearCIContextRelatingTo:(MICoreImage *)requestingObject
{
    if (requestingObject == self._requestingObject)
        self._ciContext = nil;
}

#pragma mark MICreateCGImageInterface protocol methods

-(CGImageRef)createCGImageWithOptions:(NSDictionary *)options
{
#pragma unused(options)
    MICGImage *theImage = [self _getImage];
    return CGImageRetain([theImage CGImage]);
}

-(void)passMICGImageTo:(MICGImageWrapper *)imageWrapper
          usingOptions:(NSDictionary *)options
{
#pragma unused(options)
    MICGImage *theImage = [self _getImage];
    if (imageWrapper.makeCopy)
        [imageWrapper setMICGImageStrong:theImage];
    else
        [imageWrapper setMICGImageWeak:theImage];
}

@end
