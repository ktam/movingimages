//  MIMovieImporter.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIAVAssetTrackProviderInterface.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"

@class AVURLAsset;

/**
 The `MIMovieImporter` class is an Object-C thin wrapper for a AVAsset
 object. This is one of the MovingImages Framework base classes.
 For documentation on AVAsset please refer to:
 AVFoundation Programming Guide.
*/
@interface MIMovieImporter : MIBaseObject <MICreateCGImageInterface,
                                           MIBaseObjectSemiPublicInterface,
                                           MIAVAssetTrackProviderInterface>

/**
 @brief Initializes the MIMovieImporter object.
 @param fileURL The file to create the CGImageSourceRef object from.
 @param name The name the object is given to use to refer to the object
 @param context The context within which to create image importer base object
*/
-(instancetype)initWithAVURLAsset:(AVURLAsset *)avAsset
                             name:(NSString *)theName
                        inContext:(MIContext *)context;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end
