//  MIAVAssetTrackProviderInterface.h
//  MovingImagesFramework
//
//  Copyright (c) 2015 Zukini Ltd. All rights reserved.

@import Foundation;
@class AVAssetTrack;

@protocol MIAVAssetTrackProviderInterface <NSObject>

/// Return a copy of a track belonging to the source object.
-(AVAssetTrack *)trackWithIdentifier:(NSDictionary *)trackIdentifier;

@end
