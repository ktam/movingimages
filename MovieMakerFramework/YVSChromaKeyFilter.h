//  YVSChromaKeyFilter.h
//  chromakey
//
//  Created by Kevin Meaney on 20/02/2014.
//  Copyright (c) 2015 Zukini Ltd.

extern NSString *const YVSChromaKeyFilterName;

BOOL IsYVSChromaKeyFilterInCategory(NSString *category);

@interface YVSChromaKeyFilter : CIFilter
{
    CIImage   *inputImage;
    CIVector  *inputColor;
    NSNumber  *inputDistance;
    NSNumber  *inputSlopeWidth;
}

/// initialize. Create the YVSChromaKeyFilter CIKernel object used in every .
+(void)initialize;

/// Basic init method.
-(instancetype)init;

/// Return the customAttributes
-(NSDictionary *)customAttributes;

@end
