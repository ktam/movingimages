//  MIImageExporter.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MICGImage.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIHandleCommands.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"
#import "MIImageExporter.h"
#import "MIImageImporter.h"

#import "MIBaseObject_Protected.h"

#pragma mark Private Interface for MIImageExporter

@interface MIImageExporter ()

/**
 Returns an array of CFStringRefs/NString*.
 Calls: CGImageDestinationCopyTypeIdentifiers
*/
+(NSArray *)_copyListOfAvailableImageExporterTypes;

@property (copy) NSURL *_imageExportPathURL;
@property (copy) NSString *_utiType;
@property (assign, readonly) NSInteger _numberOfImages;

/// A list of CGImageRef objects which will be saved when we call export.
@property (strong) NSMutableArray *images;

/// A list of property dictionaries. One dictionary per image.
@property (strong) NSMutableArray *imageProperties; //An array of image dicts

/// The queue to use to access this object.
@property (strong) dispatch_queue_t _objectQueue;

/**
 @brief This method will set the value into the dictionary theDict.
 @discussion The key is a key path meaning it will attempt to drill down into
 the dictionary/arrays to sent the individual value theValue
*/
-(NSMutableDictionary *)_setIntoDictionary:(NSMutableDictionary *)theDict
                                  theValue:(NSString *)theValue
                      forDictionaryPathKey:(NSString *)keyPath;

/// Obtain specific property info from the property dictionary. Drills in.
-(NSString *)_getDictionaryProperty:(NSString *)propertyKey
                    withImageIndex:(NSInteger)imageIndex;

/// Obtain specific property info from the property dictionary. Drills in.
-(NSString *)_getDictionaryProperty:(NSString *)propertyKey;

/// Add image and image properties to a list to be exported later. Return index.
-(NSInteger)_addImage:(CGImageRef)theImage
       withProperties:(NSDictionary *)properties;

/// Can we export, Do we have images to export?, a location etc.
-(BOOL)_canExport;

/// Perform the export.
-(BOOL)_export;

/// Clear the lists and export settings. Typically happens after export.
-(void)_clear;

/// Replace all the metadata for the image at image index. Returns YES on success
-(BOOL)_setImagePropertyDictionary:(NSDictionary *)metadata
                      atImageIndex:(NSInteger)index;

/**
 @brief Set the metadata value for key for the metadata at imageindex index.
 @discussion This method will either add the metadata associated with the key,
 or replace the metadata which is already defined for that key. The metadata
 being modified is the data associated with the image at image index.
*/
-(BOOL)_setMetadataValue:(id)value
                  forKey:(NSString *)key
            atImageIndex:(NSInteger)index;

/**
 @brief Set metadata value for the dictionary key path in the image metadata.
 @discussion The key path makes it possible to drill down into the dictionary of
 the metadata dictionary associated with the image at index image index in
 the image exporter object.
 @return true on success
*/
-(BOOL)_setMetadataValue:(NSString *)valueString
    forDictionaryPathKey:(NSString *)key
            atImageIndex:(NSInteger)imageIndex;

/**
 @brief Set metadata value for the dictionary key path in image export metadata.
 @discussion The key path makes it possible to drill down into the dictionary to
 assign a value deep down in the metadata dictionary.
 @return true on success
*/
-(BOOL)_setMetadataValue:(NSString *)value
    forDictionaryPathKey:(NSString *)keyPath;

/// Replace all the image file metadata.
-(void)_setImageFileMetadataDictionary:(NSDictionary *)fileMetadata;

/// Assign or replace the image file metadata for the key
-(void)_setImageFileMetadata:(id)metadata forKey:(NSString *)key;

/// get the image property dictionary at image index.
-(NSDictionary *)_imagePropertyDictionaryAtImageIndex:(NSInteger)imageIndex;

@end

#pragma mark Implementation of MIImageExporter

@implementation MIImageExporter

#pragma mark Implementation of non-synthesized properties.

-(NSInteger)_numberOfImages
{
    return self.images.count;
}

#pragma mark Designated initializer

-(instancetype)initWithURL:(NSURL *)exportURL
                   utiType:(NSString *)utiType
                      name:(NSString *)theName
                 inContext:(MIContext *)context
{
    self = [super initWithBaseObjectType:MIImageExporterKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        self->_images = [[NSMutableArray alloc] initWithCapacity:0];
        self->_imageProperties = [[NSMutableArray alloc] initWithCapacity:0];
        self->__imageExportPathURL = exportURL.copy;
        self->__utiType = utiType.copy;
        dispatch_queue_t defaultQueue;
        defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                                 0);
        dispatch_queue_t myQueue;
        NSString *name;
        name = [NSString stringWithFormat:@"com.zukini.miimageexporter.%p", self];
        myQueue = dispatch_queue_create(
                            [name cStringUsingEncoding:NSASCIIStringEncoding],
                            DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(myQueue, defaultQueue);
        self->__objectQueue = myQueue;
    }
    return self;
}

#pragma mark Private methods

-(void)_clear
{
    self.propertyDictionary = nil;
    [self.images removeAllObjects];
    [self.imageProperties removeAllObjects];
    self._imageExportPathURL = nil;
}

/*
 This function assumes that all the sub dictionaries/arrays etc of theDict
 are mutable containers. This is all a bit shit, at least it is isolated shit.
*/
-(NSMutableDictionary *)_setIntoDictionary:(NSMutableDictionary *)theDict
                                  theValue:(NSString *)theValue
                      forDictionaryPathKey:(NSString *)keyPath
{
    NSMutableDictionary *resultDict = nil; // making intention explicit.
    NSCharacterSet *dotSet;
    dotSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
    
    NSArray *elements;
    elements = [keyPath componentsSeparatedByCharactersInSet:dotSet];
    
    if ([elements count] < 2)
        return resultDict;
    
    id theObj;
    NSMutableDictionary *tempDict = theDict;
    NSMutableArray *tempArray = nil;
    for (int i = 1 ; i < [elements count] - 1 ; ++i)
    {
        if (tempDict)
        {
            theObj = tempDict[elements[i]];
            if (!theObj)
                break;
        }
        else if (tempArray)
        {
            // We need to convert the element in the dictionary path into
            // an integer index into the array. If we fail to do that or
            // the value is beyond the array bounds then we should bail.
            theObj = nil;
            NSString *element = elements[i];
            NSScanner *scanner = [[NSScanner alloc] initWithString:element];
            NSInteger index;
            BOOL gotIndex = [scanner scanInteger:&index];
            if (!gotIndex || index < 0 || index >= tempArray.count)
                break;
            theObj = tempArray[index];
            if (!theObj)
                break;
        }
        if ([theObj isKindOfClass:[NSDictionary class]])
        {
            tempDict = theObj;
            tempArray = nil;
        }
        else if ([theObj isKindOfClass:[NSArray class]])
        {
            tempArray = theObj;
            tempDict = nil;
        }
    }
    if (theObj)
    {
        NSRange decimalRange = [theValue rangeOfString:@"."];
        BOOL gotFloat = NO;
        BOOL gotInteger = NO;
        NSInteger integerVal = 0;
        double doubleVal = 0.0;
        NSScanner *scanner = [[NSScanner alloc] initWithString:theValue];
        if (decimalRange.location == NSNotFound)
            gotInteger = [scanner scanInteger:&integerVal];
        else
            gotFloat = [scanner scanDouble:&doubleVal];
        
        NSNumber *theNum = nil; // making intention explicit
        if (gotInteger || gotFloat)
        {
            if (gotInteger)
                theNum = [[NSNumber alloc] initWithInteger:integerVal];
            else
                theNum = [[NSNumber alloc] initWithDouble:doubleVal];
        }
        NSObject *theObj = theValue;
        if (theNum)
            theObj = theNum;
        
        NSString *lastElement = [elements lastObject];
        if (tempDict)
        {
            tempDict[lastElement] = theObj;
            resultDict = theDict;
        }
        else if (tempArray)
        {
            // Need to convert the last item in elements into a number.
            NSScanner *scanner = [[NSScanner alloc] initWithString:lastElement];
            NSInteger index;
            BOOL gotIndex = [scanner scanInteger:&index];
            if (!(gotIndex == FALSE || index < 0 || index >= tempArray.count))
            {
                tempArray[index] = theObj;
                resultDict = theDict;
            }
        }
        else
            return resultDict;
    }
    return resultDict;
}

-(BOOL)_canExport
{
    return self._imageExportPathURL && self._utiType && self.images.count;
}

-(BOOL)_export
{
    BOOL result = NO;
    if (self._canExport)
    {
        CGImageDestinationRef exporter;
        /*
         Since I keep thinking about this. I thought it was probably a good
         idea to get it written down here. The option to add a property
         dictionary for the file (not for a specific image) is currently
         not implemented by Apple. That is why NULL is passed and replacing it
         with a dictionary will not make a difference.
        */
        exporter = CGImageDestinationCreateWithURL(
                               (__bridge CFURLRef)(self._imageExportPathURL),
                               (__bridge CFStringRef)(self._utiType),
                               self.images.count, NULL);
        if (exporter)
        {
            // The dictionaries array and images array should alway have the
            // same number of items in them.
            for (int i = 0 ; i < self.images.count ; ++i)
            {
                CGImageRef image = (__bridge CGImageRef)(self.images[i]);
                NSDictionary *propDict = self.imageProperties[i];
                CFDictionaryRef propDictRef = NULL;
                if (propDict != (NSDictionary *)[NSNull null])
                    propDictRef = (__bridge CFDictionaryRef)(propDict);
                CGImageDestinationAddImage(exporter, image, propDictRef);
            }
            if (self.propertyDictionary)
                CGImageDestinationSetProperties(exporter,
                            (__bridge CFDictionaryRef)self.propertyDictionary);
            result = CGImageDestinationFinalize(exporter);
            CFRelease(exporter);
            [self _clear];
        }
    }
    return result;
}

-(BOOL)_setMetadataValue:(NSString *)value
    forDictionaryPathKey:(NSString *)keyPath
{
    BOOL result = NO;
    if (!self.propertyDictionary)
        self.propertyDictionary = [[NSMutableDictionary alloc]
                                   initWithCapacity:0];
    NSMutableDictionary *resultDict;
    resultDict = [self _setIntoDictionary:self.propertyDictionary
                                 theValue:value
                     forDictionaryPathKey:keyPath];
    if (resultDict)
    {
        result = YES;
        self.propertyDictionary = resultDict;
    }
    return result;
}

-(BOOL)_setMetadataValue:(NSString *)value
    forDictionaryPathKey:(NSString *)keyPath
            atImageIndex:(NSInteger)imageIndex
{
    BOOL result = NO;
    if (imageIndex >= self.imageProperties.count)
        return result;
    
    NSMutableDictionary *mutableDict = self.imageProperties[imageIndex];
    
    if (mutableDict == (NSDictionary *)[NSNull null])
        mutableDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    NSMutableDictionary *resultDict;
    resultDict = [self _setIntoDictionary:mutableDict
                                 theValue:value
                     forDictionaryPathKey:keyPath];
    if (resultDict)
    {
        result = YES;
        self.imageProperties[imageIndex] = resultDict;
    }
    
    return result;
}

-(BOOL)_setMetadataValue:(id)value
                  forKey:(NSString *)key
            atImageIndex:(NSInteger)index
{
    if (index < 0 || index >= self.imageProperties.count)
        return NO;
    
    NSMutableDictionary *mutableDict = self.imageProperties[index];
    
    if (mutableDict == (NSDictionary *)[NSNull null])
        mutableDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    mutableDict[key] = value;
    NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:mutableDict];
    self.imageProperties[index] = dict;
    return YES;
}

-(BOOL)_setImagePropertyDictionary:(NSDictionary *)metadata
                      atImageIndex:(NSInteger)index;
{
    if (index < 0 || index >= self.imageProperties.count)
        return NO;
    
    self.imageProperties[index] = [[NSMutableDictionary alloc]
                                   initWithDictionary:metadata];
    return YES;
}

-(NSString *)_getDictionaryProperty:(NSString *)propertyKey
                     withImageIndex:(NSInteger)imageIndex
{
    if (imageIndex < 0 || imageIndex >= self.imageProperties.count)
        return NULL;
    
    NSDictionary *dictionary = self.imageProperties[imageIndex];
    
    return MIUtilityGetDictionaryPropertyWithKey(dictionary, propertyKey);
}

-(void)_setImageFileMetadataDictionary:(NSDictionary *)fileMetadata
{
    self.propertyDictionary = [[NSMutableDictionary alloc]
                               initWithDictionary:fileMetadata];
}

-(void)_setImageFileMetadata:(id)metadata forKey:(NSString *)key
{
    if (!self.propertyDictionary)
        self.propertyDictionary = [[NSMutableDictionary alloc]
                                   initWithCapacity:0];
    self.propertyDictionary[key] = metadata;
}

-(NSString *)_getDictionaryProperty:(NSString *)propertyKey
{
    NSDictionary *dictionary = self.propertyDictionary;
    if (!dictionary)
        return nil;
    
    return MIUtilityGetDictionaryPropertyWithKey(dictionary, propertyKey);
}

-(NSInteger)_addImage:(CGImageRef)theImage withProperties:(NSDictionary *)props
{
    if (!theImage)
        return -1;
    
    NSInteger result = self.images.count;
    
    [self.images addObject:(__bridge id)theImage];
    if (props)
        [self.imageProperties addObject:props];
    else
        [self.imageProperties addObject:[NSNull null]];
    
    return result;
}

-(NSDictionary *)_imagePropertyDictionaryAtImageIndex:(NSInteger)imageIndex
{
    if (imageIndex < 0 || imageIndex > self.imageProperties.count)
        return NULL;
    
    return self.imageProperties[imageIndex];
}

#pragma mark Public Class methods

+(NSArray *)_copyListOfAvailableImageExporterTypes
{
    return CFBridgingRelease(CGImageDestinationCopyTypeIdentifiers());
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface class methods

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *filePath = MIUtilityGetFilePathFromCommandAndVariables(commandDict,
                                                            context.variables);

    NSString *uti = commandDict[MIJSONPropertyFileType];
    if (filePath && uti && [uti isKindOfClass:[NSString class]])
    {
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        NSString *objectName = commandDict[MIJSONKeyObjectName];
        MIImageExporter *object;
        object = [[MIImageExporter alloc] initWithURL:fileURL
                                              utiType:uti
                                                 name:objectName
                                            inContext:context];
        if (object)
        {
            MIBaseReference objectReference = [object reference];
            NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                   (long)objectReference];
            replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                              MIReplyErrorNoError,
                                                              @(objectReference));
        }
        else
        {
            MILog(@"Failed to create importer", commandDict);
            replyDict = MIMakeReplyDictionary(@"Error: Failed to create importer",
                                              MIReplyErrorOptionValueInvalid);
        }
    }
    else
    {
        MILog(@"Missing file or utifiletype", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];

    if (propertyKey == NULL)
    {
        MILog(@"Missing property", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingOption);
    }
    else if (![propertyKey isKindOfClass:[NSString class]])
    {
        MILog(@"Invalid property key", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Invalid property key",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyAvailableExportTypes])
    {
        NSArray *importTypes;
        importTypes = [MIImageExporter _copyListOfAvailableImageExporterTypes];

        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                    importTypes);
        replyDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        NSInteger numObjects = [context numberOfObjectsOfType:MIImageExporterKey];
        NSNumber *numObjectsRef = [[NSNumber alloc] initWithInteger:numObjects];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld",
                                                            (long)numObjects];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          numObjectsRef);
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(
                                    @"Error: MIImageExporter unknown property",
                                    MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    [context removeAllObjectsWithType:MIImageExporterKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface instance methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }

    if (![propertyType isKindOfClass:[NSString class]])
    {
        MILog(@"Property key is not a string.", commandDict);
        return MIMakeReplyDictionary(@"Error: Invalid property key.",
                                     MIReplyErrorInvalidOption);
    }
    NSInteger imageIndex;
    BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(
                                                         commandDict,
                                                         MIJSONKeyImageIndex,
                                                         &imageIndex,
                                                         self._numberOfImages);
    if (hasImageIndex && imageIndex == MIUtilityInvalidImageIndex)
    {
        MILog(@"Invalid image index", commandDict);
        return MIMakeReplyDictionary(@"Error: Invalid image index",
                                        MIReplyErrorInvalidIndex);
    }

    __block NSDictionary *results = NULL;
    dispatch_sync(self._objectQueue, ^
    {
        NSString *resultString = NULL;
        
        if (hasImageIndex)
        {
            if (MIUtilityDoesPropertyTypeBeginWithDictionary(propertyType))
            {
                resultString = [self _getDictionaryProperty:propertyType
                                            withImageIndex:imageIndex];
                if (resultString)
                {
                    results = MIMakeReplyDictionary(resultString,
                                                    MIReplyErrorNoError);
                }
                else
                {
                    MILog(@"Invalid dictionary path",commandDict);
                    results = MIMakeReplyDictionary(
                                            @"Error: Invalid dictionary path",
                                            MIReplyErrorUnknownProperty);
                }
            }
            else
            {
                MILog(@"Requested unknown property for image",
                                     commandDict);
                results = MIMakeReplyDictionary(
                                @"Error: Requested unknown property for image",
                                MIReplyErrorUnknownProperty);
            }
        }
        else
        {
            // Getting a property about the image file not about a specific image.
            if ([propertyType isEqualToString:MIJSONPropertyNumberOfImages])
            {
                NSNumber *numImagesRef = [[NSNumber alloc]
                                          initWithInteger:self._numberOfImages];
                resultString = [[NSString alloc] initWithFormat:@"%ld",
                                (long)self._numberOfImages];
                results = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                            MIReplyErrorNoError,
                                                            numImagesRef);
            }
            else if([propertyType isEqualToString:MIJSONPropertyFileType])
            {
                results=MIMakeReplyDictionary(self._utiType,MIReplyErrorNoError);
            }
            else if ([propertyType isEqualToString:MIJSONPropertyFile])
            {
                resultString = [self._imageExportPathURL path];
                results =MIMakeReplyDictionary(resultString,MIReplyErrorNoError);
            }
            else if ([propertyType isEqualToString:MIJSONPropertyCanExport])
            {
                resultString = [self _canExport] ? @"YES" : @"NO";
                results = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                        MIReplyErrorNoError,
                                                        @(self._canExport));
            }
            else if (MIUtilityDoesPropertyTypeBeginWithDictionary(propertyType))
            {
                resultString = [self _getDictionaryProperty:propertyType];
                if (resultString)
                {
                    results = MIMakeReplyDictionary(resultString,
                                                    MIReplyErrorNoError);
                }
                else
                {
                    MILog(@"Invalid dictionary path",commandDict);
                    results = MIMakeReplyDictionary(
                                                @"Error: Invalid dictionary path",
                                                MIReplyErrorUnknownProperty);
                }
            }
            // Further properties to be handled by the image exporter for the
            // image file will go here.
            else
            {
                results = [super handleCommonGetPropertyCommand:commandDict];
                if (!results)
                {
                    MILog(
                                @"Requested unknown property for image file",
                                commandDict);
                    results = MIMakeReplyDictionary(
                        @"Error: Requested unknown property from image file",
                        MIReplyErrorUnknownProperty);
                }
            }
        }
    });

    return results;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *errorDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        errorDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return errorDict;
    }

    NSInteger imageIndex = 0;
    BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(
                                                         commandDict,
                                                         MIJSONKeyImageIndex,
                                                         &imageIndex,
                                                         self._numberOfImages);
    
    if (hasImageIndex && imageIndex == MIUtilityInvalidImageIndex)
    {
        MILog(@"Invalid image index.", commandDict);
        errorDict = MIMakeReplyDictionary(@"Error: Invalid image index",
                                          MIReplyErrorOptionValueInvalid);
        return errorDict;
    }

    __block NSDictionary *replyDict = NULL;
    dispatch_sync(self._objectQueue, ^
    {
        NSDictionary *imageDict;
        if (hasImageIndex)
            imageDict = [self _imagePropertyDictionaryAtImageIndex:imageIndex];
        else
        {
            NSMutableDictionary *fileDict = [[NSMutableDictionary alloc]
                                             initWithCapacity:0];
            fileDict[MIJSONPropertyNumberOfImages] = @(self._numberOfImages);
            fileDict[MIJSONPropertyFileType] = self._utiType;
            fileDict[MIJSONPropertyFile] = [self._imageExportPathURL path];
            fileDict[MIJSONKeyObjectName] = self.name;
            fileDict[MIJSONKeyObjectType] = self.baseObjectType;
            fileDict[MIJSONPropertyCanExport] = @(self._canExport);
            if (self.propertyDictionary)
                fileDict[MIJSONPropertyDictionary] = self.propertyDictionary;
            imageDict = fileDict;
        }
        
        if (!imageDict || imageDict == (NSDictionary *)[NSNull null])
        {
            MILog(@"No metadata dictionary at imageindex",
                                 commandDict);
            replyDict = MIMakeReplyDictionary(
                            @"Error: No metadata dictionary at imageindex",
                            MIReplyErrorInvalidOption);
        }
        else
        {
            replyDict = MIUtilitySaveDictionaryToDestination(
                                    commandDict, //dict containing destination
                                    imageDict, //dictionary to be saved.
                                    YES);// default saving to replyDict.
        }
    });
    return replyDict;
}

-(NSDictionary *)handleSetPropertyCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *result = NULL;
    dispatch_sync(self._objectQueue, ^
    {
        MIContext *context = self.miContext;
        context = context ? context : [MIContext defaultContext];
        NSDictionary *variables = context ? context.variables : nil;
        
        NSInteger imageIndex;
        BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(commandDict,
                                                            MIJSONKeyImageIndex,
                                                            &imageIndex,
                                                            self._numberOfImages);
        
        NSString *key = commandDict[MIJSONPropertyKey];
        id value = commandDict[MIJSONPropertyValue];
        
        if (key && [key isKindOfClass:[NSString class]] && value)
        {
            if ([key isEqualToString:MIJSONPropertyFileType])
            {
                self._utiType = value;
                result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
            }
            else if ([key isEqualToString:MIJSONPropertyFile])
            {
                NSString *pathSubstitution;
                pathSubstitution = commandDict[MIJSONPropertyPathSubstitution];
                if (pathSubstitution)
                {
                    value = variables[pathSubstitution];
                }

                if ([value isKindOfClass:[NSString class]])
                {
                    value = [value stringByExpandingTildeInPath];
                    NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:value];
                    self._imageExportPathURL = fileURL.copy;
                    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                }
                else
                {
                    MILog(@"Property value not a string",
                                         commandDict);
                    result = MIMakeReplyDictionary(
                                        @"Error: Property value not a string",
                                        MIReplyErrorInvalidOption);
                }
            }
            else if ([key isEqualToString:MIJSONPropertyExportCompressionQuality])
            {
                CGFloat compressionQuality = 0.0;
                BOOL gotCompressionQual = NO;
                if ([value isKindOfClass:[NSNumber class]])
                {
                    compressionQuality = [value doubleValue];
                    gotCompressionQual = YES;
                }
                if ([value isKindOfClass:[NSString class]])
                {
                    gotCompressionQual = MIUtilityGetFloatFromString(
                                                            value,
                                                            &compressionQuality,
                                                            nil);
                }

                key = (__bridge NSString *)
                                    kCGImageDestinationLossyCompressionQuality;
                if (hasImageIndex && gotCompressionQual)
                {
                    if ([self _setMetadataValue:@(compressionQuality)
                                         forKey:key
                                   atImageIndex:imageIndex])
                    {
                        result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                    }
                    else
                    {
                        MILog(@"Invalid imageindex", commandDict);
                        result = MIMakeReplyDictionary(
                                                @"Error: Invalid image index",
                                                MIReplyErrorInvalidIndex);
                    }
                }
                else
                {
                    if (!hasImageIndex)
                    {
                        MILog(@"Missing imageindex", commandDict);
                        result = MIMakeReplyDictionary(
                                               @"Error: Missing image index.",
                                               MIReplyErrorMissingOption);
                    }
                    else
                    {
                        MILog(@"Invalid compression quality",
                                             commandDict);
                        result = MIMakeReplyDictionary(
                                       @"Error: Invalid compression quality.",
                                       MIReplyErrorOptionValueInvalid);
                    }
                }
            }
            else if ([key isEqualToString:MIJSONPropertyDictionary])
            {
                NSDictionary *propertyDict;
                propertyDict = MIUtilityCreateDictionaryFromCommand(commandDict,
                                                                    variables);
                
                if (!propertyDict)
                {
                    MILog(@"Couldn't generate dictionary",
                                         commandDict);
                    result = MIMakeReplyDictionary(@"Error: Invalid dictionary.",
                                                   MIReplyErrorInvalidOption);
                }
                if (propertyDict)
                {
                    NSArray *subKeys = [propertyDict allKeys];
                    NSObject *theSubDict = NULL; // likely a dict.
                    
                    for (NSString *theSubKey in subKeys)
                    {
                        theSubDict = propertyDict[theSubKey];
                        if (hasImageIndex)
                            [self _setMetadataValue:theSubDict
                                             forKey:theSubKey
                                      atImageIndex:imageIndex];
                        else
                        {
                            [self _setImageFileMetadata:theSubDict
                                                 forKey:theSubKey];
                        }
                    }
                    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                }
            }
            else if (MIUtilityDoesPropertyTypeBeginWithDictionary(key))
            {
                NSCharacterSet *dotSet;
                dotSet = [NSCharacterSet characterSetWithCharactersInString:@"."];
                
                NSArray *elements;
                elements = [key componentsSeparatedByCharactersInSet:dotSet];
                
                if ([elements count] < 2)
                {
                    MILog(@"Invalid dictionary path", commandDict);
                    result = MIMakeReplyDictionary(
                                            @"Error: Invalid dictionary path",
                                            MIReplyErrorUnknownProperty);
                }
                else
                {
                    // This is really hacky, I don't know what value type a
                    // particular dictionary entry needs so I'm assuming that
                    // value will be the correct type of object.
                    BOOL success = NO;
                    if (hasImageIndex)
                    {
                        success = [self _setMetadataValue:value
                                     forDictionaryPathKey:key
                                             atImageIndex:imageIndex];
                    }
                    else
                    {
                        success = [self _setMetadataValue:value
                                     forDictionaryPathKey:key];
                    }
                    if (success)
                        result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                    else
                    {
                        MILog(@"Can't assign dictionary value",
                                             commandDict);
                        result = MIMakeReplyDictionary(
                                           @"Error: Cant assign dictionary value.",
                                           MIReplyErrorOptionValueInvalid);
                    }
                }
            }
            else
            {
                MILog(@"Invalid property key", commandDict);
                result = MIMakeReplyDictionary(@"Error: Invalid property key",
                                               MIReplyErrorUnknownProperty);
            }
        }
        
        if (!result)
        {
            MILog(@"Missing property key or value", commandDict);
            result = MIMakeReplyDictionary(@"Error: Missing property key or value.",
                                           MIReplyErrorMissingProperty);
        }
    });
    return result;
}

NSDictionary *GetImageMetadataDictionaryFromSource(MIContext *context,
                                                   NSDictionary *commandDict)
{
    NSDictionary *theDict;
    BOOL grabDict = NO;
    NSNumber *grabMetadata = commandDict[MIKSONKeyGrabMetadata];
    if (grabMetadata && [grabMetadata isKindOfClass:[NSNumber class]] &&
        [grabMetadata boolValue])
    {
        grabDict = YES;
    }
    
    NSDictionary *secondaryObjectDict = commandDict[MIJSONKeySourceObject];
    
    if (grabDict && secondaryObjectDict)
    {
        MIImageImporter *importer;
        id <MICreateCGImageInterface> source =
        FindObjectFromDictionaryConformingToCreateCGImageProtocol(
                                                    context, secondaryObjectDict);
        if (source && [source isKindOfClass:[MIImageImporter class]])
        {
            importer = (MIImageImporter *)source;
        }
        
        if (importer)
        {
            NSInteger dictImageIndex = 0;
            NSDictionary *opts = commandDict[MIJSONKeyImageOptions];
            if (opts && [opts isKindOfClass:[NSDictionary class]])
            {
                NSNumber *index = opts[MIJSONKeySecondaryImageIndex];
                if (index && [index isKindOfClass:[NSNumber class]])
                {
                    dictImageIndex = index.integerValue;
                }
            }
            theDict = [importer imagePropertyDictionaryAtIndex:
                       dictImageIndex];
        }
    }
    return theDict;
}

-(NSDictionary *)handleSetPropertiesCommand:(NSDictionary *)commandDict
{
    // To be used to copy the property dictionary of an image in one file to
    // an image file in this graphics exporter, or to copy the properties from
    // a file on disk or a jsonstring to here.
    __block NSDictionary *resultsDict = NULL;
    dispatch_sync(self._objectQueue, ^
    {
        MIContext *context = self.miContext;
        context = context ? context : [MIContext defaultContext];

        NSDictionary *variables = context ? context.variables : nil;

        NSDictionary *dictionaryToAssign = NULL;
        NSInteger imageIndex = 0;
        BOOL hasImageIndex = MIUtilityGetImageIndexWithKey(commandDict,
                                                             MIJSONKeyImageIndex,
                                                             &imageIndex,
                                                             self._numberOfImages);
        NSDictionary *objectDict = commandDict[MIJSONKeySourceObject];

        MIImageImporter *imageImporter;
        if (objectDict)
        {
            MIBaseObject *object = MIGetObjectFromDictionary(context,
                                                             objectDict);
            if (object)
            {
                if ([[object baseObjectType] isEqualToString:MIImageImporterKey])
                    imageImporter = (MIImageImporter *)object;
            }
        }
        
        if (imageImporter)
        {
            // If I have an image index, for the image that we want to assign the
            // the metadata to, then assume that we want the metadata from an image
            // not from an image file. This means that if we have no secondary
            // image index then assume a secondary image index of 0.
            if (hasImageIndex)
            {
                NSInteger secondaryImageIndex = 0;
                BOOL hasSecondaryImageIndex = MIUtilityGetImageIndexWithKey(
                                                  commandDict,
                                                  MIJSONKeySecondaryImageIndex,
                                                  &imageIndex,
                                                  imageImporter.numberOfImages);
                if (hasSecondaryImageIndex &&
                    secondaryImageIndex == MIUtilityInvalidImageIndex)
                {
                    MILog(@"Invalid image index value",
                                         commandDict);
                    resultsDict = MIMakeReplyDictionary(
                                        @"Error: Invalid image index value",
                                        MIReplyErrorInvalidIndex);
                    hasSecondaryImageIndex = NO;
                }
                else if (!hasSecondaryImageIndex)
                {
                    secondaryImageIndex = 0;
                    hasSecondaryImageIndex = YES;
                }
                
                if (hasSecondaryImageIndex)
                {
                    dictionaryToAssign = [imageImporter
                            imagePropertyDictionaryAtIndex:secondaryImageIndex];
                }
            }
            else
            {
                dictionaryToAssign = [imageImporter
                                            copyImageSourcePropertyDictionary];
            }
        }
        
        if (!dictionaryToAssign && !resultsDict)
        {
            dictionaryToAssign = MIUtilityCreateDictionaryFromCommand(
                                                        commandDict, variables);

            if (!dictionaryToAssign)
            {
                MILog(@"Couldn't create property dictionary",
                                     commandDict);
                resultsDict = MIMakeReplyDictionary(
                                @"Error: Couldn't create property dictionary",
                                MIReplyErrorInvalidOption);
            }
        }
        
        if (dictionaryToAssign && !resultsDict)
        {
            if (hasImageIndex)
            {
                if (imageIndex != MIUtilityInvalidImageIndex)
                {
                    [self _setImagePropertyDictionary:dictionaryToAssign
                                         atImageIndex:imageIndex];
                }
                else
                {
                    MILog(@"Invalid image index", commandDict);
                    resultsDict = MIMakeReplyDictionary(
                                                @"Error: Invalid image index.",
                                                MIReplyErrorInvalidIndex);
                }
            }
            else
                [self _setImageFileMetadataDictionary:dictionaryToAssign];
            
            if (!resultsDict)
                resultsDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else if (!resultsDict)
        {
            MILog(@"Failed to get dictionary", commandDict);
            resultsDict = MIMakeReplyDictionary(
                                            @"Error: failed to get dictionary",
                                            MIReplyErrorMissingOption);
        }
    });
    return resultsDict;
}

-(NSDictionary *)handleAddImageCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *replyDict = NULL;
    // The source image is either from an image importer object in which case
    // I'll need the image index and need to check for the grab metatadata
    // option to see if I need to get the metadata at the same time. If no
    // image index then assume image index 0. Or the source image is from a
    // bitmap graphic context.
    dispatch_sync(self._objectQueue, ^
    {
        MIContext *context = self.miContext;
        MICGImage *image = MICGImageFromDictionary(context, commandDict, self);
        if (image)
        {
            NSDictionary *theDict;
            theDict = GetImageMetadataDictionaryFromSource(context, commandDict);
            [self _addImage:image.CGImage withProperties:theDict];
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            // We failed to get an image.
            MILog(@"Failed to get image", commandDict);
            replyDict = MIMakeReplyDictionary(@"Error: failed to get image.",
                                              MIReplyErrorOperationFailed);
        }
    });
    return replyDict;
}

-(NSDictionary *)handleExportCommand:(NSDictionary *)commandDict
{
    if (!self._canExport)
    {
        return MIMakeReplyDictionary(@"Error: Not ready to export",
                                     MIReplyErrorOperationFailed);
    }

    __block NSDictionary *result;
    dispatch_sync(self._objectQueue, ^
    {
        BOOL exported = [self _export];
        if (exported)
        {
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            MILog(@"Could not export", commandDict);
            result = MIMakeReplyDictionary(@"Error: Could not export",
                                           MIReplyErrorOperationFailed);
        }
    });
    return result;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

#pragma mark Public instance methods

-(void)close
{
    [super baseObjectClose];
}

@end
