//  MICGImageWrapper.h
//  MovieMaker
//
//  Created by Kevin Meaney on 16/01/2014.
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

#import "MIContext.h"

@class MICGImage;
@class MIContext;

/**
 @class MICGImageWrapper
 @brief A MICGImageWrapper object holds a MICGImage object strongly or weakly.
 @discussion A MICGImageWrapper objects manages a MICGImage object that is held
 either strongly or weakly. A MICGImage object is a simple Objective-C wrapper
 object for a CGImage basically as a mechanism to take advantage of ARC
 memory management. If a MICGImage object which is held weakly is disposed of
 then the MICGImageWrapper object knows where to obtain a new image from if
 a image is requested. The object which the MICGImageWrapper obtains the new
 image from decides whether the MICGImage should be held strongly or weakly,
 unless makeCopy is set in which case the image is always held strongly.
*/
@interface MICGImageWrapper : NSObject

/**
 @brief Ensure that the obtained image is held with a strong reference.
 @discussion It is up to the object that provides the image as to whether it
 should be held in a strong reference or not. This property overrides that
 behaviour ensuring that a strong reference is kept.
*/
@property (assign) BOOL makeCopy;

/// Initialize's the MICGImageWrapper object.
// -(instancetype)initWithObjectReference:(MIBaseReference)reference
//                           withContext:(MIContext *)context;

/// Designated initializer when sourcing an image from an object.
-(instancetype)initWithObjectReference:(MIBaseReference)reference
                               options:(NSDictionary *)options
                           withContext:(MIContext *)context;

/// Designated initializer when sourcing an image from the MIContext collection.
-(instancetype)initWithImageIdentifier:(NSString *)identifier
                           withContext:(MIContext *)context;

/// Get the MICGImage object. If necessary create the MICGImage object & CGImage.
-(MICGImage *)image;

/// Get the MICGImage object. If image currently doesn't exist don't generate.
-(MICGImage *)getImageDontGenerate;

/**
 @brief Set the MICGImage object and hold a strong reference to the image.
 @param     image   A MICGImage object to be held strongly.
 @discussion Objects calling this method or setMICGImageWeak will know which
 one to call. For example a MIImageImporter object will call setMICGImageStrong
 because the imported image will not change.
*/
-(void)setMICGImageStrong:(MICGImage *)image;

/**
 @brief Set the MICGImage object and keep a weak reference to the image.
 @param     image   A MICGImage object to be held strongly.
 @discussion Objects calling this method or setMICGImageStrong will know which
 one to call. For example a MICGBitmapContext object will call setMICGImageWeak
 because further drawing into the bitmap context will make the image stale and
 a new one should be created when needed. The calling object will keep a strong
 reference to the MICGImage object until the time that the image becomes stale.
*/
-(void)setMICGImageWeak:(MICGImage *)image;

/// Is the image held by a strong reference or is make copy set.
-(BOOL)isOwned;

/// If the image held then clear it.
-(void)clearImage;

@end
