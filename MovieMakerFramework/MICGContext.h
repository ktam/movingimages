//  MICGContext.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@class MIBaseObject;
@class MIContext;

@import Foundation;
@import CoreGraphics;

/**
 @class MICGContext
 @brief Defines the interface common to all graphics context objects.
 @discussion The MICGContext class defines the interface common to graphics
 context objects like MICGBitmapContext and possibly in the future a PDF
 context. Objects of this class are wrappers for a CGContextRef object. This
 class should only be created as a wrapped object of another class. See
 MICGBitmapContext as an example of use.
*/

@interface MICGContext : NSObject

/**
 @property context
 @discussion The graphic context that objects that are instantiated from a
 MICGContext wrap. Treated as a NSObject for ARC purposes.
*/
@property (readonly) CGContextRef context;

/// A dictionary of variables used when evaluating equations.
@property (strong) NSDictionary *variables;

/**
 @brief The name of the element where interpretation of the dictionary failed.
 @discussion If a debug name key "elementdebugname" was specified for an
 element where interpretation of the element failed then the debug name of the
 element will be saved in this property.
*/
@property (strong) NSString *elementErrorDebugName;

/// The designated initializer. Takes a cg context and a moving images context.
-(instancetype)initWithCGContext:(CGContextRef)ctxt
                       miContext:(MIContext *)miContext
                           owner:(id)owner;

/**
 @brief Draws into the context.
 @discussion Takes the command dictionary and converts it into drawing commands
 which are then used to draw into the graphics context. Scrubs the variables
 dictionary.
 @param command: A dictionary containing all the information needed to do the
 drawing.
*/
-(BOOL)handleDictionaryDrawElementCommand:(NSDictionary *)command;

/// Return a list of NSString objects, each string is a core graphics blend mode
+(NSArray *)blendModes;

@end
