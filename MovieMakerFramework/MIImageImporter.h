//  MMImageImporter.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"

@import ImageIO;

/**
 The `MIImageImporter` class is an Object-C thin wrapper for a CGImageSourceRef
 object. This is one of the MovingImages Framework base classes.
 For documentation on CGImageSourceRef please refer to:
 The Image I/O Programming Guide.
*/
@interface MIImageImporter : MIBaseObject <MICreateCGImageInterface,
                                            MIBaseObjectSemiPublicInterface>

/**
 @brief Initializes the MIImageImporter object.
 @param fileURL The file to create the CGImageSourceRef object from.
 @param name The name the object is given to use to refer to the object
 @param context The context within which to create image importer base object
*/
-(instancetype)initWithFileURL:(NSURL *)fileURL
                          name:(NSString *)theName
                     inContext:(MIContext *)context;

/**
 Releases the reference to the CGImageSourceRef object.
*/
-(void)dealloc;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

/// Returns the number of images in image file.
-(size_t)numberOfImages;

/**
 @name Obtain property list dictionaries for the image file or for each image
*/

/// Obtain the dictionary relating to the image file.
-(NSDictionary *)copyImageSourcePropertyDictionary;

/// Obtain the dictionary relating to the image at index.
-(NSDictionary *)imagePropertyDictionaryAtIndex:(size_t)index;

@end
