//  MIBaseObject.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObject+Internal.h"
#import "MIBaseObject_Protected.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#import "MIBaseObject_Protected.h"

#pragma mark -
#pragma mark Class MIBaseObject implementation.

#pragma mark MIBaseObject Static (local) helper functions.

/// Generate a dictionary of command properties.
static NSDictionary *GetSelectorDictionary()
{
    return
    @{
        MIJSONValueGetPropertyCommand : @"handleGetPropertyCommand:",
        MIJSONValueGetPropertiesCommand : @"handleGetPropertiesCommand:",
        MIJSONValueSetPropertyCommand : @"handleSetPropertyCommand:",
        MIJSONValueSetPropertiesCommand : @"handleSetPropertiesCommand:",
        MIJSONValueAddImageCommand : @"handleAddImageCommand:",
        MIJSONValueExportCommand : @"handleExportCommand:",
        MIJSONValueCloseCommand : @"handleCloseCommand:",
        MIJSONValueDrawElementCommand : @"handleDrawElementCommand:",
        MIJSONValueSnapshotCommand : @"handleSnapshotCommand:",
        MIJSONValueFinalizePageCommand : @"handleFinalizePageAndStartNew:",
        MIJSONValueGetPixelDataCommand : @"handleGetPixelDataCommand:",
        MIJSONValueRenderFilterChainCommand : @"handleRenderFilterChainCommand:",
        MIJSONValueAssignImageToCollectionCommand :
                                        @"handleAssignImageToCollectionCommand:",
        MIJSONValueProcessFramesCommand : @"handleProcessFramesCommand:",
        MIJSONValueCreateTrackCommand : @"handleCreateTrackCommand:",
        MIJSONValueAddMovieInstruction : @"handleAddInstruction:",
        MIJSONValueAddAudioMixInstruction : @"handleAddAudioMixInstruction:",
        MIJSONValueInsertEmptyTrackSegment : @"handleInsertEmptyTrackSegment:",
        MIJSONValueInsertTrackSegment : @"handleInsertTrackSegment:",
        MIJSONValueAddInputToMovieFrameWriterCommand :
                                            @"handleAddInputToMovieFrameWriter:",
        MIJSONValueAddImageSampleToWriterCommand :
                                                @"handleAddImageSampleToWriter:",
        MIJSONValueFinishWritingFramesCommand : @"handleFinishWritingFrames:",
        MIJSONValueCancelWritingFramesCommand : @"handleCancelWritingFrames:"
    };
}

/// Generate a dictionary of command properties.
static NSDictionary *GetClassSelectorDictionary()
{
    return
    @{
      MIJSONValueCreateCommand : @"handleMakeObjectCommand:inContext:",
      MIJSONValueGetPropertyCommand : @"handleGetPropertyCommand:inContext:",
      MIJSONValueCloseAllCommand : @"handleCloseAllObjectsCommand:inContext:",
      MIJSONValueCalculateGraphicSizeOfTextCommand :
                            @"handleCalculateGraphicSizeOfTextCommand:inContext:"
    };
}

/**
 @brief Convert the string into a method selector for calling semi public methods.
 @discussion All the base object methods should handle the selector. The objects
 are queried as to whether they can handle the selector before the selector
 message is sent. If they can't then an error is returned.
*/
static SEL MIUtilityGetSelectorFromCommandString(NSString *commandType)
{
    static NSDictionary *selectorDict = nil;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        selectorDict = GetSelectorDictionary();
    });
    
    NSString *selectorString = selectorDict[commandType];
    if (!selectorString)
        return (SEL)0;
    
    return NSSelectorFromString(selectorString);
}

static SEL MIUtilityGetClassSelectorFromCommandString(NSString *commandType)
{
    static NSDictionary *selectorDict = nil;
    static dispatch_once_t once_pred;
    dispatch_once(&once_pred, ^
    {
        selectorDict = GetClassSelectorDictionary();
    });
    
    NSString *selectorString = selectorDict[commandType];
    if (!selectorString)
        return (SEL)0;
    
    return NSSelectorFromString(selectorString);
}

@implementation MIBaseObject

#pragma mark Initializers

-(instancetype)initWithBaseObjectType:(NSString *)objectType
                                 name:(NSString *)theName
                            inContext:(MIContext *)context
{
    self = [super init];
    context = context ? context : [MIContext defaultContext];
    if (self)
    {
        self->_reference = [context addObject:self withType:objectType];
        if (self->_reference == kMIInvalidElementReference)
        {
            MILog(@"Invalid element reference", objectType);
            self = nil;
        }
        else
        {
            self->_baseObjectType = objectType.copy;
            self->_name = theName ? theName.copy : @"";
            self->_miContext = context;
        }
    }
    return self;
}

#pragma mark Public Class MIBaseObjectPublicInterface protocol method

+(NSDictionary *)handleClassCommand:(NSDictionary *)command
                          inContext:(MIContext *)context
{
    NSDictionary *result;
    @autoreleasepool
    {
        BOOL success = YES;
        NSString *theCommand = [command objectForKey:MIJSONKeyCommand];
        if (!theCommand)
        {
            MILog(@"No command key specified.", command);
            success = NO;
        }
        
        if (success && ![theCommand isKindOfClass:[NSString class]])
        {
            MILog(@"The command is not a string.", command);
            success = NO;
        }
        
        SEL theSelector = (SEL)0;

		if (success)
            theSelector = MIUtilityGetClassSelectorFromCommandString(theCommand);

		if (success && theSelector == (SEL)0)
        {
            NSString *message = [[NSString alloc]
                         initWithFormat:@"Unknown command key specified: %@",
                         theCommand];
            MILog(message, command);
            success = NO;
        }
        
        if (![self respondsToSelector:theSelector])
        {
            NSString *message = [[NSString alloc]
                     initWithFormat:@"This class doesn't repond to command: %@",
                     theCommand];
            MILog(message, command);
            success = NO;
        }
        
        if (success)
        {
            @try
            {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                result = [self performSelector:theSelector
                                    withObject:command
                                    withObject:context];
#pragma clang diagnostic pop
            }
            @catch (NSException *exception)
            {
                NSString *message = [[NSString alloc]
                     initWithFormat:@"Error handling command: %@", theCommand];
                MILog(message, command);
                result = MIMakeReplyDictionary(@"Error: Handling command failed",
                                               MIReplyErrorInvalidSubCommand);
            }
        }
        else
        {
            result = MIMakeReplyDictionary(@"Error: Command wasn't handled",
                                           MIReplyErrorInvalidSubCommand);
        }
    }
    
    return result;
}

#pragma mark Methods internal to framework

// This method should be overridden by inherited classes where necessary.
// Inherited classes shouldn't assume this method is empty.
-(void)finalize
{
    
}

#pragma mark Public Methods

-(void)baseObjectClose
{
    MIContext *theContext = self->_miContext;
    MIBaseReference ref = self->_reference;
    if (theContext)
        [theContext removeObjectWithReference:ref];
}

-(NSString *)description
{
    return [[NSString alloc]
        initWithFormat:@"MIBaseObject::baseObjectType:%@ reference:%ld name:%@",
                            self.baseObjectType, (long)self.reference, self.name];
}

-(NSDictionary *)handleCommonGetPropertyCommand:(NSDictionary *)command
{
    NSDictionary *results = NULL;
    NSString *resultString = NULL;
    
    NSString *propertyType = command[MIJSONPropertyKey];
    if ([propertyType isEqualToString:MIJSONKeyObjectReference])
    {
        MIBaseReference ref = self.reference;
        resultString = [[NSString alloc] initWithFormat:@"%ld", (long)ref];
        results = MIMakeReplyDictionaryWithNumericValue(resultString,
                                                        MIReplyErrorNoError,
                                                        @(ref));
    }
    else if ([propertyType isEqualToString:MIJSONKeyObjectName])
    {
        results = MIMakeReplyDictionary(self.name, MIReplyErrorNoError);
    }
    else if ([propertyType isEqualToString:MIJSONKeyObjectType])
    {
        results = MIMakeReplyDictionary(self.baseObjectType, MIReplyErrorNoError);
    }
    
    return results;
}

#pragma mark Protocol MIBaseObjectPublicInterface instanc methods.

-(NSDictionary *)handleObjectCommand:(NSDictionary *)command
{
    NSDictionary *result;
    @autoreleasepool
    {
        BOOL success = YES;
        NSString *theCommand = [command objectForKey:MIJSONKeyCommand];
        if (!theCommand)
        {
            MILog(@"No command key specified.", command);
            success = NO;
        }
        
        if (success && ![theCommand isKindOfClass:[NSString class]])
        {
            MILog(@"The command is not a string.", command);
            success = NO;
        }

        SEL theSelector = (SEL)0;

        if (success)
        {
            theSelector = MIUtilityGetSelectorFromCommandString(theCommand);
        }
        if (success && theSelector == (SEL)0)
        {
            NSString *message = [[NSString alloc]
                         initWithFormat:@"Unknown command key specified: %@",
                         theCommand];
            MILog(message, command);
            success = NO;
        }

        if (![self respondsToSelector:theSelector])
        {
            NSString *message = [[NSString alloc]
                     initWithFormat:@"This object doesn't repond to command: %@",
                     theCommand];
            MILog(message, command);
            success = NO;
        }
        
        if (success)
        {
            @try
            {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
                result = [self performSelector:theSelector
                                    withObject:command];
                
#pragma clang diagnostic pop
            }
            @catch (NSException *exception)
            {
                NSString *message = [[NSString alloc]
                                    initWithFormat:@"Error handling command: %@",
                                     theCommand];
                MILog(message, command);
                result = MIMakeReplyDictionary(@"Error: Handling command failed",
                                               MIReplyErrorInvalidSubCommand);
            }
        }
        else
        {
            result = MIMakeReplyDictionary(@"Error: Command wasn't handled",
                                           MIReplyErrorInvalidSubCommand);
        }
    }
    
    return result;
}

@end
