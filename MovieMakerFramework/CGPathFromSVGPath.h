//  CGPathFromSVGPath.h

#ifndef CGPathFromSVGPath_h
#define CGPathFromSVGPath_h

CGPathRef CGCreatePathFromSVGPath(const char* s);

#endif /* CGPathFromSVGPath_h */
