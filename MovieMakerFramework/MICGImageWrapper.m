//  MICGImageWrapper.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MICreateCGImageInterface.h"
#import "MIJSONConstants.h"

// static size_t kMICGImageWrapperNotAnIndex = NSIntegerMax;

#pragma mark CLASS MICGImageWrapper Private Interface

@interface MICGImageWrapper ()

@property (strong, atomic) MICGImage *_strongMICGImage;
@property (weak, atomic) MICGImage *_weakMICGImage;
@property (assign) MIBaseReference _objectReference;
@property (copy) NSDictionary *_options;
@property (weak) MIContext *_miContext;

@property (copy) NSString *_identifier;

-(void)_getMICGImage;

@end

#pragma mark - CLASS MICGImageWrapper implementation

@implementation MICGImageWrapper

#pragma mark Private methods

-(void)_getMICGImage
{
    MIContext *theContext = self._miContext;
    if (self._identifier)
    {
        MICGImage *image = [theContext getImageWithIdentifier:self._identifier];
        if (image)
        {
            if (self.makeCopy)
            {
                [self setMICGImageStrong:image];
            }
            else
            {
                [self setMICGImageWeak:image];
            }
        }
    }
    else
    {
        MIBaseObject *tempObject;
        tempObject = [theContext objectWithReference:self._objectReference];
        if (tempObject &&
            [tempObject conformsToProtocol:@protocol(MICreateCGImageInterface)])
        {
            MIBaseObject<MICreateCGImageInterface> *baseObject;
            baseObject = (MIBaseObject<MICreateCGImageInterface> *)tempObject;
            [baseObject passMICGImageTo:self usingOptions:self._options];
        }
    }
}

#pragma mark Public methods

-(instancetype)initWithObjectReference:(MIBaseReference)reference
                               options:(NSDictionary *)options
                           withContext:(MIContext *)context
{
    self = [super init];
    if (self)
    {
        context = context ? context : [MIContext defaultContext];
        self._objectReference = reference;
        self._options = options ? options.copy : nil;
        self._miContext = context;
        self._identifier = nil;
    }
    return self;
}

-(instancetype)initWithImageIdentifier:(NSString *)identifier
                           withContext:(MIContext *)context
{
    self = [super init];
    if (self)
    {
        context = context ? context : [MIContext defaultContext];
        self._objectReference = kMIInvalidElementReference;
        self._options = nil;
        self._miContext = context;
        self._identifier = identifier.copy;
    }
    return self;
}

-(MICGImage *)getImageDontGenerate
{
    MICGImage *result;
    MICGImage *weakImage = self._weakMICGImage;
    if (self._strongMICGImage)
        result = self._strongMICGImage;
    else if (weakImage)
        result = weakImage;
    return result;
}

-(MICGImage *)image
{
    MICGImage *theImage = [self getImageDontGenerate];
    if (theImage)
        return theImage;
    
    [self _getMICGImage];
    return [self getImageDontGenerate];
}

-(void)setMICGImageStrong:(MICGImage *)image
{
    if (self.makeCopy)
        image = [image copy];

    [self set_strongMICGImage:image];
    [self set_weakMICGImage:nil];
}

-(void)setMICGImageWeak:(MICGImage *)image
{
    [self set_weakMICGImage:image];
    [self set_strongMICGImage:nil];
}

-(BOOL)isOwned
{
    return self.makeCopy || self._strongMICGImage;
}

-(void)clearImage
{
    self._weakMICGImage = nil;
    self._strongMICGImage = nil;
}

@end
