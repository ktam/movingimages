//  NSAffineTransform+DictionaryRepresentation.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIJSONConstants.h"
#import "NSAffineTransform+DictionaryRepresentation.h"

@implementation NSAffineTransform (DictionaryRepresentation)

-(NSDictionary *)dictionaryRepresentation
{
    NSAffineTransformStruct affineStruct = [self transformStruct];
    return @{ MIJSONKeyAffineTransformM11 : @(affineStruct.m11),
              MIJSONKeyAffineTransformM12 : @(affineStruct.m12),
              MIJSONKeyAffineTransformM21 : @(affineStruct.m21),
              MIJSONKeyAffineTransformM22 : @(affineStruct.m22),
              MIJSONKeyAffineTransformtX : @(affineStruct.tX),
              MIJSONKeyAffineTransformtY : @(affineStruct.tY)
              };
}

+(NSAffineTransform *)transformWithDictionary:(NSDictionary *)affineDict
{
    NSAffineTransformStruct affineStruct;
    
    NSNumber *theNum;
    theNum = affineDict[MIJSONKeyAffineTransformM11];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.m11 = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM12];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.m12 = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM21];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.m21 = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformM22];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.m22 = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformtX];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.tX = [theNum doubleValue];
    
    theNum = affineDict[MIJSONKeyAffineTransformtY];
    if (!theNum || ![theNum isKindOfClass:[NSNumber class]])
        return nil;
    
    affineStruct.tY = [theNum doubleValue];
    
    NSAffineTransform *transform = [NSAffineTransform transform];
    [transform setTransformStruct:affineStruct];
    return transform;
}

@end
