//  NSAffineTransform+DictionaryRepresentation.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

@interface NSAffineTransform (DictionaryRepresentation)

-(NSDictionary *)dictionaryRepresentation;
+(NSAffineTransform *)transformWithDictionary:(NSDictionary *)affineDict;

@end
