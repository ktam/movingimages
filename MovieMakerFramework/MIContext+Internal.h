//  MIContext+Internal.h
//  MovingImagesFramework
//
//  Copyright (c) 2014 Zukini Ltd. All rights reserved.

#import "MIContext.h"

@class MICGImage;

@interface MIContext ()

/// Return the number of objects.
@property (readonly, assign) NSInteger numberOfObjects;

/// The generated variables dictionary.
@property (readonly, strong) NSDictionary *variables;

/// Add an object to the context and return its unique object reference.
-(MIBaseReference)addObject:(MIBaseObject *)object withType:(NSString *)baseType;

/// Remove the object with object reference. Return YES for success otherwise NO.
-(BOOL)removeObjectWithReference:(MIBaseReference)objectReference;

/// Remove the object. Returns YES if successful otherwise NO.
-(BOOL)removeObject:(MIBaseObject *)object;

/// Remove all base objects
-(void)removeAllObjects;

/// Remove all base objects by type
-(void)removeAllObjectsWithType:(NSString *)objectType;

/// Return the object which has the object reference. Returns nil on failure
-(MIBaseObject *)objectWithReference:(MIBaseReference)reference;

/// Return the object which has object type and name. Returns nil on failure
-(MIBaseObject *)objectWithType:(NSString *)type name:(NSString *)name;

/// Return the object which has object type and index. Return nil on failure
-(MIBaseObject *)objectWithType:(NSString *)type index:(NSInteger)index;

/// Return the number of objects of a particular type. Returns -1 if unknown type
-(NSInteger)numberOfObjectsOfType:(NSString *)objectType;

/// Assign a image to the image collection with identifier
-(BOOL)assignImage:(MICGImage *)image identifer:(NSString *)identifier;

/// Get the image from the image collection with identifier.
-(MICGImage *)getImageWithIdentifier:(NSString *)identifier;

@end
