//  MISafeMutableArray.m
//  MovieMaker
//
// Copyright (c) 2014 Kevin Meaney. MIT license.
// Written after reading the dispatch barrier section by Mike Ash.
// https://www.mikeash.com/pyblog/friday-qa-2011-10-14-whats-new-in-gcd.html

#import "MISafeMutableArray.h"

@interface MISafeMutableArray ()
@property (strong) NSMutableArray *_marray;
@property (strong) dispatch_queue_t _arrayQueue;
@end

@implementation MISafeMutableArray

- (instancetype)initWithQueueName:(NSString *)queueName
{
    self = [super init];
    if (self)
    {
        dispatch_queue_t defaultPriorityQueue;
        defaultPriorityQueue = dispatch_get_global_queue(
                                            DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        dispatch_queue_t myQueue;
        // Creating a concurrent queue. Using dispatch_barrier_sync to limit
        // mutating the array of elements in the context.
        myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_CONCURRENT);
        dispatch_set_target_queue(myQueue, defaultPriorityQueue);
        self._arrayQueue = myQueue;

        self._marray = [[NSMutableArray alloc] init];
    }
    return self;
}

-(id)objectAtIndex:(NSInteger)index
{
    __block id object = nil;
    if (index < 0)
        return object;

    dispatch_sync(self._arrayQueue, ^
    {
        if (index < [self._marray count])
            object = [self._marray objectAtIndex:index];
    });
    return object;
}

-(void)removeObject:(id)object
{
    dispatch_barrier_sync(self._arrayQueue, ^
    {
        [self._marray removeObject:object];
    });
}

-(void)removeObjectAtIndex:(NSInteger)index
{
    if (index < 0)
        return;

    dispatch_barrier_sync(self._arrayQueue, ^
    {
        if (index < [self._marray count])
            [self._marray removeObjectAtIndex:index];
    });
}

-(void)addObject:(id)object
{
    dispatch_barrier_sync(self._arrayQueue, ^
    {
        [self._marray addObject:object];
    });
}

-(NSInteger)count
{
    __block NSInteger numItems;
    dispatch_sync(self._arrayQueue, ^
    {
        numItems = [self._marray count];
    });
    return numItems;
}

-(void)replaceObjectAtIndex:(NSInteger)objectIndex withObject:(id)anObject
{
    dispatch_barrier_sync(self._arrayQueue, ^
    {
        [self._marray replaceObjectAtIndex:objectIndex withObject:anObject];
    });
}

-(void)removeAllObjects
{
    dispatch_barrier_sync(self._arrayQueue, ^
    {
        [self._marray removeAllObjects];
    });
}

/*
-(NSInteger)indexOfObjectPassingTest:(ArrayObjectPredicate)predicate
{
    __block NSInteger foundIndex = NSNotFound;
    dispatch_sync(self._arrayQueue, ^
    {
        foundIndex = [self._marray indexOfObjectPassingTest:predicate];
    });
    return foundIndex;
}
*/

-(id)firstObjectPassingTest:(ArrayObjectPredicate)predicate
{
    __block id theObject;
    dispatch_sync(self._arrayQueue, ^
    {
        NSInteger foundIndex = NSNotFound;
        foundIndex = [self._marray indexOfObjectPassingTest:predicate];
        if (foundIndex != NSNotFound)
            theObject = [self._marray objectAtIndex:foundIndex];
    });
    return theObject;
}

-(NSArray *)immutableArray
{
    __block NSArray *theArray;
    dispatch_sync(self._arrayQueue, ^
    {
        theArray = [[NSArray alloc] initWithArray:self._marray];
    });
    return theArray;
}

@end
