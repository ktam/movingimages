//  MINSGraphicContext.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MICGContext.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIHandleCommands.h"
#import "MIJSONConstants.h"
#import "MINSGraphicContext.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

#import "MIBaseObject_Protected.h"

@import AppKit;
@import QuartzCore;
@import CoreGraphics;

@interface MINSGraphicContext ()

@property (strong) MICGContext *_miCGContext;
@property (strong) dispatch_queue_t _drawingQueue; // uses main queue only.

@property (strong) MICGImage *_image;
@property (strong) MICGImage *_snapShotImage;
@property (weak) MICoreImage *_requestingObject;
@property (strong, nonatomic, readonly) NSWindow *_window;

/// Debug name of the element in which drawing failed.
@property (readonly) NSString *_elementDebugName;

/// Return the width of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _width;

/// Return the height of the bitmap graphic context in pixels.
@property (nonatomic, readonly) size_t _height;

/// Set the position of the window's bottom left corner, on main queue.
-(void)_setWindowOrigin:(NSPoint)windowOrigin;

/// Set position of the bottom left corner of the window. Return YES on success.
-(BOOL)_setWindowOriginFromDict:(NSDictionary *)windowOriginDict;

/// Get the context size of the window as a dict with keys width, height
-(NSDictionary *)_windowContentSize;

/// Return an array of string objects. Each string is a different blend mode.
+(NSArray *)_blendModes;

/// Make an image from the full bitmap context.
-(MICGImage *)_makeImage;

/// _getImage Calls _makeImage and assigns the image to _image.
-(MICGImage *)_getImage;

/// _takeSnapshot Calls _makeImage and assigns the image to _snapShotImage.
-(void)_takeSnapshot;

@end

#pragma mark -
#pragma mark CLASS MINSGraphicContext implementation.

@implementation MINSGraphicContext

#pragma mark Public methods

-(instancetype)initWithWindow:(NSWindow *)window
                         name:(NSString *)theName
                    inContext:(MIContext *)context
{
    self = [super initWithBaseObjectType:MINSGraphicContextKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        dispatch_queue_t myQueue;
        NSString *queueName = [NSString stringWithFormat:
                               @"com.zukini.minsgraphiccontext.%p", self];
        myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                        DISPATCH_QUEUE_SERIAL);
        dispatch_set_target_queue(myQueue, dispatch_get_main_queue());
        self->__window = window;
        self._drawingQueue = myQueue;
    }
    
    return self;
}

-(void)close
{
    [super baseObjectClose];
}

#pragma mark Manual implementation of MINSGraphicContext properties.

-(size_t)_width
{
    NSRect contentRect;
    contentRect = [self._window contentRectForFrameRect:self._window.frame];
    
    return contentRect.size.width;
}

-(size_t)_height
{
    NSRect contentRect;
    contentRect = [self._window contentRectForFrameRect:self._window.frame];
    
    return contentRect.size.height;
}

-(void)_setWindowOrigin:(NSPoint)windowOrigin
{
    dispatch_sync(self._drawingQueue, ^
    {
        [self._window setFrameOrigin:windowOrigin];
    });
}

-(BOOL)_setWindowOriginFromDict:(NSDictionary *)windowOriginDict
{
    BOOL valid = NO;
    NSPoint origin;
    if (windowOriginDict && [windowOriginDict isKindOfClass:[NSDictionary class]])
    {
        if (windowOriginDict[MIJSONKeyX] && windowOriginDict[MIJSONKeyY])
        {
            NSNumber *xNum = windowOriginDict[MIJSONKeyX];
            NSNumber *yNum = windowOriginDict[MIJSONKeyY];
            if (xNum && yNum && [xNum isKindOfClass:[NSNumber class]] &&
                                    [yNum isKindOfClass:[NSNumber class]])
            {
                valid = YES;
                origin.x = xNum.doubleValue;
                origin.y = yNum.doubleValue;
            }
        }
    }
    if (valid)
    {
        [self _setWindowOrigin:origin];
    }
    else
    {
        MILog(@"Couldn't set window origin.", windowOriginDict);
    }
    return valid;
}

-(NSDictionary *)_windowContentSize
{
    NSRect contentRect;
    contentRect = [self._window contentRectForFrameRect:self._window.frame];
    
    size_t width = contentRect.size.width;
    size_t height = contentRect.size.height;
    return @{ MIJSONKeyWidth : @(width), MIJSONKeyHeight : @(height) };
}

#pragma mark Protocol MIBaseObjectSemiPublicInterface instance methods

-(NSDictionary *)handleSetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    id propertyValue = commandDict[MIJSONPropertyValue];
    if (!propertyKey || !propertyValue)
    {
        NSString *whatsMissing;
        whatsMissing = propertyValue ? @"property key" : @"property value";
        NSString *message = [[NSString alloc] initWithFormat:@"Missing %@",
                             whatsMissing];
        MILog(message, commandDict);
        NSString *errorMessage;
        errorMessage = [[NSString alloc] initWithFormat:@"Error: %@", message];
        replyDict = MIMakeReplyDictionary(errorMessage,
                                          MIReplyErrorMissingProperty);
        return replyDict;
    }

    NSPoint origin = [self._window frame].origin;
    
    if ([propertyKey isEqualToString:MIJSONKeyOrigin])
    {
        NSDictionary *propertyDict;
        if ([propertyValue isKindOfClass:[NSDictionary class]])
        {
            propertyDict = propertyValue;
        }
        else if ([propertyValue isKindOfClass:[NSString class]])
        {
            propertyDict = MIUtilityCreateDictionaryFromJSONString(
                                                                propertyValue);
        }
        if ([self _setWindowOriginFromDict:propertyDict])
        {
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message = @"Failed to set window origin";
            MILog(message, commandDict);
            NSString *errorMessage = [[NSString alloc] initWithFormat:@"%@%@",
                                                        @"Error: ", message];
            replyDict = MIMakeReplyDictionary(errorMessage,
                                              MIReplyErrorOperationFailed);
        }
    }
    else if ([propertyKey isEqualToString:MIJSONKeyX])
    {
        if ([propertyValue isKindOfClass:[NSNumber class]])
        {
            origin.x = [propertyValue doubleValue];
            [self _setWindowOrigin:origin];
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else if ([propertyValue isKindOfClass:[NSString class]] &&
                 MIUtilityGetFloatFromString(propertyValue, &(origin.x), nil))
        {
            [self _setWindowOrigin:origin];
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message, *errorMessage;
            message = @"Couldn't convert value to a number.";
            MILog(message, commandDict);
            errorMessage=[[NSString alloc] initWithFormat:@"Error: %@", message];
            replyDict = MIMakeReplyDictionary(errorMessage,
                                              MIReplyErrorMissingProperty);
        }
    }
    else if ([propertyKey isEqualToString:MIJSONKeyY])
    {
        if ([propertyValue isKindOfClass:[NSNumber class]])
        {
            origin.y = [propertyValue doubleValue ];
            [self _setWindowOrigin:origin];
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else if ([propertyValue isKindOfClass:[NSString class]] &&
                 MIUtilityGetFloatFromString(propertyValue, &(origin.y), nil))
        {
            [self _setWindowOrigin:origin];
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message, *errorMessage;
            message = @"Couldn't convert value to a number.";
            MILog(message, commandDict);
            errorMessage=[[NSString alloc] initWithFormat:@"Error: %@", message];
            replyDict = MIMakeReplyDictionary(errorMessage,
                                              MIReplyErrorMissingProperty);
        }
    }
    else
    {
        MILog(@"Unknown property to set", commandDict);
        NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                             @"Error: Unknown property: ", propertyKey];
        replyDict = MIMakeReplyDictionary(message,MIReplyErrorUnknownProperty);
    }
    
    return replyDict;
}

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    NSString *resultString;
    NSNumber *resultNum;
    NSDictionary *resultDict;
    BOOL saveAsJSONString = NO;
    
    NSString *saveResultsType = commandDict[MIJSONKeySaveResultsType];
    if (saveResultsType)
    {
        if ([saveResultsType isEqualToString:MIJSONPropertyJSONString])
            saveAsJSONString = YES;
    }

    size_t resultVal;
    if ([propertyType isEqualToString:MIJSONKeyWidth])
    {
        resultVal = self._width;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeyHeight])
    {
        resultVal = self._height;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeySize])
    {
        resultDict = @{ MIJSONKeyWidth : @(self._width),
                        MIJSONKeyHeight : @(self._height) };
    }
    else if ([propertyType isEqualToString:MIJSONKeyOrigin])
    {
        NSPoint windowOrigin = self._window.frame.origin;
        resultDict = @{ MIJSONKeyX : @(windowOrigin.x),
                        MIJSONKeyY : @(windowOrigin.y) };
    }
    else if ([propertyType isEqualToString:MIJSONPropertyWindowRectangle])
    {
        NSRect windowRect = self._window.frame;
        resultDict = @{ MIJSONKeyOrigin :
                            @{MIJSONKeyX : @(windowRect.origin.x),
                            MIJSONKeyY : @(windowRect.origin.y) },
                        MIJSONKeySize :
                            @{ MIJSONKeyWidth : @(windowRect.size.width),
                               MIJSONKeyHeight : @(windowRect.size.width) }
                        };
    }
    else if ([propertyType isEqualToString:MIJSONKeyX])
    {
        resultVal = self._window.frame.origin.x;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else if ([propertyType isEqualToString:MIJSONKeyY])
    {
        resultVal = self._window.frame.origin.y;
        resultNum = @(resultVal);
        resultString = [[NSString alloc] initWithFormat:@"%ld", resultVal];
    }
    else
    {
        replyDict = [super handleCommonGetPropertyCommand:commandDict];
        if (!replyDict)
        {
            MILog(@"Requested unknown window context property",
                                 commandDict);
            replyDict = MIMakeReplyDictionary(
							  @"Error: Requested unknown window context property",
							  MIReplyErrorUnknownProperty);
        }
    }
	
    if (!replyDict && (resultString || resultDict))
    {
        if (resultDict)
        {
            if (saveAsJSONString)
            {
                resultString = MIUtilityConvertDictionaryToJSONString(resultDict);
                replyDict = MIMakeReplyDictionary(resultString,
                                                        MIReplyErrorNoError);
            }
            else
            {
                replyDict = MIMakeReplyDictionaryWithDictionaryValue(resultDict,
                                                                     NO);
            }
        }
        else
        {
            if (resultNum)
            {
                replyDict = MIMakeReplyDictionaryWithNumericValue(
                                                        resultString,
                                                        MIReplyErrorNoError,
                                                        resultNum);
            }
            else
            {
                replyDict = MIMakeReplyDictionary(resultString,
                                                  MIReplyErrorNoError);
            }
        }
    }
    return replyDict;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(@"Invalid destination for properties.", commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }
	   
    NSMutableDictionary *propertyDict;
    propertyDict = [[NSMutableDictionary alloc] initWithCapacity:0];
    
    propertyDict[MIJSONKeyObjectType] = self.baseObjectType;
    propertyDict[MIJSONKeyObjectName] = self.name;
    propertyDict[MIJSONKeyObjectReference] = @(self.reference);
    propertyDict[MIJSONKeyWidth] = @(self._width);
    propertyDict[MIJSONKeyHeight] = @(self._height);
    propertyDict[MIJSONKeyX] = @(self._window.frame.origin.x);
    propertyDict[MIJSONKeyY] = @(self._window.frame.origin.y);
	
    replyDict = MIUtilitySaveDictionaryToDestination(commandDict,
                                                       propertyDict,
                                                       YES);
    return replyDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleDrawElementCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict = NULL;
    NSDictionary *drawDictionary = commandDict[MIJSONPropertyDrawInstructions];
    if (drawDictionary)
    {
        // OK we might have a valid property dict.
        __block BOOL success = NO;
        dispatch_sync(self._drawingQueue, ^
		{
			// We've might have kept a local reference to the last generated image.
			// But this image becomes stale as soon as drawing happens so discard it
			// now.
			self._image = nil;
			
            CGContextRef context;
            context = (CGContextRef)[[self._window graphicsContext] graphicsPort];
            CGContextSetTextMatrix(context, CGAffineTransformIdentity);
            self._miCGContext = [[MICGContext alloc]
                                 initWithCGContext:context
                                         miContext:self.miContext
                                             owner:self];

			success = [self._miCGContext
					   handleDictionaryDrawElementCommand:drawDictionary];
            [self._window flushWindow];
            self->__elementDebugName = self._miCGContext.elementErrorDebugName;
            self._miCGContext = nil;
		});
        
        if (success)
        {
            BOOL shouldCreateImage = MIUtilityGetBOOLPropertyWithDefault(
                                    commandDict, MIJSONPropertyCreateImage, NO);
            if (shouldCreateImage)
            {
                self._image = [self _makeImage];
            }
            replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            NSString *elementDebugName = self._elementDebugName;
            if (elementDebugName)
            {
                message = [[NSString alloc] initWithFormat:@"%@%@",
                           @"Error: Badly formed element with name: ",
                           elementDebugName];
            }
            else
                message = @"Error: Problem interpreting draw dictionary";
            
            MILog(message, commandDict);
            replyDict = MIMakeReplyDictionary(message,
                                              MIReplyErrorOperationFailed);
        }
    }
    else
    {
        MILog(@"Missing drawing information dict.", commandDict);
        replyDict = MIMakeReplyDictionary(
							  @"Error: Missing drawing information dictionary.",
							  MIReplyErrorMissingOption);
    }
    return replyDict;
}

-(NSDictionary *)handleSnapshotCommand:(NSDictionary *)commandDict
{
    NSDictionary *replyDict;
    NSString *snapShotAction = commandDict[MIJSONPropertySnapshotAction];
    // Error checking.
    if (!snapShotAction)
    {
        MILog(@"Missing snapshot action.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing snapshot action",
                                     MIReplyErrorMissingOption);
    }
    
    if ([snapShotAction isEqualToString:MIJSONValueDrawSnapshot] &&
        !self._snapShotImage)
    {
        MILog(@"Snap to be drawn hasn't be taken", commandDict);
        return MIMakeReplyDictionary(@"Error: Snap to be drawn hasn't be taken",
                                     MIReplyErrorMissingOption);
    }
    
    // now perform the command.
    if ([snapShotAction isEqualToString:MIJSONValueTakeSnapshot])
    {
        [self _takeSnapshot];
    }
    else if ([snapShotAction isEqualToString:MIJSONValueDrawSnapshot])
    {
        dispatch_sync(self._drawingQueue, ^
        {
            size_t width, height;
            CGContextRef context;
            self._image = nil;  // Previously captured image made stale.
            context = (CGContextRef)[[self._window graphicsContext] graphicsPort];
            width = CGImageGetWidth(self._snapShotImage.CGImage);
            height = CGImageGetHeight(self._snapShotImage.CGImage);
            CGRect theRect = CGRectMake(0.0, 0.0, width, height);
            CGContextDrawImage(context, theRect, self._snapShotImage.CGImage);
            [self._window flushWindow];
        });
    }
    else if ([snapShotAction isEqualToString:MIJSONValueClearSnapshot])
    {
        self._snapShotImage = nil;
    }
    else
    {
        MILog(@"Invalid snapshot action", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Invalid snapshot action",
                                          MIReplyErrorInvalidOption);
    }
    if (!replyDict)
    {
        replyDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    return replyDict;
}

-(NSDictionary *)handleAssignImageToCollectionCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *ident = commandDict[MIJSONPropertyImageIdentifier];
    if (ident)
    {
        MICGImage *image = [self _getImage];
        MIContext *context = self.miContext;
        if (image && context)
        {
            [context assignImage:image identifer:ident];
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            NSString *message;
            message = image ? @"Error: No context" : @"Error: No image";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    else
    {
        NSString *message = @"Error: command dict doesn't contain image ident";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return result;
}

#pragma mark Private methods.

-(MICGImage *)_makeImage
{
    __block CGImageRef cgImage = nil;
    dispatch_sync(self._drawingQueue, ^{
        cgImage = CGWindowListCreateImage(CGRectNull,
                                      kCGWindowListOptionIncludingWindow,
                                      (CGWindowID)[self._window windowNumber],
                                      kCGWindowImageBoundsIgnoreFraming);
    });

    if (cgImage)
    {
        // Remove the window title bar from the top of the image.
        size_t contentHeight = self._height;
        if (CGImageGetHeight(cgImage) > contentHeight)
        {
            CGRect croppedRect = CGRectMake(0.0,
                                    CGImageGetHeight(cgImage) - contentHeight,
                                    CGImageGetWidth(cgImage),
                                    contentHeight);
            CGImageRef croppedImage = CGImageCreateWithImageInRect(cgImage,
                                                               croppedRect);
            if (croppedImage)
            {
                CGImageRelease(cgImage);
                cgImage = croppedImage;
            }
        }
    }
    
    MICGImage *newImage;
    if (cgImage)
    {
        newImage = [[MICGImage alloc] initWithCGImage:cgImage];
        CGImageRelease(cgImage);
    }
    return newImage;
}


-(MICGImage *)_getImage
{
    // If we already have an image, then get it and return
    MICGImage *theImage = self._image;
    if (theImage)
        return theImage;
	
    self._image = [self _makeImage];
    return self._image;
}

-(void)_takeSnapshot
{
    self._snapShotImage = [self _makeImage];
}

#pragma mark Public Class Methods

+(NSArray *)_blendModes
{
    return [MICGContext blendModes];
}

#pragma mark Public Class MIBaseObjectSemiPublicInterface methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    context = context ? context : [MIContext defaultContext];

    if (propertyKey == NULL)
    {
        MILog(@"Missing property key", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property key",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyBlendModes])
    {
        NSArray *blendModes = [MINSGraphicContext _blendModes];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                    blendModes);
        replyDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MINSGraphicContextKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property requested.", commandDict);
        replyDict = MIMakeReplyDictionary(
							  @"Error: MINSGraphicContext unknown property",
							  MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
	CGRect windowRect;
    NSUInteger windowStyleMask = NSTitledWindowMask;

    NSNumber *borderLess = commandDict[MIJSONPropertyBorderlessWindow];
    
    if (borderLess && [borderLess boolValue])
        windowStyleMask = NSBorderlessWindowMask;

    BOOL gotWindowRect = MIUtilityGetCGRectFromDictionary(
                                        commandDict[MIJSONKeyRect],
                                        &windowRect,
                                        kMINoLog,
                                        context.variables);

    if (gotWindowRect)
    {
        MINSGraphicContext *object;
        NSString *objectName;
        objectName = commandDict[MIJSONKeyObjectName];
		__block NSWindow *theWindow;
        __block NSDictionary *dispatchDict;
        dispatch_sync(dispatch_get_main_queue(), ^
        {
            @try
            {
                theWindow = [[NSWindow alloc]
                             initWithContentRect:windowRect
                             styleMask:windowStyleMask
                             backing:NSBackingStoreBuffered
                             defer:NO
                             screen:NULL];
                // Set the window level to floating
                if (theWindow)
                {
                    theWindow.level = NSFloatingWindowLevel;
                    theWindow.hasShadow = YES;
                    [theWindow makeKeyAndOrderFront:NULL];
                }

            }
            @catch (NSException *exception)
            {
                MILog(@"The exception description: %@",
                                     [exception description]);
                MILog(@"Exception raised creating window: ",
                                     commandDict);
                dispatchDict = MIMakeReplyDictionary(
                                      @"Error: exception raised creating window",
                                      MIReplyErrorOperationFailed);
                theWindow = NULL;
            }
        });
        
        if (dispatchDict)
            replyDict = dispatchDict;

        if (!replyDict)
        {
            object = [[MINSGraphicContext alloc] initWithWindow:theWindow
                                                           name:objectName
                                                      inContext:context];
            if (!object)
            {
                MILog(@"Failed to create nsgraphicscontext",
                                     commandDict);
                replyDict = MIMakeReplyDictionary(
                                  @"Error: Failed to create nsgraphicscontext",
                                  MIReplyErrorOptionValueInvalid);
            }
        }

        if (object)
        {
            MIBaseReference objectReference = [object reference];
            NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                   (long)objectReference];
            replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                              MIReplyErrorNoError,
                                                              @(objectReference));
        }
    }
    else
    {
        MILog(@"Missing option creating context.", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing Option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleCalculateGraphicSizeOfTextCommand:(NSDictionary *)dict
                                               inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    return MIUtilityCalculateGraphicSizeOfTest(dict, context.variables);
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MINSGraphicContextKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark MICIRenderDestinationInterface protocol methods

-(BOOL)drawCoreImage:(CIImage *)ciImage
     fromFilterChain:(MICoreImage *)coreImage
            fromRect:(CGRect)fromRect
              toRect:(CGRect)toRect
   shouldCreateImage:(BOOL)shouldCreateImage
{
    __block BOOL result = YES;
    dispatch_sync(self._drawingQueue, ^
	{
		self._image = nil;
		CIContext *ciContext = [[self._window graphicsContext] CIContext];
		if (ciContext && ciImage)
		{
			CGRect localTo = toRect;
			if (CGRectIsEmpty(localTo))
			{
				localTo = CGRectMake(0.0, 0.0, (CGFloat)self._width,
									 (CGFloat)self._height);
			}
			
			CGRect localFrom = fromRect;
			if (CGRectIsEmpty(localFrom))
			{
				localFrom = ciImage.extent;
				if (localFrom.size.width > 1.0e+6 ||
					localFrom.size.height > 1.0e+6)
				{
					localFrom = localTo;
				}
			}
			
			@try
			{
				[ciContext drawImage:ciImage inRect:localTo
								  fromRect:localFrom];
                [self._window flushWindow];
			}
			@catch (NSException *exception)
			{
				NSString *message = [[NSString alloc] initWithFormat:@"%@",
			    @"Attempting to filter chain into a CIContext. Exception thrown."];
				MILog(message, [exception reason]);
				result = NO;
			}
		}
		else
			result = NO;
	});
    if (shouldCreateImage && result)
        self._image = [self _makeImage];
    return result;
}

-(void)clearCIContextRelatingTo:(MICoreImage *)requestingObject
{
	// Do nothing. The NSGraphicContext manages the CIContext.
}

#pragma mark MICreateCGImageInterface protocol methods

-(CGImageRef)createCGImageWithOptions:(NSDictionary *)options // ignores options.
{
    MICGImage *theImage = [self _getImage];
    return CGImageRetain([theImage CGImage]);
}

-(void)passMICGImageTo:(MICGImageWrapper *)imageWrapper
          usingOptions:(NSDictionary *)options
{
    MICGImage *theImage = [self _getImage];
    if (imageWrapper.makeCopy)
        [imageWrapper setMICGImageStrong:theImage];
    else
        [imageWrapper setMICGImageWeak:theImage];
}

@end
