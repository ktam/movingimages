//  MIUtility.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
@import CoreGraphics;
@import CoreText;
@import AVFoundation;

#import "MIContext.h"
#import "MIHandleCommands.h"

typedef NS_ENUM(BOOL, MI_LOG) {
    kMINoLog,
    kMILog
};

/// MMCGLine specified by two end points.
struct MMCGLine {
    CGPoint start;
    CGPoint end;
};
typedef struct MMCGLine MMCGLine;

@class MICGImage;

/// The invalid image index constant value.
extern const NSInteger MIUtilityInvalidImageIndex;

/**
 @brief Get a CGFloat from the floatValue param.
 @param floatValue [IN] Should be NSNumber or NSString with equation string number.
 @param value [OUT] The CGFloat value will be returned in this param.
 @param vars [IN] A dictionary of variables for evaluating equation.
 @return YES on success. NO if fails to obtain CGFloat
*/
BOOL MIUtilityGetCGFloatFromID(id floatValue, CGFloat *value, NSDictionary *vars);

/**
 @brief Get a CGFloat value specified by a key from the dictionary.
 @param dict [IN] A dictionary to get the CGFloat value out of.
 @param key [IN] The key to get property from dict.
 @param floatVal [OUT] A pointer to a CGFloat that will be assigned the value.
 @param variablesDict [IN] A dictionary of variables and their values.
 @return YES on success. NO if fails to obtain CGFloat
*/
BOOL MIUtilityGetCGFloatFromDictionaryNoLog(NSDictionary *dict, // in
                                            NSString *key,      // in
                                            CGFloat *floatVal,  // out
                                            NSDictionary *variablesDict); // in

/**
 @brief Get a CGFloat value specified by a key from the dictionary. Log errors.
 @discussion If fails to get the value will log an error using DDLog.
 @param dict [IN] A dictionary to get the CGFloat value out of.
 @param key [IN] The key to get property from dict.
 @param floatVal [OUT] A pointer to a CGFloat that will be assigned the value.
 @param variablesDict [IN] A dictionary of variables and their values.
 @return YES on success. NO if fails to obtain CGFloat
*/
BOOL MIUtilityGetCGFloatFromDictionary(NSDictionary *dict, // in
                                       NSString *key,      // in
                                       CGFloat *floatVal,   // out
                                       NSDictionary *variablesDict); // in

/**
 @brief Get an Integer value specified by a key from the dictionary.
 @param dict [IN] A dictionary to get the property from.
 @param key [IN] The key to get property from dict.
 @param floatVal [OUT] A pointer to a NSInteger that will be assigned the value.
 @param variablesDict [IN] A dictionary of variables and their values.
 @return YES on success. NO if fails to obtain CGFloat
 */
BOOL MIUtilityGetIntegerFromDictionaryNoLog(NSDictionary *dict, // in
                                            NSString *key,      // in
                                            NSInteger *intVal,  // out
                                            NSDictionary *variablesDict); // in

/**
 @brief Get an Integer value specified by a key from the dictionary. Log errors.
 @discussion If fails to get the value will log an error using DDLog.
 @param dict [IN] A dictionary to get the property from.
 @param key [IN] The key to get property from dict.
 @param floatVal [OUT] A pointer to a NSInteger that will be assigned the value.
 @param variablesDict [IN] A dictionary of variables and their values.
 @return YES on success. NO if fails to obtain CGFloat
*/
BOOL MIUtilityGetIntegerFromDictionary(NSDictionary *dict,  // in
                                       NSString *key,       // in
                                       NSInteger *intVal,   // out
                                       NSDictionary *variablesDict); // in

/// Get the MIBaseReference from the dictionary.
BOOL MIUtilityGetMIBaseReferenceFromDictionary(NSDictionary *dict,
                                               MIBaseReference *baseRef);

/**
 Get the image index from dict. Returns true if property with key exists. If
 the image index value is not within range or the property value wasn't a 
 NSNumber type then the returned imageIndex is MIUtilityInvalidImageIndex
*/
BOOL MIUtilityGetImageIndexWithKey(NSDictionary *dict,
                                     NSString *key,
                                     NSInteger *imageIndex,
                                     size_t numImages);

/// Does the property key begin with "dictionary". Return YES if true.
BOOL MIUtilityDoesPropertyTypeBeginWithDictionary(NSString *propertyKey);

/**
 @brief Get a constrained CGFloat value specified by a key from the dictionary.
 @discussion Similar to MIUtilityGetCGFloatFromDictionary except that this
 funciton will constrain the value to within the min and maximum range.
 @param varDict [IN] A dictionary of variables and their names.
 @return YES on success. NO if any arguments are NULL or fails to obtain CGFloat
*/
BOOL MIUtilityGetConstrainedCGFloatFromDictionary(NSDictionary *dict, // in
                                                  NSString *key,      // in
                                                  CGFloat minVal,     // in
                                                  CGFloat maxVal,     // in
                                                  CGFloat *floatVal,  // out
                                                  NSDictionary *varDict); // in

/// Get the line width from dict. Returns YES=success. NO if fails.
BOOL MIUtilityGetLineWidthFromDictionary(NSDictionary *dict,CGFloat *lineWidth,
                                         NSDictionary *variablesDict);

/**
 @brief Get a CGSize struct from dict. Returns YES=success. NO on failure.
 @param dict [IN] A dictionary to get the size object out of.
 @param size [OUT] A pointer to a CGSize that will be filled in on success.
 @param log [IN] An option as to whether we should log failure or not.
 @param variablesDict [IN] A dictionary of variables and their values.
*/
BOOL MIUtilityGetCGSizeFromDictionary(NSDictionary *dict,
                                      CGSize *size,
                                      MI_LOG log,
                                      NSDictionary *variablesDict);

/**
 @brief Get a CGSize struct from property value. Returns YES=success else NO.
 @param dict [IN] A property value to get the size object out of.
 @param size [OUT] A pointer to a CGSize that will be filled in on success.
 @param variables [IN] A dictionary of variables and their values.
*/
BOOL MIUtilityGetCGSizeFromPropertyValue(id propertyValue, CGSize *size,
                                         NSDictionary *variables);

/**
 @brief Get a CGPoint struct from dict. Returns YES=success. NO on failure.
 @param dict [IN] A dictionary to get the point out of.
 @param point [OUT] A pointer to a CGPoint that will be filled in on success.
 @param log [IN] An option as to whether we should log failure or not.
 @param variablesDict [IN] A dictionary of variables and their values.
*/
BOOL MIUtilityGetCGPointFromDictionary(NSDictionary *dict,
                                       CGPoint *point,
                                       MI_LOG log,
                                       NSDictionary *variablesDict);

/**
 @brief Get a CGRect from dict. Returns YES=success. NO if dict NULL or fails
 @param dict [IN] A dictionary to get the rect out of.
 @param rect [OUT] A pointer to a CGRect that will be filled in on success.
 @param log [IN] An option as to whether we should log failure or not.
 @param variablesDict [IN] A dictionary of variables and their values.
*/
BOOL MIUtilityGetCGRectFromDictionary(NSDictionary *dict,
                                      CGRect *rect,
                                      MI_LOG log,
                                      NSDictionary *variablesDict);

/**
 @brief Get a MMCGLine from dict. Returns YES=success. NO if dict NULL or fails.
 @param dict [IN] A dictionary to obtain the line out of.
 @param line [OUT] A pointer to a struct containing start & end points on exit.
 @param variablesDict [IN] A dictionary of variables and their values.
*/
BOOL MIUtilityGetMMCGLineFromDictionary(NSDictionary *dict,
                                        MMCGLine *line,
                                        NSDictionary *variablesDict);

/**
 @brief Get a list of radius values from the dictionary.
 @param dict The dictionary to pull the radius values from.
 @param radiuses An array of 4 values to return the radius for each corner.
 @param variablesdict [IN] A dictionary of variables and their values.
 @return YES on success. NO if any arguments are NULL or fails.
 @discussion If there is a single radius value in the dictionary then all 4
 elements in the array radiuses will be filled with the same value.
*/
BOOL MIUtilityGetRadiusesFromDictionary(NSDictionary *dict, CGFloat *radiuses,
                                        NSDictionary *variablesDict);

/// Create a CGColor from NSDictionary. Returns NULL on failure.
CGColorRef MIUtilityCreateColorFromDictionary(NSDictionary *dict,
                                              NSDictionary *variablesDict);

/// Create a CGColor from a hash string. Returns NULL on failure.
CGColorRef MIUtilityCreateColorFromString(NSString *colorString);

/// Create a CGColor. Returns NULL on failure. Input is a string or dictionary.
CGColorRef MIUtilityCreateColor(id colorRep, NSDictionary *variablesDict);

/// Create a NSDictionary with width & height. Returns NULL on failure.
NSDictionary *MIUtilityCalculateGraphicSizeOfTest(NSDictionary *textDictionary,
                                                  NSDictionary *variables);

/// With a key path starting with dictionary, pull property from the dictionary
NSString *MIUtilityGetDictionaryPropertyWithKey(NSDictionary *dictionary,
                                                NSString *key);

/// Get the User Interface font type.
CTFontUIFontType MIUtilityGetUserInterfaceFontType(NSString *uiFontTypeString);

/// Get the list of user interface font names. NSArray<NSString>
NSArray *MIUtilityGetListOfUserInterfaceFonts();

/// Convert a dictionary to a JSON string
NSString *MIUtilityConvertDictionaryToJSONString(NSDictionary *theDict);

/**
 @brief Creates a string representation of an object if possible.
 @discussion Return the string representation of an object. For a NSString
 object it just returns the string, for an NSNumber just request it's string
 value. For NSArray and NSDictionary this function will return a json
 representation.
*/
NSString *MIUtilityCreateStringRepresentationOfObject(id object,
                                                NSJSONWritingOptions opts);

/// Create a string which combines strings from an array with a delimiter.
NSString *MIUtilityCreateStringFromArrayOfStrings(NSString *delimiter,
                                                  NSArray *strings);

/// Before creating and saving a file ensure directory path exists.
void MIUtilityMakeSureParentDirectoryOfFilePathExists(NSString *filePath);

/// Write the NSDictionary as a plist file to a file with path.
BOOL MIUtilityWriteDictionaryToPLISTFile(NSDictionary *dictionary,
                                                    NSString *path);

/// Write the NSDictionary as a JSON file to a file with path.
BOOL MIUtilityWriteDictionaryToJSONFile(NSDictionary *dictionary,
                                        NSString *path);

/// Convert JSON string to NSDictionary. Returns a dict if successful else nil
NSDictionary *MIUtilityCreateDictionaryFromJSONString(NSString *jsonString);

/// Convert a CGSize struct into a NSDictionary object.
NSDictionary *MIUtilityCreateSizeDictionaryFromCGSize(CGSize size);

/// Make a CGAffineTransform from an array of transformations.
BOOL MIMakeCGAffineTransformFromArray(NSArray *transformations,
                                      CGAffineTransform *affine,
                                      NSDictionary *variables);

/// Make a CGAffineTransform from a transformation dictionary.
BOOL MIMakeCGAffineTransformFromDictionary(NSDictionary *transformDict,
                                           CGAffineTransform *affine);

/// Make a CGAffineTransform from a dictionary or an array.
BOOL MIMakeCGAffineTransform(id inputTransformData, CGAffineTransform *affine,
                             NSDictionary *variables);

/// Create a transform dictionary from a CGAffineTransform.
NSDictionary *MIUtilityCreateCGAffineTransformDictionary(CGAffineTransform at);

/**
 @brief Replace values in a dictionary which can't be represented in a plist.
 @discussion Values in dictionaries can be any object that inherits from
 NSObject that conforms to the copying protocol or any CoreFoundation object.
 However only a few different types of objects can be included in a NSDictionary
 if we want to save the dictionary to a plist or a json file. This function
 converts CIVector and NSURL objects to string objects before creating the
 duplicate dictionaries.
 */
NSDictionary *MIUtilityCreateDictionarySuitableForPlistOrJSON(
                                                NSDictionary *inputDictionary);

/**
 @brief Replace values in an array which can't be represented in a plist.
 @discussion Values in array can be any object that inherits from
 NSObject that conforms to the copying protocol or any CoreFoundation object.
 However only a few different types of objects can be included in a NSArray
 if we want to save the array to a plist or a json file. This function
 converts CIVector and NSURL objects to string objects before creating the
 duplicate arrays.
*/
NSArray *MIUtilityCreateArraySuitableForPlistOrJSON(NSArray *inputArray);

/**
 @brief Get the BOOL value from the value parameter.
 @discussion If the value object is nil or neither a string or a NSNumber then
 this function will set error to be true.
*/
BOOL MIUtilityGetBOOLValue(NSObject *value, BOOL *isError);

/**
 @brief Get the bool value from the property dictionary with the key.
 @discussion This function takes a default bool value which is to be used if for
 any reason the property value associated with the property key cannot be
 obtained.
*/
BOOL MIUtilityGetBOOLPropertyWithDefault(NSDictionary *dict, NSString *key,
                                           BOOL theDefault);

/**
 @brief Does the command dictionary specify where to send results.
 @discussion This function determines if the command dictionary, most likely
 handling the getproperties command contains the necessary information to
 determine where the results of the getproperties command should be replied to.
 If the function returns nil, then the command dictionary has the necessary
 information, otherwise the function returns a string indicating an error
 message.
*/
NSString *MIUtilityDoesCommandDictContainDestination(NSDictionary *commandDict);

/// Get the file path from the command dictionary or variables dictionary.
NSString *MIUtilityGetFilePathFromCommandAndVariables(NSDictionary *commandDict,
                                                    NSDictionary *variables);

/**
 @brief Create the dictionary from the command.
 @discussion The commandDict should contain the information as to where to get
 the information from to create the dictionary. The information can be sourced
 from a json string, a json file on disk, a property file on disk, or the
 dictionary can actually just be a property of the command dictionary. This
 function just gets that dictionary.
*/
NSDictionary *MIUtilityCreateDictionaryFromCommand(NSDictionary *commandDict,
                                                   NSDictionary *variables);

/**
 @brief Saves the result dictionary to a destination.
 @discussion The destination can be to a plist or json file, or added to the
 reply dictionary as a json object, or a plist dictionary. When adding to the
 reply dictionary the json object is saved as a string to the string value
 property of the reply dictionary. When adding the resultDictionary to the
 reply dictionary it is added to dictionary value property of the reply
 dictionary. This assumes that MIUtilityDoesCommandDictContainDestination
 has been called previously and confirmed that the commandDict contains the
 necessary information.
 */
NSDictionary *MIUtilitySaveDictionaryToDestination(NSDictionary *commandDict,
                                                 NSDictionary *resultDictionary,
                                                 BOOL defaultToReplyDict);

/**
 @brief Generate a MICGImage based on the properties of the image dictionary.
 @discussion MICGImageFromDictionary will first see if it can obtain an
 image from the image collection in the context, but if the image identifier key
 is not specified then MICGImageFromDictionary will determine the object to
 create the image and get the image options from the image dictionary and then
 call MICGImageFromObjectAndOptions to create the image.
*/
MICGImage *MICGImageFromDictionary(MIContext *context, NSDictionary *imageDict,
                                   id cantBeThisObject);

/**
 @brief Generate a MICGImage using object represented by objectDict and options.
 @discussion The contents of the option dictionary should change depending on
 the object described in objectDict. A bitmap context takes no options whereas
 a movie importer object requires the frame time to be specified, while an image
 importer object takes an optional image index. If none is supplied then an index
 of 0 is assumed.
 @param context The context which contains the object to get image from.
 @param objectDict  A dictionary with info to find the image source object
 @param imageOptions  An image options dictionary. Contents depends on receiver.
 @param cantBeThisObject    This object is not available to get the image from
 @result a MICGImage wrapping a CGImageRef and returns nil on failure.
*/
MICGImage *MICGImageFromObjectAndOptions(MIContext *context,
                                                     NSDictionary *objectDict,
                                                     NSDictionary *imageOptions,
                                                     id cantBeThisObject);

/**
 @brief Saves the CGImage as a png image file.
 @discussion The filePath is the location where the file is saved. No check is
 made as to whether a previous file will be overwritten.
 @param theImage The image to be saved as a png file.
 @param filePath The path to where the image file will be saved.
 @return true on success, false if the file could not be saved.
*/
BOOL MISaveCGImageToAPNGFileWithPath(CGImageRef theImage, NSString *filePath);

MILoggingFunction miLog;

#define MILog(message, object) \
    do { \
        if (miLog) { \
            NSString *fString = [[NSString alloc] initWithUTF8String:__FUNCTION__]; \
            NSString *_type; \
            NSString *_stringRep; \
            if (object) \
            { \
                _type = NSStringFromClass([(id)object class]); \
                _stringRep = MIUtilityCreateStringRepresentationOfObject(object, \
                                                    NSJSONWritingPrettyPrinted); \
            } \
            NSString *fileName = @__FILE__.lastPathComponent; \
            miLog(message, _type, _stringRep, fileName, __LINE__, fString); \
        } \
    } \
    while (0)

#define MILogBreak() \
    do { \
        if (miLog) { \
            miLog(@"======================================================", \
                  nil, nil, nil, 0, nil); \
        } \
    } \
    while (0)

#if DEBUG
void SaveCGImageToAPNGFile(CGImageRef theImage, NSString *fileName);
void SaveCGBitmapContextToAPNGFile(CGContextRef context, NSString *fileName);
#endif

