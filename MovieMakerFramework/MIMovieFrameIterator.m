//  MIMovieFrameIterator.m
//  MovingImagesFramework
//
//  Copyright (c) 2014 Apple Inc. All rights reserved.
// The code for the movie frame iterator is based on the following apple technote 2404.
// https://developer.apple.com/library/mac/technotes/tn2404/_index.html#//apple_ref/doc/uid/DTS40015060
// For now it looks like the added functionality this approach can add is not
// worth the additional investment in time.

// Currently not in use. Not linked in to project so untested.

@import AVFoundation;

#import "MIAVFoundationUtilities.h"
#import "MIBaseObject.h"
#import "MIBaseObject_Protected.h"
#import "MICGImage.h"
#import "MICGImageWrapper.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIMovieFrameIterator.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

static const int ddLogLevel = LOG_LEVEL_INFO;

@interface MIMovieFrameIterator ()

@property (readonly, strong) AVURLAsset *_asset;
@property (readonly, strong) AVAssetTrack *_track;
@property (readonly, strong) AVSampleCursor *_cursor;

// A MIMovieFrameIterator object is initialized with a buffer request object
// which itself is initialized with a cursor set to the start position and
// that the samples are obtained in the forward direction. The requester object
// will change if the start position is modified.
@property (readonly, strong) AVSampleBufferRequest *_requester;
@property (readonly, strong) AVSampleBufferGenerator *_generator;

@end

@implementation MIMovieFrameIterator

#pragma mark Public Class MIBaseObjectSemiPublicInterface protocol methods

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    context = context ? context : [MIContext defaultContext];
    NSString *filePath = MIUtilityGetFilePathFromCommandAndVariables(commandDict,
                                                            context.variables);

    NSDictionary *trackD = commandDict[MIJSONPropertyMovieTrack];
    if (filePath && trackD)
    {
        NSString *objectName = commandDict[MIJSONKeyObjectName];
        
        NSURL *fileURL = [[NSURL alloc] initFileURLWithPath:filePath];
        NSDictionary *options = @{
                          AVURLAssetPreferPreciseDurationAndTimingKey: @YES,
                          AVURLAssetReferenceRestrictionsKey:
                              @(AVAssetReferenceRestrictionForbidNone)
                                  };
        AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:fileURL
                                                    options:options];
        if (!asset)
        {
            MILog(@"Failed to create an AVURLAsset. ",
                                 commandDict);
            replyDict = MIMakeReplyDictionary(
                        @"Error: Failed to create a AVURLAsset from file path",
                                              MIReplyErrorOperationFailed);
            return replyDict;
        }

        AVAssetTrack *track;
        AVSampleCursor *cursor;
        AVSampleBufferRequest *requester;
        AVSampleBufferGenerator *generator;

        if (asset)
        {
            track = MIGetTrackFromAVAssetWithDictionary(asset, trackD);
            if (track && track.canProvideSampleCursors &&
                [[track mediaType] isEqualToString:AVMediaTypeVideo])
            {
                cursor = [track makeSampleCursorAtFirstSampleInDecodeOrder];
                requester = [[AVSampleBufferRequest alloc]
                                              initWithStartCursor:cursor];
                if (requester)
                {
                    requester.direction = AVSampleBufferRequestDirectionForward;
                    generator = [[AVSampleBufferGenerator alloc]
                                                     initWithAsset:asset
                                                          timebase:nil];
                }
                if (!generator)
                {
                    // generator being the last object to be created. If it is nil
                    // then we should nil everything else.
                    MILog(
                                    @"Couldn't make a sample buffer generator",
                                    commandDict);
                    requester = nil;
                    cursor = nil;
                    track = nil;
                    asset = nil;
                }
    //          CMTime startTime;
    //          cursor = [track makeSampleCursorWithPresentationTimeStamp:startTime];
    //          cursor = [track makeSampleCursorAtLastSampleInDecodeOrder];
            }
            else
            {
                MILog(
                        @"Couldn't get a track which can provide sample cursors",
                                     commandDict);
                track = nil;
                asset = nil;
            }
        }

        // Basically if anything has gone wrong then all the associated objects
        // are nulled so only need to check the asset object here to see if
        // we need to bail.
        if (!asset)
        {
            replyDict = MIMakeReplyDictionary(
                          @"Error: Failed to create a AVSampleBufferGenerator",
                          MIReplyErrorOperationFailed);
            return replyDict;
        }

        // OK everything seems to be setup correctly, now create the movie frame
        // iterator object
        MIMovieFrameIterator *object;
        object = [[MIMovieFrameIterator alloc] initWithAVURLAsset:asset
                                                             name:objectName
                                                            track:track
                                                     sampleCursor:cursor
                                                  sampleRequester:requester
                                                  sampleGenerator:generator
                                                        inContext:context];
        if (object)
        {
            MIBaseReference objectReference = [object reference];
            NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                                                    (long)objectReference];
            replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                          MIReplyErrorNoError,
                                                          @(objectReference));
        }
        else
        {
            MILog(@"Failed to create movie frame iterator",
                                 commandDict);
            NSString *message = [[NSString alloc] initWithFormat:@"%@%@",
                         @"Error: Failed to movie create importer with path: ",
                         filePath];
            replyDict = MIMakeReplyDictionary(message,
                                              MIReplyErrorOptionValueInvalid);
        }
    }
    else
    {
        MILog(@"Missing path to movie file or track dictionary",
                             commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing file path option",
                                          MIReplyErrorMissingOption);
    }
    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MIMovieFrameIteratorKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                                inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    context = context ? context : [MIContext defaultContext];
    if (propertyKey == NULL)
    {
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingProperty);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        size_t resultVal = [context numberOfObjectsOfType:MIMovieImporterKey];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld", resultVal];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          @(resultVal));
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(
                                      @"Error: MIMovieImporter unknown property",
                                      MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

#pragma mark Public methods

-(instancetype)initWithAVURLAsset:(AVURLAsset *)avAsset
                             name:(NSString *)theName
                            track:(AVAssetTrack *)track
                     sampleCursor:(AVSampleCursor *)cursor
                  sampleRequester:(AVSampleBufferRequest *)requester
                  sampleGenerator:(AVSampleBufferGenerator *)generator
                        inContext:(MIContext *)context
{
    if (!theName || theName.length == 0)
        theName = [avAsset.URL path];
    
    self = [super initWithBaseObjectType:MIMovieFrameIteratorKey
                                    name:theName
                               inContext:context];
    if (self)
    {
        self->__asset = avAsset;
        self->__track = track;
        self->__cursor = cursor;
        self->__requester = requester;
        self->__generator = generator;
    }
    
    return self;
}

-(void)close
{
    [super baseObjectClose];
}

@end
