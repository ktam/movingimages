//  MIMovieFrameIterator.h
//  MovingImagesFramework
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"

@class AVURLAsset;

/**
 Objects of the `MIMovieFrameIterator` class are wrappers of a AVURLAsset and
 a specific video track and manages the iterative access to frames in that asset made up of frames of
 individual video tracks, from a single track to all the tracks.
 */
@interface MIMovieFrameIterator : MIBaseObject <MICreateCGImageInterface,
                                                MIBaseObjectSemiPublicInterface>

/**
 @brief Initializes the MIMovieImporter object.
 @param fileURL The file to create the CGImageSourceRef object from.
 @param name The name the object is given to use to refer to the object
 @param context The context within which to create image importer base object
*/
-(instancetype)initWithAVURLAsset:(AVURLAsset *)avAsset
                             name:(NSString *)theName
                            track:(AVAssetTrack *)track
                     sampleCursor:(AVSampleCursor *)cursor
                  sampleRequester:(AVSampleBufferRequest *)requester
                  sampleGenerator:(AVSampleBufferGenerator *)generator
                        inContext:(MIContext *)context;

/// Removes this object from arrays. If no other strong refs, will be dealloc'd.
-(void)close;

@end
