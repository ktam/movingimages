//  MIRenderer.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

@import CoreGraphics;

#import "MIContext.h"
#import "MIRenderer.h"

#import "MICGContext.h"
#import "MIHandleCommands.h"

NSString *const MISetupDictionaryKey = @"setupcommandsdictionary";
NSString *const MIBackgroundCommandsDictionaryKey = @"backgroundcommandsdictionary";
NSString *const MIMainThreadCommandsDictionaryKey = @"mainthreadcommandsdictionary";
NSString *const MIDrawDictionaryKey = @"drawdictionary";
NSString *const MICleanupDictionaryKey = @"cleanupdictionary";

@interface MIRenderer ()

@property (assign) BOOL isReady;

@property (strong) NSDictionary *_setupDictionary;
@property (strong) NSDictionary *_backgroundCommandsDictionary;
@property (strong) NSDictionary *_mainThreadCommandsDictionary;
@property (strong) NSDictionary *_drawingDictionary;
@property (strong) NSDictionary *_cleanupDictionary;
@property (assign) BOOL _setupDone;

@end

@implementation MIRenderer

-(instancetype)initWithMIContext:(MIContext *)miContext
{
    self = [super init];
    if (self)
    {
        self->_rendererContext = miContext;
    }
    return self;
}

-(instancetype)init
{
    MIContext *theContext = [MIContext new];
    return [self initWithMIContext:theContext];
}

-(BOOL)configure:(NSDictionary *)configDict
{
	self._setupDictionary = configDict[MISetupDictionaryKey];
	self._backgroundCommandsDictionary =
                                configDict[MIBackgroundCommandsDictionaryKey];
    self._mainThreadCommandsDictionary =
                                configDict[MIMainThreadCommandsDictionaryKey];
	self._drawingDictionary = configDict[MIDrawDictionaryKey];
	self._cleanupDictionary = configDict[MICleanupDictionaryKey];
	if (self._drawingDictionary)
	{
		self->_isReady = YES;
	}
    return self->_isReady;
}

-(NSDictionary *)setupDrawing
{
    NSDictionary *resultDict;
	self._setupDone = YES;
	MIReplyErrorEnum errorCode = MIReplyErrorNoError;
	if (self._setupDictionary)
	{
        [self.rendererContext appendVariables:self.variables];
		resultDict = MIMovingImagesHandleCommands(
											self.rendererContext,
											self._setupDictionary,
                                            nil, nil);
        [self.rendererContext dropVariablesDictionary:self.variables];
		if (resultDict)
		{
			errorCode = MIGetErrorCodeFromReplyDictionary(resultDict);
			if (errorCode != MIReplyErrorNoError)
			{
				self._setupDone = NO;
			}
		}
	}
	return resultDict;
}

-(NSDictionary *)cleanup
{
    NSDictionary *resultDict;
    self._setupDone = NO;
    if (self._cleanupDictionary)
    {
        [self.rendererContext appendVariables:self.variables];
        resultDict = MIMovingImagesHandleCommands(
                                                  self.rendererContext,
                                                  self._cleanupDictionary,
                                                  nil, nil);
        [self.rendererContext dropVariablesDictionary:self.variables];
    }
    self.variables = nil;
	self.isReady = NO;
    return resultDict;
}

-(NSDictionary *)performBackgroundCommandsOnQueue:(dispatch_queue_t)workQueue
                completionHandler:(MICommandCompletionHandler)completionHandler
{
    if (!self._backgroundCommandsDictionary)
    {
        return MIMakeReplyDictionary(
                            @"Error: Background commands dictionary unavailable",
                            MIReplyErrorMissingOption);
    }

    __block NSDictionary *resultDict;
    dispatch_async(workQueue, ^{
        MIReplyErrorEnum errorCode = MIReplyErrorNoError;
        if (self._backgroundCommandsDictionary)
        {
            [self.rendererContext appendVariables:self.variables];
            resultDict = MIMovingImagesHandleCommands(
                                          self.rendererContext,
                                          self._backgroundCommandsDictionary,
                                          nil, nil);
            [self.rendererContext dropVariablesDictionary:self.variables];
            if (resultDict)
            {
                errorCode = MIGetErrorCodeFromReplyDictionary(resultDict);
                self.isReady = (errorCode == MIReplyErrorNoError);
            }
        }
        if (completionHandler)
        {
            dispatch_sync(dispatch_get_main_queue(), ^{
                completionHandler(resultDict);
            });
                          
        }
    });
    
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

-(NSDictionary *)performMainthreadCommands
{
    NSDictionary *resultDict;
    if (!self._mainThreadCommandsDictionary)
    {
        resultDict = MIMakeReplyDictionary(
                            @"Error: Main thread draw dictionary not available",
                            MIReplyErrorMissingOption);
        return resultDict;
    }
	MIReplyErrorEnum errorCode = MIReplyErrorNoError;
	if (self._mainThreadCommandsDictionary)
	{
        [self.rendererContext appendVariables:self.variables];
		resultDict = MIMovingImagesHandleCommands(
										  self.rendererContext,
										  self._mainThreadCommandsDictionary,
										  nil, nil);
        [self.rendererContext dropVariablesDictionary:self.variables];
		if (resultDict)
		{
			errorCode = MIGetErrorCodeFromReplyDictionary(resultDict);
            self.isReady = (errorCode == MIReplyErrorNoError);
		}
	}
	return resultDict;
}

-(BOOL)drawIntoCGContext:(CGContextRef)context
{
    BOOL result;
    @autoreleasepool {
        MICGContext *theContext;
        theContext = [[MICGContext alloc] initWithCGContext:context
                                                  miContext:self.rendererContext
                                                      owner:self];
        [self.rendererContext appendVariables:self.variables];
        result = [theContext handleDictionaryDrawElementCommand:
                  self._drawingDictionary];
        [self.rendererContext dropVariablesDictionary:self.variables];
    }
    return result;
}

@end
