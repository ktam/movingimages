//  MIMovieEditor.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIAVFoundationUtilities.h"
#import "MIAVAssetTrackProviderInterface.h"
#import "MIBaseObject+Internal.h"
#import "MIBaseObject_Protected.h"
#import "MIBaseObjectPublicInterface.h"
#import "MICGImage.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIMovieEditor.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"

@import AVFoundation;

// it looks like we only need a video composition when it comes time to export.
// But that means we need a container for instructions.

@interface MIMovieEditor ()<AVVideoCompositionValidationHandling>

@property (strong) AVMutableComposition *_composition;
@property (strong) dispatch_queue_t _editQueue;

/// instructions array of AVMutableVideoCompositionInstruction.
@property (strong) NSMutableArray *_videoCompositionInstructions;

/// Audio mix instructions that contains an array of audio mix input parameters.
@property (strong) AVMutableAudioMix *_audioMix;

@property (readonly) BOOL _canExport;

/// Add a video composition insturction to the list of instructions.
-(void)_addToVideoCompositionInstructions:(AVVideoCompositionInstruction *)instr;

/// Add a video composition instruction.
-(NSDictionary *)_addVideoCompositionInstruction:(NSDictionary *)commandDict;

/// Get the audio mix parameter with the specified track id.
-(AVMutableAudioMixInputParameters *)_parameterWithTrackID:(CMPersistentTrackID)trackID;

/// Add an audio mix instruction.
-(void)_addToAudioMixParameters:(AVAudioMixInputParameters *)commandDict;

/// Add an audio mix instruction.
-(NSDictionary *)_addAudioMixInstruction:(NSDictionary *)commandDict;

/// Create a track.
-(NSDictionary *)_createTrack:(NSDictionary *)commandDict;

/// Get a property.
-(NSDictionary *)_getProperty:(NSDictionary *)commandDict;

/// Set a property.
-(NSDictionary *)_setProperty:(NSDictionary *)commandDict;

/// Insert an empty track segment with a specified time range.
-(NSDictionary *)_insertEmptyTrackSegment:(NSDictionary *)commandDict;

/// Insert a track segment, taking a source track, a time range & insertion time
-(NSDictionary *)_insertTrackSegment:(NSDictionary *)commandDict;

/// Export the composition as a movie.
-(NSDictionary *)_exportMovie:(NSDictionary *)commandDict;

/// Store an image of the AVComposition map to the image collection.
-(NSDictionary *)_assignCompositionMapToImageCollection:(NSDictionary *)commandDict;

/// Set a track property.
-(NSDictionary *)_setTrackProperty:(NSDictionary *)commandDict;

/// Get a property of a track.
-(NSDictionary *)_getTrackProperty:(NSDictionary *)commandDict;

/// Private designated initializer.
-(instancetype)initWithName:(NSString *)objectName
               editingQueue:(dispatch_queue_t)editingQueue
                  inContext:(MIContext *)context;

@end

#pragma Mark Local Helper functions

static dispatch_queue_t CreateMovieEditorQueue(NSString *queueName)
{
    dispatch_queue_t defaultQueue;
    defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_queue_t myQueue;
    myQueue = dispatch_queue_create(
                        [queueName cStringUsingEncoding:NSASCIIStringEncoding],
                                    DISPATCH_QUEUE_SERIAL);
    dispatch_set_target_queue(myQueue, defaultQueue);
    return myQueue;
}

static BOOL IsAllowedMediaType(NSString *mediaType)
{
    static NSArray *mediaTypeList;
    if (!mediaTypeList)
    {
        mediaTypeList = @[
                    MIJSONValueMovieMediaTypeAudio,
                    MIJSONValueMovieMediaTypeVideo
                        ];
    }
    return [mediaTypeList containsObject:mediaType];
}

@implementation MIMovieEditor

#pragma mark Public Class MIBaseObjectSemiPublicInterface methods

+(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
                              inContext:(MIContext *)context
{
    NSDictionary *replyDict = NULL;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    
    if (propertyKey == NULL)
    {
        MILog(@"Missing property", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Missing property",
                                          MIReplyErrorMissingOption);
    }
    else if (![propertyKey isKindOfClass:[NSString class]])
    {
        MILog(@"Invalid property key", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Invalid property key",
                                          MIReplyErrorMissingOption);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyPresets])
    {
        NSArray *exportTypes;
        exportTypes = [AVAssetExportSession allExportPresets];
        NSString *resultStr = MIUtilityCreateStringFromArrayOfStrings(@" ",
                                                                      exportTypes);
        replyDict = MIMakeReplyDictionary(resultStr, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyNumberOfObjects])
    {
        NSInteger numObjects = [context numberOfObjectsOfType:MIMovieEditorKey];
        NSNumber *numObjectsRef = [[NSNumber alloc] initWithInteger:numObjects];
        NSString *result = [[NSString alloc] initWithFormat:@"%ld",
                            (long)numObjects];
        replyDict = MIMakeReplyDictionaryWithNumericValue(result,
                                                          MIReplyErrorNoError,
                                                          numObjectsRef);
    }
    else
    {
        MILog(@"Unknown property", commandDict);
        replyDict = MIMakeReplyDictionary(@"Error: Unknown property",
                                          MIReplyErrorOptionValueInvalid);
    }
    return replyDict;
}

+(NSDictionary *)handleMakeObjectCommand:(NSDictionary *)commandDict
                               inContext:(MIContext *)context
{
    NSDictionary *replyDict;
    NSString *objectName = commandDict[MIJSONKeyObjectName];
    MIMovieEditor *editor = [[MIMovieEditor alloc] initWithName:objectName
                                                      inContext:context];
    if (editor)
    {
        MIBaseReference objectReference = [editor reference];
        NSString *refString = [[NSString alloc] initWithFormat:@"%ld",
                               (long)objectReference];
        replyDict = MIMakeReplyDictionaryWithNumericValue(refString,
                                                          MIReplyErrorNoError,
                                                          @(objectReference));
    }
    else
    {
        MILog(@"Failed to create movie editor", commandDict);
        NSString *message = @"Error: Failed to create movie editor";
        replyDict = MIMakeReplyDictionary(message,
                                          MIReplyErrorOptionValueInvalid);
    }

    return replyDict;
}

+(NSDictionary *)handleCloseAllObjectsCommand:(NSDictionary *)commandDict
                                    inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    [context removeAllObjectsWithType:MIMovieEditorKey];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Private Methods

-(instancetype)initWithName:(NSString *)objectName
               editingQueue:(dispatch_queue_t)editingQueue
                  inContext:(MIContext *)context
{
    context = context ? context : [MIContext defaultContext];
    self = [super initWithBaseObjectType:MIMovieEditorKey
                                    name:objectName
                               inContext:context];
    if (!self)
    {
        return self;
    }

    self._editQueue = editingQueue;
    self._composition = [AVMutableComposition composition];
    return self;
}

-(NSDictionary *)_createTrack:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *mediaType = commandDict[MIJSONPropertyMovieMediaType];
    NSNumber *trackID = commandDict[MIJSONPropertyMovieTrackID];
    AVMutableCompositionTrack *track;
    NSString *message;
    MIContext *theContext = self.miContext;
    NSDictionary *variables = theContext ? theContext.variables : nil;

    if (mediaType && IsAllowedMediaType(mediaType))
    {
        CMPersistentTrackID pTrackid = kCMPersistentTrackID_Invalid;
        if (trackID)
        {
            pTrackid = trackID.intValue;
        }
        track = [self._composition addMutableTrackWithMediaType:mediaType
                                               preferredTrackID:pTrackid];
        if (track)
        {
            if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
            {
                id transform = commandDict[MIJSONKeyContextTransformation];
                if (!transform)
                {
                    transform = commandDict[MIJSONKeyAffineTransform];
                }
                CGAffineTransform aT;
                if (MIMakeCGAffineTransform(transform, &aT, variables))
                {
                    track.preferredTransform = aT;
                }
            }
            else if ([track hasMediaCharacteristic:AVMediaCharacteristicAudible])
            {
                CGFloat preferredVol;
                if (MIUtilityGetConstrainedCGFloatFromDictionary(commandDict,
                                    MIJSONKeyMovieTrackPreferredVolume,
                                    0.0, 1.0, &preferredVol, variables))
                {
                    track.preferredVolume = preferredVol;
                }
            }
            pTrackid = track.trackID;
            message = [[NSString alloc] initWithFormat:@"%d", track.trackID];
            result = MIMakeReplyDictionaryWithNumericValue(message,
                                                           MIReplyErrorNoError,
                                                           @(pTrackid));
        }
        else
        {
            message = @"Error: Failed to create track";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    else
    {
        if (mediaType)
        {
            message = @"Error: Invalid media type to create a track";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }
        else
        {
            message = @"Error: Media type missing when creating a track";
            MILog(message, commandDict);
            result = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        }
    }
    
    return result;
}

-(BOOL)_canExport
{
    if (!self._composition.tracks)
        return NO;
    
    if (self._composition.tracks.count == 0)
        return NO;
    
    if (!self._videoCompositionInstructions)
        return NO;
    
    if (self._videoCompositionInstructions.count == 0)
        return NO;
    
    return YES;
}

-(void)_addToAudioMixParameters:(AVMutableAudioMixInputParameters *)parameter
{
    if (!self._audioMix)
    {
        self._audioMix = [AVMutableAudioMix audioMix];
    }
    
    if (!self._audioMix.inputParameters)
    {
        self._audioMix.inputParameters = @[parameter];
    }
    else
    {
        if ([self _parameterWithTrackID:parameter.trackID] == nil)
        {
            NSMutableArray *parameters = self._audioMix.inputParameters.mutableCopy;
            [parameters addObject:parameter];
            self._audioMix.inputParameters = parameters.copy;
        }
    }
}

-(AVMutableAudioMixInputParameters *)_parameterWithTrackID:(CMPersistentTrackID)trackID
{
    if (!self._audioMix)
    {
        return nil;
    }
    
    NSArray *parameters = self._audioMix.inputParameters;
    if (!parameters)
    {
        return nil;
    }
    
    for (AVMutableAudioMixInputParameters *param in parameters)
    {
        if (param.trackID == trackID)
        {
            return param;
        }
    }
    return nil;
}

-(NSDictionary *)_addAudioMixInstruction:(NSDictionary *)commandDict
{
    NSString *message;

    AVAssetTrack *track;
    track = MIAVGetTrackFromAVAssetWithDictionary(self._composition,
                                            commandDict[MIJSONPropertyMovieTrack]);
    if (!track)
    {
        message = @"Error: Could not obtain an audio track.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
    }
    
    if (![track.mediaType isEqualToString:AVMediaTypeAudio])
    {
        message = @"Error: Required track needs to be an audio track.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
    }
    
    NSString *type;
    type = commandDict[MIJSONKeyMovieEditorAudioInstruction];
    
    if (!(type && [type isKindOfClass:[NSString class]]))
    {
        message = @"Error: Could not get the audio instruction type.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
    }

    MIContext *theContext = self.miContext;
    NSDictionary *vars = theContext.variables;
    
    if ([type isEqualToString:MIJSONValueMovieEditorVolumeRampInstruction])
    {
        CGFloat startVolume, endVolume;
        if (MIUtilityGetConstrainedCGFloatFromDictionary(commandDict,
                                         MIJSONPropertyMovieEditorStartRampValue,
                                         0.0, // minimum value
                                         1.0, // maximum value
                                         &startVolume,
                                         vars) &&
            MIUtilityGetConstrainedCGFloatFromDictionary(commandDict,
                                         MIJSONPropertyMovieEditorEndRampValue,
                                         0.0, // minimum value
                                         1.0, // maximum value
                                         &endVolume,
                                         vars))
        {
            // Get the audio mix instruction time range, if there is no layer
            // instruction time range then use the instruction time range.
            NSDictionary *timeRangeD;
            timeRangeD = commandDict[MIJSONPropertyMovieTimeRange];
            CMTimeRange volumeRampTimeRange;
            if (MIAVMakeACMTimeRangeFromDictionary(theContext, timeRangeD,
                                               6000, &volumeRampTimeRange))
            {
                AVMutableAudioMixInputParameters *parameter;
                parameter = [self _parameterWithTrackID:track.trackID];
                if (parameter == nil)
                {
                    parameter = [AVMutableAudioMixInputParameters
                                 audioMixInputParametersWithTrack:track];
                }
                [parameter setVolumeRampFromStartVolume:(float)startVolume
                                            toEndVolume:(float)endVolume
                                              timeRange:volumeRampTimeRange];
                [self _addToAudioMixParameters:parameter];
            }
            else
            {
                message = @"Error: Could not get audio volume ramp time range.";
                MILog(message, commandDict);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else
        {
            message = @"Error: Invalid or missing start/end volume.";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        }

    }
    else if ([type isEqualToString:MIJSONValueMovieEditorVolumeInstruction])
    {
        CGFloat volume;
        if (MIUtilityGetConstrainedCGFloatFromDictionary(commandDict,
                                         MIJSONPropertyMovieEditorInstructionValue,
                                         0.0, // minimum value
                                         1.0, // maximum value
                                         &volume,
                                         vars))
        {
            NSDictionary *timeD;
            timeD = commandDict[MIJSONPropertyMovieTime];
            CMTime volumeTime;
            if (MIAVMakeACMTimeFromDictionary(theContext, timeD, 6000, &volumeTime))
            {
                AVMutableAudioMixInputParameters *parameter;
                parameter = [self _parameterWithTrackID:track.trackID];
                if (parameter == nil)
                {
                    parameter = [AVMutableAudioMixInputParameters
                                 audioMixInputParametersWithTrack:track];
                }
                [parameter setVolume:(float)volume atTime:volumeTime];
                [self _addToAudioMixParameters:parameter];
            }
            else
            {
                message = @"Error: Could not get audio volume time.";
                MILog(message, commandDict);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else
        {
            message = @"Error: Invalid or missing volume.";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        }
    }
    else
    {
        message = @"Error: Unknown audio mix type.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
    }
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

-(void)_addToVideoCompositionInstructions:(AVVideoCompositionInstruction *)instr
{
    if (!self._videoCompositionInstructions)
    {
        self._videoCompositionInstructions = [[NSMutableArray alloc]
                                              initWithCapacity:0];
    }
    [self._videoCompositionInstructions addObject:instr];
}

-(NSDictionary *)_addVideoCompositionInstruction:(NSDictionary *)commandDict
{
    // The target track for the instruction needs to exist prior to
    // adding the instruction.
    NSString *message;
    
    NSDictionary *timeRangeD = commandDict[MIJSONPropertyMovieTimeRange];
    CMTimeRange transitionTimeRange;
    if (!MIAVMakeACMTimeRangeFromDictionary(self.miContext, timeRangeD, 6000,
                                           &transitionTimeRange))
    {
        message = @"Error: Invalid time range for instruction";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
    }
    
    NSArray *layerInstA;
    layerInstA = commandDict[MIJSONPropertyMovieEditorLayerInstructions];
    if (!(layerInstA && [layerInstA isKindOfClass:[NSArray class]]))
    {
        message = @"Error: Invalid or missing instructions array";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
    }

    NSMutableArray *layerInstructions;
    layerInstructions = [[NSMutableArray alloc] initWithCapacity:0];

    for (NSDictionary *layerInstruction in layerInstA)
    {
        if (![layerInstruction isKindOfClass:[NSDictionary class]])
        {
            message = @"Error: Layer instruction is not a dictionary.";
            MILog(message, layerInstruction);
            return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }

        AVAssetTrack *track;
        track = MIAVGetTrackFromAVAssetWithDictionary(self._composition,
                                layerInstruction[MIJSONPropertyMovieTrack]);
        if (!track)
        {
            message = @"Error: Could not obtain a layer instruction track.";
            MILog(message, layerInstruction);
            return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }

        if (![track.mediaType isEqualToString:AVMediaTypeVideo])
        {
            message = @"Error: Require a video track.";
            MILog(message, layerInstruction);
            return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }
        
        // Need to set layer instruction, it will be one of transform ramp,
        // opacity ramp, crop rectangle ramp.
        
        NSString *type;
        type = layerInstruction[MIJSONKeyMovieEditorLayerInstructionType];
        if (!(type && [type isKindOfClass:[NSString class]]))
        {
            message = @"Error: Could not get the type of layer instruction.";
            MILog(message, layerInstruction);
            return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        }

        AVMutableVideoCompositionLayerInstruction *avcli;
        avcli = [AVMutableVideoCompositionLayerInstruction
                 videoCompositionLayerInstructionWithAssetTrack:track];

        MIContext *theContext = self.miContext;
        NSDictionary *vars = theContext.variables;
        
        if ([type isEqualToString:MIJSONValueMovieEditorTransformRampInstruction])
        {
            id startVal, endVal;
            startVal = layerInstruction[MIJSONPropertyMovieEditorStartRampValue];
            endVal = layerInstruction[MIJSONPropertyMovieEditorEndRampValue];
            CGAffineTransform startTransform;
            CGAffineTransform endTransform;
            
            if (MIMakeCGAffineTransform(startVal, &startTransform, vars) &&
                MIMakeCGAffineTransform(endVal, &endTransform, vars))
            {
                // Get the layer instruction time range, if there is no layer
                // instruction time range then use the instruction time range.
                NSDictionary *timeRangeD;
                timeRangeD = commandDict[MIJSONPropertyMovieTimeRange];
                CMTimeRange layerTimeRange = transitionTimeRange;
                MIAVMakeACMTimeRangeFromDictionary(theContext, timeRangeD,
                                                   6000, &layerTimeRange);
                [avcli setTransformRampFromStartTransform:startTransform
                                           toEndTransform:endTransform
                                                timeRange:layerTimeRange];
            }
            else
            {
                message = @"Error: Invalid or missing start/end transform dict.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if ([type isEqualToString:
                                MIJSONValueMovieEditorOpacityRampInstruction])
        {
            CGFloat startOpacity, endOpacity;
            if (MIUtilityGetCGFloatFromDictionary(layerInstruction,
                                        MIJSONPropertyMovieEditorStartRampValue,
                                        &startOpacity, vars) &&
                MIUtilityGetCGFloatFromDictionary(layerInstruction,
                                        MIJSONPropertyMovieEditorEndRampValue,
                                        &endOpacity, vars))
            {
                // Get the layer instruction time range, if there is no layer
                // instruction time range then use the instruction time range.
                NSDictionary *timeRangeD;
                timeRangeD = commandDict[MIJSONPropertyMovieTimeRange];
                CMTimeRange layerTimeRange = transitionTimeRange;
                MIAVMakeACMTimeRangeFromDictionary(theContext, timeRangeD,
                                                   6000, &layerTimeRange);
                [avcli setOpacityRampFromStartOpacity:startOpacity
                                         toEndOpacity:endOpacity
                                            timeRange:layerTimeRange];
            }
            else
            {
                message = @"Error: Invalid or missing start/end opacity.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if ([type isEqualToString:MIJSONValueMovieEditorCropRampInstruction])
        {
            NSDictionary *startRectD, *endRectD;
            startRectD = layerInstruction[MIJSONPropertyMovieEditorStartRampValue];
            endRectD = layerInstruction[MIJSONPropertyMovieEditorEndRampValue];
            
            CGRect startRect, endRect;
            if (MIUtilityGetCGRectFromDictionary(startRectD, &startRect,
                                                 kMILog, vars) &&
                MIUtilityGetCGRectFromDictionary(endRectD, &endRect, kMILog, vars))
            {
                // Get the layer instruction time range, if there is no layer
                // instruction time range then use the instruction time range.
                NSDictionary *timeRangeD;
                timeRangeD = commandDict[MIJSONPropertyMovieTimeRange];
                CMTimeRange layerTimeRange = transitionTimeRange;
                MIAVMakeACMTimeRangeFromDictionary(theContext, timeRangeD,
                                                   6000, &layerTimeRange);
                [avcli setCropRectangleRampFromStartCropRectangle:startRect
                                               toEndCropRectangle:endRect
                                                        timeRange:layerTimeRange];
            }
            else
            {
                message = @"Error: Invalid or missing start/end opacity.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if ([type isEqualToString:MIJSONValueMovieEditorTransformInstruction])
        {
            id value;
            value = layerInstruction[MIJSONPropertyMovieEditorInstructionValue];
            CGAffineTransform transform;
            CMTime instructionTime = transitionTimeRange.start;

            if (MIMakeCGAffineTransform(value, &transform, vars))
            {
                MIAVMakeACMTimeFromDictionary(theContext,
                                        layerInstruction[MIJSONPropertyMovieTime],
                                        6000, &instructionTime);
                [avcli setTransform:transform atTime:instructionTime];
            }
            else
            {
                message = @"Error: Invalid or missing transform value.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if ([type isEqualToString:MIJSONValueMovieEditorOpacityInstruction])
        {
            CMTime instructionTime = transitionTimeRange.start;
            CGFloat opacity;
            if (MIUtilityGetCGFloatFromDictionary(layerInstruction,
                    MIJSONPropertyMovieEditorInstructionValue, &opacity, vars))
            {
                MIAVMakeACMTimeFromDictionary(theContext,
                                    layerInstruction[MIJSONPropertyMovieTime],
                                    6000, &instructionTime);
                [avcli setOpacity:opacity atTime:instructionTime];
            }
            else
            {
                message = @"Error: Invalid or missing value or apply time.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if ([type isEqualToString:MIJSONValueMovieEditorCropInstruction])
        {
            NSDictionary *rectD;
            rectD = layerInstruction[MIJSONPropertyMovieEditorInstructionValue];
            
            CMTime instructionTime = transitionTimeRange.start;
            CGRect cropRect;
            if (MIUtilityGetCGRectFromDictionary(rectD, &cropRect,
                                                 kMILog, vars) &&
                MIAVMakeACMTimeFromDictionary(theContext,
                                    layerInstruction[MIJSONPropertyMovieTime],
                                    6000, &instructionTime))
            {
                [avcli setCropRectangle:cropRect atTime:instructionTime];
            }
            else
            {
                message = @"Error: Invalid or missing start/end opacity.";
                MILog(message, layerInstruction);
                return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
            }
        }
        else if (![type isEqualToString:MIJSONValueMovieEditorPassthruInstruction])
        {
            message = @"Error: Unknown layer instruction type.";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }

        [layerInstructions addObject:avcli];
    }
    AVMutableVideoCompositionInstruction *instruction;
    instruction = [AVMutableVideoCompositionInstruction new];
    instruction.timeRange = transitionTimeRange;
    instruction.layerInstructions = layerInstructions;
    [self _addToVideoCompositionInstructions:instruction.copy];
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

-(NSDictionary *)_getTrackProperty:(NSDictionary *)commandDict
{
    AVAssetTrack *track;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    track = MIAVGetTrackFromAVAssetWithDictionary(self._composition, trackDict);
    return MIAVGetMovieTrackProperty(track, commandDict);
}

-(NSDictionary *)_setTrackProperty:(NSDictionary *)commandDict
{
    AVMutableCompositionTrack *track;
    NSString *message;
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    id propertyValue = commandDict[MIJSONPropertyValue];
    if (!propertyKey)
    {
        message = @"Error: Missing property key for set property";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingProperty);
    }

    if (!propertyValue)
    {
        message = @"Error: Missing property value for set property";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingProperty);
    }

    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    track = (AVMutableCompositionTrack *)MIAVGetTrackFromAVAssetWithDictionary(
                                                self._composition, trackDict);
    if (track)
    {
        MIContext *context = self.miContext;
        NSDictionary *variables = context ? context.variables : nil;
        if ([track hasMediaCharacteristic:AVMediaCharacteristicVisual])
        {
            if ([propertyKey isEqualToString:MIJSONKeyContextTransformation] ||
                [propertyKey isEqualToString:MIJSONKeyAffineTransform])
            {
                CGAffineTransform aT;
                if (MIMakeCGAffineTransform(propertyValue, &aT, variables))
                {
                    track.preferredTransform = aT;
                    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                }
                else
                {
                    message = @"Error: Failed to make a CGAffineTransform";
                    MILog(message, commandDict);
                    return MIMakeReplyDictionary(message,
                                                 MIReplyErrorInvalidProperty);
                }
            }
            else
            {
                message = @"Error: Unknown visual track property to be set";
                MILog(message, commandDict);
                return MIMakeReplyDictionary(message, MIReplyErrorUnknownProperty);
            }
        }
        else if ([track hasMediaCharacteristic:AVMediaCharacteristicAudible])
        {
            if ([propertyKey isEqualToString:MIJSONKeyMovieTrackPreferredVolume])
            {
                CGFloat preferredVol;
                if (MIUtilityGetCGFloatFromID(propertyValue, &preferredVol,
                                              variables))
                {
                    track.preferredVolume = preferredVol;
                    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
                }
                else
                {
                    message = @"Error: Couldn't convert to preferred volume.";
                    MILog(message, commandDict);
                    return MIMakeReplyDictionary(message,
                                                 MIReplyErrorInvalidProperty);
                }
            }
            else
            {
                message = @"Error: Unknown audio track property to be set.";
                MILog(message, commandDict);
                return MIMakeReplyDictionary(message, MIReplyErrorInvalidProperty);
            }
        }
        else
        {
            message = @"Error: Tracks media character is not visible or audible";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
        }
    }
    else
    {
        message = @"Error: Failed to obtain a track to assign property.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidProperty);
    }
}

-(NSDictionary *)_getTrackProperties:(NSDictionary *)commandDict
{
    AVAssetTrack *track;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    track = MIAVGetTrackFromAVAssetWithDictionary(self._composition, trackDict);
    if (!track)
    {
        MILog(@"Failed to get a track to get properties from",
                             commandDict);
        return MIMakeReplyDictionary(
                        @"Error: Failed to get a track to get properties from.",
                                     MIReplyErrorOperationFailed);
    }
    NSDictionary *propertiesDict = MIAVGetTrackProperties(track, commandDict);
    // default return to reply dict;
    return MIUtilitySaveDictionaryToDestination(commandDict,
                                                  propertiesDict, NO);
}

-(NSDictionary *)_getProperty:(NSDictionary *)commandDict
{
    // First check to see if we are requesting a property of a track.
    // If there is a property of the command dict that is a track specification
    // then assume we are wanting a track property.
    
    if (commandDict[MIJSONPropertyMovieTrack])
    {
        return [self _getTrackProperty:commandDict];
    }
    
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    if ([propertyKey isEqualToString:MIJSONPropertyMovieExportCompatiblePresets])
    {
        NSString *message;
        NSArray *presets;
        presets = [AVAssetExportSession exportPresetsCompatibleWithAsset:
                   self._composition];
        if (presets)
        {
            message = MIUtilityCreateStringFromArrayOfStrings(@" ", presets);
            return MIMakeReplyDictionary(message, MIReplyErrorNoError);
        }
        else
        {
            message = @"Error: Failed to get list of compatible export presets";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieExportTypes])
    {
        NSString *message;
        NSString *exportPreset = commandDict[MIJSONPropertyMovieExportPreset];
        AVAssetExportSession *session;
        session = [[AVAssetExportSession alloc] initWithAsset:self._composition
                                                   presetName:exportPreset];
        if (!session)
        {
            message = @"Error: Failed to create export session to get file types";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
        NSArray *exportTypes = [session supportedFileTypes];
        message = MIUtilityCreateStringFromArrayOfStrings(@" ", exportTypes);
        return MIMakeReplyDictionary(message, MIReplyErrorNoError);
    }
    else if ([propertyKey isEqualToString:MIJSONPropertyMovieNaturalSize])
    {
        CGSize naturalSize = self._composition.naturalSize;
        
        NSDictionary *dict = MIUtilityCreateSizeDictionaryFromCGSize(naturalSize);
        return MIMakeReplyDictionaryWithDictionaryValue(dict, YES);
    }

    NSDictionary *replyDict;
    replyDict = MIAVGetMovieAssetProperty(self._composition, commandDict);
    if (!replyDict)
    {
        NSString *message;
        message = @"Error: Unknown property for a movie editor";
        MILog(message, commandDict);
        replyDict =  MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    return replyDict;
}

-(NSDictionary *)_setProperty:(NSDictionary *)commandDict
{
    if (commandDict[MIJSONPropertyMovieTrack])
    {
        return [self _setTrackProperty:commandDict];
    }
    
    NSString *message;
    
    NSString *propertyKey = commandDict[MIJSONPropertyKey];
    NSString *propertyValue = commandDict[MIJSONPropertyValue];
    MIContext *theContext = self.miContext;
    
    if ([propertyKey isEqualToString:MIJSONPropertyMovieNaturalSize])
    {
        CGSize naturalSize;
        if (MIUtilityGetCGSizeFromPropertyValue(propertyValue, &naturalSize,
                                                theContext.variables))
        {
            self._composition.naturalSize = naturalSize;
            return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
        else
        {
            message = @"Error: Could not convert property value into a size";
            MILog(message, commandDict);
            return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        }
    }
    else
    {
        message = @"Error: Unknown property.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorUnknownProperty);
    }
}

-(NSDictionary *)_insertEmptyTrackSegment:(NSDictionary *)commandDict
{
    NSString *message;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    AVMutableCompositionTrack *track;
    track = (AVMutableCompositionTrack *)MIAVGetTrackFromAVAssetWithDictionary(
                                                  self._composition, trackDict);

    if (!track)
    {
        message = @"Error: Couldn't get the track to add empty segment to.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }

    NSDictionary *timeRange = commandDict[MIJSONPropertyMovieTimeRange];
    CMTimeRange segmentTimeRange;
    if (MIAVMakeACMTimeRangeFromDictionary(self.miContext, timeRange, 90000,
                                           &segmentTimeRange))
    {
        [track insertEmptyTimeRange:segmentTimeRange];
        return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    else
    {
        message = @"Error: Couldn't get add empty segment time range.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
}

-(NSDictionary *)_insertTrackSegment:(NSDictionary *)commandDict
{
    NSString *message;
    NSDictionary *trackDict = commandDict[MIJSONPropertyMovieTrack];
    AVMutableCompositionTrack *track;
    track = (AVMutableCompositionTrack *)MIAVGetTrackFromAVAssetWithDictionary(
                                                self._composition, trackDict);
    
    if (!track)
    {
        message = @"Error: Couldn't get the track to add segment to.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    // Get the source object for the track.
    NSDictionary *sourceObjD = commandDict[MIJSONKeySourceObject];
    MIContext *theContext = self.miContext;
    id <MIAVAssetTrackProviderInterface> trackProvider;
    MIBaseObject *obj = MIGetObjectFromDictionary(theContext, sourceObjD);
    if (obj && [obj conformsToProtocol:@protocol(MIAVAssetTrackProviderInterface)])
        trackProvider = (id <MIAVAssetTrackProviderInterface>)obj;

    if (!trackProvider)
    {
        message = @"Error: Could not obtain the source object.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    NSDictionary *timeRangeD = commandDict[MIJSONPropertyMovieSourceTimeRange];
    CMTimeRange segmentTimeRange;
    if (!MIAVMakeACMTimeRangeFromDictionary(theContext, timeRangeD, 90000,
                                           &segmentTimeRange))
    {
        message = @"Error: Missing or invalid source time range.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }

    NSDictionary *timeD = commandDict[MIJSONPropertyMovieInsertionTime];
    CMTime insertionTime;
    if (!MIAVMakeACMTimeFromDictionary(theContext, timeD, 90000, &insertionTime))
    {
        message = @"Error: Missing or invalid insertion time.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    NSDictionary *sourceTrackD = commandDict[MIJSONPropertyMovieSourceTrack];    
    AVAssetTrack *sourceTrack = [trackProvider trackWithIdentifier:sourceTrackD];
    if (!sourceTrack)
    {
        message = @"Error: Could not obtain the source track.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorInvalidOption);
    }

    BOOL insertionResult;
    NSError *__autoreleasing theError;
    insertionResult = [track insertTimeRange:segmentTimeRange
                                     ofTrack:sourceTrack
                                      atTime:insertionTime
                                       error:&theError];
    if (!insertionResult)
    {
        if (theError)
        {
            message = [[NSString alloc] initWithFormat:
                       @"%@ failure reason: %@ failure description: %@",
                       @"Error: Failed to insert track segment time range",
                       theError.localizedFailureReason,
                       theError.localizedDescription];
        }
        else
        {
            message = @"Error: Failed to insert track segment time range.";
        }
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    else
    {
        return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
}

-(NSDictionary *)_assignCompositionMapToImageCollection:(NSDictionary *)commandDict
{
    NSDictionary *result;
    NSString *ident = commandDict[MIJSONPropertyImageIdentifier];
    if (!(ident && [ident isKindOfClass:[NSString class]]))
    {
        NSString *message = @"Error: Command does not contain an image identifier";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    AVMutableVideoComposition *videoComposition;
    videoComposition = [AVMutableVideoComposition
                videoCompositionWithPropertiesOfAsset:self._composition];

    if (!videoComposition)
    {
        NSString *message = @"Error: Could not create the video composition";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
        
    videoComposition.instructions = self._videoCompositionInstructions;

    CGImageRef imageCG = MIAVCreateImageOfCompositionMap(self._composition,
                                                videoComposition, self._audioMix);
    // CGImageRef imageCG = MIAVCreateImageOfCompositionMap(self._composition,
    //                                                     videoComposition, nil);
    MICGImage *image = [[MICGImage alloc] initWithCGImage:imageCG];
    CGImageRelease(imageCG);
    
    MIContext *context = self.miContext;
    if (image && context)
    {
        [context assignImage:image identifer:ident];
        result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    }
    else
    {
        NSString *message;
        message = image ? @"Error: No context" : @"Error: No image";
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    return result;
}

-(NSDictionary *)_exportMovie:(NSDictionary *)commandDict
{
    NSString *message;
    NSDictionary *result;
    
    if (!self._canExport)
    {
        message = @"Error: Movie editor not ready to export";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }

    MIContext *context = self.miContext;
    context = context ? context : [MIContext defaultContext];
    
    NSString *exportFileType = commandDict[MIJSONPropertyFileType];
    NSString *exportPreset = commandDict[MIJSONPropertyMovieExportPreset];
    NSString *exportPath = MIUtilityGetFilePathFromCommandAndVariables(
                                                commandDict, context.variables);
    NSURL *exportURL;
    
    if (!(exportFileType && [exportFileType isKindOfClass:[NSString class]]))
    {
        message = @"Error: Movie export file type missing or not a string";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
    }

    if (!(exportPreset && [exportPreset isKindOfClass:[NSString class]]))
    {
        message = @"Error: Movie export preset missing or not a string";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
    }

    if (exportPath)
    {
        exportURL = [NSURL fileURLWithPath:exportPath isDirectory:NO];
    }

    if (!exportURL)
    {
        message = @"Error: Movie export path missing or not a string";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
    }

    AVAssetExportSession *exportSession;
    AVComposition *composition = self._composition.copy;
    exportSession = [[AVAssetExportSession alloc] initWithAsset:composition
                                                     presetName:exportPreset];
    
    if (!exportSession)
    {
        message = @"Error: Could not create a movie editor export session.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }
    
    AVMutableVideoComposition *videoComposition =
                                    [AVMutableVideoComposition videoComposition];

    AVAssetTrack *track = [composition 
                            tracksWithMediaType:AVMediaTypeVideo].firstObject;

    videoComposition = [AVMutableVideoComposition videoComposition];
    videoComposition.instructions = self._videoCompositionInstructions;
    videoComposition.renderSize = track.naturalSize;
    videoComposition.frameDuration = CMTimeMake(1, 30);

    [videoComposition isValidForAsset:composition
                            timeRange:CMTimeRangeMake(kCMTimeZero,
                                                      kCMTimePositiveInfinity)
                   validationDelegate:self];


    if (![videoComposition isValidForAsset:composition
        timeRange:CMTimeRangeMake(kCMTimeZero, kCMTimePositiveInfinity)
                        validationDelegate:self])
    {
        message = @"Error: Could not validate video composition.";
        MILog(message, commandDict);
        return MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
    }

    videoComposition = [AVMutableVideoComposition
                        videoCompositionWithPropertiesOfAsset:self._composition];
    videoComposition.instructions = self._videoCompositionInstructions;
    NSException *localException;
    @try
    {
        NSFileManager *fm = [NSFileManager defaultManager];
        if ([fm fileExistsAtPath:exportPath])
        {
            [fm removeItemAtPath:exportPath error:nil];
        }

        exportSession.outputFileType = exportFileType;
        exportSession.videoComposition = videoComposition.copy;
        exportSession.outputURL = exportURL;
        if (self._audioMix)
        {
            exportSession.audioMix = self._audioMix;
        }
        dispatch_semaphore_t sessionWaitSemaphore = dispatch_semaphore_create(0);
        [exportSession exportAsynchronouslyWithCompletionHandler:^
        {
             dispatch_semaphore_signal(sessionWaitSemaphore);
        }];
        dispatch_semaphore_wait(sessionWaitSemaphore, DISPATCH_TIME_FOREVER);
    }
    @catch (NSException *exception) {
        localException = exception;
    }

    if (localException)
    {
        message = [[NSString alloc] initWithFormat:
                   @"%@ name: %@, reason: %@",
                   @"Error: Exporting movie failed.",
                   localException.name, localException.reason];
        MILog(message, commandDict);
        result = MIMakeReplyDictionary(message,
                                       MIReplyErrorOperationFailed);
    }
    else
    {
        AVAssetExportSessionStatus status = exportSession.status;
        if (status == AVAssetExportSessionStatusCancelled ||
            status == AVAssetExportSessionStatusFailed)
        {
            NSError *error = exportSession.error;
            if (error)
            {
                message = [[NSString alloc] initWithFormat:
                           @"%@ description: %@, reason: %@",
                           @"Error: Exporting movie failed.",
                           error.localizedDescription,
                           error.localizedFailureReason];
                MILog(message, commandDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorOperationFailed);
            }
            else
            {
                message = @"Error: Exporting movie failed.";
                MILog(message, commandDict);
                result = MIMakeReplyDictionary(message,
                                               MIReplyErrorOperationFailed);
            }
        }
        else
        {
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
    }
    return result;
}

#pragma mark Public Methods

-(instancetype)initWithName:(NSString *)objectName inContext:(MIContext *)context
{
    NSString *queueName = [NSString stringWithFormat:
                           @"com.zukini.mimovieeditor.%p", self];
    
    return [self initWithName:objectName
                 editingQueue:CreateMovieEditorQueue(queueName)
                    inContext:context];
}

-(void)close
{
    [super baseObjectClose];
}

#pragma mark Implementing MIBaseObjectSemiPublicInterface protocol public methods

-(NSDictionary *)handleGetPropertyCommand:(NSDictionary *)commandDict
{
    NSString *propertyType = commandDict[MIJSONPropertyKey];
    if (!propertyType)
    {
        MILog(@"Missing property type.", commandDict);
        return MIMakeReplyDictionary(@"Error: Missing property type",
                                     MIReplyErrorMissingProperty);
    }
    
    if (![propertyType isKindOfClass:[NSString class]])
    {
        MILog(@"Property type is not a string.", commandDict);
        return MIMakeReplyDictionary(@"Error: Property type is not a string",
                                     MIReplyErrorMissingProperty);
    }

    NSDictionary *resultDict;
    resultDict = [super handleCommonGetPropertyCommand:commandDict];
    if (resultDict)
    {
        return resultDict;
    }

    __block NSDictionary *result;
    dispatch_sync(self._editQueue, ^
    {
        result = [self _getProperty:commandDict];
    });
    return result;
}

-(NSDictionary *)handleSetPropertyCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *result;
    dispatch_sync(self._editQueue, ^
    {
        // Currently only have track properties to set.
        result = [self _setProperty:commandDict];
    });
    return result;
}

-(NSDictionary *)handleGetPropertiesCommand:(NSDictionary *)commandDict
{
    NSString *errorString;
    NSDictionary *replyDict;
    errorString = MIUtilityDoesCommandDictContainDestination(commandDict);
    if (errorString)
    {
        MILog(errorString, commandDict);
        NSString *message;
        message = [[NSString alloc] initWithFormat:@"Error: %@", errorString];
        replyDict = MIMakeReplyDictionary(message, MIReplyErrorMissingOption);
        return replyDict;
    }
    
    // First check to see if we are requesting a property of a track.
    // If there is a property of the command dict that is a track specification
    // then assume we are wanting a track property.
    
    __block NSDictionary *result;
    dispatch_sync(self._editQueue, ^
    {
        if (commandDict[MIJSONPropertyMovieTrack])
        {
            result = [self _getTrackProperties:commandDict];
        }
        else
        {
            NSDictionary *propDict;
            propDict = MIAVGetMovieAssetProperties(self, self._composition,
                                                   commandDict);
            result = MIUtilitySaveDictionaryToDestination(commandDict,
                                                            propDict, NO);
        }
    });
    
    return result;
}

-(NSDictionary *)handleAddInstruction:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^
    {
        resultDict = [self _addVideoCompositionInstruction:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleAddAudioMixInstruction:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^
    {
        resultDict = [self _addAudioMixInstruction:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleCloseCommand:(NSDictionary *)commandDict
{
    NSDictionary *result;
    [self close];
    result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
    return result;
}

-(NSDictionary *)handleCreateTrackCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^
    {
        resultDict = [self _createTrack:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleInsertEmptyTrackSegment:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^
    {
        resultDict = [self _insertEmptyTrackSegment:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleInsertTrackSegment:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^
    {
        resultDict = [self _insertTrackSegment:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleAssignImageToCollectionCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^{
        resultDict = [self _assignCompositionMapToImageCollection:commandDict];
    });
    return resultDict;
}

-(NSDictionary *)handleExportCommand:(NSDictionary *)commandDict
{
    __block NSDictionary *resultDict;
    dispatch_sync(self._editQueue, ^{
        resultDict = [self _exportMovie:commandDict];
    });
    return resultDict;
}

#pragma mark Implementing MIBaseObjectSemiPublicInterface protocol public methods

- (BOOL)videoComposition:(AVVideoComposition *)videoComposition
        shouldContinueValidatingAfterFindingInvalidValueForKey:(NSString *)key {
    MILog(@"invalid key", key);
    return YES;
}

- (BOOL)videoComposition:(AVVideoComposition *)videoComposition
        shouldContinueValidatingAfterFindingEmptyTimeRange:(CMTimeRange)timeRange {
    CFDictionaryRef dict = CMTimeRangeCopyAsDictionary(timeRange,
                                                       kCFAllocatorDefault);
    if (dict)
    {
        MILog(@"Empty time range:", (__bridge id)dict);
        CFRelease(dict);
    }
    return YES;
}


- (BOOL)videoComposition:(AVVideoComposition *)videoComposition
        shouldContinueValidatingAfterFindingInvalidTimeRangeInInstruction:
                (AVVideoCompositionInstruction *)videoCompositionInstruction {
    MILog(@"Invalid time range for instruction:",
                         videoCompositionInstruction.description);
    return YES;
}

- (BOOL)videoComposition:(AVVideoComposition *)videoComposition
    shouldContinueValidatingAfterFindingInvalidTrackIDInInstruction:
            (AVVideoCompositionInstruction *)videoCompositionInstruction
    layerInstruction:(AVVideoCompositionLayerInstruction *)layerInstruction
               asset:(AVAsset *)asset {
    MILog(@"Invalid track ID in instruction:",
                         layerInstruction.description);
    return YES;
}

@end
