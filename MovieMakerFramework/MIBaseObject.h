//  MIBaseObject.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

#import "MIBaseObjectPublicInterface.h"

@class MIContext;

/**
 The MIBaseObject class is the parent class for the base MovingImages classes.
 
 The MovingImages Framework has a number of base object types. All these objects
 class's inherit from the MIBaseObject class. Any object that is instantiated
 from a class that inherits from the MIBaseObject class will have a reference
 to it that remains invariant for the lifetime of the object. The reference
 will be reused after the object is removed from the base object warehouse.
 
 The MovingImages base object types are MIImageImporter, MIImageExporter,
 MMImageFilter, MMMovieImporter, MMMovieEditor and MMGraphic.
*/

@interface MIBaseObject : NSObject <MIBaseObjectPublicInterface>

/// The base object type for object identification
@property (readonly, copy) NSString *baseObjectType;

/// A reference to a MIBaseObject object in the object warehouse.
@property (readonly, assign) MIBaseReference reference;

/// The context which manages the base object.
@property (readonly, weak) MIContext *miContext;

/// The name of the object for identification purposes.
@property (readonly, copy) NSString *name;

/**
 initWithBaseObjectType:name:inContext designated initializer.
*/
-(instancetype)initWithBaseObjectType:(NSString *)objectType
                                 name:(NSString *)name
                            inContext:(MIContext *)context;

/// The object description for debugging purposes.
-(NSString *)description;

/**
 @brief baseObjectClose. Remove this object from MIWarehouse.
 @discussion After removal from MMWarehouse the object should be deallocated.
 In rare cases there might be some code where there is temporary strong
 reference to the object in which case the object will stay alive until all
 strong references go out of scope. Don't override.
 */
-(void)baseObjectClose;

@end
