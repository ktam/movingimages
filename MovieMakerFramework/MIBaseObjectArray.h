//  MIBaseObjectArray.h
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MISafeMutableArray.h"
@class MIBaseObject;

@interface MIBaseObjectArray : MISafeMutableArray
-(instancetype)init;
-(MIBaseObject *)baseObjectWithName:(NSString *)objectName;
@end
