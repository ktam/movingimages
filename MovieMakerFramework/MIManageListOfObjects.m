//  MIManageListOfObjects.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MICGImage.h"
#import "MICIRenderDestinationInterface.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MICreateCGImageInterface.h"
#import "MIJSONConstants.h"
#import "MIUtilities.h"

/// Helper function for MIManageListOfObjectsInterface protocol.

/// Find object defined by dict, conforming to the createCGImageProtocol.
id <MICreateCGImageInterface>
FindObjectFromDictionaryConformingToCreateCGImageProtocol(MIContext *context,
                                                          NSDictionary *dict)
{
    MIBaseObject *obj = MIGetObjectFromDictionary(context, dict);
    if (obj && [obj conformsToProtocol:@protocol(MICreateCGImageInterface)])
        return (id <MICreateCGImageInterface>)obj;
    
    return nil;
}

/// Find object defined by dict, conforming to  MICIRenderDestinationInterface
id <MICIRenderDestinationInterface>
FindObjectFromDictionaryConformingToRenderDestinationProtocol(MIContext *context,
                                                              NSDictionary *dict)
{
    MIBaseObject *obj = MIGetObjectFromDictionary(context, dict);
    if (obj && [obj conformsToProtocol:@protocol(MICIRenderDestinationInterface)])
        return (id <MICIRenderDestinationInterface>)obj;
    
    return nil;
}

/// Find the image creating object and ask it to create the image.
CGImageRef MICreateImageFromObjectAndOptions(MIContext *context,
                                           NSDictionary *objectDict,
                                           NSDictionary *imageOptions,
                                           id cantBeThisObject)
{
    id <MICreateCGImageInterface> imageCreator;
    imageCreator = FindObjectFromDictionaryConformingToCreateCGImageProtocol(
                                                                    context,
                                                                    objectDict);
    if (!imageCreator)
    {
        MILog(@"Couldn't get object to get image from", objectDict);
        return NULL;
    }
    
    if (imageCreator == cantBeThisObject)
    {
        MILog(@"Not allowed to get the image from obtained object",
                          objectDict);
        return NULL;
    }
    CGImageRef theImage = [imageCreator createCGImageWithOptions:imageOptions];
    return theImage;
}

MICGImage *MICGImageFromObjectAndOptions(MIContext *context,
                                         NSDictionary *objectDict,
                                         NSDictionary *imageOptions,
                                         id cantBeThisObject)
{
    CGImageRef theImage = MICreateImageFromObjectAndOptions(context,
                                                            objectDict,
                                                            imageOptions,
                                                            cantBeThisObject);
    MICGImage *image;
    if (theImage)
    {
        image = [[MICGImage alloc] initWithCGImage:theImage];
        CGImageRelease(theImage);
    }
    return image;
}

MICGImage *MICGImageFromDictionary(MIContext *context,
                                   NSDictionary *imageDict,
                                   id cantBeThisObject)
{
    context = context ? context : [MIContext defaultContext];
    NSString *imageIdentifier = imageDict[MIJSONPropertyImageIdentifier];
    if (imageIdentifier && [imageIdentifier isKindOfClass:[NSString class]])
    {
        return [context getImageWithIdentifier:imageIdentifier];
    }
    
    // Now attempt to create the image from the source object.
    NSDictionary *sourceObject = imageDict[MIJSONKeySourceObject];
    NSDictionary *options = imageDict[MIJSONKeyImageOptions];
    return MICGImageFromObjectAndOptions(context, sourceObject,
                                             options, cantBeThisObject);
}

id MIGetObjectFromDictionary(MIContext *context, NSDictionary *objectDict)
{
    if (!(objectDict && [objectDict isKindOfClass:[NSDictionary class]]))
    {
        MILog(@"Object dictionary is nil or not a dictionary",
                          objectDict);
        return nil;
    }

    MIBaseReference ref;
    context = context ? context : [MIContext defaultContext];
    BOOL gotRef = MIUtilityGetMIBaseReferenceFromDictionary(objectDict, &ref);
    if (gotRef)
    {
        MIBaseObject *obj = [context objectWithReference:ref];
        
        // The object reference may have become stale. We may still be able
        // the find the object by type and name so only return here if we
        // have an object.
        if (obj)
            return obj;
    }
    
    // OK, so we don't have an object reference. Now see if the dictionary
    // contains an object type and an object name.
    NSString *objectType = objectDict[MIJSONKeyObjectType];
    if (objectType)
    {
        NSString *objectName = objectDict[MIJSONKeyObjectName];
        if (objectName && [objectName isKindOfClass:[NSString class]])
        {
            MIBaseObject *obj = [context objectWithType:objectType
                                                   name:objectName];
            if (!obj)
            {
                MILog(@"Couldn't find object with name", objectName);
            }
            return obj;
        }
        
        NSNumber *objectIndex = objectDict[MIJSONKeyObjectIndex];
        if (objectIndex)
        {
            return [context objectWithType:objectType
                                     index:objectIndex.integerValue];
        }
    }
    return nil;
}

static NSString *MIImageImporterClassName = @"MIImageImporter";
static NSString *MIImageExporterClassName = @"MIImageExporter";
static NSString *MIImageFilterClassName = @"MICoreImage";
static NSString *MIMovieImporterClassName = @"MIMovieImporter";
// static NSString *MIMovieFrameIteratorClassName = @"MIMovieFrameIterator";
static NSString *MIMovieEditorClassName = @"MIMovieEditor";
static NSString *MIMovieVideoFrameWriterClassName = @"MIMovieVideoFramesWriter";
static NSString *MICGBitmapContextClassName = @"MICGBitmapContext";
static NSString *MINSGraphicContextClassName = @"MINSGraphicContext";
static NSString *MICGPDFContextClassName = @"MIPDFContext";

static NSString *GetClassNameFromKey(NSString *objectTypeKey)
{
    static NSDictionary *classDictionary = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
      {
          classDictionary =
          @{
            MIImageImporterKey : MIImageImporterClassName,
            MIImageExporterKey : MIImageExporterClassName,
            MIImageFilterKey : MIImageFilterClassName,
            MIMovieImporterKey : MIMovieImporterClassName,
//            MIMovieFrameIteratorKey : MIMovieFrameIteratorClassName,
            MIMovieEditorKey : MIMovieEditorClassName,
            MIMovieVideoFramesWriterKey : MIMovieVideoFrameWriterClassName,
            MICGBitmapContextKey : MICGBitmapContextClassName,
            MINSGraphicContextKey : MINSGraphicContextClassName,
            MICGPDFContextKey : MICGPDFContextClassName
          };
      });
    return classDictionary[objectTypeKey];
}

Class MIGetClassForObjectType(NSString *objectType)
{
    Class result;
    
    NSString *className = GetClassNameFromKey(objectType);
    if (className)
        result = NSClassFromString(className);
    
    if (!result)
    {
        MILog(@"Unknown object type key", objectType);
    }
    return result;
}
