//  MIHandleCommands+Internal.h
//  MovieMaker
//
//  Created by Kevin Meaney on 23/07/2013.
//  Copyright (c) 2014 Kevin Meaney. All rights reserved.

@import Foundation;

#import "MIContext.h"
#import "MIReplyDictionary.h"

/**
 @brief Perform cleanup commands will perform all commands in the array.
 @discussion The purpose of running these commands is to close any objects that
 need to be closed, and remove any images from the image collection that need
 to be removed. All the commands will be performed even if an earlier command
 failed. This is not meant for running any post processing commands. Commands
 will be run synchronously.
 @param context The context within which to perform the commands.
 @param listOfCleanupCommands The commands to run to perform the cleanup.
*/
void MIPerformCleanupCommands(MIContext *context, NSArray *listOfCleanupCommands);

/**
 @brief Perform the commands in the command list.
 @discussion The purpose is to run the commands in the context. The commands
 will be run synchronously. This is intended to be used for running
 pre- and post-processing commands. If an error occurs no further commands will
 be run and the command will return the error code of the failed command. It is
 likely that the log file will provide the context within which the command
 failed.
 @param context The context within which to perform the commands.
 @param commands The commands to be run.
 @result A reply dictionary containing an error code.
*/
NSDictionary *MIPerformCommands(MIContext *context, NSArray *commands);

