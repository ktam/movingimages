//  MIBaseObjectArray.m
//  MovieMaker
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObjectArray.h"
#import "MIBaseObject.h"

@implementation MIBaseObjectArray

-(instancetype)init
{
    NSString *queueName = [NSString stringWithFormat:
                                        @"com.zukini.mibaseobjectarray.%p", self];
    self = [super initWithQueueName:queueName];
    return self;
}

-(MIBaseObject *)baseObjectWithName:(NSString *)objectName
{
    ArrayObjectPredicate objectWithName = ^BOOL(id object, NSUInteger index,
                                                BOOL *stop)
    {
        if (![object isKindOfClass:[MIBaseObject class]])
            return false;
        MIBaseObject *theObject = (MIBaseObject *)object;
        return [[theObject name] isEqualToString:objectName];
    };
    MIBaseObject *theObject = [self firstObjectPassingTest:objectWithName];
    return theObject;
}

@end
