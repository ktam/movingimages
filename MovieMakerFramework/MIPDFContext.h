//  MIPDFContext.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIBaseObject.h"
#import "MIBaseObjectSemiPublicInterface.h"
#import "MICreateCGImageInterface.h"
#import "MICIRenderDestinationInterface.h"

@class MICGContext;

/**
 @class MIPDFContext
 @abstract A pdf graphics context.
*/
@interface MIPDFContext : MIBaseObject <MIBaseObjectSemiPublicInterface>

/**
 @brief Initialize a MICGBitmapContext object using the preset and dimensions.
 @discussion Each preset defines a number of parameters used to create the
 bitmap context.
*/
-(instancetype)initWithPath:(NSString *)pathToPDFFile
                       size:(CGSize)size
                  cgContext:(CGContextRef)cgContext
                       name:(NSString *)theName
                  inContext:(MIContext *)context;

/// Remove this object from lists. If no other strong refs it will be deleted
-(void)close;
@end
