//  MIHandleCommands.m
//  Copyright (c) 2015 Zukini Ltd.

#include "sys/sysctl.h"

// #import "DDFileLogger.h"
// #import "DDTTYLogger.h"

#import "MIHandleCommands.h"
#import "MIHandleCommands+Internal.h"
#import "MIBaseObject.h"
#import "MIBaseObjectPublicInterface.h"
#import "MIConstants.h"
#import "MIContext.h"
#import "MIContext+Internal.h"
#import "MIJSONConstants.h"
#import "MIReplyDictionary.h"
#import "MIUtilities.h"
#import "MIBaseObjectPublicInterface.h"

static BOOL sIsCommanded = NO;

void MISetLogging(MILoggingFunction loggingFunction)
{
    miLog = loggingFunction;
}

void MIRegisterCommand(NSString *commandString)
{
    if (!commandString)
        return;

    NSArray *commandStrings = @[
        @"videoframeswriter",
        @"whatismovingimages",
        @"representations",
        @"namespaces",
        @"coregraphicsdrawing",
        @"pixelmodule",
        @"spotlightcommand",
        @"mimeta",
        @"smighelpers",
        @"smigcommands",
        @"drawelement",
        @"renderfilterchain",
        @"aldwych",
        @"lubbock",
        @"amarillo",
        @"guatemala",
        @"ecclesfield"
    ];
    
    for (NSString *theString in commandStrings) {
        if ([commandString isEqualToString:theString])
        {
            sIsCommanded = YES;
            break;
        }
    }
}

static int GetNumberOfPhysicalCPUs()
{
    static int numberOfCPUs = 2;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
    {
        size_t len = sizeof(numberOfCPUs);
        
        // I'm assuming that this will work, or at least if it doesn't
        // then MIMovingImages_numberOfCPUs will keep it's factory settings
        // of 2. I will log the failure of sysctlbyname call though.
        int status = sysctlbyname("hw.physicalcpu",
                                  &numberOfCPUs,
                                  &len, NULL, 0);
        if (status)
        {
            MILog(@"Error obtaining number of CPUs", NULL);
        }
    });
    return numberOfCPUs;
}

static dispatch_semaphore_t GetCPUsSemaphore()
{
    static dispatch_semaphore_t cpus_sema;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^
    {
        cpus_sema = dispatch_semaphore_create(GetNumberOfPhysicalCPUs() + 2);
    });
    return cpus_sema;
}

static Class GetClassFromDictionary(NSDictionary *objectDict)
{
    NSString *objectTypeKey = objectDict[MIJSONKeyObjectType];
    if (objectTypeKey)
        return MIGetClassForObjectType(objectTypeKey);
    
    return nil;
}

static BOOL DoesReturnNoResultsWithDefault(NSString *returns, BOOL defaultVal)
{
    if (!returns)
        return defaultVal;
    
    if ([returns isEqualToString:MIJSONValueReturnNone])
        return YES;
    
    return NO;
}

static BOOL DoesReturnListOfCommandResultsWithDefault(NSString *returns,
                                                      BOOL defaultVal)
{
    if (!returns)
        return defaultVal;
    
    if ([returns isEqualToString:MIJSONValueReturnListOfResults])
        return YES;
    
    return NO;
}

#pragma mark Internal functions.

void MIPerformCleanupCommands(MIContext *context, NSArray *listOfCleanupCommands)
{
    if (!listOfCleanupCommands)
        return;

    for (NSDictionary *theCommand in listOfCleanupCommands)
    {
        @autoreleasepool
        {
            MIMovingImagesHandleCommand(context, theCommand);
        }
    }
}

NSDictionary *MIPerformCommands(MIContext *context, NSArray *commands)
{
    for (NSDictionary *theCommand in commands)
    {
        @autoreleasepool
        {
            NSDictionary *result;
            result = MIMovingImagesHandleCommand(context, theCommand);
            MIReplyErrorEnum error =  MIGetErrorCodeFromReplyDictionary(result);
            if (error != MIReplyErrorNoError)
                return result;
        }
    }
    return MIMakeReplyDictionary(@"", MIReplyErrorNoError);
}

#pragma mark Public functions.

MIContext *MICreateContext()
{
	return [[MIContext alloc] init];
}

NSDictionary *MIFrameworkHandleCommand(MIContext *context, NSDictionary *command)
{
	NSDictionary *result;
	// The idle time is not a property of the framework. It can only be a
	// a property of the launch agent. That leaves just one property that
	// belongs to the framework and that is the number of objects. Though
	// we perhaps can use this to query the list of base objects.
	NSString *commandType = command[MIJSONKeyCommand];
	if (!commandType)
    {
        MILog(@"No command specified in command dictionary.",
                          command);
		result = MIMakeReplyDictionary(@"Error: Command not specified",
                                       MIReplyErrorMissingSubCommand);
        return result;
    }
	
    if (![commandType isKindOfClass:[NSString class]])
    {
        MILog(@"Command is not a string.",
                          command);
        result = MIMakeReplyDictionary(@"Error: Unknown command",
                                       MIReplyErrorMissingSubCommand);
        return result;
    }

	if ([commandType isEqualToString:MIJSONValueGetPropertyCommand])
	{
		NSString *theProperty = command[MIJSONPropertyKey];
		if (theProperty)
		{
			if ([theProperty isEqualToString:MIJSONPropertyNumberOfObjects])
			{
				NSInteger numObjects = [context numberOfObjects];
				NSString *numString;
				numString = [[NSString alloc] initWithFormat:@"%ld",
                                                            (long)numObjects];
				result = MIMakeReplyDictionaryWithNumericValue(
                                                           numString,
                                                           MIReplyErrorNoError,
                                                           @(numObjects));
			}
			else if ([theProperty isEqualToString:MIJSONPropertyVersion])
			{
				NSString *string = [[NSString alloc]
									initWithUTF8String:MIFRAMEWORK_CODEVERSION];
				result = MIMakeReplyDictionary(string, MIReplyErrorNoError);
			}
		}
	}
	else if ([commandType isEqualToString:MIJSONValueCloseAllCommand])
	{
		[context removeAllObjects];
		result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
	}
    else if ([commandType isEqualToString:
                                    MIJSONValueRemoveImageFromCollectionCommand])
    {
        NSString *ident = command[MIJSONPropertyImageIdentifier];
        [context removeImageWithIdentifier:ident];
    }
	else
	{
		NSString *message, *errorMessage;
		message = [[NSString alloc] initWithFormat:@"%@%@",
				   @"Could not find an object to receive the command. ",
				   @"No type specified and command not handled by framework."];
		MILog(message, command);
		errorMessage = [[NSString alloc] initWithFormat:@"Error: %@", message];
		result = MIMakeReplyDictionary(errorMessage, MIReplyErrorMissingOption);
	}
	return result;
}

static inline BOOL IsCommanded()
{
    if (!sIsCommanded)
    {
        // Now do a sneak registering of the framework.
        NSBundle *mainBundle = [NSBundle mainBundle];
        if (mainBundle)
        {
            NSString *bundleID = mainBundle.bundleIdentifier;
            NSDictionary *info = mainBundle.infoDictionary;
            // MILog(@"Bundle id is: ", bundleID);
            // MILog(@"Bundle info is: ", info);
            if (!bundleID && info.allKeys.count == 0)
            {
                sIsCommanded = YES;
            }
            else if (bundleID && (
                [bundleID hasPrefix:@"com.zukini"] ||
                [bundleID isEqualToString:@"U6TV63RN87.com.zukini.MovingImagesAgent"])) 
            {
                sIsCommanded = YES;
            }
        }
    }
    return sIsCommanded;
}

NSDictionary *MIMovingImagesHandleCommand(MIContext *context,
                                          NSDictionary *command)
{
    NSDictionary *result;

    if (!IsCommanded())
    {
        NSString *message = @"Error: Command not registered";
        result = MIMakeReplyDictionary(message, MIReplyErrorOperationFailed);
        return result;
    }

    // Needs to determine whether the command should be handled by the
    // MovingImages framework, or by a class that inherits from MIBaseObject
    // or by an object that is instance of a class that inherits from the
    // MIBaseObject class. Once this function knows what should handle the
    // command it passes the command to that object/class to handle it.
    
    if (!(command && [command isKindOfClass:[NSDictionary class]]))
    {
        NSString *message = command ? @"Error: Command is not a dictionary" :
                                      @"Error: Command is missing";
        MILog(message, command);
        result = MIMakeReplyDictionary(message, MIReplyErrorMissingProperty);
        return result;
    }
    
    // If there is no source then assume that this is a job for the framework.
    NSDictionary *objectDict = command[MIJSONKeyReceiverObject];
    MIBaseObject *commandHandler;

    context = context ? context : [MIContext defaultContext];

    if (objectDict)
    {
        if ([objectDict isKindOfClass:[NSDictionary class]])
            commandHandler = MIGetObjectFromDictionary(context, objectDict);
		
        if (!commandHandler)
        {
            MILog(@"Invalid receiver object: command not handled ",
                              command);
            MILogBreak();
            result = MIMakeReplyDictionary(@"Error: Invalid receiver object",
                                           MIReplyErrorInvalidOption);
            return result;
        }
    }
    
    if (commandHandler)
    {
        result = [commandHandler handleObjectCommand:command];
    }
    else
    {
        Class baseObjClass = GetClassFromDictionary(command);
        if (baseObjClass)
            result = [baseObjClass handleClassCommand:command inContext:context];
        else
            result = MIFrameworkHandleCommand(context, command);
    }
    
    MIReplyErrorEnum value = MIGetErrorCodeFromReplyDictionary(result);
    if (value != MIReplyErrorNoError)
    {
        MILogBreak();
    }
    
    return result;
}

NSDictionary *MIMovingImagesHandleCommands(MIContext * __nullable context,
                                   NSDictionary *commands,
                                   __nullable MIProgressHandler progressHandler,
                                   __nullable MICommandCompletionHandler handler)
{
    if (!(commands && [commands isKindOfClass:[NSDictionary class]]))
    {
        MILog(@"Commands dictionary is missing or not a dictionary",
                          commands);
        return MIMakeReplyDictionary(
                @"Error: Commands dictionary is missing or not a dictionary",
                MIReplyErrorMissingProperty);
    }
    
    __block NSDictionary *result;
    
    BOOL runsAsync = MIUtilityGetBOOLPropertyWithDefault(commands,
                                                MIJSONKeyRunAsynchronously,
                                                NO);
    NSArray *commandList = commands[MIJSONKeyCommands];
    
    // Don't check for the number of entries in the commandList because we
    // might only have cleanup commands to be run.
    if (!(commandList && [commandList isKindOfClass:[NSArray class]]))
    {
        MILog(@"Missing command list", commands);
        return MIMakeReplyDictionary(@"Error: Missing command list",
                                     MIReplyErrorMissingProperty);
    }
    // implement the block.
    void (^processCommands)() = ^
    {
        @autoreleasepool
        {
            BOOL stopOnFailure = MIUtilityGetBOOLPropertyWithDefault(commands,
                                                        MIJSONKeyStopOnFailure,
                                                        YES);
            NSString *returns = commands[MIJSONKeyReturns];
            NSString *pathToSaveResultsFile;
            // If we are running commands async then by default don't return any
            // results.
            if (!DoesReturnNoResultsWithDefault(returns, runsAsync))
            {
                pathToSaveResultsFile = commands[MIJSONKeySaveResultsTo];
                if (pathToSaveResultsFile)
                    pathToSaveResultsFile = [pathToSaveResultsFile
                                                stringByExpandingTildeInPath];
            }
            BOOL exitLoop = NO;
            NSMutableArray *resultList;
            if (DoesReturnListOfCommandResultsWithDefault(returns, NO))
            {
                resultList = [NSMutableArray new];
            }
            
            MIContext *lCTX = context ? context : [MIContext defaultContext];
            NSDictionary *variablesDict = commands[MIJSONKeyVariablesDictionary];
            [lCTX appendVariables:variablesDict];

            NSInteger commandIndex = 0;
            NSDictionary *resultDict;
            for (NSDictionary *command in commandList)
            {
                @autoreleasepool
                {
                    if (progressHandler)
                    {
                        progressHandler(commandIndex);
                    }
                    resultDict = MIMovingImagesHandleCommand(lCTX, command);
                    NSInteger resultVal;
                    NSString *resultStr;
                    resultStr = MIGetReplyValuesFromDictionary(resultDict,
                                                               &resultVal);
                        
                    if (resultVal != MIReplyErrorNoError)
                    {
                        if (stopOnFailure)
                            exitLoop = YES;
                    }
                    
                    if (resultList)
                        [resultList addObject:resultStr];

                    if (exitLoop)
                        break;
                }
                commandIndex++;
            }
            if (!resultDict)
            {
                resultDict = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
            }

            MIPerformCleanupCommands(lCTX, commands[MIJSONKeyCleanupCommands]);
            
            [lCTX dropVariablesDictionary:variablesDict];

            // If we only have one item in the result list just use resultDict.
            if (resultList && ([resultList count] == 1))
                resultList = nil;
            
            if (resultList)
            {
                NSNumber *errorCode = MIGetNSNumberErrorCodeFromReplyDictionary(
                                                                    resultDict);
                resultDict = MIMakeReplyDictionaryWithArray(resultList, errorCode);
            }

            if (pathToSaveResultsFile)
            {
                // This will overwrite any previously saved file. Each file path
                // should be unique so that you don't overright a file that
                // you still want data from.
                BOOL saveAsJSON = YES;
                NSString *saveResultsType = commands[MIJSONKeySaveResultsType];
                if ([saveResultsType
                                isEqualToString:MIJSONPropertyPropertyFilePath])
                    saveAsJSON = NO;
                
                BOOL fileSaved = YES;
                if (saveAsJSON)
                {
                    fileSaved = MIUtilityWriteDictionaryToJSONFile(resultDict,
                                                       pathToSaveResultsFile);
                }
                else
                {
                    fileSaved = MIUtilityWriteDictionaryToPLISTFile(resultDict,
                                                        pathToSaveResultsFile);
                }
                if (!fileSaved)
                {
                    MILog(@"Failed to save command results",
                                      pathToSaveResultsFile);
                }
            }
            if (!runsAsync)
            {
                // only set the result if the block is called directly.
                result = resultDict;
            }
            else
            {
                if (handler)
                {
                    dispatch_async(dispatch_get_main_queue(), ^
                    {
                        handler(resultDict);
                    });
                }
                dispatch_semaphore_signal(GetCPUsSemaphore());
            }
        }
    };

    if (runsAsync)
    {
        dispatch_semaphore_t cpus_sema = GetCPUsSemaphore();
        dispatch_semaphore_wait(cpus_sema, DISPATCH_TIME_FOREVER);
        {
            dispatch_queue_t queue;
            queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_async(queue, processCommands);
            // Since we can't know at this point if the commands ran successfully
            // At least we know we've successfully added running the commands
            // to our dispatch asycn queue so just return no error.
            result = MIMakeReplyDictionary(@"", MIReplyErrorNoError);
        }
    }
    else
    {
        processCommands();
    }

    return result;
}

