//  MICIRenderDestinationInterface.h
//  Moving Images
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

@class MICoreImage;
@class CIImage;

@protocol MICIRenderDestinationInterface <NSObject>

/**
 @brief Draw a CIImage obtained from the MICoreImage object to our context.
 @param coreImage   The filter chain object that we get the CIImage from.
 @param fromRect    The rectangle in the source coordinate system.
 @param toRect      The rectangle to draw into in our CIContext/CGContext.
 @param softwareRender  Is rendering done by software or on the GPU.
 @param shouldCreateImage After rendering should generate a CGImageRef.
 @discussion Obtaining the CIImage and drawing it into the context will
 trigger the filter chain to render. If fromRect is an empty rect, then we use
 the extent from MICoreImage's output CIImage, if toRect is an empty rect then
 we use dimensions of the bitmap context.
*/
-(BOOL)drawCoreImage:(CIImage *)ciImage
     fromFilterChain:(MICoreImage *)coreImage
            fromRect:(CGRect)fromRect
              toRect:(CGRect)toRect
   shouldCreateImage:(BOOL)createImage;

/**
 @brief Clear the CIContext if it's the one the MICoreImage object renders to.
 @param     requestingObject    Object requesting CIContext to be cleared.
 @discussion If the object requesting the release of the CIContext object is the
 same as the object that requested the CIContext in the first place then set
 the strong CIContext reference to nil.
*/
-(void)clearCIContextRelatingTo:(MICoreImage *)requestingObject;

@end
