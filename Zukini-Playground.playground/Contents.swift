//: Playground - noun: a place where people can play

import Cocoa
import MovingImages
import XCPlayground

class SimpleRendererView: NSView {
    
    var drawDictionary:[String:AnyObject]?
    
    var variables:[String:AnyObject]? {
        get {
            let theDict = self.simpleRenderer.variables
            if let theDict = theDict {
                return theDict as? [String:AnyObject]
            }
            return Optional.None
        }
        
        set(newVariables) {
            self.simpleRenderer.variables = newVariables
        }
    }
    
    override func drawRect(dirtyRect: NSRect) {
        let theContext = NSGraphicsContext.currentContext()!.CGContext
        CGContextSetTextMatrix(theContext, CGAffineTransformIdentity)
        if let drawDict = self.drawDictionary {
            simpleRenderer.drawDictionary(drawDict, intoCGContext: theContext)
        }
    }
    
    func assignImage(image: CGImage, identifier: String) -> Void {
        simpleRenderer.assignImage(image, withIdentifier: identifier)
    }
    
    private
    let simpleRenderer = MISimpleRenderer()
}

let theRotation:[String:AnyObject] = [
    MIJSONKeyTransformationType : MIJSONValueRotate,
    MIJSONKeyRotation : 0.77
]

let squareLength = 250.0

let translationVect:[String:AnyObject] = [
    MIJSONKeyX : 200,
    MIJSONKeyY : 200
]

let translation:[String:AnyObject] = [
    MIJSONKeyTransformationType : MIJSONValueTranslate,
    MIJSONKeyTranslation : translationVect
]

let squareSize:[String:AnyObject] = [
    MIJSONKeyWidth : squareLength,
    MIJSONKeyHeight : squareLength
]

let squareOrigin:[String:AnyObject] = [
    MIJSONKeyX : -squareLength * 0.5,
    MIJSONKeyY : -squareLength * 0.5
]

let rectFrame:[String:AnyObject] = [
    MIJSONKeySize : squareSize,
    MIJSONKeyOrigin : squareOrigin
]

/*
let strokeColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : MIJSONValueDeviceRGB,
    MIJSONKeyRed : 1.0,
    MIJSONKeyGreen : 0.25,
    MIJSONKeyBlue : 0.0
]
*/

let strokeColor = "#DD3301"

let strokeRectElement:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValueRectangleStrokeElement,
    MIJSONKeyRect : rectFrame,
    MIJSONKeyStrokeColor : strokeColor,
    MIJSONKeyContextTransformation : [ translation, theRotation ],
    MIJSONKeyLineWidth : 20
]

let renderStrokeRectView = SimpleRendererView(frame: NSRect(x: 0, y: 0,
    width: 400, height: 400))
renderStrokeRectView.drawDictionary = strokeRectElement
// XCPShowView("A Zukini", view: renderStrokeRectView)

let theSize:[String:AnyObject] = [
    MIJSONKeyWidth : 250,
    MIJSONKeyHeight : 150
]

let fillColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : String(kCGColorSpaceGenericGray),
    MIJSONKeyGray : 0.5,
    MIJSONKeyAlpha : 0.5
]

let theOrigin:[String:AnyObject] = [
    MIJSONKeyX : 50.0,
    MIJSONKeyY : 50.0
]

let theRect:[String:AnyObject] = [
    MIJSONKeySize : theSize,
    MIJSONKeyOrigin : theOrigin
]

let fillRectElement:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValueRectangleFillElement,
    MIJSONKeyRect : theRect,
    MIJSONKeyFillColor : fillColor
]

let renderView = SimpleRendererView(frame: NSRect(x: 0, y: 0, width: 400, height: 300))
renderView.drawDictionary = fillRectElement
    
// XCPShowView("Zukini", view: renderView)

// XCPlaygroundPage.currentPage.liveView = renderView

let roundedRectSize:[String:AnyObject] = [
    MIJSONKeyWidth : 300,
    MIJSONKeyHeight : 300
]

let roundedRectFrame:[String:AnyObject] = [
    MIJSONKeySize : roundedRectSize,
    MIJSONKeyOrigin : theOrigin
]

let pathRoundedRect:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValuePathRoundedRectangle,
    MIJSONKeyRect : roundedRectFrame,
    MIJSONKeyRadius : 32
]

let endPoint = [
    MIJSONKeyX : 350.0,
    MIJSONKeyY : 350.0
]

let theLine:[String:AnyObject] = [
    MIJSONKeyStartPoint : theOrigin,
    MIJSONKeyEndPoint : endPoint
]

let locations = [ 0.0, 1.0 ]

let startColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : MIJSONValueDeviceRGB,
    MIJSONKeyRed : 1.0,
    MIJSONKeyGreen : 0.25,
    MIJSONKeyBlue : 0.0
]

let endColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : MIJSONValueDeviceRGB,
    MIJSONKeyRed : 0.0,
    MIJSONKeyGreen : 0.25,
    MIJSONKeyBlue : 1.0
]

let linearGradientFill:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValueLinearGradientFill,
    MIJSONKeyLine : theLine,
    MIJSONKeyStartPoint : theOrigin,
    MIJSONKeyArrayOfPathElements : [ pathRoundedRect ],
    MIJSONKeyArrayOfLocations : locations,
    MIJSONKeyArrayOfColors : [ startColor, endColor ]
]

let renderView2 = SimpleRendererView(frame: NSRect(x: 0, y: 0, width: 400, height: 400))

renderView2.drawDictionary = linearGradientFill
XCPlaygroundPage.currentPage.liveView = renderView2
// XCPShowView("Sukini2", view: renderView2)

let affineTransform:[String:Double] = [
    MIJSONKeyAffineTransformM11 : -1.0,
    MIJSONKeyAffineTransformM12 : 0.0,
    MIJSONKeyAffineTransformM21 : -1.0,
    MIJSONKeyAffineTransformM22 : 0.0,
    MIJSONKeyAffineTransformtX : 0.0,
    MIJSONKeyAffineTransformtY : 0.0
]

let destinationOrigin:[String:Double] = [
    MIJSONKeyX : 20.0,
    MIJSONKeyY : 20.0
]

let destinationSize:[String:Double] = [
    MIJSONKeyWidth : 960,
    MIJSONKeyHeight : 760
]

let destinationRectangle:[String:AnyObject] = [
    MIJSONKeySize : destinationSize,
    MIJSONKeyOrigin : destinationOrigin
]

let sourceObject:[String:String] = [
    MIJSONKeyObjectType : MICGBitmapContextKey,
    MIJSONKeyObjectName : "blendtransition.backgroundimage"
]

let drawImageElement:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValueDrawImage,
    MIJSONKeySourceObject : sourceObject,
    MIJSONKeyDestinationRectangle : destinationRectangle,
    MIJSONKeyInterpolationQuality : "kCGInterpolationHigh",
    MIJSONKeyBlendMode : "kCGBlendModeCopy"
]

let textFillColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : MIJSONValueDeviceRGB,
    MIJSONKeyRed : 0.2,
    MIJSONKeyGreen : 0.2,
    MIJSONKeyBlue : 0.2
]

let drawBasicString:[String:AnyObject] = [
    MIJSONKeyElementType : MIJSONValueBasicStringElement,
    MIJSONKeyFillColor : textFillColor,
    MIJSONKeyStringText : "What is the width and height needed to draw this string on a single line.",
    MIJSONKeyStringPostscriptFontName : "Georgia-Italic",
    MIJSONKeyStringFontSize : 16.0,
    MIJSONKeyPoint : [ MIJSONKeyX : 4.0, MIJSONKeyY : 2.0 ]
]

let shadowColor:[String:AnyObject] = [
    MIJSONKeyColorColorProfileName : MIJSONValueDeviceRGB,
    MIJSONKeyRed : 0.1,
    MIJSONKeyGreen : 0.1,
    MIJSONKeyBlue : 0.1
]

let shadowOffset:[String:Double] = [
    MIJSONKeyWidth : 8.0,
    MIJSONKeyHeight : 12
]

/*
let innerShadow:[String:AnyObject] = [
    MIJSONKeyFillColor : shadowColor,
    MIJSONKeyShadowOffset : shadowOffset,
    MIJS
*/
