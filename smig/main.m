//  main.m
//  smig
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

#import "Interfaces.h"
#import "MIReplyDictionary.h"
#import "SMIGUtilities.h"

#include <stdio.h>

#define SMIGVERSION "1.0.1"
#define SERVICE_NAME @"U6TV63RN87.com.zukini.MovingImagesAgent"

BOOL IsSmigVersionRequest(NSArray *argv)
{
    // Requires a getproperty request.
    if (![argv[1] isEqualToString:@"getproperty"])
        return NO;

    for (NSString *string in argv)
    {
        if ([string isEqualToString:@"smigversion"])
            return YES;
    }
    return NO;
}

BOOL IsGetIdleTime(NSArray *argv)
{
    if (argv.count != 4)
        return NO;

    if (![argv[1] isEqualToString:@"getproperty"])
        return NO;
    
    if (![argv[2] isEqualToString:@"-property"])
        return NO;

    if (![argv[3] isEqualToString:@"idletime"])
        return NO;
    
    return YES;
}

BOOL IsSetIdleTime(NSArray *argv)
{
    if (argv.count != 5)
        return NO;

    if (![argv[1] isEqualToString:@"setproperty"])
        return NO;
    
    if (![argv[2] isEqualToString:@"-property"])
        return NO;
    
    if (![argv[3] isEqualToString:@"idletime"])
        return NO;
    
    return YES;
}

int main(int argc, const char * argv[])
{
    int result = 0;
    if (argc < 3)
    {
        printf("Not enough arguments. Please supply sub command and options.\n");
        exit(2);
    }

	@autoreleasepool
	{
        // Make an array of the arguments passed in.
		NSMutableArray *arguments = [NSMutableArray new];
		for (int i=0 ; i < argc ; ++i)
		{
            NSString *argument = @(*argv++);
			[arguments addObject:argument];
		}
        NSArray *argumentArray = [[NSArray alloc] initWithArray:arguments];
        
        // Is this a smig version request. If so return the version and exit.
        if (IsSmigVersionRequest(argumentArray))
        {
            printf("%s", SMIGVERSION);
            exit(0);
        }
        
		id <MIXPCExportedInterface> agent;
		// Setup our connection to the launch item's service.
		NSError *error = nil;
		NSXPCConnection *connection;
		connection = [[NSXPCConnection alloc]
					  initWithMachServiceName:SERVICE_NAME options:0];
		if (connection == nil)
		{
            NSString *string = [[NSString alloc] initWithFormat:
                @"Failed to connect to Launch Agent: %@\n", [error description]];
            printf("%s", [string UTF8String]);
			printf("smig version: %s\n", SMIGVERSION);
			exit(EXIT_FAILURE);
		}
		connection.remoteObjectInterface = [NSXPCInterface
                        interfaceWithProtocol:@protocol(MIXPCExportedInterface)];
		[connection resume];
        
		// Get a proxy DecisionAgent object for the connection.
		agent = [connection remoteObjectProxy];
        if (IsGetIdleTime(argumentArray))
        {
            [agent getIdleTimeWithReply:^(NSInteger idleTime)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    printf("%ld", (long)idleTime);
                    exit(0);
                });
            }];
        }
        else if (IsSetIdleTime(argumentArray))
        {
            NSString *idleTimeString = argumentArray[4];
            NSScanner *scanner;
            scanner = [[NSScanner alloc] initWithString:idleTimeString];
            NSInteger outInteger;
            BOOL result = [scanner scanInteger:&outInteger];
            if (!result)
            {
                printf("Error: Could not convert %s into an idle time", argv[4]);
                exit(246);
            }
            [agent setIdleTime:outInteger reply:^(NSInteger idleTime)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    printf("%ld", (long)idleTime);
                    exit(0);
                });
            }];
        }
        else
        {
            NSDictionary *commands = SMIGConvertCommandLineToDictionaryCommand(
                                                                argumentArray);
            if (!commands)
            {
                printf("Error: Could not interpret command line for MovingImages");
                exit(246);
            }
            [agent handleDictionaryCommands:commands
                                      reply:^(NSDictionary *replyDict)
             {
                 dispatch_async(dispatch_get_main_queue(), ^
                 {
                     MIReplyErrorEnum resultVal;
                     NSString *outputString = MIGetReplyValuesFromDictionary(
                                                        replyDict, &resultVal);
                     printf("%s", [outputString UTF8String]);
                     exit((int)resultVal);
                 });
             }];
        }
        // Time out after 120 seconds and exit.
        NSDate *date = [[NSDate alloc] initWithTimeIntervalSinceNow:120.0];
		[[NSRunLoop currentRunLoop] runUntilDate:date];
        printf("Error: Timeout. Is Moving Images Launch loaded?");
        result = 248; // operation failed error code.
	}
    return result;
}

