//  SMIGUtilities.m
//  MovieMaker
//
//  Copyright (c) 2014 Apple Inc. All rights reserved.

#import "MILAConstants.h"
#import "MIJSONConstants.h"
#import "SMIGUtilities.h"

typedef NS_ENUM(BOOL, SMIG_JSONTYPE) {
    kSMIG_String,
    kSMIG_Integer,
    kSMIG_Float,
    kSMIG_Bool,
};

static NSString *SMIGUtilitiesGetValueForOption(NSArray *command, NSString *option)
{
    NSString *result = NULL;
    // When we find the component for example the string "-object" the next item
    // in the list will be the value, so keep track of when we find the
    // component.
    BOOL foundObjectRef = NO;
    for(NSString *component in command)
    {
        if (foundObjectRef)
        {
            result = component;
            break;
        }
        else if ([option isEqualToString:component])
            foundObjectRef = YES;
    }
    return result;
}

static NSDictionary *SMIGUtilitiesCreateDictionaryFromJSONString(NSString
                                                                 *jsonString)
{
    NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *theDict;
    theDict =[NSJSONSerialization JSONObjectWithData:data options:0 error:NULL];
    
    if (!theDict || ![theDict isKindOfClass:[NSDictionary class]])
        theDict = NULL;
    
    return theDict;
}

static NSDictionary *SMIGUtilitiesCreateDictionaryFromCommand(NSArray *command)
{
    NSString *propertyFilePath = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionPropertyFile);
    NSString *jsonFilePath = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionJSONFile);
    NSString *jsonString = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionJSONString);
    if (!(propertyFilePath || jsonFilePath || jsonString))
        return nil;
    
    NSDictionary *dictionary = NULL;
    if (jsonString)
    {
        dictionary = SMIGUtilitiesCreateDictionaryFromJSONString(jsonString);
    }
    else if (propertyFilePath)
    {
        NSString *path = [propertyFilePath stringByExpandingTildeInPath];
        dictionary = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    else if (jsonFilePath)
    {
        NSString *path = [jsonFilePath stringByExpandingTildeInPath];
        NSFileManager *fileManager = [NSFileManager defaultManager];
        BOOL fileExists = [fileManager fileExistsAtPath:path isDirectory:NO];
        
        id container;
        if (fileExists)
        {
            NSInputStream *inStream;
            inStream = [[NSInputStream alloc] initWithFileAtPath:path];
            [inStream open];
            container = [NSJSONSerialization JSONObjectWithStream:inStream
                                                          options:0
                                                            error:nil];
            if (container && [container isKindOfClass:[NSDictionary class]])
                dictionary = container;
        }
    }
    return dictionary;
}

static BOOL SMIGUtilitiesGetIntegerFromString(NSString *stringValue,
                                              NSInteger *outInteger)
{
    BOOL result = NO;
    
    if (stringValue)
    {
        NSScanner *scanner = [[NSScanner alloc] initWithString:stringValue];
        result = [scanner scanInteger:outInteger];
    }
    return result;
}

static BOOL SMIGUtilitiesGetFloatFromString(NSString *string, CGFloat *value)
{
    NSScanner *scanner = [[NSScanner alloc] initWithString:string];
    CGFloat floatVal;
    BOOL gotValue = [scanner scanDouble:&floatVal];
    if (gotValue)
    {
        *value = floatVal;
    }
    return gotValue;
}

NSDictionary *SMIGUtilitiesGetPropertyKeyAndValue(NSArray *command)
{
    NSDictionary *result = NULL;
    NSString *property = NULL;
    NSString *propertyVal = NULL;
    BOOL foundObjectRef = NO;
    for (NSString *component in command)
    {
        if (property)
        {
            propertyVal = component;
            break;
        }
        if (foundObjectRef)
            property = component;
        else if ([component isEqualToString:MILACommandOptionProperty])
            foundObjectRef = YES;
    }
    if (propertyVal)
        result = @{property : propertyVal};
    return result;
}

NSDictionary *GetSubCommandDictionary()
{
    static NSDictionary *subCommandDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
    {
        subCommandDict =
        @{
               MILASubCommandCreate : MIJSONValueCreateCommand,
               MILASubCommandGetProperty : MIJSONValueGetPropertyCommand,
               MILASubCommandGetProperties : MIJSONValueGetPropertiesCommand,
               MILASubCommandSetProperty : MIJSONValueSetPropertyCommand,
               MILASubCommandSetProperties : MIJSONValueSetPropertiesCommand,
        };
    });
	return subCommandDict;
}

NSDictionary *GetDoActionDictionary()
{
    static NSDictionary *doActionDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
		doActionDict =
        @{
 MILACommandOptionClose : MIJSONValueCloseCommand,
 MILACommandOptionCloseAll : MIJSONValueCloseAllCommand,
 MILACommandOptionDrawElement : MIJSONValueDrawElementCommand,
 MILACommandOptionSnapshot : MIJSONValueSnapshotCommand,
 MILACommandOptionGetPixelData : MIJSONValueGetPixelDataCommand,
 MILACommandOptionTextDrawingSize : MIJSONValueCalculateGraphicSizeOfTextCommand,
 MILACommandOptionRenderFilterChain : MIJSONValueRenderFilterChainCommand,
 MILACommandOptionExport : MIJSONValueExportCommand,
 MILACommandOptionAddImage : MIJSONValueAddImageCommand
        };
	});
	return doActionDict;
}

NSString *SMIGConvertCommandLineCommandToJSONCommand(NSArray *commands)
{
	if (commands.count < 3)
		return nil;
    
	NSString *jsonString;
	if ([commands[1] isEqualToString:MILASubCommandDoAction])
	{
		NSDictionary *doActionDict = GetDoActionDictionary();
		jsonString = doActionDict[commands[2]];
	}
	else
	{
		NSDictionary *subCommandDict = GetSubCommandDictionary();
		jsonString = subCommandDict[commands[1]];
	}
	return jsonString;
}

NSDictionary *GetPropertiesDictionary()
{
    static NSDictionary *propertiesDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^{
		propertiesDict =
        @{
           MILAPropertyKeyNumberOfObjects : MIJSONPropertyNumberOfObjects,
           MILAPropertyKeyImageImportTypes : MIJSONPropertyImageImportTypes,
           MILAPropertyKeyBitmapContextPresets : MIJSONPropertyPresets,
           MILAPropertyKeyBitmapBlendModes : MIJSONPropertyBlendModes,
           MILAPropertyKeyImageExportTypes : MIJSONPropertyAvailableExportTypes,
           MILAPropertyKeyImageFilters : MIJSONPropertyImageFilters,
           MILAPropertyKeyImageFilterAttributes :
               MIJSONPropertyImageFilterAttributes,
           MILAPropertyKeyNumberOfImages : MIJSONPropertyNumberOfImages,
           MILAPropertyKeySourceFilePath : MIJSONPropertyFile,
           MILAPropertyKeyUTIFileType : MIJSONPropertyFileType,
           MILAPropertyKeyImageSourceStatus : MIJSONPropertyImageSourceStatus,
           MILAPropertyKeyAllowFloatingPoint :
               MIJSONPropertyAllowFloatingPointImages,
           MILAPropertyKeyObjectName : MIJSONKeyObjectName,
           MILAPropertyKeyObjectType : MIJSONKeyObjectType,
           MILAPropertyKeyDictionary : MIJSONPropertyDictionary,
           MILAPropertyKeyCanExport : MIJSONPropertyCanExport,
           MILAPropertyKeyExportCompressionQuality :
                                        MIJSONPropertyExportCompressionQuality,
           MILAPropertyKeyWidth : MIJSONKeyWidth,
           MILAPropertyKeyHeight : MIJSONKeyHeight,
           MILAPropertyKeyXLoc : MIJSONKeyX,
           MILAPropertyKeyYLoc : MIJSONKeyY,
           MILAPropertyKeyWindowRectangle : MIJSONPropertyWindowRectangle,
           MILAPropertyKeyBitsPerComponent : MIJSONPropertyBitsPerComponent,
           MILAPropertyKeyBitsPerPixel : MIJSONPropertyBitsPerPixel,
           MILAPropertyKeyBytesPerRow : MIJSONPropertyBytesPerRow,
           MILAPropertyKeyColorSpaceName : MIJSONPropertyColorSpaceName,
           MILAPropertyKeyAlphaAndBitmapInfo : MIJSONPropertyAlphaAndBitmapInfo,
           MILAPropertyKeyPreset : MIJSONPropertyPreset,
           MILAPropertyKeySoftwareRender : MIJSONPropertySoftwareRender,
           MILAPropertyKeyUseSRGBColorSpace : MIJSONPropertyUseSRGBColorSpace
        };
	});
	return propertiesDict;
}

NSString *SMIGConvertCommandLinePropertyToJSONProperty(NSString *commandKey)
{
	if (!commandKey)
        return nil;
    
    NSDictionary *propertyDictionary = GetPropertiesDictionary();
    NSString *result = propertyDictionary[commandKey];
    return  result ? result : commandKey;
}

NSDictionary *GetOptionsDictionary()
{
    static NSDictionary *optionsDict = NULL;
    static dispatch_once_t pred = 0;
    dispatch_once(&pred, ^
    {
        optionsDict =
        @{
            MILACommandOptionName : MIJSONKeyObjectName,
            MILACommandOptionType : MIJSONKeyObjectType,
            MILACommandOptionObject : MIJSONKeyObjectReference,
            MILACommandOptionFile : MIJSONPropertyFile,
            MILACommandOptionProperty : MIJSONPropertyKey,
            MILACommandOptionPreset : MIJSONPropertyPreset,
            MILACommandOptionPropertyFile : MIJSONPropertyPropertyFilePath,
            MILACommandOptionJSONFile : MIJSONPropertyJSONFilePath,
            MILACommandOptionJSONString : MIJSONPropertyJSONString,
            MILACommandOptionIndex : MIJSONKeyObjectIndex,
            MILACommandOptionImageIndex : MIJSONKeyImageIndex,
            MILACommandOptionAllowFloatingPointImages :
                                    MIJSONPropertyAllowFloatingPointImages,
            MILACommandOptionHeight : MIJSONKeyHeight,
            MILACommandOptionWidth : MIJSONKeyWidth,
            MILACommandOptionXLoc : MIJSONKeyX,
            MILACommandOptionYLoc : MIJSONKeyY,
            MILACommandOptionSnapshotAction : MIJSONPropertySnapshotAction,
            MILACommandOptionBorderlessWindow : MIJSONPropertyBorderlessWindow,
            MILACommandOptionColorSpaceName : MIJSONPropertyColorSpaceName,
            MILACommandOptionSecondaryObject : MIJSONKeySourceObject,
            MILACommandOptionSecondaryImageIndex : MIJSONKeySecondaryImageIndex,
            MILACommandOptionUTIFileType : MIJSONPropertyFileType,
            MILACommandOptionFilterName : MIJSONPropertyImageFilterName,
            MILACommandOptionFilterCategory : MIJSONPropertyFilterCategory
        };
    });
	return optionsDict;
}

static NSString *SMIGConvertCommandLineOptionToJSONProperty(
                                                        NSString *commandOption)
{
	if (!commandOption)
        return nil;
    
    // If the command option is not a key in the dictionary just return it.
    NSDictionary *optionsDictionary = GetOptionsDictionary();
    NSString *result = optionsDictionary[commandOption];
    return  result ? result : commandOption;
}

static NSDictionary *SMIGUtilitiesCreateObjectDictionaryFromCommandLine(
                                                                NSArray *command)
{
    NSString *objectRef = SMIGUtilitiesGetValueForOption(command,
                                                         MILACommandOptionObject);
    if (objectRef)
    {
        NSInteger baseRef;
        if (SMIGUtilitiesGetIntegerFromString(objectRef, &baseRef))
        {
            return @{ MIJSONKeyObjectReference : @(baseRef) };
        }
        else
        {
            return nil;
        }
    }
    
    NSString *typeRef = SMIGUtilitiesGetValueForOption(command,
                                                       MILACommandOptionType);
    if (!typeRef)
        return nil;
    
    NSString *nameRef = SMIGUtilitiesGetValueForOption(command,
                                                       MILACommandOptionName);
    if (nameRef)
    {
        return @{ MIJSONKeyObjectType : typeRef,
                  MIJSONKeyObjectName : nameRef };
    }
    
    NSString *objectIndexRef = SMIGUtilitiesGetValueForOption(command,
                                                        MILACommandOptionIndex);
    if (objectIndexRef)
    {
        NSInteger objectIndex;
        if (SMIGUtilitiesGetIntegerFromString(objectIndexRef, &objectIndex))
        {
            return @{ MIJSONKeyObjectType : typeRef,
                      MIJSONKeyObjectIndex : @(objectIndex) };
        }
    }
    return nil;
}

BOOL SMIGAddSMIGCommandLineOptionToJSONDictionary(NSArray *commandLine,
                                                  NSMutableDictionary *jsonDict,
                                                  NSString *milaCommand,
                                                  SMIG_JSONTYPE jsonType)
{
    BOOL result = NO;
    NSString *propertyValue = SMIGUtilitiesGetValueForOption(commandLine,
                                                             milaCommand);
    if (propertyValue)
    {
        NSString *propertyKey;
        propertyKey = SMIGConvertCommandLineOptionToJSONProperty(milaCommand);
        if (propertyKey)
        {
            if (jsonType == kSMIG_Float)
            {
                CGFloat theFloat;
                if (SMIGUtilitiesGetFloatFromString(propertyValue, &theFloat))
                {
                    jsonDict[propertyKey] = @(theFloat);
                }
                else
                {
                    jsonDict[propertyKey] = propertyValue;
                }
            }
            else if (jsonType == kSMIG_Integer)
            {
                NSInteger theInteger;
                if (SMIGUtilitiesGetIntegerFromString(propertyValue, &theInteger))
                {
                    jsonDict[propertyKey] = @(theInteger);
                }
                else
                {
                    jsonDict[propertyKey] = propertyValue;
                }
            }
            else if (jsonType == kSMIG_Bool)
            {
                jsonDict[propertyKey] = [propertyValue isEqualToString:@"YES"] ?
                @(YES) : @(NO);
            }
            else
            {
                jsonDict[propertyKey] = propertyValue;
            }
            result = YES;
        }
    }
    return result;
}

/*
 Multiple ways to break down command. Do I first find out what type of object
 is being dealt with and then pass to the object class the command.
 
 Different object types:
 * Image importer            "imageimporter"
 * Image exporter            "imageexporter"
 * Image filter            "imagefilterchain"
 * Movie importer            "movieimporter"
 * Movie editor/exporter  "movieeditor"
 * Bitmap context            "bitmapcontext"
 * pdf context            "pdfcontext"
 
 Or do I break things down to what type of operation is being performed.
 * Object creation
 * Obtain property value
 * Obtain properties
 * Set property value
 * Set properties
 * Object deletion
 * Perform action.
 
 If I go down the track of factoring the code around the different object types
 then I need to think about how to capture code that is common between different
 object types. For example there is likely common code for obtaining properties,
 setting properties, performing actions like export, object creation etc..
 
 If I go down the route of type of operation then the operation needs to know
 about the different objects each deals with. This doesn't feel right. Each
 object knows what it needs to do any task.
 
 I realize I could use pure virtual functions of C++ to sort out part of
 problem and how to go about refactoring code.
 
 Perhaps I should just get on with it and then aftewards I might find a way
 to refactor common code. By getting on with it, I'm going down the different
 object type route. Lets hope it works.
 
 Need to determine type of object to handle the command. Then have a class
 handler to do the work.
 
 What are the smig sub commands.
 
 create
 getproperty
 getproperties
 setproperty
 setproperties
 doaction
 
 "create"
 creates a new object. Returns a reference to the created object r.
 Takes the following options:
 -type objecttypetocreate
 -file "/path/to/file" # valid for "imageimporter", "movieimporter",
 # "movieeditor"
 -preset "namedpreset" #bitmap context
 -width # (width of context)
 -height # (height of context)
 
 options which are possibly valid for the following sub commands.
 -type objecttype # the type of object to handle the operation.
 -index n # The index into list of objects of type -type to handle op.
 # index may not be needed. For example when requesting the list
 # of available image format types the image importer can import.
 -object r # The object referred to by reference r to handle the operation.
 # the -type option is not needed but can be specified to confirm that
 # the object handling the command is of the expected type. If -object
 # and -type are specified and -index is specified, index will be ignored.
 
 "getproperty"
 returns a property of the object type or object. If the property key
 starts with "dictionary" then the rest of the property key is a path
 into the appropriate dictionary. Because some keys in the dictionary can
 contain characters that don't get passed straight to the smig command
 the property key path in this case needs to be quoted. Since dictionaries
 can then contain arrays the property key path can contain numbers which
 represent the index into the array for the desired element. An example
 of a smig command in this case follows at end of this section.
 -property "propertykey"
 -imageindex n #index into list of images in a image importer.
 ./smig getproperty -object 0 -imageindex 0
 -property "dictionary.{Exif}.ComponentsConfiguration.1"
 
 "getproperties"
 returns a file listing object properties. Can be a plist or json file.
 -propertyfile "/path/to/file/to/create.extension" #extension = plist/json
 -imageindex n #index into list of images in a image importer.
 
 "setproperty"
 sets a property of the object.
 -property "propertykey" value
 -imageindex n #index into list of images in a image importer.
 
 "setproperties"
 sets properties of the object
 -propertyfile "/path/to/file.extension" #extension = plist/json.
 -imageindex n #index into list of images in a image importer.
 
 "doaction"
 performs a action on the object or all objects. One of the following.
 -drawelement
 -close
 -closeall
 -renderfilterchain
 -
 
 With -closeall if -type is also specified then closeall will closes all objects
 of a specific type otherwise it will close all the base objects.
 
 This is still a work in progress. How I deal with tracks for movie importer
 and movie editor objects I haven't yet decided on. Plus I haven't yet thought
 at all about how the image filter object will work. I also haven't thought
 about how to handle all the information to be passed in to draw composition
 elements in the graphic document.
 
 Getting properties from the CGImageSourceRef object is more complicated.
 There are properties associated with the image source itself, and then there
 are properties associated with each image in the image source. If the command
 option includes "-imageindex" then I'm assuming that we want the property or
 the property dictionary for for an image file. If the option is missing then
 we are obtaining the information for the container for the images. The same
 goes for setting properties.
 */

NSDictionary *SMIGConvertCommandLineToDictionaryCommand(NSArray *command)
{
    // Basic error checking
    NSUInteger numCommandItems = [command count];
    if (numCommandItems < 2)
        return nil;
        
    NSString *subCommand = command[1];
    
    if ([subCommand isEqualToString:MILASubCommandPerformCommand])
    {
        return SMIGUtilitiesCreateDictionaryFromCommand(command);
    }
    
    NSMutableDictionary *commandDict = [NSMutableDictionary new];
    NSString *jsonCommand = SMIGConvertCommandLineCommandToJSONCommand(command);
    if (!jsonCommand)
    {
        return nil;
    }
    
    // I'm not going to do any further checking here as to what options are
    // required for any action. I'm just going to go though the list of any
    // possible command line options and if we have one I'll create an
    // dictionary option for that.
    
    // First deal with the create command, as that is one of the exceptional
    // cases because the object name, and type are likely to be specified which
    // are normally associated with specifying the object to deal with, but in
    // this case define the type and name of object to create.
    
    commandDict[MIJSONKeyCommand] = jsonCommand;
    
    // Every sub command except the create command may need an object to handle
    // the command. Some of the information needed for the create command is also
    // the same as what is needed to identify an object. Therefore exclude
    // creating an object dictionary if the sub command is create.
    if (![subCommand isEqualToString:MILASubCommandCreate])
    {
        NSDictionary *objectDict;
        objectDict = SMIGUtilitiesCreateObjectDictionaryFromCommandLine(command);
        if (objectDict)
        {
            commandDict[MIJSONKeyReceiverObject] = objectDict;
        }
    }
    
    if (!commandDict[MIJSONKeyReceiverObject])
    {
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionType,
                                                     kSMIG_String);
    }
    
    NSString *propertyValue;
    if ([subCommand isEqualToString:MILASubCommandCreate])
    {
        NSMutableDictionary *sizeDict = [NSMutableDictionary new];
        NSMutableDictionary *originDict = [NSMutableDictionary new];
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionName,
                                                     kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionFile,
                                                     kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionPreset,
                                                     kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, sizeDict,
                                                     MILACommandOptionHeight,
                                                     kSMIG_Integer);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, sizeDict,
                                                     MILACommandOptionWidth,
                                                     kSMIG_Integer);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, originDict,
                                                     MILACommandOptionXLoc,
                                                     kSMIG_Integer);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, originDict,
                                                     MILACommandOptionYLoc,
                                                     kSMIG_Integer);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(
                                                 command, commandDict,
                                                 MILACommandOptionColorSpaceName,
                                                 kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionUTIFileType,
                                                     kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                             MILACommandOptionBorderlessWindow,
                                             kSMIG_Bool);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                     MILACommandOptionAllowFloatingPointImages,
                                     kSMIG_Bool);
        
        // The filter chain dictionary when creating a core image filter chain obj
        NSDictionary *filterChainDict;
        filterChainDict = SMIGUtilitiesCreateDictionaryFromCommand(command);
        if (filterChainDict)
        {
            commandDict[MIJSONPropertyImageFilterChain] = filterChainDict;
        }
        
        if (originDict.count)
        {
            commandDict[MIJSONKeyRect] = @{ @"origin" : originDict,
                                            @"size" : sizeDict };
        }
        else if (sizeDict.count)
        {
            commandDict[MIJSONKeySize] = sizeDict;
        }
    }
    else if ([subCommand isEqualToString:MILASubCommandGetProperty])
    {
        propertyValue = SMIGUtilitiesGetValueForOption(command,
                                                       MILACommandOptionProperty);
        if (propertyValue)
        {
            propertyValue = SMIGConvertCommandLinePropertyToJSONProperty(
                                                                propertyValue);
            commandDict[MIJSONPropertyKey] = propertyValue;
        }
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionImageIndex,
                                                     kSMIG_Integer);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(
                                                 command, commandDict,
                                                 MILACommandOptionFilterCategory,
                                                 kSMIG_String);
        
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionFilterName,
                                                     kSMIG_String);
        
        NSString *propertyFilePath, *jsonFilePath;
        propertyFilePath = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionPropertyFile);
        jsonFilePath = SMIGUtilitiesGetValueForOption(command,
                                                      MILACommandOptionJSONFile);
        
        if (propertyFilePath)
        {
            commandDict[MIJSONKeySaveResultsType]=MIJSONPropertyPropertyFilePath;
            commandDict[MIJSONKeySaveResultsTo] = propertyFilePath;
        }
        else if (jsonFilePath)
        {
            commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONFilePath;
            commandDict[MIJSONKeySaveResultsTo] = jsonFilePath;
        }
        // else // actuall default to if (jsonString)
        // {
        //    commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONString;
        // }
    }
    else if ([subCommand isEqualToString:MILASubCommandGetProperties])
    {
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionImageIndex,
                                                     kSMIG_Integer);
        
        NSString *propertyFilePath, *jsonFilePath;
        // NSString *jsonString;
        propertyFilePath = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionPropertyFile);
        jsonFilePath = SMIGUtilitiesGetValueForOption(command,
                                                      MILACommandOptionJSONFile);
        if (propertyFilePath)
        {
            commandDict[MIJSONKeySaveResultsType]=MIJSONPropertyPropertyFilePath;
            commandDict[MIJSONKeySaveResultsTo] = propertyFilePath;
        }
        else if (jsonFilePath)
        {
            commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONFilePath;
            commandDict[MIJSONKeySaveResultsTo] = jsonFilePath;
        }
        else // actuall default to jsonstring // if (jsonString)
        {
            commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONString;
        }
    }
    else if ([subCommand isEqualToString:MILASubCommandSetProperty])
    {
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionImageIndex,
                                                     kSMIG_Integer);
        NSDictionary *property = SMIGUtilitiesGetPropertyKeyAndValue(command);
        if (property)
        {
            NSString *key = [property allKeys][0];
            NSString *value = property[key];
            commandDict[MIJSONPropertyKey] =
            SMIGConvertCommandLinePropertyToJSONProperty(key);
            commandDict[MIJSONPropertyValue] = value;
        }
        
        property = SMIGUtilitiesCreateDictionaryFromCommand(command);
        if (property)
        {
            commandDict[MIJSONKeyGetDataType] = MIJSONPropertyDictionaryObject;
            commandDict[MIJSONPropertyValue] = property;
        }
    }
    else if ([subCommand isEqualToString:MILASubCommandSetProperties])
    {
        SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                     MILACommandOptionImageIndex,
                                                     kSMIG_Integer);
        
        NSDictionary *property = SMIGUtilitiesCreateDictionaryFromCommand(command);
        if (property)
        {
            commandDict[MIJSONKeyGetDataType] = MIJSONPropertyDictionaryObject;
            commandDict[MIJSONKeyInputData] = property;
        }
    }
    else if ([subCommand isEqualToString:MILASubCommandDoAction])
    {
        // Short cut here using the jsonCommand.
        // Also we don't need to do anything more for the close command or the
        // closeall commands, or the export command
        if ([jsonCommand isEqualToString:MIJSONValueAddImageCommand])
        {
            SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                         MILACommandOptionSecondaryImageIndex,
                                         kSMIG_Integer);
            NSString *secondaryObject = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionSecondaryObject);
            
            if (secondaryObject)
            {
                NSInteger baseReference = -1;
                if (SMIGUtilitiesGetIntegerFromString(secondaryObject,
                                                      &baseReference))
                {
                    commandDict[MIJSONKeySourceObject] =
                    @{ MIJSONKeyObjectReference : @(baseReference) };
                }
            }
        }
        else if ([jsonCommand isEqualToString:MIJSONValueDrawElementCommand])
        {
            NSDictionary *property;
            property = SMIGUtilitiesCreateDictionaryFromCommand(command);
            if (property)
            {
                commandDict[MIJSONPropertyDrawInstructions] = property;
            }
        }
        else if ([jsonCommand isEqualToString:MIJSONValueGetPixelDataCommand])
        {
            NSDictionary *propertyDict;
            NSString *jsonString = SMIGUtilitiesGetValueForOption(command,
                                                    MILACommandOptionJSONString);
            if (jsonString)
            {
                propertyDict = SMIGUtilitiesCreateDictionaryFromJSONString(
                                                    jsonString);
            }
            if (propertyDict)
            {
                commandDict[MIJSONKeyGetDataType]=MIJSONPropertyDictionaryObject;
                commandDict[MIJSONKeyInputData] = propertyDict;
            }
            
            NSString *propertyFilePath, *jsonFilePath;
            propertyFilePath = SMIGUtilitiesGetValueForOption(command,
                                                    MILACommandOptionPropertyFile);
            jsonFilePath = SMIGUtilitiesGetValueForOption(command,
                                                    MILACommandOptionJSONFile);
            if (propertyFilePath)
            {
                commandDict[MIJSONKeySaveResultsType] =
                MIJSONPropertyPropertyFilePath;
                commandDict[MIJSONKeySaveResultsTo] = propertyFilePath;
            }
            else if (jsonFilePath)
            {
                commandDict[MIJSONKeySaveResultsType] =
                MIJSONPropertyJSONFilePath;
                commandDict[MIJSONKeySaveResultsTo] = jsonFilePath;
            }
            else
            {
                commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONString;
            }
        }
        else if ([jsonCommand isEqualToString:
                  MIJSONValueCalculateGraphicSizeOfTextCommand])
        {
            NSString *jsonString = SMIGUtilitiesGetValueForOption(command,
                                                MILACommandOptionJSONString);
            if (jsonString)
            {
                NSDictionary *textDictionary = NULL;
                textDictionary = SMIGUtilitiesCreateDictionaryFromJSONString(
                                                                    jsonString);
                commandDict[MIJSONKeyGetDataType]=MIJSONPropertyDictionaryObject;
                commandDict[MIJSONKeyInputData] = textDictionary;
                commandDict[MIJSONKeySaveResultsType] = MIJSONPropertyJSONString;
            }
        }
        else if ([jsonCommand isEqualToString:MIJSONValueRenderFilterChainCommand])
        {
            NSDictionary *property;
            property = SMIGUtilitiesCreateDictionaryFromCommand(command);
            if (property)
            {
                commandDict[MIJSONPropertyRenderInstructions] = property;
            }
        }
        else if ([jsonCommand isEqualToString:MIJSONValueSnapshotCommand])
        {
            SMIGAddSMIGCommandLineOptionToJSONDictionary(command, commandDict,
                                                 MILACommandOptionSnapshotAction,
                                                 kSMIG_String);
        }
    }
    return @{ MIJSONKeyCommands : @[ commandDict ] };
}

