//  MILAConstants.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MILAConstants.h"

/* Subcommands */
NSString *const MILASubCommandCreate = @"create";
NSString *const MILASubCommandGetProperty = @"getproperty";
NSString *const MILASubCommandGetProperties = @"getproperties";
NSString *const MILASubCommandSetProperty = @"setproperty";
NSString *const MILASubCommandSetProperties = @"setproperties";
NSString *const MILASubCommandDoAction = @"doaction";
NSString *const MILASubCommandPerformCommand = @"performcommand";

/* Command options */
NSString *const MILACommandOptionObject = @"-object";

/*
 The different object types. Defined in MIConstants.h
 Included here for completeness.
 NSString *const MIImageImporterKey = @"imageimporter";
 NSString *const MIImageExporterKey = @"imageexporter";
 NSString *const MIImageFilterKey = @"imagefilterchain";
 NSString *const MIMovieImporterKey = @"movieimporter";
 NSString *const MIMovieFrameIteratorKey = @"movieframeiterator";
 NSString *const MIMovieEditorKey = @"movieeditor";
 NSString *const MIMovieVideoFramesWriterKey = @"videoframeswriter";
 NSString *const MICGBitmapContextKey = @"bitmapcontext";
 NSString *const MICGPDFContextKey = @"pdfcontext";
 NSString *const MIUIGraphicsContext = @"uigraphicscontext";
 NSString *const MINSGraphicsContext = @"nsgraphicscontext";
*/

NSString *const MILACommandOptionName = @"-name";
NSString *const MILACommandOptionType = @"-type";
NSString *const MILACommandOptionFile = @"-file";
NSString *const MILACommandOptionProperty = @"-property";
NSString *const MILACommandOptionPreset = @"-preset";
NSString *const MILACommandOptionPropertyFile = @"-propertyfile";
NSString *const MILACommandOptionJSONFile = @"-jsonfile";
NSString *const MILACommandOptionJSONString = @"-jsonstring";
NSString *const MILACommandOptionIndex = @"-index";
NSString *const MILACommandOptionImageIndex = @"-imageindex";
NSString *const MILACommandOptionHeight = @"-height";
NSString *const MILACommandOptionWidth = @"-width";
NSString *const MILACommandOptionXLoc = @"-xloc";
NSString *const MILACommandOptionYLoc = @"-yloc";
NSString *const MILACommandOptionSnapshotAction = @"-snapshotaction";
NSString *const MILACommandOptionColorSpaceName = @"-colorspacename";
NSString *const MILACommandOptionSecondaryObject = @"-secondaryobject";
NSString *const MILACommandOptionSecondaryImageIndex = @"-secondaryimageindex";
NSString *const MILACommandOptionUTIFileType = @"-utifiletype";
NSString *const MILACommandOptionFilterName = @"-filtername";
NSString *const MILACommandOptionFilterCategory = @"-filtercategory";
NSString *const MILACommandOptionAllowFloatingPointImages=@"-allowfloatingpoint";
/* doaction command options irrespective of the object. */
NSString *const MILACommandOptionCloseAll = @"-closeall";
NSString *const MILACommandOptionClose = @"-close";

/* doaction command options for a graphic context */
#if DEBUGWINDOW
NSString *const MILACommandOptionShowWindow = @"-showwindow"; // YES/NO
#endif

NSString *const MILACommandOptionDrawElement = @"-drawelement";
NSString *const MILACommandOptionGetPixelData = @"-getpixeldata";
NSString *const MILACommandOptionSnapshot = @"-snapshot";

NSString *const MILACommandOptionTextDrawingSize = @"-getstringdrawingsize";

/* doaction command option for an core image filter chain. */
NSString *const MILACommandOptionRenderFilterChain = @"-renderfilterchain";

/* doaction command options for a graphic exporter */
NSString *const MILACommandOptionExport = @"-export";
NSString *const MILACommandOptionAddImage = @"-addimage";

/* an option for the doaction -addimage command option. */ 
NSString *const MILACommandOptionGrabMetadataDictionary = @"-grabmetadata";

/* Property of the launch agent. Read/Write */
NSString *const MILAPropertyKeyIdleTime = @"idletime";

/* Property of the launch agent. Readonly */
NSString *const MILAPropertyKeyVersion = @"version";

/* Property of smig, included here for completeness. But handled by smig. */
// NSString *const MILAPropertyKeySmigVersion = @"smigversion";

/* Properties of classes not objects. Readonly. */
NSString *const MILAPropertyKeyNumberOfObjects = @"numberofobjects";
NSString *const MILAPropertyKeyImageImportTypes = @"imageimporttypes";
NSString *const MILAPropertyKeyBitmapContextPresets = @"bitmappresets";
NSString *const MILAPropertyKeyBitmapBlendModes = @"bitmapblendmodes";
NSString *const MILAPropertyKeyImageExportTypes = @"imageexporttypes";
NSString *const MILAPropertyKeyImageFilters = @"imagefilters";
NSString *const MILAPropertyKeyImageFilterAttributes = @"imagefilterattributes";

/* Properties common to all objects. Readonly. */
NSString *const MILAPropertyKeyObjectType = @"objecttype";
NSString *const MILAPropertyKeyObjectReference = @"objectreference";
NSString *const MILAPropertyKeyObjectName = @"objectname";

/* Property common to both image importers and exporters. Readonly. */
NSString *const MILAPropertyKeyNumberOfImages = @"numberofimages";

/* Properties of image importer. Readonly. */
NSString *const MILAPropertyKeySourceFilePath = @"sourcefilepath";

NSString *const MILAPropertyKeyImageSourceStatus = @"imagesourcestatus";
NSString *const MILAPropertyKeyAllowFloatingPoint = @"allowfloatingpoint";

/* Readonly properties for image importer. Readwrite for image exporter */
NSString *const MILAPropertyKeyDictionary = @"dictionary";
NSString *const MILAPropertyKeyUTIFileType = @"utifiletype";

/* Properties of image exporter. Readonly. */
NSString *const MILAPropertyKeyCanExport = @"canexport";

/* Properties of image exporter. Readwrite. */
NSString *const MILAPropertyKeyExportFilePath = @"exportfilepath";
NSString *const MILAPropertyKeyExportCompressionQuality =
                                                @"exportcompressionquality";

/* Properties of bitmap & nsgraphic context objects. Readonly. */
NSString *const MILAPropertyKeyWidth = @"width";
NSString *const MILAPropertyKeyHeight = @"height";

/* Properties of the nsgraphic context objects. Readwrite. */
NSString *const MILAPropertyKeyXLoc = @"xloc";
NSString *const MILAPropertyKeyYLoc = @"yloc";

NSString *const MILAPropertyKeyWindowRectangle = @"windowrectangle";
NSString *const MILACommandOptionBorderlessWindow = @"-borderlesswindow";

NSString *const MILAPropertyKeyBitsPerComponent = @"bitspercomponent";
NSString *const MILAPropertyKeyBitsPerPixel = @"bitsperpixel";
NSString *const MILAPropertyKeyBytesPerRow = @"bytesperrow";
NSString *const MILAPropertyKeyColorSpaceName = @"colorspacename";
NSString *const MILAPropertyKeyAlphaAndBitmapInfo = @"alphaandbitmapinfo";
NSString *const MILAPropertyKeyPreset = @"preset";

/* Properties of image filter chain. Readwrite. */
NSString *const MILAPropertyKeySoftwareRender = @"coreimagesoftwarerender";
NSString *const MILAPropertyKeyUseSRGBColorSpace = @"use_srgbcolorspace";

