//  MIXPCExportedObject.m
//  MovingImages
//  Copyright (c) 2015 Zukini Ltd.

@import MovingImages;

#import "MIAgentDelegate.h"
#import "MILaunchAgentInterface.h"
#import "MILAConstants.h"
#import "MMXPCExportedObject.h"

// static const int ddLogLevel = LOG_LEVEL_VERBOSE;

#include <stdlib.h>

static NSInteger idleTime = 10; // default idle time is 10 seconds.
static NSTimer *killTimer = NULL; // If timer fires then LaunchAgent is exited.

@interface MIXPCExportedObject ()

@property (weak) MIAgentDelegate *launchAgentDelegate;
/**
 @name Managing a timer that will exit the launch agent if it fires.
*/

/**
 If there are no objects in the MovingImages Framework GetWarehouseArray then
 createKillTimer will create a timer that will fire in idleTime seconds
 and when fired will call launchAgentDelegate's exitLaunchAgent method.
*/
-(void)createKillTimer;

/**
 Cancels any kill timer that might exist. Will get called whenever there has
 been a xpc call.
*/
-(void)invalidateAndRemoveKillTimer;

/**
 Cancels any kill timer that might exist. If there are no MovieFramework base
 objects in the MovingImages Framework then will generate a new timer to kill
 the launch agent process.
*/
-(void)invalidateOldTimerAndCreateNew;

/// Do the work of performing the command.
// -(NSDictionary *)_performCommand:(NSArray *)commmand;

@end

@implementation MIXPCExportedObject

-(instancetype)initWithLaunchAgentDelegate:(MIAgentDelegate *)agentDelegate
{
    self = [super init];
    self.launchAgentDelegate = agentDelegate;
    return self;
}

-(id)init
{
    self = [super init];
    return self;
}

#pragma mark Private Methods

// In relation to creating and deleting the timer, by posting blocks to the same
// queue for both the creating and removal of the NSTimer will achieve the
// necessary atomic separation. The reason they are being posted to the main
// queue is that it makes life easier if the NSTimer is created on the main
// queue.
-(void)createKillTimer
{
    dispatch_sync(dispatch_get_main_queue(), ^
    {
        // Whilst the GetWarehouseArray containing created base objects is empty
        // the launch agent can be terminated, so set a timer to kill the launch
        // agent at a specified future time.
        if ([[MIContext defaultContext] isEmpty])
        {
            killTimer = [NSTimer
                  scheduledTimerWithTimeInterval:(NSTimeInterval)idleTime
                                          target:self.launchAgentDelegate
                                        selector:@selector(exitLaunchAgent:)
                                        userInfo:nil
                                         repeats:NO];
        }
    });
}

- (void)invalidateAndRemoveKillTimer
{
    dispatch_sync(dispatch_get_main_queue(), ^
    {
        if (killTimer)
        {
            [killTimer invalidate];
            killTimer = NULL;
        }
    });
}

-(void)invalidateOldTimerAndCreateNew
{
    [self invalidateAndRemoveKillTimer];
    [self createKillTimer];
}

#pragma mark Implementation of @protocol MIXPCExportedInterface

-(void)setIdleTime:(NSInteger)theIdleTime reply:(void (^)(NSInteger))reply
{
    if (theIdleTime < 1)
        theIdleTime = 1;
    
    if (theIdleTime > 900)
        theIdleTime = 900;
    
    idleTime = theIdleTime;
    reply(idleTime);
    [self invalidateOldTimerAndCreateNew];
}

-(void)getIdleTimeWithReply:(void (^)(NSInteger))reply
{
    reply(idleTime);
    [self invalidateOldTimerAndCreateNew];
}

-(void)handleDictionaryCommands:(NSDictionary *)commands
                          reply:(void (^)(NSDictionary *))reply
{
    MIAgentDelegate *delegate = self.launchAgentDelegate;
    if (delegate)
    {
        delegate.handlingConnection++;
        [self invalidateAndRemoveKillTimer];
    }
    
    NSDictionary *replyDict = NULL;
    @try
    {
        @autoreleasepool
        {
            MIContext *context = nil;
            replyDict = MIMovingImagesHandleCommands(context, commands, nil, nil);
        }
    }
    @catch (NSException *exception)
    {
        NSString *exceptionString = [[NSString alloc] initWithFormat:
                     @"Error: Exception \"%@\" raised with reason \"%@\". %@",
                     [exception name], [exception reason],
                     @"MovingImages may no longer function properly."];
        replyDict = MIMakeReplyDictionary(exceptionString,
                                          MILAErrorOperationFailed);
    }
    
    reply(replyDict);
    [self invalidateOldTimerAndCreateNew];
    
    if (delegate)
        delegate.handlingConnection--;
}

@end
