//  MIAgentDelegate.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

/**
 MIAgentDelegate is the delegate to the XPC listener.
 
 The MovingImages Launch Agent communicates with its clients using NSXPCConnection
 and NSXPCListener. The listener is listening for a xpc request. When it
 receives that request it passes it to the listener delegate which here is
 `MIAgentDelegate`.
*/
@interface MIAgentDelegate : NSObject <NSXPCListenerDelegate>

@property (atomic, assign) NSInteger handlingConnection;

-(void)exitLaunchAgent:(NSTimer *)killTimer;
@end
