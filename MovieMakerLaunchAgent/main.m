//  main.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
@import AppKit;
@import MovingImages;

#import "MIAgentDelegate.h"
#import "MILAConstants.h"

#import "MICocoaLumberjack.h"

int main(int argc, const char *argv[])
{
#if DEBUGWINDOW
    NSApplicationLoad();
#endif
    MIInitializeCocoaLumberjack();

    // LaunchServices automatically registers a mach service of the same
    // name as our bundle identifier.
    NSString *bundleId = [[NSBundle mainBundle] bundleIdentifier];
    NSXPCListener *listener;
    listener = [[NSXPCListener alloc] initWithMachServiceName:bundleId];

    // Create the delegate of the listener.
    MIAgentDelegate *delegate = [MIAgentDelegate new];
    listener.delegate = delegate;

    // Begin accepting incoming connections.
    // For mach service listeners, the resume method returns immediately so
    // we need to start our event loop manually.
    [listener resume];
    // NSApplicationLoad needed for displaying the debug window. It might
    // make sense to if def all debug window code and the NSApplicationLoad
    // function and linking against AppKit so that it can be easily removed.
    // It might also be a good idea to look at the difference in resource
    // demands with NSApplicationLoad called and not.
    [[NSRunLoop currentRunLoop] run];
    return 0;
}
