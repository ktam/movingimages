//  MILaunchAgentInterface.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

/**
 * MILaunchAgentInterface.h defines the required protocol interface that classes
 * must implement. The classes that must implement this interface are ones that
 * inherit from the MovingImages Framework base object class. MIBaseObject.
*/
@import Foundation;

/**
 @brief The MILaunchAgentInterface specifies the interface to deal with commands
 @discussion The MIXPCExportedObject class command handler determines which
 object type should handle the command. If the command relates to an object that
 already exists then that object is sent a handleCommand message. In the case of
 an object creation command for example then the MMXPCExportedObject class
 command handler sends a performCommand message to the appropriate class which
 then creates the appropriate object.
*/
@protocol MILaunchAgentInterface <NSObject>

@required

/**
 @brief performCommand A class method called if we have no object reference.
 @discussion The types of situations when +performCommand is called are when we
 want to  create an object of a particular type, when we want to close all the
 objects of a particular type or obtain a property value that relates to the
 class not the object.
 
 Properties that relate to a class are, the number of objects that have been
 created of that class, for an imagefilter a list of the different image
 file types the image importer can read, for a bit map graphic context a list of
 the different presets that are available for instantiating a bit map graphic
 context. For a imagefilter a list of the available image filters and attributes
 related to a particular filter.
*/
+(NSDictionary *)performCommand:(NSArray *)commandComponents;

/**
 The object method handleCommand is called when the command line option
 "-object" is specified with an object reference. Once the object is obtained
 using the object reference the object then handles the command.
*/
-(NSDictionary *)handleCommand:(NSArray *)commandComponents;

@end
