//  MILAConstants.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

#define DEBUGWINDOW 1

/**
 @name Error codes returned to the client.
 */

typedef NS_ENUM(NSInteger, MILAErrorEnum)
{
    MILAErrorNoError = 0,
    MILAErrorMissingOption = 254,
    MILAErrorInvalidObjectReference = 253,
    MILAErrorInvalidIndex = 252,
    MILAErrorOptionValueInvalid = 251,
    MILAErrorInvalidSubCommand = 250,
    MILAErrorMissingSubCommand = 249,
    MILAErrorOperationFailed = 248,
    MILAErrorUnknownProperty = 247,
    MILAErrorInvalidOption = 246,
    MILAErrorMissingProperty = 245
};

/**
 @name Constants that define the various sub commands.
*/

/// The create an element subcommand. "create"
extern NSString *const MILASubCommandCreate;

/// The getproperty subcommand "getproperty"
extern NSString *const MILASubCommandGetProperty;

/// The getproperties subcommand. "getproperties"
extern NSString *const MILASubCommandGetProperties;

/// The setproperty subcommand. "setproperty"
extern NSString *const MILASubCommandSetProperty;

/// The setpropeties subcommand. "setproperties"
extern NSString *const MILASubCommandSetProperties;

/// The doaction subcommand. "doaction"
extern NSString *const MILASubCommandDoAction;

/// The perform command subcommand. "performcommand"
extern NSString *const MILASubCommandPerformCommand;

/// The dictionary command subcommand. "dictionarycommand"
//extern NSString *const MILASubCommandDictionaryCommand;

/// The object reference option. "-object"
extern NSString *const MILACommandOptionObject;

/// The object name option. "-name"
extern NSString *const MILACommandOptionName;

/// The object type option. "-type"
extern NSString *const MILACommandOptionType;

/// The file option. "-file"
extern NSString *const MILACommandOptionFile;

/// The property option. "-property"
extern NSString *const MILACommandOptionProperty;

/// The preset option. "-preset"
extern NSString *const MILACommandOptionPreset;

/// The property file option. "-propertyfile"
extern NSString *const MILACommandOptionPropertyFile;

/// The json file option. "-jsonfile"
extern NSString *const MILACommandOptionJSONFile;

/// The json string option. "-jsonstring"
extern NSString *const MILACommandOptionJSONString;

/// How do we want to save the results, a reply dict, a property file, ...
// extern NSString *const MILACommandOptionSaveResultsType;

/// The path to where we want to save the results to, this will be plist/json file
// extern NSString *const MILACommandOptionSaveResultsTo;

/// The object index option. "-index"
extern NSString *const MILACommandOptionIndex;

/**
 @brief The image index option. "-imageindex"
 @discussion An option when accessing specific images in a imageimporter or a
 imageexporter object.
*/
extern NSString *const MILACommandOptionImageIndex;

/// The height option used in bitmapcontext/nsgraphiccontext. readonly. "-height"
extern NSString *const MILACommandOptionHeight;

/// The width option used in bitmapcontext/nsgraphiccontext. readonly. "-width"
extern NSString *const MILACommandOptionWidth;

/// The x location option used in nsgraphiccontext. "-xloc"
extern NSString *const MILACommandOptionXLoc;

/// The y location option used in bitmapcontext/nsgraphiccontext. "-yloc"
extern NSString *const MILACommandOptionYLoc;

/// The type of snapshot action to take in bitmapcontext. "-snapshotaction"
extern NSString *const MILACommandOptionSnapshotAction;

/// Optional colorspace name used when creating a bitmapcontext. "-colorspacename"
extern NSString *const MILACommandOptionColorSpaceName;

/// The secondary object reference option. "-secondaryobject"
extern NSString *const MILACommandOptionSecondaryObject;

/// The secondary image index option. "-secondaryimageindex"
extern NSString *const MILACommandOptionSecondaryImageIndex;

/// The uti file type option, for creating image exporter object. "-utifiletype"
extern NSString *const MILACommandOptionUTIFileType;

/// The close all doaction subcommand. A MIBaseObject CLASS action. "-closeall"
extern NSString *const MILACommandOptionCloseAll; // MIBaseObject CLASS action.

/// The close doaction subcommand. All objects handle this subcommand. "-close"
extern NSString *const MILACommandOptionClose; // MIBaseObject.

#if DEBUGWINDOW
/// The display window doaction subcommand. Handled by the bitmapcontext object.
extern NSString *const MILACommandOptionShowWindow;// CGBitmapContext "YES","NO"
#endif

/**
 @brief The doaction draw element subcommand. "-drawelement"
 @discussion Requires a string in json format to define drawing the element or
 a path to a json text file containing the draw element instructions or
 a path a a plist dictionary file containing the draw element instructions.
 Handled by the bitmapcontext base object.
 */
extern NSString *const MILACommandOptionDrawElement;

/**
 @brief The snapshot subcommand handled by a bitmapcontext object. "-snapshot"
 @discussion Requires a "-snapshotaction" option, which defines the type of
 snapshot action to take. One of "takesnapshot", "drawsnapshot", "clearsnapshot"
*/
extern NSString *const MILACommandOptionSnapshot;

/**
 @brief The doaction get pixel data subcommand. "-getpixeldata"
 @discussion Requires a string in json format that defines the rectangle within
 which the pixel data is obtained from inside the bitmap context. For example:
 -jsonstring '{ "origin" : { "x" : 0.0, "y" : 0.0 }, "size" : { "width" : 256,
 "height" : 256 } }'. The "-propertyfile" or "-jsonfile" option also needs to
 be specified which specifies the location of the file where the pixel data will
 be saved. The output data is a dictionary with two keys: "columnNames" and
 "pixelData". The "columnNames" value is an array of column names which is
 in the same order as the inner array of the "pixelData" array. The "pixelData"
 value is an array with the number of entries being the number of pixels that
 pixel data was requested for. Each entry is another array specifying the
 location in the bitmap where the pixel data was obtained from and values for
 each component of the pixel data.
*/
extern NSString *const MILACommandOptionGetPixelData;

/// Send render filter chain doaction message to filter chain "-renderfilterchain"
extern NSString *const MILACommandOptionRenderFilterChain;

/**
 @brief Export images to a file doaction subcommand. "-export".
 @discussion An action handled by imageexporter.
*/
extern NSString *const MILACommandOptionExport;

/**
 @brief Add an image to the list of images doaction subcommand. "-addimage"
 @discussion An action handled by "imageexporter".
 */
extern NSString *const MILACommandOptionAddImage;

/**
 @brief Grab metadata dictionary from source when adding image to exporter.
 @discussion If this command option is included in the addimage doaction
 subcommand and the source image is from an importer then grab the image
 metadata at the same time as getting the image file. "-grabmetadata"
*/
extern NSString *const MILACommandOptionGrabMetadataDictionary;

/**
 @brief Get bounding box for a string doaction subcommand. "-getstringdrawingsize"
 @discussion This doaction subcommand will return a json string which will look
 like { "width" : 322.0, "height" : 22 }. This action takes a json string which
 describes the string and attributes needed for calculating the bounding box.
 Example1: '{ "stringtext" : "This is some Palatine 40.0 point text",
            "postscriptfontname" : "Palatino",
            "fontsize" : 40.0 }'
 Example2:'{ "stringtext" : "This is some mini emphasized system text",
             "userinterfacefont" : "kCTFontUIFontMiniEmphasizedSystem" }'
*/
extern NSString *const MILACommandOptionTextDrawingSize;

/**
 @brief How long in seconds before the LaunchAgent will exit if no base objects.
 @discussion A property of the MovingImages LaunchAgent. "idletime". Readonly.
*/
extern NSString *const MILAPropertyKeyIdleTime;

/**
 @brief The LaunchAgent version number. "version"
 @discussion Handled by the MovingImages LaunchAgent. Readonly.
*/
extern NSString *const MILAPropertyKeyVersion;

/// Property of smig, included here for completeness.
// NSString *const MILAPropertyKeySmigVersion = @"smigversion";

/**
 @name Non object readonly Property keys.
 These are keys for readonly properly that don't apply to a specific object.
 Though keys like MILAPropertyKeyImageImportTypes will be handled by a specific
 class.
*/

/**
 @brief The number of objects. "numberofobjects"
 @discussion A property of the MovingImages LaunchAgent and for each of the
 base object classes. Readonly.
 Example 1: To get the number of base objects.
    smig getproperty -property numberofobjects
 Example 2: To get the number of imageimporter base objects.
    smig getproperty -property numberofobjects -type imageimporter
 Example 3: To get the number of bitmapcontext base objects.
    smig getproperty -property numberofobjects -type bitmapcontext
 Example 4: To get the number of imageexporter base objects.
    smig getproperty -property numberofobjects -type imageexporter
*/
extern NSString *const MILAPropertyKeyNumberOfObjects;

/**
 @brief Get the image file types an imageimporter can import property.
 @discussion The image importer class handles this property. The property
 returns a string containing the list of uti file types delimited by a space
 "imageimporttypes". Readonly.
*/
extern NSString *const MILAPropertyKeyImageImportTypes; // MILAImageImporter

/**
 @brief Get list of presets used to create a bitmapcontext property.
 @discussion The bitmap context class handles this property. The property
 returns a string containing the list of presets types delimited by a space.
 "bitmappresets". Readonly.
*/
extern NSString *const MILAPropertyKeyBitmapContextPresets; // MILABitmapContext

/**
 @brief Get list of blend modes for drawing images and shapes to a bitmapcontext.
 @discussion The bitmap context class handles this property. The property
 returns a string containing the list blend modes delimited by a space.
 "bitmapblendmodes". Readonly.
*/
extern NSString *const MILAPropertyKeyBitmapBlendModes;

/**
 @brief Get the image file types an image imageexporter can export property.
 @discussion The image exporter class handles this property. The property
 returns a string containing the list of uti types delimited by a space.
  "imageexportypes". Readonly.
*/
extern NSString *const MILAPropertyKeyImageExportTypes; // MILAImageExporter

/**
 @brief Get the list of image filters that can be added to a imagefilterchain.
 @discussion The core image class handles this property. The property
 returns a string containing the list of core image filters delimited by a space.
 "imagefiltertypes". Readonly.
*/
extern NSString *const MILAPropertyKeyImageFilters; // MILACoreImage

/**
 @brief Get the dictionary of attributes associated with a particular filter.
 @discussion The core image class handles this property. The property takes
 the "-filtername" option which selects the filter to get the attribtues from.
 The property requires one of "-propertyfile", "-jsonfile", "-jsonstring"
 to specify how the attributes should be returned. If "-propertyfile" or
 "-jsonfile" is specified then a path to a file is required immediately after
 one of these options.
 "imagefilterattributes". Readonly.
*/
extern NSString *const MILAPropertyKeyImageFilterAttributes; // MILACoreImage

/**
 @brief Get the object type property. "objecttype"
 @discussion All base objects handle this property. They return the object type
 specified when the object was created. Readonly.
*/
extern NSString *const MILAPropertyKeyObjectType;

/**
 @brief Get the object reference property. "objectreference". Readonly.
 @discussion The object reference. You will nearly always have the object
 reference because using the object reference is the usual way for referring
 to the object to send actions, and set and get properties. There is an alternate
 way to refer to objects and that is by object type and object index. For example
 if you know there is at least one "imageimporter" which you can determine using
 the "numberofobjects" property and specifying the type. To get the object
 reference of the first image importer object in the list you do:
    FIRST_IIR=`smig getproperty -type imageimporter -index 0 -property
                                                                objectreference`
*/
extern NSString *const MILAPropertyKeyObjectReference;

/**
 @brief Get the name of the object. "objectname". Readonly.
 @discussion The object name. This is the same as the "-name" option.
 When needing to obtain an objects "objectreference" use "-type" and "-name"
 and request the property "objectreference". This provides an alternative way
 to find an object.
*/
extern NSString *const MILAPropertyKeyObjectName;

/**
 @brief The path to the image file that the image importer is created from.
 @discussion Image importer objects only handle this property. "sourcefilepath".
 Readonly.
*/
extern NSString *const MILAPropertyKeySourceFilePath; //readwrite image exporter

// extern NSString *const MILAPropertyKeySourceFileSize; // readonly

/**
 Each image in a image file has a large quantity of meta data that is associated
 with it. The metadata is stored in a plist dictionary file. By using the
 MILAPropertyKeyDictionary key you will be able to pull out the bits of
 the meta data that you want. The key MILAPropertyKeyDictionary is
 only the start of the full key path and is equal to "dictionary".
 
 then you can specify which specific named meta data you want.
 
 The full property key to find the image width is:
 dictionary.PixelWidth
 
 Others are:
 dictionary.PixelHeight
 dictionary.ColorModel
 dictionary.Depth
 
 There are also more complex property paths because there are dictionaries in
 dictionaries and arrays in dictionaries etc.
 
 So for example if you want to obtain the date and time the image was digitized
 then you need to have a full propery key like so:
 "dictionary.{Exit}.DateTimeDigitized"
 
 Another useful property might be:
 "dictionary.{Exif}.FNumber"
 
 To access items in an array you specify a number, the arrays are zero based.
 So for example the ISOSpeedRating in the {Exif} metadata is an array, so if
 we want the first entry in the array we would do:
 "dictionary.{Exif}.ISOSpeedRating.0"
 
 As soon as any key no longer matches the metadata path in the dictionary it
 will return with a invalid property key error.
 
 A typical use would be:
 ./smig getproperty -object 0 -imageindex 0 -property "dictionary.PixelHeight"
 
 This is not restricted to reading dictionary properties but writing to them
 as well. The images in a image importer or exporter have far more metadata
 associated with them than the dictionary metadata associated with the image
 file but there is also a dictionary associated with the file.
*/

/**
 @brief A dictionary containing key value pairs. "dictionary"
 @discussion Readonly for image importer objects. Read/Write for image exporter.
 When setting the dictionary as a property if a dictionary already
 exists for the exporter object or an associated image to be exported then the
 dictionary is not replaced but instead each key in the new dictionary the
 old dictionary is assigned the value.
*/
extern NSString *const MILAPropertyKeyDictionary;

/**
 @brief The number of images in a image file property. "numberofimages"
 @discussion Readonly property. This is a property of both the image importer
 and the image exporter object. For the image importer this is the number of
 images in the image file that the image importer object interacts with. For
 the image exporter object this is the number of images that will be exported
 into the image file that will be created when the image exporter object exports.
 For the image exporter the number of images to be exported will increase if
 a "addimage" "doaction" subcommand is sent to the image exporter object. Note
 also that not all image file formats permit multiple images to be embedded in
 the image file.
*/
extern NSString *const MILAPropertyKeyNumberOfImages;

/**
 @brief Allow floating point image representation. YES/NO. "allowfloatingpoint"
 @discussion A read/write property of the image importer object. If this property
 is set to YES and the image file format supports floating point pixel value
 representation then when the CGImageRef object is created it can be created
 with a floating point representation.
*/
extern NSString *const MILAPropertyKeyAllowFloatingPoint;

/**
 @name MILAImageImporter Object readonly specific property keys.
*/

/**
 @brief Is everything about the image importer object ok. "imagesourcestatus"
 @discussion A readonly property. After creating the image importer object you
 can find out if the image importer is in a usable state. If you also specify
 an image index when getting this property you can find out if it is likely
 that drawing the image at index will work. A value of 0 indicates that
 every is ok.
 kCGImageStatusUnexpectedEOF = -5,
 kCGImageStatusInvalidData = -4,
 kCGImageStatusUnknownType = -3,
 kCGImageStatusReadingHeader = -2,
 kCGImageStatusIncomplete = -1,
 kCGImageStatusComplete = 0
*/
extern NSString *const MILAPropertyKeyImageSourceStatus;

/**
 @brief Is the image exporter in a position to export property key. "canexport"
 @discussion Readonly. Handled by image exporter object only. Returns "YES"/"NO"
*/
extern NSString *const MILAPropertyKeyCanExport;

/**
 @brief The export file path property key. "exportfilepath".
 @discussion Readwrite. Handled by image exporter object only.
*/
extern NSString *const MILAPropertyKeyExportFilePath;

/**
 @brief The export compression quality property key. Value is CGFloat. 0.0..1.0.
 @discussion Write only. Applies only to an image at imageindex in image exporter
*/
extern NSString *const MILAPropertyKeyExportCompressionQuality;

/**
 @brief The image uti file type. For example: "public.jpeg"
 @discussion This is a readonly property for a image importer and a readwrite
 property for 
*/
extern NSString *const MILAPropertyKeyUTIFileType;

/**
 @brief The name of a CoreImage filter. For example: "CIHueAdjust"
 @discussion Used for obtaining the attributes of a particular filter.
*/
extern NSString *const MILACommandOptionFilterName;

/**
 @brief The filter category. "-filtercategory"
 @discussion Used for getting the list of filters that belong to a category.
*/
extern NSString *const MILACommandOptionFilterCategory;

/**
 @brief Creating a CGImageRef from an image importer allows floating point images
 @discussion If the image format allows it, and the data in file makes it
 possible then when creating the CGImageRef allow it to be created with floating
 point numbers backing the pixels.
*/
extern NSString *const MILACommandOptionAllowFloatingPointImages;

/**
 @name MILACGBitmapContext Object readonly specific property keys.
*/

/// The width of the bitmap & ns context property key. Readonly. "width"
extern NSString *const MILAPropertyKeyWidth;

/// The height of the bitmap & ns context property key. Readonly. "height"
extern NSString *const MILAPropertyKeyHeight;

/// The x location property key of the ns context's window. read/write "xloc"
extern NSString *const MILAPropertyKeyXLoc;

/// The y location property key of the ns context's window. read/write "xloc"
extern NSString *const MILAPropertyKeyYLoc;

/// A property of the nsgraphicscontext. Window frame.readonly. "windowrectangle"
extern NSString *const MILAPropertyKeyWindowRectangle;

/// An option when creating nsgraphicscontext. bool (YES/NO). "-borderlesswindow"
extern NSString *const MILACommandOptionBorderlessWindow;

/**
 @brief The number of bits per component for the bitmap context property key.
 @discussion Readonly property. "bitspercomponent".
*/
extern NSString *const MILAPropertyKeyBitsPerComponent;

/**
 @brief The number of bits per pixel for the bitmap context property key.
 @discussion Readonly property. "bitsperpixel".
*/
extern NSString *const MILAPropertyKeyBitsPerPixel;

/**
 @brief The number of bytes per row for the bitmap context property key.
 @discussion Readonly property. "bytesperrow".
*/
extern NSString *const MILAPropertyKeyBytesPerRow;

/**
 @brief The color space property key for the bitmap context.
 @discussion Readonly property. "colorspacename"
*/
extern NSString *const MILAPropertyKeyColorSpaceName;

/**
 @brief The alpha and bitmapinfo property key for the bitmap context.
 @discussion Readonly property. "alphaandbitmapinfo"
    kCGImageAlphaNone,                  0
    kCGImageAlphaPremultipliedLast,     1
    kCGImageAlphaPremultipliedFirst,    2
    kCGImageAlphaLast,                  3
    kCGImageAlphaFirst,                 4
    kCGImageAlphaNoneSkipLast,          5
    kCGImageAlphaNoneSkipFirst,         6
    kCGImageAlphaOnly                   7

    kCGBitmapAlphaInfoMask = 0x1F,
    kCGBitmapFloatComponents = (1 << 8),
    
    kCGBitmapByteOrderMask = 0x7000,
    kCGBitmapByteOrderDefault = (0 << 12),
    kCGBitmapByteOrder16Little = (1 << 12),
    kCGBitmapByteOrder32Little = (2 << 12),
    kCGBitmapByteOrder16Big = (3 << 12),
    kCGBitmapByteOrder32Big = (4 << 12)
*/
extern NSString *const MILAPropertyKeyAlphaAndBitmapInfo;

/**
 @brief The preset property key for the bitmap context. Readonly. "preset".
 @discussion The preset that was used when the bitmap context was created.
*/
extern NSString *const MILAPropertyKeyPreset;

#pragma mark Image Filter Chain Properties. Readonly.

/// The number of filters in a filter chain. "numberoffilters"
// extern NSString *const MILAPropertyKeyNumberOfFilters;

/// The CoreImage filter name of the filter at filterindex. "filtername"
extern NSString *const MILAPropertyFilterName;

/// Whether to software render filter chain. YES/NO. "coreimagesoftwarerender"
extern NSString *const MILAPropertyKeySoftwareRender;

/**
 @brief Tell the core image context to use a working colorspace of sRGB.
 @discussion CoreImage filters default to working in linear RGB color space.
 So if this option has not been set, or value is NO then coreimage will
 process images working in the linear generic RGB color space. But if we set
 the colorspace to be sRGB then processing will happen within this colorspace.
 Values: YES/NO. "use_srgbcolorspace"
*/
extern NSString *const MILAPropertyKeyUseSRGBColorSpace;

