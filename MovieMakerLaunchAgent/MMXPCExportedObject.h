//  MIXPCExportedObject.h
//  MovingImages
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;
#import "Interfaces.h"

@class MIAgentDelegate;

/**
 This is the class that provides the XPC Exported interface for the MovingImages
 Launch Agent.
*/
@interface MIXPCExportedObject : NSObject <MIXPCExportedInterface>
/**
 Initialize the export object with the agent delegate. The agent delegate is
 used to provide a target method to kill the agent.
*/
-(instancetype)initWithLaunchAgentDelegate:(MIAgentDelegate *)agentDelegate;

@end
