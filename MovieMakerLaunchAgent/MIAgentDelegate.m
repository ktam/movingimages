//  MIAgentDelegate.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import MovingImages;

#import "MIAgentDelegate.h"
#import "MMXPCExportedObject.h"

static NSXPCListener *theListener;

@implementation MIAgentDelegate

#pragma mark Public Methods

-(void)exitLaunchAgent:(NSTimer *)killTimer
{
    MIContext *theContext = [MIContext defaultContext];
    if (self.handlingConnection <= 0 && [theContext isEmpty])
    {
        exit(EXIT_SUCCESS);
    }
}

#pragma mark NSXPCListener delegate method.

- (BOOL)listener:(NSXPCListener *)listener shouldAcceptNewConnection:
                                            (NSXPCConnection *)newConnection
{
    // This method is called by the NSXPCListener to filter/configure
    // incoming connections.
    theListener = listener;
    @autoreleasepool
    {
        newConnection.exportedInterface = [NSXPCInterface interfaceWithProtocol:
                                           @protocol(MIXPCExportedInterface)];        
    }
    newConnection.exportedObject =
                [[MIXPCExportedObject alloc] initWithLaunchAgentDelegate:self];
    
    // Start processing incoming messages.
    [newConnection resume];
    return YES;
}

@end
