//  MIReplyDictionary.m
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

#import "MIReplyDictionary.h"

static const NSString *MIReplyDictionaryStringValue = @"mi.xpc.reply.stringvalue";
static const NSString *MIReplyDictionaryNumericValueKey =
                                                    @"mi.xpc.reply.numericvalue";
static const NSString *MIReplyDictionaryDictionaryValue =
                                                @"mi.xpc.reply.dictionaryvalue";
static const NSString *MIReplyDictionaryErrorValue = @"mi.xpc.reply.errorvalue";
static const NSString *MIReplyDictionaryArrayValue = @"mi.xpc.reply.arrayvalue";

NSDictionary *MIMakeReplyDictionary(NSString *replyString, NSInteger eCode)
{
    NSDictionary *replyDict = @{ MIReplyDictionaryStringValue : replyString ,
                                 MIReplyDictionaryErrorValue : @(eCode) };
    return replyDict;
}

NSDictionary *MIMakeReplyDictionaryWithNumericValue(NSString *replyString,
                                                    NSInteger eCode,
                                                    NSNumber *numericValue)
{
    NSDictionary *replyDict= @{ MIReplyDictionaryStringValue : replyString ,
                                MIReplyDictionaryNumericValueKey : numericValue,
                                MIReplyDictionaryErrorValue : @(eCode) };
    return replyDict;
}

/**
 This will return the string representation of an object. For a NSString
 object it just returns the string, for an NSNumber just request it's string
 value. For NSArray and NSDictionary this function will return a json
 representation.
 */
NSString *CreateStringRepresentationOfObject(id object,
                                             NSJSONWritingOptions opts)
{
    if ([object isKindOfClass:[NSString class]])
        return object;
    else if ([object isKindOfClass:[NSNumber class]])
        return [object stringValue];
    else if ([object isKindOfClass:[NSArray class]])
    {
        NSArray *array = object;
        BOOL canJSONSerialize = [NSJSONSerialization isValidJSONObject:array];
        if (canJSONSerialize)
        {
            NSData *data;
            data = [NSJSONSerialization dataWithJSONObject:array
                                                   options:opts
                                                     error:NULL];
            return [[NSString alloc] initWithData:data
                                         encoding:NSUTF8StringEncoding];
        }
        else
            return [array description];
    }
    else if ([object isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dict = object;
        BOOL canJSONSerialize = [NSJSONSerialization isValidJSONObject:dict];
        if (canJSONSerialize)
        {
            NSData *data;
            data = [NSJSONSerialization dataWithJSONObject:dict
                                                   options:opts
                                                     error:NULL];
            return [[NSString alloc] initWithData:data
                                         encoding:NSUTF8StringEncoding];
        }
        else
            return [dict description];
    }
    return [object description];
}

NSDictionary *MIMakeReplyDictionaryWithDictionaryValue(NSDictionary *dictValue,
                                                       BOOL makeJSONString)
{
    NSString *strResult = @"See dictionary value";
    if (makeJSONString)
    {
        strResult = CreateStringRepresentationOfObject(dictValue, 0);
    }

    NSDictionary *replyDict;
    replyDict =
    @{
        MIReplyDictionaryErrorValue : @(MIReplyErrorNoError),
        MIReplyDictionaryDictionaryValue : dictValue,
        MIReplyDictionaryStringValue : strResult
    };
    return replyDict;
}

NSDictionary *MIMakeReplyDictionaryWithArray(NSArray *replyList,
                                             NSNumber *returnVal)
{
    NSDictionary *replyDict = @{ MIReplyDictionaryArrayValue : replyList ,
                                 MIReplyDictionaryErrorValue : returnVal };
    return replyDict;
}

NSString *MIGetReplyValuesFromDictionary(NSDictionary *replyDictionary,
                                         MIReplyErrorEnum *returnVal)
{
    if (returnVal)
    {
        NSNumber *numberVal = replyDictionary[MIReplyDictionaryErrorValue];
        *returnVal = [numberVal integerValue];
    }
    return replyDictionary[MIReplyDictionaryStringValue];
}

NSNumber *MIGetNumericReplyValueFromDictionary(NSDictionary *replyDictionary)
{
    return replyDictionary[MIReplyDictionaryNumericValueKey];
}

NSNumber *MIGetNSNumberErrorCodeFromReplyDictionary(NSDictionary *replyDict)
{
    return replyDict[MIReplyDictionaryErrorValue];
}

MIReplyErrorEnum MIGetErrorCodeFromReplyDictionary(NSDictionary *replyDict)
{
    NSNumber *errorRef = replyDict[MIReplyDictionaryErrorValue];
    if (errorRef)
        return [errorRef integerValue];
    
    return MIReplyErrorNoError;
}

NSString *MIGetStringFromReplyDictionary(NSDictionary *replyDictionary)
{
    NSString *replyString = replyDictionary[MIReplyDictionaryStringValue];
    if (!replyString)
    {
        NSArray *stringArray = replyDictionary[MIReplyDictionaryArrayValue];
        if (stringArray)
        {
            NSMutableString *mutableString = [NSMutableString new];
            for (NSString *string in stringArray)
            {
                [mutableString appendFormat:@"%@\n", string];
            }
            return mutableString;
        }
    }
    return replyString;
}

NSArray *MIGetStringArrayFromReplyDictionary(NSDictionary *replyDictionary)
{
    return replyDictionary[MIReplyDictionaryArrayValue];
}

NSDictionary *MIGetDictionaryValueFromReplyDictionary(NSDictionary *replyDict)
{
    return replyDict[MIReplyDictionaryDictionaryValue];
}

