//  Interfaces.h
//  MovingImages
//
//  Copyright (c) 2015 Zukini Ltd.

@import Foundation;

/**
 The XPC exported interface that the MovingImages Launch Agent supports.
*/
@protocol MIXPCExportedInterface

/**
 @brief Set the idle time
 @discussion If there are no active object MovingImage's objects then
 MovingImages will exit when it has been idle for for idleTime seconds. The
 default value is 10 seconds. This value can range from 1 .. 900 (15 mins).
 The call returns the idle time actually set.
*/
-(void)setIdleTime:(NSInteger)idleTime reply:(void (^)(NSInteger))reply;

/// Return the idle time.
-(void)getIdleTimeWithReply:(void (^)(NSInteger))reply;

/**
 @brief Performs the command and sends back the reply.
 @param command This is an array of strings as passed by argv at the command
 line or one that is similarly generated and passed to the launch agent.
 @param reply is a block that is called from the receiver of the xpc message but
 handled by the sender of the xpc message. Calling the block by the receiver
 happens when the receiver has finished performing the command.
*/
// -(void)performCommand:(NSArray *)command reply:(void (^)(NSDictionary *))reply;

/**
 @brief Handle the dictionary commands.
 @discussion All the information needed for handling the command is in the
 command dictionary. This version assumes that everything that the caller has
 converted the json object or a json orplist file on disk containing the
 command instructions into a dictionary.
 @param commands A dictionary containing properties for describing how to run
 the commands and a list of commands property.
*/
-(void)handleDictionaryCommands:(NSDictionary *)commands
                          reply:(void (^)(NSDictionary *))reply;

@end
