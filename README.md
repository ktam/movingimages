[Documentation](https://gitlab.com/ktam/the-using-movingimages-documentation/wikis/Contents)

## What is MovingImages?

MovingImages is a video and image content creation tool. MovingImages is a JSON object parser and converts JSON objects into instructions for creating movies and images. MovingImages also works with the audio tracks of movie files.

MovingImages is made up of a number of components:

* An iOS Framework
* An OS X Framework
* An OS X Launch Agent
* An OS X command line tool
* A ruby gem
* Documentation

The OS X and iOS Framework behave the same and can be included in applications. The Framework is a parser for a dictionary or if you prefer a JSON object. The Frameworks convert the JSON objects into commands, these commands can be drawing commands, application of image filter commands and reading and writing video and audio content commands.

On OS X the LaunchAgent, command line tool, and ruby gem work together. Ruby scripts have been written and users of MovingImages can write their own scripts for processing images, and movies. These scripts generate the JSON objects that MovingImages processes.

There is extensive documentation from the reference documentation for the ruby gem api to the using MovingImages documentation which describes the different components, base objects, JSON object commands and how they fit together.

There is also a two OS X demonstration applications. The MovingImages Demonstration application covers the draw element features of MovingImages, whilst the Zukini Demonstration application covers some bits of all aspects of MovingImages. For example videos are generated using the Zukini Demonstration application that combine video frames from imported videos with drawing commands and application of image filters.

### Requirements

The OS X MovingImages framework requires Yosemite.

The iOS MovingImages framework requires iOS.

### smig

smig is short for: Scripting Moving ImaGes.

Smig is the command line tool for communicating with the MovingImages Launch Agent. Smig sends a message to a base object in the MovingImages Launch Agent, and returns when it receives the reply from the MovingImages Launch Agent.

All communicating between MovingImages and ruby is done through smig. The 'moving_images' ruby gem uses the smig subcommand "performcommand" which takes a json object.

### MovingImages Launch Agent

In Mac OS X Launch Agents are background tasks or application helpers that are user processes. The MovingImages Launch Agent (MILA) is the worker tool for MovingImages. It performs tasks as directed by the commands sent to it using smig.

### MovingImages ruby gem

MovingImages can be scripted from many scripting languages on OS X. Ruby was selected as the scripting language to most actively support for scripting MovingImages with. Ruby uses gems as reusable library of functionality. The MovingImages ruby gem is such a library providing a consistent way to use MovingImages from ruby scripts.

## Installing the OS X components

1. [Download](http://zukini.eu/largeuploads/MovingImages.dmg)
2. Double click the downloaded disk image
3. Drag the MovingImages application to Applications
4. Run the MovingImages application
5. Click the "Install" button
	* You will be asked for your user account password
6. Confirm installation
	* The smig version number should display (1.0)
	* The MovingImages LaunchAgent should be loaded and ready for use
	* The MovingImages gem should be listed as installed, version (1.0.0)
	* Click the "Get Version" button
		* The MovingImages version should be returned
		* Is running should change to Yes
		* After 10 seconds the Is running text will change back to No

To uninstall run the MovingImages application, click uninstall, quit the application and drag it to the trash.

### Installing issues

If you are using a shell† other than bash† the installer will not install the moving_images ruby gem. You can install the ruby gem manually either from ruby gems.org or by sourcing the gem from within the MovingImages application bundle.

† If you do not know what I mean by shell or bash, then this should not be an issue for you.    

[Documentation](https://gitlab.com/ktam/the-using-movingimages-documentation/wikis/Contents)

© Zukini Ltd. 2015