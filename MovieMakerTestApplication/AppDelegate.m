#import "AppDelegate.h"
#import "Interfaces.h"
#import "MSWeakTimer.h"

#define MACH_SERVICE_NAME "U6TV63RN87.com.zukini.MovingImagesAgent"
NSString *machServiceName = @MACH_SERVICE_NAME;
NSString *smigInstallLocation = @"/usr/local/bin/smig";

@interface AppDelegate ()

/// Refactored getting version number
+(NSString *)_getVersion:(NSString *)versionType;

+(NSString *)_whichTool:(NSString *)commandLineTool;

+(BOOL)_determineIfSmigIsInstalled;

/// Get smig version number. Returns nil if smig is not installed.
+(NSString *)_getSmigVersion;
+(BOOL)_determineIfLaunchAgentIsLoaded;
+(BOOL)_determineIfLaunchAgentIsRunning;

+(NSString *)_whichRuby;
+(BOOL)_determineIfGemInstalled;
+(NSString *)_whichGem; // Basically the result of `which gem`.
+(NSString *)_getMovingImagesGemVersion;
+(BOOL)_isStandardOSXRubyInstall;

+(void)_loadLaunchAgent;
+(void)_unloadLaunchAgent;

-(void)_install;
-(void)_uninstall;

+(void)_performLaunchctlSubCommand:(NSString *)subCommand
                     plistFilePath:(NSString *)path;

-(MSWeakTimer *)createThenScheduleTimer;

/// Get LaunchAgent version number. Returns nil if not registered.
+(NSString *)_getLaunchAgentVersion;

@property (strong) MSWeakTimer *timer;

@end

/*
 dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 1 * NSEC_PER_SEC);
 dispatch_semaphore_wait(self._writeSemaphore, time);
 
 Could also assign a termination handler to an NSTask but that requires a little
 more work than I've time for now.
*/

@implementation AppDelegate

+(NSString *)_getVersion:(NSString *)versionType
{
    NSString *result = nil;
    if (versionType && [self _determineIfSmigIsInstalled])
    {
        NSTask *task = [[NSTask alloc] init];
        [task setLaunchPath:smigInstallLocation];

        [task setArguments:@[@"getproperty", @"-property", versionType]];
        NSPipe *outPipe = [NSPipe pipe];
        [task setStandardOutput:outPipe];
        [task launch];
        [task waitUntilExit];
        task = nil;
        
        NSFileHandle *read = [outPipe fileHandleForReading];
        NSData *dataRead = [read readDataToEndOfFile];
        NSString *stringRead;
        stringRead = [[NSString alloc] initWithData:dataRead
                                           encoding:NSUTF8StringEncoding];
        result = stringRead;
    }
    return result;
}

+(BOOL)_isBashShell
{
    NSDictionary *environmentDict = [[NSProcessInfo processInfo] environment];
    NSString *shellString = [environmentDict objectForKey:@"SHELL"];
    return [shellString isEqualToString:@"/bin/bash"];
//    return NO;
}

+(NSString *)_doBashCommand:(NSString *)bashCommand
{
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/bash"];
    [task setArguments:@[@"-l", @"-c", bashCommand]];
    NSPipe *outPipe = [NSPipe pipe];
    [task setStandardOutput:outPipe];
    [task launch];
    [task waitUntilExit];
    task = nil;
    NSFileHandle *read = [outPipe fileHandleForReading];
    NSData *dataRead = [read readDataToEndOfFile];
    NSString *commandResult = [[NSString alloc] initWithData:dataRead
                                               encoding:NSUTF8StringEncoding];
    return commandResult;
}

+(NSString *)_whichTool:(NSString *)commandLineTool
{
    NSString *bashCommand = [NSString stringWithFormat:@"which %@", commandLineTool];
    NSString *result = [self _doBashCommand:bashCommand];
    NSCharacterSet *newLineCharacterSet = [NSCharacterSet newlineCharacterSet];
    return [result stringByTrimmingCharactersInSet:newLineCharacterSet];
}

// Uses the command line which to determine which ruby we will be running.
// Returns a path to the ruby command line tool.
+(NSString *)_whichRuby
{
    static NSString *rubyPath = nil;
    if (!rubyPath)
    {
        rubyPath = [self _whichTool:@"ruby"];
    }
    return rubyPath;
}

// Uses the command line which to determine which gem clt we will be running.
// Returns a path to the gem command line tool.
+(NSString *)_whichGem
{
    static NSString *gemPath = nil;
    if (!gemPath)
    {
        gemPath = [self _whichTool:@"gem"];
    }
    return gemPath;
}

+(BOOL)_isStandardOSXRubyInstall
{
    return [[self _whichRuby] isEqualToString:@"/usr/bin/ruby"];
}

+(BOOL)_determineIfGemInstalled
{
    BOOL result = NO;
    NSString *commandRes = [self _doBashCommand:@"gem list -i moving_images"];
    if (commandRes && [commandRes isEqualToString:@"true\n"])
    {
        result = YES;
    }
    return result;
}

+(NSString *)_getMovingImagesGemVersion
{
    NSString *result = @"Failed to get gem version.";
    if ([self _determineIfGemInstalled])
    {
        NSString *command = @"gem list --versions moving_images";
        NSString *stringRead = [AppDelegate _doBashCommand:command];

        if (stringRead)
        {
            NSRange range = [stringRead rangeOfString:@"moving_images"];
            NSInteger subStringStart = range.location + range.length;
            if (range.length)
            {
                NSString *subString = [stringRead substringFromIndex:subStringStart];
                if (subString)
                {
                    NSCharacterSet *set = [NSCharacterSet
                                                    whitespaceAndNewlineCharacterSet];
                    result = [subString stringByTrimmingCharactersInSet:set];
                }
            }
        }
    }
    return result;
}

+(BOOL)_determineIfSmigIsInstalled
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    return [fileManager fileExistsAtPath:smigInstallLocation];
}

+(NSString *)_getSmigVersion
{
    return [self _getVersion:@"smigversion"];
}

+(BOOL)_determineIfLaunchAgentIsLoaded
{
    // Not needed here, but an advantage of popen as compared to using NSTask
    // is that variables like $PWD and $HOME will get expanded using popen but
    // NSTask will not expand these variables. Though this expansion of session
    // variables could be a security risk.
    FILE *myPipe;
    BOOL result = NO;
    myPipe = popen("launchctl list | grep \"U6TV63RN87.com.zukini.MovingImagesAgent\"",
                   "r");
    if (myPipe && getc(myPipe) != EOF)
        result = YES;
    pclose(myPipe);
    return result;
}

+(BOOL)_determineIfLaunchAgentIsRunning
{
    BOOL result = NO;
    NSArray *array;
    array = [NSRunningApplication runningApplicationsWithBundleIdentifier:
                                    machServiceName];
    if (array && array.count)
        result = YES;
    return result;
}

+(NSString *)_getLaunchAgentVersion
{
    return [self _getVersion:@"version"];
}

// used to launchctl "load" or launchctl "unload".
+(void)_performLaunchctlSubCommand:(NSString *)subCommand
                     plistFilePath:(NSString *)path
{
    NSTask *task = [[NSTask alloc] init];
    [task setLaunchPath:@"/bin/launchctl"];
    NSArray *args = @[subCommand, path];
    [task setArguments:args];
    [task launch];
    [task waitUntilExit];
    task = nil;
}

-(void)setSmigState
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        __block BOOL isSmigInstalled = [AppDelegate _determineIfSmigIsInstalled];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isSmigInstalled = isSmigInstalled ? @"Yes" : @"No";
            NSString *theTitle = isSmigInstalled ? @"Uninstall" : @"Install";
            [self.install setTitle:theTitle];
            // NSString *smigVersion = @"Version unavailable";
            NSString *smigVersion = @"";
            if (isSmigInstalled)
            {
                smigVersion = [AppDelegate _getSmigVersion];
            }
            self.smigVersion = smigVersion;
        });
    });
}

-(void)setMovingImagesGemState
{
    if ([AppDelegate _isBashShell])
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            __block BOOL isGemInstalled = [AppDelegate _determineIfGemInstalled];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.isGemInstalled.stringValue = isGemInstalled ? @"Yes" : @"No";
                NSString *gemVersion = @"";
                if (isGemInstalled)
                {
                    gemVersion = [AppDelegate _getMovingImagesGemVersion];
                }
                self.gemVersionTextField.stringValue = gemVersion;
            });
        });
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isGemInstalled.stringValue = @"You are not using a bash shell.";
            self.gemVersionTextField.stringValue = @"See documentation to install.";
        });
    }
}

-(void)_installSmigOnly
{
    NSDictionary *error;
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle bundlePath];
    NSString *fullPath = [[NSString alloc] initWithFormat:@"%@/%@",
                          bundlePath, @"Contents/Tools/bin/smig"];
    NSString *shellCommand = [[NSString alloc] initWithFormat:
                              @"/bin/cp '%@' '%@'", fullPath,
                              smigInstallLocation];
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
                                   @"do shell script \"%@\" with administrator privileges",
                                   shellCommand];
    NSAppleScript *run;
    run = [[NSAppleScript alloc] initWithSource:appleScriptSource];
    [run executeAndReturnError:&error];
    if (error)
    {
        NSLog(@"Dictionary: %@", [error description]);
        self.isSmigInstalled = @"Failed to install";
    }
    else
    {
        [self setSmigState];
        if([self.isLaunchAgentLoaded isEqualToString:@"Yes"])
        {
            [self.getLaunchAgentVersionButton setEnabled:YES];
        }
    }
}

-(void)_installWithBuiltinRuby
{
    self.isGemInstalled.stringValue = @"Installing...";
    self.install.enabled = NO;
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle bundlePath];
    NSString *fullPath = [[NSString alloc] initWithFormat:@"%@/%@",
                          bundlePath, @"Contents/Tools/bin/smig"];
    NSString *gemPath = [[NSString alloc] initWithFormat:@"%@/%@",
                         bundlePath, @"Contents/Tools/Gems/moving_images-1.0.1.gem"];
    NSString *shellCommand = [[NSString alloc] initWithFormat:
                              @"/bin/cp '%@' '%@' ; %@ install '%@'", fullPath,
                              smigInstallLocation, [AppDelegate _whichGem],
                              gemPath];
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
                                   @"do shell script \"%@\" with administrator privileges",
                                   shellCommand];

    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *error;
        NSAppleScript *run;
        run = [[NSAppleScript alloc] initWithSource:appleScriptSource];
        [run executeAndReturnError:&error];
        if (error)
        {
            NSLog(@"Dictionary: %@", [error description]);
            self.isSmigInstalled = @"Failed to install";
        }
        else
        {
            [self setSmigState];
            if([self.isLaunchAgentLoaded isEqualToString:@"Yes"])
            {
                [self.getLaunchAgentVersionButton setEnabled:YES];
            }
        }
        self.install.enabled = YES;
        [self setMovingImagesGemState];
    });
}

-(void)_installWithCustomRuby
{
    NSDictionary *error;
    NSBundle *appBundle = [NSBundle mainBundle];
    NSString *bundlePath = [appBundle bundlePath];
    NSString *fullPath = [[NSString alloc] initWithFormat:@"%@/%@",
                          bundlePath, @"Contents/Tools/bin/smig"];
    NSString *shellCommand = [[NSString alloc] initWithFormat:
                              @"/bin/cp '%@' '%@'", fullPath,
                              smigInstallLocation];
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
                                   @"do shell script \"%@\" with administrator privileges",
                                   shellCommand];
    NSAppleScript *run;
    run = [[NSAppleScript alloc] initWithSource:appleScriptSource];
    [run executeAndReturnError:&error];
    if (error)
    {
        NSLog(@"Dictionary: %@", [error description]);
        self.isSmigInstalled = @"Failed to install";
    }
    else
    {
        // Now install the gem.
        self.isGemInstalled.stringValue = @"Installing...";
        self.install.enabled = NO;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
            NSString *gemPath = [[NSString alloc] initWithFormat:@"%@/%@",
                                 bundlePath, @"Contents/Tools/Gems/moving_images-1.0.1.gem"];
            NSString *bashCommand = [NSString stringWithFormat:@"gem install %@", gemPath];
            NSString *result = [AppDelegate _doBashCommand:bashCommand];
            result = result;
            self.install.enabled = YES;
            [self setMovingImagesGemState];
        });
        
        [self setSmigState];
        if([self.isLaunchAgentLoaded isEqualToString:@"Yes"])
        {
            [self.getLaunchAgentVersionButton setEnabled:YES];
        }
    }
}

-(void)_install
{
    // Load the launch agent.
    [AppDelegate _loadLaunchAgent];
    
    if ([AppDelegate _isBashShell])
    {
        if ([AppDelegate _isStandardOSXRubyInstall])
        {
            [self _installWithBuiltinRuby];
        }
        else
        {
            [self _installWithCustomRuby];
        }
    }
    else
    {
        [self _installSmigOnly];
    }
}

-(void)_uninstallWithBuiltinRuby
{
    NSString *shellCommand;
    shellCommand = [[NSString alloc] initWithFormat:
                    @"%@ uninstall -x -a moving_images ; /bin/rm '%@'",
                    [AppDelegate _whichGem], smigInstallLocation];
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
                                   @"do shell script \"%@\" with administrator privileges",
                                   shellCommand];
    NSAppleScript *appleScript;
    appleScript = [[NSAppleScript alloc] initWithSource:appleScriptSource];
    NSDictionary *error;
    [appleScript executeAndReturnError:&error];
    if (error)
    {
        NSLog(@"Error info when uninstalling smig: %@", [error description]);
        self.isSmigInstalled = @"Failed to uninstall";
    }
    else
    {
        [self setSmigState];
        [self.getLaunchAgentVersionButton setEnabled:NO];
    }
}

-(void)_uninstallWithCustomRuby
{
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
               @"do shell script \"/bin/rm '%@'\" with administrator privileges",
               smigInstallLocation];
    NSAppleScript *appleScript;
    appleScript = [[NSAppleScript alloc] initWithSource:appleScriptSource];
    NSDictionary *error;
    [appleScript executeAndReturnError:&error];
    if (error)
    {
        NSLog(@"Error info when uninstalling smig: %@", [error description]);
        self.isSmigInstalled = @"Failed to uninstall";
    }
    else
    {
        [self setSmigState];
        [self.getLaunchAgentVersionButton setEnabled:NO];
    }
    NSString *bashCommand = @"gem uninstall -a -x moving_images";
    NSString *result = [AppDelegate _doBashCommand:bashCommand];
    result = result;
}

-(void)_uninstallSmigOnly
{
    NSString *appleScriptSource = [[NSString alloc] initWithFormat:
                                   @"do shell script \"/bin/rm '%@'\" with administrator privileges",
                                   smigInstallLocation];
    NSAppleScript *appleScript;
    appleScript = [[NSAppleScript alloc] initWithSource:appleScriptSource];
    NSDictionary *error;
    [appleScript executeAndReturnError:&error];
    if (error)
    {
        NSLog(@"Error info when uninstalling smig: %@", [error description]);
        self.isSmigInstalled = @"Failed to uninstall";
    }
    else
    {
        [self setSmigState];
        [self.getLaunchAgentVersionButton setEnabled:NO];
    }
}

-(void)_uninstall
{
    [AppDelegate _unloadLaunchAgent];
    if ([AppDelegate _isBashShell])
    {
        if ([AppDelegate _isStandardOSXRubyInstall])
        {
            [self _uninstallWithBuiltinRuby];
        }
        else
        {
            [self _uninstallWithCustomRuby];
        }
    }
    else
    {
        [self _uninstallSmigOnly];
    }
}

-(void)install:(id)sender
{
    BOOL isInstalled = [AppDelegate _determineIfSmigIsInstalled];

    if (isInstalled)
    {
        [self _uninstall];
        [self setMovingImagesGemState];
    }
    else
    {
        [self _install];
    }
    // After installing update the UI.
    [self setSmigState];
    [self setLaunchAgentState];
}

-(void)obtainLaunchAgentVersion:(id)sender
{
    NSString *versionString = [AppDelegate _getLaunchAgentVersion];
    if (versionString && versionString.length)
    {
        self.launchAgentVersion = versionString;
        [self setLaunchAgentState];
    }
    else
        self.launchAgentVersion = @"Unknown";
}

+(void)_unloadLaunchAgent
{
    NSString *fileName;
    fileName = [[NSString alloc] initWithFormat:@"%@.plist", machServiceName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *tempURL = [fileManager URLForDirectory:NSLibraryDirectory
                                         inDomain:NSUserDomainMask
                                appropriateForURL:nil
                                           create:NO
                                            error:nil];
    NSString *tempPath = [tempURL path];
    NSString *dirPath = [[NSString alloc] initWithFormat:@"%@/%@",
                         tempPath, @"LaunchAgents"];
    NSString *filePath;
    filePath = [[NSString alloc] initWithFormat:@"%@/%@", dirPath, fileName];
    [AppDelegate _performLaunchctlSubCommand:@"unload" plistFilePath:filePath];
    [fileManager removeItemAtPath:filePath error:nil];
}

+(void)_loadLaunchAgent
{
    // Get appropriate paths.
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *tempURL = [fileManager URLForDirectory:NSLibraryDirectory
                                         inDomain:NSUserDomainMask
                                appropriateForURL:nil
                                           create:NO
                                            error:nil];
    NSString *tempPath = [tempURL path];
    NSString *dirPath = [[NSString alloc] initWithFormat:@"%@/%@",
                         tempPath, @"LaunchAgents"];
    
    NSString *fileName;
    fileName = [[NSString alloc] initWithFormat:@"%@.plist", machServiceName];
    NSString *filePath;
    filePath = [[NSString alloc] initWithFormat:@"%@/%@", dirPath, fileName];
    
    // Get paths needed for installing the launch agent.
    NSString *pathToBundle = [[NSBundle mainBundle] bundlePath];
    NSString *sourceFolder = [[NSString alloc]
                              initWithFormat:@"%@/Contents/Tools/LaunchAgent/",
                              pathToBundle];
    
    NSString *sourcePlist = [[NSString alloc] initWithFormat:@"%@%@",
                             sourceFolder, fileName];
    
    NSString *launchAgent = [[NSString alloc]
                             initWithFormat:@"%@%@.app/Contents/MacOS/%@",
                             sourceFolder, machServiceName, machServiceName];
    
    NSDictionary *launchAgentPlist;
    launchAgentPlist = [NSDictionary dictionaryWithContentsOfFile:sourcePlist];
    if (!launchAgentPlist)
        return;
    
    // Obtain launch agent plist.
    NSMutableDictionary *mutLaunchAgentPlist;
    mutLaunchAgentPlist = [[NSMutableDictionary alloc]
                           initWithDictionary:launchAgentPlist];
    
    // Prepare to modify the plist.
    NSArray *array = @[launchAgent];
    
    // Create the Launch Agents folder in the Library folder.
    if(![fileManager createDirectoryAtPath:dirPath
               withIntermediateDirectories:YES
                                attributes:nil
                                     error:nil])
    {
        return;
    }
    
    // Write the Launch Agent plist file to the LaunchAgents folder.
    [mutLaunchAgentPlist setObject:array forKey:@"ProgramArguments"];
    BOOL result = [mutLaunchAgentPlist writeToFile:filePath atomically:YES];
    if (!result)
        return;
    
    // load the LaunchAgent with launchd.
    [AppDelegate _performLaunchctlSubCommand:@"load" plistFilePath:filePath];
}

-(void)setLaunchAgentState
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),^{
        __block BOOL isLALoaded = [AppDelegate _determineIfLaunchAgentIsLoaded];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.isLaunchAgentLoaded = isLALoaded ? @"Yes" : @"No";
            [self.getLaunchAgentVersionButton setEnabled:isLALoaded];
            if (isLALoaded)
            {
                if (!self.timer)
                    self.timer = [self createThenScheduleTimer];
                [self.timer fire];
            }
            else
            {
                self.launchAgentVersion = @"Unknown";
                self.isLaunchAgentRunning = @"No";
                self.timer = nil;
            }
        });
    });
}

-(void)isLaunchAgentRunningTimerHandler
{
    BOOL wasRunning = [self.isLaunchAgentRunning isEqualToString:@"Yes"];
    BOOL isRunning = [AppDelegate _determineIfLaunchAgentIsRunning];
    if (wasRunning != isRunning)
    {
        dispatch_async(dispatch_get_main_queue(), ^
        {
            self.isLaunchAgentRunning = isRunning ? @"Yes" : @"No";
        });
    }
}

-(void)handleHelpMenuItem:(id)sender
{
    NSWorkspace *workSpace = [NSWorkspace sharedWorkspace];
    NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:@"zukini.eu"
                                          path:@"/docs"];
    [workSpace openURL:url];
}

-(MSWeakTimer *)createThenScheduleTimer
{
    SEL isLaunchAgentRunningSelector = @selector(isLaunchAgentRunningTimerHandler);
    dispatch_queue_t timerQueue = dispatch_get_global_queue(
                                                DISPATCH_QUEUE_PRIORITY_DEFAULT,
                                                0);
    MSWeakTimer *timer;
    timer = [[MSWeakTimer alloc] initWithTimeInterval:4.0
                                               target:self
                                             selector:isLaunchAgentRunningSelector
                                             userInfo:nil
                                              repeats:YES
                                        dispatchQueue:timerQueue];
    [timer setTolerance:0.1]; // set tolerance of firing timer to 1/10 of a second.
    [timer schedule];
    return timer;
}

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [self setSmigState];
    [self setMovingImagesGemState];
    self.isLaunchAgentRunning = @"No";
    [self setLaunchAgentState];
}

@end
