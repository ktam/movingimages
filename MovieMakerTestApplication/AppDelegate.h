@import Cocoa;

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *isSmigInstalledTextField;
@property (assign) IBOutlet NSTextField *smigVersionTextField;

@property (assign) IBOutlet NSTextField *isLaunchAgentLoadedTextField;
@property (assign) IBOutlet NSTextField *isLaunchAgentRunningTextField;
@property (assign) IBOutlet NSTextField *launchAgentVersionTextField;
@property (assign) IBOutlet NSButton *getLaunchAgentVersionButton;

@property (assign) IBOutlet NSTextField *isGemInstalled;
@property (assign) IBOutlet NSTextField *gemVersionTextField;

@property (assign) IBOutlet NSButton *install;

@property (strong) NSString *isSmigInstalled;
@property (strong) NSString *smigVersion;

@property (strong) NSString *isLaunchAgentLoaded;
@property (strong) NSString *isLaunchAgentRunning;
@property (strong) NSString *launchAgentVersion;

-(IBAction)obtainLaunchAgentVersion:(id)sender;
-(IBAction)install:(id)sender;

-(IBAction)handleHelpMenuItem:(id)sender;
-(void)setSmigState;
-(void)setLaunchAgentState;
-(void)setMovingImagesGemState;
-(void)isLaunchAgentRunningTimerHandler;

@end
